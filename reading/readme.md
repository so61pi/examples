### [Title](https://example.com)

Summary???

*STATUS: Added $(date -Iseconds) | Incomplete*

### [Title](https://example.com)

Summary???

*STATUS: Added $(date -Iseconds) | Completed $(date -Iseconds)*

### io_uring

- https://thenewstack.io/how-io_uring-and-ebpf-will-revolutionize-programming-in-linux/
- https://lwn.net/Articles/663879/

>  Moving from an interrupt-driven mode to polling for performance reasons may seem counter-intuitive but, in a high-traffic situation, it makes sense. Servicing interrupts is expensive; it's also pointless if you know that there will be new packets available whenever you get around to looking for them. If the CPU has nothing else to do while waiting for packets, polling is also a good way to minimize latency. It will always be faster to watch for an arriving packet than to wait for the entire interrupt-handling machinery (in both hardware and software) to do its thing.

Faster async IO for linux kernel. IO requests (e.g. read) and results are placed in separated queues. Kernel reads requests from request queue, does its job, then add results to result queue.

*STATUS: Added 2020-05-14T14:14:58+07:00 | Completed 2020-05-14T14:14:58+07:00*

### https://meltingasphalt.com/ads-dont-work-that-way/

Summary???

*STATUS: Added 2020-05-13T11:33:34+07:00 | Incomplete*

### https://blog.acolyer.org/2019/05/13/an-open-source-benchmark-suite-for-microservices-and-their-hardware-software-implications-for-cloud-edge-systems/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://www.deconstructconf.com/2018

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### http://adventures.michaelfbryan.com/posts/plugins-in-rust/

How to write a plugin system in Rust:

- Plugin interface that both plugins and main application depend on.
- Plugin implementation.
- Main application implementation to load plugins.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-13T09:58:37+07:00*

### https://www.cs.kent.ac.uk/people/staff/srk21//research/talks/kell14abis-slides.pdf

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://www.vice.com/en_us/article/wjwbmm/inside-the-phone-company-secretly-run-by-drug-traffickers

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://github.com/kdeldycke/awesome-falsehood

List of falsehoods programmers believe articles.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-13T12:41:00+07:00*

### https://www.akkadia.org/drepper/nptl-design.pdf

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://www.youtube.com/watch?v=kw-U6smcLzk

Fast float-to-string algorithm

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-14T12:12:08+07:00*

### https://ftalphaville.ft.com/2019/10/21/1571680970000/The-average-lifespan-of-a-fiat-currency-isn-t-27-years/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### http://boringtechnology.club/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://smallcultfollowing.com/babysteps/blog/2019/10/26/async-fn-in-traits-are-hard/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### Game Theory

- https://www.reddit.com/r/explainlikeimfive/comments/1qnocp/eli5_what_is_game_theory/
- https://en.wikipedia.org/wiki/Prisoner's_dilemma
- https://en.wikipedia.org/wiki/Chicken_(game)

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://twitter.com/cynosurae/status/1196567663977230336

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://thomashartmann.dev/blog/async-rust/

Simple introduction to Rust async.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-26T14:45:34+07:00*

### https://m.signalvnoise.com/reconsider/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://www.youtube.com/watch?v=lJ8ydIuPFeU

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### http://www.brendangregg.com/blog/2015-06-23/netflix-instance-analysis-requirements.html

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://myaut.github.io/dtrace-stap-book/app/proc.html

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### Rust async interviews

- https://smallcultfollowing.com/babysteps/blog/2019/12/09/async-interview-2-cramertj/
- https://smallcultfollowing.com/babysteps/blog/2019/12/10/async-interview-2-cramertj-part-2/
- https://smallcultfollowing.com/babysteps/blog/2019/12/11/async-interview-2-cramertj-part-3/
- https://smallcultfollowing.com/babysteps/blog/2019/12/23/async-interview-3-carl-lerche/
- https://smallcultfollowing.com/babysteps/blog/2020/01/13/async-interview-4-florian-gilcher/
- https://smallcultfollowing.com/babysteps/blog/2020/01/20/async-interview-5-steven-fackler/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### Time

- https://www.youtube.com/watch?v=adSAN282YIw
- https://shipilev.net/blog/2014/nanotrusting-nanotime/
- https://lwn.net/Articles/388286/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### Rust async

- https://blog.aloni.org/posts/a-stack-less-rust-coroutine-100-loc/
- https://stjepang.github.io/2020/01/25/build-your-own-block-on.html
- https://stjepang.github.io/2019/12/04/blocking-inside-async-code.html
- https://internals.rust-lang.org/t/warning-when-calling-a-blocking-function-in-an-async-context/11440
- https://blog.yoshuawuyts.com/streams-concurrency/
- https://tmandry.gitlab.io/blog/posts/optimizing-await-1/
- https://www.snoyman.com/blog/2019/12/rust-crash-course-09-tokio-0-2

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### Kubernetes

- https://www.reddit.com/r/docker/comments/eq4il5/the_only_kubernetes_video_you_need_to_watch_to/
- https://www.reddit.com/r/devops/comments/edbe20/free_course_introduction_to_site_reliability/
- https://www.reddit.com/r/sysadmin/comments/cwih01/vmware_releases_an_agnostic_free_kubernetescloud/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### TLA+

- https://medium.com/espark-engineering-blog/formal-methods-in-practice-8f20d72bce4f

  * Walk through eSpark init TLA+ model. Some caveats of TLA+ are placed at the end of the post.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-14T17:42:21+07:00*

### Swift ABI

- https://www.quora.com/What-are-similarities-and-differences-between-C-and-Swift/answer/Dave-Abrahams?ch=2&srid=R1zp
- https://gankra.github.io/blah/swift-abi/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### [John Cleese on Creativity In Management](https://www.youtube.com/watch?v=Pb5oIIPO62g)

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://www.tarynpivots.com/post/perils-querying-sql-replica-under-load/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://www.realworldtech.com/forum/?threadid=189711&curpostid=189723

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://foonathan.net/2020/01/type-erasure/

`std::polymorphic_value`

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-14T17:37:48+07:00*

### https://callingbullshit.org/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://blogs.scientificamerican.com/life-unbounded/death-on-mars1/?amp

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### http://www.cppmove.com/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://fasterthanli.me/blog/2020/working-with-strings-in-rust/

Handle strings (UTF-8) in Rust.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-14T17:22:46+07:00*

### _

- https://scicomp.stackexchange.com/questions/2173/what-are-some-good-strategies-for-improving-the-serial-performance-of-my-code/2719#2719
- https://stackoverflow.com/questions/27842281/unknown-events-in-nodejs-v8-flamegraph-using-perf-events/27867426#27867426

Pay attention to IO latency when profiling.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-14T17:16:03+07:00*

### https://www.agner.org/optimize/optimizing_cpp.pdf

Summary???

*STATUS: Added 2020-05-14T17:15:35+07:00 | Incomplete*

### http://archive.dimacs.rutgers.edu/Workshops/EAA/slides/bentley.pdf

Summary???

*STATUS: Added 2020-05-14T17:15:35+07:00 | Incomplete*

### https://os.phil-opp.com/async-await/

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://github.com/crossbeam-rs/rfcs/wiki

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://www.ralfj.de/blog/2019/07/14/uninit.html

The blog talks about uninitialized memory in C/C++/Rust and its effects on program behaviours.

Sources of values when reading uninitialized memory:

- a register
- memory on stack
- memory on heap
- ...

All of the above values are not guaranteed to be consistent. That means first read and subsequent reads can yield completely different values. Even worse, when combining with various optimizations, it can return counter-intuitive result (see https://godbolt.org/z/JX4B4N).

After all, it's undefined behaviour for reading uninitialized memory.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 2020-05-14T17:07:09+07:00*

### _

- https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html
- https://matklad.github.io/2020/04/15/from-pratt-to-dijkstra.html

Summary???

*STATUS: Added 1970-01-01T00:00:00+00:00 | Incomplete*

### https://wiki.postgresql.org/wiki/Fsync_Errors

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://danluu.com/filesystem-errors/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://devblogs.microsoft.com/oldnewthing/20031024-00/?p=42053

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://smallcultfollowing.com/babysteps/blog/2018/04/27/an-alias-based-formulation-of-the-borrow-checker/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://blog.cloudflare.com/http3-the-past-present-and-future/

HTTP3 is based on UDP

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://twitter.com/Nick_Craver/status/1178737894908928000

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://www.youtube.com/watch?v=v_yzLe-wnfk

The talk is about refactoring large C++ codebase. The idea is

- Do it in multiple steps instead of one single big step.
- New code and old code can live happily together during the process until we can delete old code.
- Create the least impact on users the code in question.

The talk also shows an example about renaming type and problems of a few refactoring strategies.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://smallcultfollowing.com/babysteps/blog/2013/06/11/on-the-connection-between-memory-management-and-data-race-freedom/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://www.youtube.com/watch?v=rHIkrotSwcc

Basically we don't have zero-cost abstraction. The talk gives an example with `unique_ptr` vs raw pointer. In that example, to make the code safe and correct (due to the semantics of `unique_ptr` and the presence of exceptions), the compiler must add extra code for `unique_ptr` version.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://annehelen.substack.com/p/what-great-inconvenience

> I get asked a lot about “tips for alleviating burnout,” and if you’ve been reading this newsletter for awhile, you know I have a few: put your phone on airplane mode before you go into the bedroom; don’t listen to podcasts on walks; dedicate time to hang out with your own mind.

> think deeply and consistently about how your own actions, and standards, and practices create burnout in others.

> These are scare tactics designed to appeal to your totally self-centered inner-optimizer, inviting you to frame the ethical treatment of employees as a massive personal inconvenience.

> Just because something’s cheap and efficient doesn’t mean that it should be that way — or that your ability to access it doesn’t have significant human cost.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://randomascii.wordpress.com/2019/10/20/63-cores-blocked-by-seven-instructions/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://dsprenkels.com/cmov-conversion.html

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://eli.thegreenplace.net/2018/measuring-context-switching-and-memory-overheads-for-linux-threads/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://news.ycombinator.com/item?id=20342060

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://blog.cloudflare.com/announcing-cfnts/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://travisdowns.github.io/blog/2019/11/19/toupper.html

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://twitter.com/whitequark/status/1195125217850544128

CPUs* don't provide side channel resistance

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://twitter.com/math_rachel/status/1191064500834750464

Even if race & gender are not inputs to your algorithm, it can still be biased on these factors.  Machine learning excels at finding latent variables.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://speakerdeck.com/skade/dot-await-with-async-std

An introduction to Rust async/await

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### http://cliffle.com/blog/rust-typestate/

Converting run-time state to types. This provides better correctness as we shift run-time errors/checking to compile-time ones.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### _

- https://github.com/flamegraph-rs/flamegraph/releases/tag/v0.2.0
- https://rust-fuzz.github.io/book/cargo-fuzz.html
- https://github.com/pemistahl/grex
- https://github.com/erikbern/git-of-theseus

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### _

- https://interrupt.memfault.com/blog/zero-to-main-rust-1

  * Building from reset handler up until main, manually.

- https://craigjb.com/2019/12/31/stm32l0-rust/

  * Creating embedded rust using foundational crates.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://yoric.github.io/post/uom.rs/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://travisdowns.github.io/blog/2020/01/20/zero.html

`std::fill` can call different functions based on the input types (via `enable_if`). This can cause differences in performance.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://stackoverflow.com/questions/24973086/are-comments-allowed-in-email-address-domain-part/24973439

Email addresses can have comments :| .

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://lemire.me/blog/2019/12/19/xor-filters-faster-and-smaller-than-bloom-filters/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://kubasejdak.com/modern-cmake-is-like-inheritance

An introduction to modern CMake, basically target + properties + visibility.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://immunant.com/blog/2020/01/quake3/

Transpiling Quake 3 from C to Rust using C2Rust.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://fasterthanli.me/blog/2020/a-half-hour-to-learn-rust/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://fasterthanli.me/blog/2019/making-our-own-ping-11/

Parsing ICMP packets, including technique to handle sub-byte parsing.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://blog.plan99.net/modern-garbage-collection-part-2-1c88847abcfd

Comparing Shenandoah and ZGC, 2 garbage collectors.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### http://craftinginterpreters.com/classes-and-instances.html

How to handle classes when you write an interpreter.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://www.pietroalbini.org/blog/shipping-a-compiler-every-six-weeks/

Several techniques are employed here

- Test suite.
- User report.
- Use previous compiler version to compile next one.
- Use Crater to get and test compile all Rust code on GitHub public repositories.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### _

- https://www.johndcook.com/blog/2008/10/21/what-happens-when-you-add-a-new-teller/
- https://www.johndcook.com/blog/2009/01/30/server-utilization-joel-on-queuing/
- http://web.mst.edu/~gosavia/queuing_formulas.pdf#page=2 & http://web.mst.edu/~gosavia/queuing_formulas.pdf#page=4

  * λ: mean rate  of arrival
  * μ: mean service rate
  * ρ = λ / μ: utilization of the server
  * W: mean waiting time in the system
  * Wq: mean waiting time in the queue
  * L = λ * W
  * Lq = λ * Wq
  * Lq = ρ^2 / (1 - ρ)
  * W = Wq + 1/μ = ... = ρ / (1 - ρ) / μ + 1 / μ

The Poisson distribution is the discrete probability distribution of the number of events occurring in a given time period (k), given the average number of times the event occurs over that time period (λ).

https://brilliant.org/wiki/poisson-distribution/#probabilities-with-the-poisson-distribution

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### http://www.brendangregg.com/offcpuanalysis.html

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://fasterthanli.me/blog/2019/declarative-memory-management

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://developers.redhat.com/blog/2015/03/24/live-migrating-qemu-kvm-virtual-machines/

> One of the features that KVM exposes for userspace is keeping track of the pages in the guest memory area that the guest has modified since the previous time such data was requested.

> Stages in Live Migration
>
> - Live migration happens in 3 stages
> - Stage 1: Mark all RAM dirty
> - Stage 2: Keep sending dirty RAM pages since last iteration
>   + stop when some low watermark or condition reached
> - Stage 3: Stop guest, transfer remaining dirty RAM, device state
> - Continue execution on destination qemu

A few sets of data need to be migrated:
- RAM content (stage 1 & 2)
- Device state
  + Disk content (stage 3)
  + Network configuration (stage 3)
  + ...

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### _

- https://en.opensuse.org/openSUSE:Reproducible_Builds
- https://reproducible-builds.org/
- https://reproducible-builds.org/docs/buy-in/

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### http://sled.rs/errors

> Over time, I developed several strategies for finding these bugs. The most successful efforts that resulted in finding the most bugs boiled down to randomly causing different operations to fail by triggering them through PingCAP’s fail crate and combining it with property testing to cause various combinations of failures to be triggered under test. This kind of testing is among the highest bug:test code ratios that I’ve written for sled so far.

> That solution: make the global Error enum specifically only hold errors that should cause the overall system to halt - reserved for situations that require human intervention. Keep errors which relate to separate concerns in totally separate error types. By keeping errors that must be handled separately in their own types, we reduce the chance that the try ? operator will accidentally push a local concern into a caller that can’t deal with it. If you make such errors representable, they will happen. And they certainly have as I’ve been developing sled.

### https://docs.rs/fail

> Fail points are code instrumentations that allow errors and other behavior to be injected dynamically at runtime, primarily for testing purposes.

Trigger hard to happen errors on demand (deterministically or randomly).

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### _

- https://guide.elm-lang.org/architecture/
- https://github.com/hecrj/iced

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://paper.dropbox.com/doc/IO-Buffer-Initialization-MvytTgjIOTNpJAS6Mvw38

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://marcan.st/2017/12/debugging-an-evil-go-runtime-bug/

Go runtime used smaller than expect stack buffer. This, combined with vDSO and GCC stack checking, created random crashes on Go applications.

The process of investigating this is worth a read.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://matklad.github.io//2020/01/02/spinlocks-considered-harmful.html

Using spin lock in user-space applications with threads with different priority can lead to situation in which low priority thread holding the lock cannot complete operation as kernel only schedules high priory ones.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://dropbox.tech/infrastructure/-testing-our-new-sync-engine

> All randomized testing frameworks must be fully deterministic and easily reproducible.

> In order to provide the desired determinism guarantees, all our randomized testing systems share the following structure:
>
> 1. At the beginning of a random test run, generate a random seed.
> 1. Instantiate a pseudorandom number generator (PRNG) with that seed. (Personally, given its name, I like this one.)
> 1. Run the test using that PRNG for all random decisions, e.g. generating initial filesystem state, task scheduling, or network failure injection.
> 1. If the test fails, output the seed.

> In order to uphold this guarantee, we take great care to make Nucleus itself fully deterministic, provided a fixed PRNG input. For example, Rust’s default HashMap uses a randomized hashing algorithm under the hood to resist denial of service attacks that can force hash collisions. However, we don’t need collision-resistance in Nucleus, since an adversarial user could only degrade their own performance with such an attack. So, we override this behavior with a deterministic hasher to make sure all behavior is reproducible.

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*

### https://www.youtube.com/watch?v=r-TLSBdHe1A

*STATUS: Added 1970-01-01T00:00:00+00:00 | Completed 1970-01-01T00:00:00+00:00*
