=========
 Compile
=========

Official documents can be found at https://sourceware.org/gdb/wiki/BuildingCrossGDBandGDBserver. Below is the summary of steps:

#. Get the source code

   We can download it from https://git.linaro.org/toolchain/binutils-gdb.git or https://www.gnu.org/software/gdb/current/

#. Compile

   - For running on PC, debugging PC programs

     .. code-block:: sh

        mkdir build && cd build
        ../configure
        make -j4

   - For running on PC, debugging ARM programs on development boards (i.e. remote debugging)

     .. code-block:: sh

        mkdir build && cd build
        ../configure --target=arm-linux-gnueabi
        make -j4

   - For running on ARM board and debug ARM programs (i.e. cross compiling)

     .. code-block:: sh

        mkdir build && cd build
        ../configure --build=x86_64-pc-linux-gnu --host=arm-linux-gnueabi
        make -j4
