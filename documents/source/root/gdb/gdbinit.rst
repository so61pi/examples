=========
 gdbinit
=========

Syntax
======

.. code-block:: sh

    set prompt \001\033[COLORm\002TEXT \001\033[0m\002
    set prompt \001\033[FORMATm\002TEXT \001\033[0m\002
    set prompt \001\033[FORMAT;COLORm\002TEXT \001\033[0m\002

Colors
======

.. code-block::

    Name                 Code
    ---------------------------
    Default              39
    Black                30
    Red                  31
    Green                32
    Yellow               33
    Blue                 34
    Magenta              35
    Cyan                 36
    Light gray           37
    Dark gray            90
    Light red            91
    Light green          92
    Light yellow         93
    Light blue           94
    Light magenta        95
    Light cyan           96
    White                97

Format
======

.. code-block::

    Name                 Code
    ---------------------------
    Reset                0
    Bold                 1
    Underlined           4
    Reverse              7

Text
====

.. code-block::

    (gdb)
    gdb>
    >>>

gdbinit
=======

We can place this file at ``~/.gdbinit`` and it will be loaded by ``gdb`` at startup.

.. code-block:: sh
    :caption: gdbinit

    #set prompt \001\033[1;90m\002>>> \001\033[0m\002
    #set print elements 0
    #set disassemble-next-line on
    #set disassembly-flavor intel
    #set follow-fork-mode child

    set prompt \001\033[1;92m\002>>> \001\033[0m\002
    set print address on
    set print array on
    set print array-indexes on
    set print pretty on
    set print union on
    set print demangle on
    set print asm-demangle on
    set demangle-style gnu-v3
    set print object on
    set print static-members on
    set print vtbl on

    define ctrl-c
      signal SIGINT
    end

Reference
=========

- http://misc.flogisoft.com/bash/tip_colors_and_formatting
- http://askubuntu.com/questions/771925/avoid-terminal-characters-from-previous-terminal-commands

.. code-block::

    \001  Begin of non-printable character
    \002  End of non-printable character
    \033  Escape
