===========
 Profiling
===========

``main.go``
===========

#. Run program and generate profile files

   .. code-block:: sh

      go build main.go && ./main

#. View profile files

   .. code-block:: sh

      go tool pprof -http localhost:8888 main example-cpu.prof
      go tool pprof -http localhost:8888 main example-mem.prof

References
----------

- https://golang.org/pkg/runtime/pprof/

``main-web.go``
===============

#. Run web server

   .. code-block:: sh

      go run main-web.go

#. View memory profile

   - Trigger memory usage

     .. code-block:: sh

        curl http://localhost:6060/mem

   - View profile

     .. code-block:: sh

        go tool pprof -http localhost:8888 http://localhost:6060/debug/pprof/heap

#. View CPU profile

   - Start collecting profile before triggering CPU usage

     .. code-block:: sh

        go tool pprof -http localhost:8888 'http://localhost:6060/debug/pprof/profile?seconds=60'

   - Trigger CPU usage

     .. code-block:: sh

        curl http://localhost:6060/cpu

References
----------

- https://golang.org/pkg/net/http/pprof/
- http://localhost:6060/debug/pprof/
