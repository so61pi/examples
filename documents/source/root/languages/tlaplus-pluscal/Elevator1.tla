----------------------------- MODULE Elevator1 -----------------------------
EXTENDS Integers, TLC, Sequences

(* --algorithm elevator
variables
  numOfStories \in 1..10,
  \* numOfStories = 5,

  elevator \in [currentFloor : 1..numOfStories],
  \* elevator = [currentFloor |-> 1],

  selected \in [src : 1..numOfStories, dst : 1..numOfStories],
  \* selected = [src |-> 5, dst |-> 1],

  src2dstSteps = <<>>;

define
  AtLeastTwoStepsOrNone == <>[](Len(src2dstSteps) /= 1)
  Monotonic(tuple) == \/ \A i \in 2..Len(tuple): tuple[i] > tuple[i-1]
                      \/ \A i \in 2..Len(tuple): tuple[i] < tuple[i-1]
end define;

begin
  Start:
  if selected.src = selected.dst then
    goto Done;
  end if;

  \* Move to src first
  MoveToSrcUp:
  while elevator.currentFloor < selected.src do
    MoveToSrcUpStep:
    elevator.currentFloor := elevator.currentFloor + 1;
  end while;

  MoveToSrcDown:
  while elevator.currentFloor > selected.src do
      MoveToSrcDownStep:
      elevator.currentFloor := elevator.currentFloor - 1;
  end while;

  assert elevator.currentFloor = selected.src;

  \* Then move to dst
  MoveToDstPrep:
  src2dstSteps := Append(src2dstSteps, elevator.currentFloor);

  MoveToDstUp:
  while elevator.currentFloor < selected.dst do
    MoveToDstUpStep:
    elevator.currentFloor := elevator.currentFloor + 1;
    src2dstSteps := Append(src2dstSteps, elevator.currentFloor);
  end while;

  MoveToDstDown:
  while elevator.currentFloor > selected.dst do
    MoveToDstDownStep:
    elevator.currentFloor := elevator.currentFloor - 1;
    src2dstSteps := Append(src2dstSteps, elevator.currentFloor);
  end while;

  assert elevator.currentFloor = selected.dst;

end algorithm; *)
\* BEGIN TRANSLATION - the hash of the PCal code: PCal-3a035ea30bda1cb3bc65ae43a111c3c8
VARIABLES numOfStories, elevator, selected, src2dstSteps, pc

(* define statement *)
AtLeastTwoStepsOrNone == <>[](Len(src2dstSteps) /= 1)
Monotonic(tuple) == \/ \A i \in 2..Len(tuple): tuple[i] > tuple[i-1]
                    \/ \A i \in 2..Len(tuple): tuple[i] < tuple[i-1]


vars == << numOfStories, elevator, selected, src2dstSteps, pc >>

Init == (* Global variables *)
        /\ numOfStories \in 1..10
        /\ elevator \in [currentFloor : 1..numOfStories]
        /\ selected \in [src : 1..numOfStories, dst : 1..numOfStories]
        /\ src2dstSteps = <<>>
        /\ pc = "Start"

Start == /\ pc = "Start"
         /\ IF selected.src = selected.dst
               THEN /\ pc' = "Done"
               ELSE /\ pc' = "MoveToSrcUp"
         /\ UNCHANGED << numOfStories, elevator, selected, src2dstSteps >>

MoveToSrcUp == /\ pc = "MoveToSrcUp"
               /\ IF elevator.currentFloor < selected.src
                     THEN /\ pc' = "MoveToSrcUpStep"
                     ELSE /\ pc' = "MoveToSrcDown"
               /\ UNCHANGED << numOfStories, elevator, selected, src2dstSteps >>

MoveToSrcUpStep == /\ pc = "MoveToSrcUpStep"
                   /\ elevator' = [elevator EXCEPT !.currentFloor = elevator.currentFloor + 1]
                   /\ pc' = "MoveToSrcUp"
                   /\ UNCHANGED << numOfStories, selected, src2dstSteps >>

MoveToSrcDown == /\ pc = "MoveToSrcDown"
                 /\ IF elevator.currentFloor > selected.src
                       THEN /\ pc' = "MoveToSrcDownStep"
                       ELSE /\ Assert(elevator.currentFloor = selected.src, 
                                      "Failure of assertion at line 42, column 3.")
                            /\ pc' = "MoveToDstPrep"
                 /\ UNCHANGED << numOfStories, elevator, selected, 
                                 src2dstSteps >>

MoveToSrcDownStep == /\ pc = "MoveToSrcDownStep"
                     /\ elevator' = [elevator EXCEPT !.currentFloor = elevator.currentFloor - 1]
                     /\ pc' = "MoveToSrcDown"
                     /\ UNCHANGED << numOfStories, selected, src2dstSteps >>

MoveToDstPrep == /\ pc = "MoveToDstPrep"
                 /\ src2dstSteps' = Append(src2dstSteps, elevator.currentFloor)
                 /\ pc' = "MoveToDstUp"
                 /\ UNCHANGED << numOfStories, elevator, selected >>

MoveToDstUp == /\ pc = "MoveToDstUp"
               /\ IF elevator.currentFloor < selected.dst
                     THEN /\ pc' = "MoveToDstUpStep"
                     ELSE /\ pc' = "MoveToDstDown"
               /\ UNCHANGED << numOfStories, elevator, selected, src2dstSteps >>

MoveToDstUpStep == /\ pc = "MoveToDstUpStep"
                   /\ elevator' = [elevator EXCEPT !.currentFloor = elevator.currentFloor + 1]
                   /\ src2dstSteps' = Append(src2dstSteps, elevator'.currentFloor)
                   /\ pc' = "MoveToDstUp"
                   /\ UNCHANGED << numOfStories, selected >>

MoveToDstDown == /\ pc = "MoveToDstDown"
                 /\ IF elevator.currentFloor > selected.dst
                       THEN /\ pc' = "MoveToDstDownStep"
                       ELSE /\ Assert(elevator.currentFloor = selected.dst, 
                                      "Failure of assertion at line 62, column 3.")
                            /\ pc' = "Done"
                 /\ UNCHANGED << numOfStories, elevator, selected, 
                                 src2dstSteps >>

MoveToDstDownStep == /\ pc = "MoveToDstDownStep"
                     /\ elevator' = [elevator EXCEPT !.currentFloor = elevator.currentFloor - 1]
                     /\ src2dstSteps' = Append(src2dstSteps, elevator'.currentFloor)
                     /\ pc' = "MoveToDstDown"
                     /\ UNCHANGED << numOfStories, selected >>

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == pc = "Done" /\ UNCHANGED vars

Next == Start \/ MoveToSrcUp \/ MoveToSrcUpStep \/ MoveToSrcDown
           \/ MoveToSrcDownStep \/ MoveToDstPrep \/ MoveToDstUp
           \/ MoveToDstUpStep \/ MoveToDstDown \/ MoveToDstDownStep
           \/ Terminating

Spec == Init /\ [][Next]_vars

Termination == <>(pc = "Done")

\* END TRANSLATION - the hash of the generated TLA code (remove to silence divergence warnings): TLA-8e02ea8113d5c4eeb7f7a4a9e241241c

=============================================================================
\* Modification History
\* Last modified Sun Nov 29 22:27:44 ICT 2020 by so61pi
\* Created Sat Nov 07 20:35:31 ICT 2020 by so61pi
