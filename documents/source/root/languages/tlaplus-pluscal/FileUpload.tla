----------------------------- MODULE FileUpload -----------------------------
EXTENDS Integers, TLC, Sequences, FiniteSets

CONSTANTS NULL
CONSTANTS RInitiateUpload, RUploadPart, RCheckMissingParts
CONSTANTS CInitiateUpload, CUploadPart, CCheckMissingParts, CComplete
CONSTANTS RRSuccess, RRError

(* --algorithm FileUpload
variables
  dtparts = 1..5,
  reqKillAllow = 3, \* Number of times we can prevent requests from reaching server.
  resKillAllow = 4, \* Number of times we can alter responses from server.
  cRebootAllow = 5, \* Number of times client can restart/crash.

  cReqQueue = <<>>, \* Client's request and response queues.
  cResQueue = <<>>,

  sReqQueue = <<>>, \* Server's request and response queues.
  sResQueue = <<>>,

  clientWorking = TRUE, \* Used by client to signal other processes to end, otherwise we'll have deadlock error.

  uploaded = {}; \* Data parts uploaded to server.

define
  set ++ x == set \union {x}
  set -- x == set \ {x}

  CreateRequest(t, d) == [type |-> t, data |-> d]
  CreateResponse(r, d) == [result |-> r, data |-> d]

  UploadCompleted == <>[](uploaded = dtparts)
end define;

macro CMayCrash()
begin
  either
    if cRebootAllow > 0 then
      cRebootAllow := cRebootAllow - 1;
      goto LCSystemRebooted;
    end if;
  or skip;
  end either;
end macro;

\* We don't simulate server crashing for 2 reasons:
\* - It's hard to do properly.
\* - We can have multiple servers running at a same time to remedy this.
\* - The result that clients can see is timeout, and we can simulate that in other ways.
fair process Server = "s"
variables
  suid = "upload-id-12345A54321",
  req = NULL
begin LSWhile:
  while clientWorking do
    req := NULL;

    LSWaitRequest:
    \* Using `await queue /= <<>>;` would lead to deadlock when client is done.
    if sReqQueue = <<>> then
      goto LSWhile;
    end if;

    LSExtractRequest:
    req := Head(sReqQueue); sReqQueue := Tail(sReqQueue);

    \* Return suid which will be sent by client in subsequent requests.
    if req.type = RInitiateUpload then
      LSInitiateUpload:
      sResQueue := Append(sResQueue, CreateResponse(RRSuccess, suid));

    \* Add received part to uploaded set. If a part is sent twice, we will still have one in the set.
    elsif req.type = RUploadPart then
      assert req.data.uid = suid;
      LSUploadPart:
      uploaded := uploaded ++ req.data.part;
      sResQueue := Append(sResQueue, CreateResponse(RRSuccess, 0 (* whatever *)));

    \* Return missing parts to client to it can upload those.
    elsif req.type = RCheckMissingParts then
      assert req.data.uid = suid;
      LSCheckMissingParts:
      if uploaded = dtparts then
        sResQueue := Append(sResQueue, CreateResponse(RRSuccess, {}));
      else
        sResQueue := Append(sResQueue, CreateResponse(RRSuccess, CHOOSE x \in SUBSET (dtparts \ uploaded): Cardinality(x) > 0));
      end if;
    end if
  end while;
end process;

fair process Client = "c"
variables
  tmppart,
  res = <<>>,

  \* NV = None Volatile
  cuidNV,
  stateNV = CInitiateUpload,
  curpartsNV,
begin
  LCSystemRebooted:
  while stateNV /= CComplete do
    \* Without this, we could encounter `Attempted to check equality of integer 0 with non-integer: "upload-id-12345A54321"`
    \* It is because `res` receives different conflicting function/struct types (from RInitiateUpload and RCheckMissingParts).
    \* Have no idea why adding this line works though.
    res := NULL;

    \* Send first request to get upload id.
    if stateNV = CInitiateUpload then
      LCSendRInitiateUpload:
      \* Crashing before sending out requests is useless in our case here, so we don't do it.
      cReqQueue := Append(cReqQueue, CreateRequest(RInitiateUpload, NULL));

      LCWaitResRInitiateUpload:
      await cResQueue /= <<>>; res := Head(cResQueue); cResQueue := Tail(cResQueue);
      \* Crashing right after sending out requests is similar to crashing when we receive
      \* the response and have not done anything yet. And the latter is easier to simulate.
      CMayCrash();
      
      LCHandleResRInitiateUpload:
      if res.result = RRSuccess then
        cuidNV := res.data;
        curpartsNV := dtparts;
        stateNV := CUploadPart;
      end if;
      \* Crashing at this point is meaningless since all data is already persisted.

    \* Upload part by part.
    elsif stateNV = CUploadPart then
      LCPrepairDataCUploadPart:
      tmppart := CHOOSE p \in curpartsNV: TRUE;
      curpartsNV := curpartsNV -- tmppart;
      stateNV := IF curpartsNV = {} THEN CCheckMissingParts ELSE CUploadPart;
      CMayCrash(); \* We can crash even before sending this part out, and at the end everything is still uploaded.

      LCSendRUploadPart:
      cReqQueue := Append(cReqQueue, CreateRequest(RUploadPart, [uid |-> cuidNV, part |-> tmppart]));

      LCHandleResRUploadPart:
      \* Just ignore the result, whether it is RRSuccess or RRError.
      await cResQueue /= <<>>; cResQueue := Tail(cResQueue);
      CMayCrash();

    \* Check if there are any missing pieces.
    elsif stateNV = CCheckMissingParts then
      LCSendRCheckMissingParts:
      cReqQueue := Append(cReqQueue, CreateRequest(RCheckMissingParts, [uid |-> cuidNV]));

      LCWaitResRCheckMissingParts:
      await cResQueue /= <<>>; res := Head(cResQueue); cResQueue := Tail(cResQueue);
      CMayCrash();

      LCHandleResRCheckMissingParts:
      if res.result = RRSuccess then
        curpartsNV := res.data;
        stateNV := IF curpartsNV = {} THEN CComplete ELSE CUploadPart;
      end if;
    end if;
  end while;

  clientWorking := FALSE;
end process;

\* +--------+                                     +--------+
\* |        |                                     |        |
\* |        +----->NetworkLayerRequest+---------->+        |
\* |        |      - Pass                         |        |
\* |        |      - Drop and return RRError      |        |
\* |        |                                     |        |
\* | Client |                                     | Server |
\* |        |                                     |        |
\* |        |                                     |        |
\* |        |                                     |        |
\* |        +<----+NetworkLayerResponse<----------+        |
\* |        |      - Pass                         |        |
\* +--------+      - Alter with RRError           +--------+

\* Using strong fairness here since this is just an interceptor to inject errors.
\* Note: This could be turned into a macro and the client process can use
\*    SendRequest(CreateRequest(...), res);
fair+ process NetworkLayerRequest = "req"
variables
  drop
begin LNetReqWhile:
  while clientWorking do
    LReqWaitClientReq:
    \* Using `await queue /= <<>>;` would lead to deadlock when client is done.
    if cReqQueue = <<>> then
      goto LNetReqWhile;
    end if;

    LReqIntercept:
    either
      drop := IF reqKillAllow > 0 THEN CHOOSE x \in BOOLEAN: TRUE ELSE FALSE;
      reqKillAllow := reqKillAllow - 1;
    or
      drop := FALSE;
    end either;

    if drop then
      \* Server doesn't even have a chance to see this package.
      cResQueue := Append(cResQueue, CreateResponse(RRError, 0 (* whatever *)));
    else
      sReqQueue := Append(sReqQueue, Head(cReqQueue));
    end if;
    cReqQueue := Tail(cReqQueue);
  end while;
end process;

\* Using strong fairness here since this is just an interceptor to inject errors.
\* Note: This could be turned into a macro and the server process can use
\*    SendResponse(CreateResponse(...));
fair+ process NetworkLayerResponse = "res"
variables
  alter
begin LNetResWhile:
  while clientWorking do
    LReqWaitServerRes:
    \* Using `await queue /= <<>>;` would lead to deadlock when client is done.
    if sResQueue = <<>> then
      goto LNetResWhile;
    end if;

    LResIntercept:
    either
      alter := IF resKillAllow > 0 THEN CHOOSE x \in BOOLEAN: TRUE ELSE FALSE;
      resKillAllow := resKillAllow - 1;
    or
      alter := FALSE;
    end either;

    if alter then
      \* Alter server's response to simulate error.
      cResQueue := Append(cResQueue, CreateResponse(RRError, 0 (* whatever *)));
    else
      cResQueue := Append(cResQueue, Head(sResQueue));
    end if;
    sResQueue := Tail(sResQueue);
  end while;
end process;

end algorithm; *)
\* BEGIN TRANSLATION - the hash of the PCal code: PCal-35071de55001100928ee8bc049d19182
CONSTANT defaultInitValue
VARIABLES dtparts, reqKillAllow, resKillAllow, cRebootAllow, cReqQueue, 
          cResQueue, sReqQueue, sResQueue, clientWorking, uploaded, pc

(* define statement *)
set ++ x == set \union {x}
set -- x == set \ {x}

CreateRequest(t, d) == [type |-> t, data |-> d]
CreateResponse(r, d) == [result |-> r, data |-> d]

UploadCompleted == <>[](uploaded = dtparts)

VARIABLES suid, req, tmppart, res, cuidNV, stateNV, curpartsNV, drop, alter

vars == << dtparts, reqKillAllow, resKillAllow, cRebootAllow, cReqQueue, 
           cResQueue, sReqQueue, sResQueue, clientWorking, uploaded, pc, suid, 
           req, tmppart, res, cuidNV, stateNV, curpartsNV, drop, alter >>

ProcSet == {"s"} \cup {"c"} \cup {"req"} \cup {"res"}

Init == (* Global variables *)
        /\ dtparts = 1..5
        /\ reqKillAllow = 3
        /\ resKillAllow = 4
        /\ cRebootAllow = 5
        /\ cReqQueue = <<>>
        /\ cResQueue = <<>>
        /\ sReqQueue = <<>>
        /\ sResQueue = <<>>
        /\ clientWorking = TRUE
        /\ uploaded = {}
        (* Process Server *)
        /\ suid = "upload-id-12345A54321"
        /\ req = NULL
        (* Process Client *)
        /\ tmppart = defaultInitValue
        /\ res = <<>>
        /\ cuidNV = defaultInitValue
        /\ stateNV = CInitiateUpload
        /\ curpartsNV = defaultInitValue
        (* Process NetworkLayerRequest *)
        /\ drop = defaultInitValue
        (* Process NetworkLayerResponse *)
        /\ alter = defaultInitValue
        /\ pc = [self \in ProcSet |-> CASE self = "s" -> "LSWhile"
                                        [] self = "c" -> "LCSystemRebooted"
                                        [] self = "req" -> "LNetReqWhile"
                                        [] self = "res" -> "LNetResWhile"]

LSWhile == /\ pc["s"] = "LSWhile"
           /\ IF clientWorking
                 THEN /\ req' = NULL
                      /\ pc' = [pc EXCEPT !["s"] = "LSWaitRequest"]
                 ELSE /\ pc' = [pc EXCEPT !["s"] = "Done"]
                      /\ req' = req
           /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, cRebootAllow, 
                           cReqQueue, cResQueue, sReqQueue, sResQueue, 
                           clientWorking, uploaded, suid, tmppart, res, cuidNV, 
                           stateNV, curpartsNV, drop, alter >>

LSWaitRequest == /\ pc["s"] = "LSWaitRequest"
                 /\ IF sReqQueue = <<>>
                       THEN /\ pc' = [pc EXCEPT !["s"] = "LSWhile"]
                       ELSE /\ pc' = [pc EXCEPT !["s"] = "LSExtractRequest"]
                 /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                 cRebootAllow, cReqQueue, cResQueue, sReqQueue, 
                                 sResQueue, clientWorking, uploaded, suid, req, 
                                 tmppart, res, cuidNV, stateNV, curpartsNV, 
                                 drop, alter >>

LSExtractRequest == /\ pc["s"] = "LSExtractRequest"
                    /\ req' = Head(sReqQueue)
                    /\ sReqQueue' = Tail(sReqQueue)
                    /\ IF req'.type = RInitiateUpload
                          THEN /\ pc' = [pc EXCEPT !["s"] = "LSInitiateUpload"]
                          ELSE /\ IF req'.type = RUploadPart
                                     THEN /\ Assert(req'.data.uid = suid, 
                                                    "Failure of assertion at line 75, column 7.")
                                          /\ pc' = [pc EXCEPT !["s"] = "LSUploadPart"]
                                     ELSE /\ IF req'.type = RCheckMissingParts
                                                THEN /\ Assert(req'.data.uid = suid, 
                                                               "Failure of assertion at line 82, column 7.")
                                                     /\ pc' = [pc EXCEPT !["s"] = "LSCheckMissingParts"]
                                                ELSE /\ pc' = [pc EXCEPT !["s"] = "LSWhile"]
                    /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                    cRebootAllow, cReqQueue, cResQueue, 
                                    sResQueue, clientWorking, uploaded, suid, 
                                    tmppart, res, cuidNV, stateNV, curpartsNV, 
                                    drop, alter >>

LSInitiateUpload == /\ pc["s"] = "LSInitiateUpload"
                    /\ sResQueue' = Append(sResQueue, CreateResponse(RRSuccess, suid))
                    /\ pc' = [pc EXCEPT !["s"] = "LSWhile"]
                    /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                    cRebootAllow, cReqQueue, cResQueue, 
                                    sReqQueue, clientWorking, uploaded, suid, 
                                    req, tmppart, res, cuidNV, stateNV, 
                                    curpartsNV, drop, alter >>

LSUploadPart == /\ pc["s"] = "LSUploadPart"
                /\ uploaded' = uploaded ++ req.data.part
                /\ sResQueue' = Append(sResQueue, CreateResponse(RRSuccess, 0               ))
                /\ pc' = [pc EXCEPT !["s"] = "LSWhile"]
                /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                cRebootAllow, cReqQueue, cResQueue, sReqQueue, 
                                clientWorking, suid, req, tmppart, res, cuidNV, 
                                stateNV, curpartsNV, drop, alter >>

LSCheckMissingParts == /\ pc["s"] = "LSCheckMissingParts"
                       /\ IF uploaded = dtparts
                             THEN /\ sResQueue' = Append(sResQueue, CreateResponse(RRSuccess, {}))
                             ELSE /\ sResQueue' = Append(sResQueue, CreateResponse(RRSuccess, CHOOSE x \in SUBSET (dtparts \ uploaded): Cardinality(x) > 0))
                       /\ pc' = [pc EXCEPT !["s"] = "LSWhile"]
                       /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                       cRebootAllow, cReqQueue, cResQueue, 
                                       sReqQueue, clientWorking, uploaded, 
                                       suid, req, tmppart, res, cuidNV, 
                                       stateNV, curpartsNV, drop, alter >>

Server == LSWhile \/ LSWaitRequest \/ LSExtractRequest \/ LSInitiateUpload
             \/ LSUploadPart \/ LSCheckMissingParts

LCSystemRebooted == /\ pc["c"] = "LCSystemRebooted"
                    /\ IF stateNV /= CComplete
                          THEN /\ res' = NULL
                               /\ IF stateNV = CInitiateUpload
                                     THEN /\ pc' = [pc EXCEPT !["c"] = "LCSendRInitiateUpload"]
                                     ELSE /\ IF stateNV = CUploadPart
                                                THEN /\ pc' = [pc EXCEPT !["c"] = "LCPrepairDataCUploadPart"]
                                                ELSE /\ IF stateNV = CCheckMissingParts
                                                           THEN /\ pc' = [pc EXCEPT !["c"] = "LCSendRCheckMissingParts"]
                                                           ELSE /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                               /\ UNCHANGED clientWorking
                          ELSE /\ clientWorking' = FALSE
                               /\ pc' = [pc EXCEPT !["c"] = "Done"]
                               /\ res' = res
                    /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                    cRebootAllow, cReqQueue, cResQueue, 
                                    sReqQueue, sResQueue, uploaded, suid, req, 
                                    tmppart, cuidNV, stateNV, curpartsNV, drop, 
                                    alter >>

LCSendRInitiateUpload == /\ pc["c"] = "LCSendRInitiateUpload"
                         /\ cReqQueue' = Append(cReqQueue, CreateRequest(RInitiateUpload, NULL))
                         /\ pc' = [pc EXCEPT !["c"] = "LCWaitResRInitiateUpload"]
                         /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                         cRebootAllow, cResQueue, sReqQueue, 
                                         sResQueue, clientWorking, uploaded, 
                                         suid, req, tmppart, res, cuidNV, 
                                         stateNV, curpartsNV, drop, alter >>

LCWaitResRInitiateUpload == /\ pc["c"] = "LCWaitResRInitiateUpload"
                            /\ cResQueue /= <<>>
                            /\ res' = Head(cResQueue)
                            /\ cResQueue' = Tail(cResQueue)
                            /\ \/ /\ IF cRebootAllow > 0
                                        THEN /\ cRebootAllow' = cRebootAllow - 1
                                             /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                                        ELSE /\ pc' = [pc EXCEPT !["c"] = "LCHandleResRInitiateUpload"]
                                             /\ UNCHANGED cRebootAllow
                               \/ /\ TRUE
                                  /\ pc' = [pc EXCEPT !["c"] = "LCHandleResRInitiateUpload"]
                                  /\ UNCHANGED cRebootAllow
                            /\ UNCHANGED << dtparts, reqKillAllow, 
                                            resKillAllow, cReqQueue, sReqQueue, 
                                            sResQueue, clientWorking, uploaded, 
                                            suid, req, tmppart, cuidNV, 
                                            stateNV, curpartsNV, drop, alter >>

LCHandleResRInitiateUpload == /\ pc["c"] = "LCHandleResRInitiateUpload"
                              /\ IF res.result = RRSuccess
                                    THEN /\ cuidNV' = res.data
                                         /\ curpartsNV' = dtparts
                                         /\ stateNV' = CUploadPart
                                    ELSE /\ TRUE
                                         /\ UNCHANGED << cuidNV, stateNV, 
                                                         curpartsNV >>
                              /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                              /\ UNCHANGED << dtparts, reqKillAllow, 
                                              resKillAllow, cRebootAllow, 
                                              cReqQueue, cResQueue, sReqQueue, 
                                              sResQueue, clientWorking, 
                                              uploaded, suid, req, tmppart, 
                                              res, drop, alter >>

LCPrepairDataCUploadPart == /\ pc["c"] = "LCPrepairDataCUploadPart"
                            /\ tmppart' = (CHOOSE p \in curpartsNV: TRUE)
                            /\ curpartsNV' = curpartsNV -- tmppart'
                            /\ stateNV' = (IF curpartsNV' = {} THEN CCheckMissingParts ELSE CUploadPart)
                            /\ \/ /\ IF cRebootAllow > 0
                                        THEN /\ cRebootAllow' = cRebootAllow - 1
                                             /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                                        ELSE /\ pc' = [pc EXCEPT !["c"] = "LCSendRUploadPart"]
                                             /\ UNCHANGED cRebootAllow
                               \/ /\ TRUE
                                  /\ pc' = [pc EXCEPT !["c"] = "LCSendRUploadPart"]
                                  /\ UNCHANGED cRebootAllow
                            /\ UNCHANGED << dtparts, reqKillAllow, 
                                            resKillAllow, cReqQueue, cResQueue, 
                                            sReqQueue, sResQueue, 
                                            clientWorking, uploaded, suid, req, 
                                            res, cuidNV, drop, alter >>

LCSendRUploadPart == /\ pc["c"] = "LCSendRUploadPart"
                     /\ cReqQueue' = Append(cReqQueue, CreateRequest(RUploadPart, [uid |-> cuidNV, part |-> tmppart]))
                     /\ pc' = [pc EXCEPT !["c"] = "LCHandleResRUploadPart"]
                     /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                     cRebootAllow, cResQueue, sReqQueue, 
                                     sResQueue, clientWorking, uploaded, suid, 
                                     req, tmppart, res, cuidNV, stateNV, 
                                     curpartsNV, drop, alter >>

LCHandleResRUploadPart == /\ pc["c"] = "LCHandleResRUploadPart"
                          /\ cResQueue /= <<>>
                          /\ cResQueue' = Tail(cResQueue)
                          /\ \/ /\ IF cRebootAllow > 0
                                      THEN /\ cRebootAllow' = cRebootAllow - 1
                                           /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                                      ELSE /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                                           /\ UNCHANGED cRebootAllow
                             \/ /\ TRUE
                                /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                                /\ UNCHANGED cRebootAllow
                          /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                          cReqQueue, sReqQueue, sResQueue, 
                                          clientWorking, uploaded, suid, req, 
                                          tmppart, res, cuidNV, stateNV, 
                                          curpartsNV, drop, alter >>

LCSendRCheckMissingParts == /\ pc["c"] = "LCSendRCheckMissingParts"
                            /\ cReqQueue' = Append(cReqQueue, CreateRequest(RCheckMissingParts, [uid |-> cuidNV]))
                            /\ pc' = [pc EXCEPT !["c"] = "LCWaitResRCheckMissingParts"]
                            /\ UNCHANGED << dtparts, reqKillAllow, 
                                            resKillAllow, cRebootAllow, 
                                            cResQueue, sReqQueue, sResQueue, 
                                            clientWorking, uploaded, suid, req, 
                                            tmppart, res, cuidNV, stateNV, 
                                            curpartsNV, drop, alter >>

LCWaitResRCheckMissingParts == /\ pc["c"] = "LCWaitResRCheckMissingParts"
                               /\ cResQueue /= <<>>
                               /\ res' = Head(cResQueue)
                               /\ cResQueue' = Tail(cResQueue)
                               /\ \/ /\ IF cRebootAllow > 0
                                           THEN /\ cRebootAllow' = cRebootAllow - 1
                                                /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                                           ELSE /\ pc' = [pc EXCEPT !["c"] = "LCHandleResRCheckMissingParts"]
                                                /\ UNCHANGED cRebootAllow
                                  \/ /\ TRUE
                                     /\ pc' = [pc EXCEPT !["c"] = "LCHandleResRCheckMissingParts"]
                                     /\ UNCHANGED cRebootAllow
                               /\ UNCHANGED << dtparts, reqKillAllow, 
                                               resKillAllow, cReqQueue, 
                                               sReqQueue, sResQueue, 
                                               clientWorking, uploaded, suid, 
                                               req, tmppart, cuidNV, stateNV, 
                                               curpartsNV, drop, alter >>

LCHandleResRCheckMissingParts == /\ pc["c"] = "LCHandleResRCheckMissingParts"
                                 /\ IF res.result = RRSuccess
                                       THEN /\ curpartsNV' = res.data
                                            /\ stateNV' = (IF curpartsNV' = {} THEN CComplete ELSE CUploadPart)
                                       ELSE /\ TRUE
                                            /\ UNCHANGED << stateNV, 
                                                            curpartsNV >>
                                 /\ pc' = [pc EXCEPT !["c"] = "LCSystemRebooted"]
                                 /\ UNCHANGED << dtparts, reqKillAllow, 
                                                 resKillAllow, cRebootAllow, 
                                                 cReqQueue, cResQueue, 
                                                 sReqQueue, sResQueue, 
                                                 clientWorking, uploaded, suid, 
                                                 req, tmppart, res, cuidNV, 
                                                 drop, alter >>

Client == LCSystemRebooted \/ LCSendRInitiateUpload
             \/ LCWaitResRInitiateUpload \/ LCHandleResRInitiateUpload
             \/ LCPrepairDataCUploadPart \/ LCSendRUploadPart
             \/ LCHandleResRUploadPart \/ LCSendRCheckMissingParts
             \/ LCWaitResRCheckMissingParts
             \/ LCHandleResRCheckMissingParts

LNetReqWhile == /\ pc["req"] = "LNetReqWhile"
                /\ IF clientWorking
                      THEN /\ pc' = [pc EXCEPT !["req"] = "LReqWaitClientReq"]
                      ELSE /\ pc' = [pc EXCEPT !["req"] = "Done"]
                /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                cRebootAllow, cReqQueue, cResQueue, sReqQueue, 
                                sResQueue, clientWorking, uploaded, suid, req, 
                                tmppart, res, cuidNV, stateNV, curpartsNV, 
                                drop, alter >>

LReqWaitClientReq == /\ pc["req"] = "LReqWaitClientReq"
                     /\ IF cReqQueue = <<>>
                           THEN /\ pc' = [pc EXCEPT !["req"] = "LNetReqWhile"]
                           ELSE /\ pc' = [pc EXCEPT !["req"] = "LReqIntercept"]
                     /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                     cRebootAllow, cReqQueue, cResQueue, 
                                     sReqQueue, sResQueue, clientWorking, 
                                     uploaded, suid, req, tmppart, res, cuidNV, 
                                     stateNV, curpartsNV, drop, alter >>

LReqIntercept == /\ pc["req"] = "LReqIntercept"
                 /\ \/ /\ drop' = (IF reqKillAllow > 0 THEN CHOOSE x \in BOOLEAN: TRUE ELSE FALSE)
                       /\ reqKillAllow' = reqKillAllow - 1
                    \/ /\ drop' = FALSE
                       /\ UNCHANGED reqKillAllow
                 /\ IF drop'
                       THEN /\ cResQueue' = Append(cResQueue, CreateResponse(RRError, 0               ))
                            /\ UNCHANGED sReqQueue
                       ELSE /\ sReqQueue' = Append(sReqQueue, Head(cReqQueue))
                            /\ UNCHANGED cResQueue
                 /\ cReqQueue' = Tail(cReqQueue)
                 /\ pc' = [pc EXCEPT !["req"] = "LNetReqWhile"]
                 /\ UNCHANGED << dtparts, resKillAllow, cRebootAllow, 
                                 sResQueue, clientWorking, uploaded, suid, req, 
                                 tmppart, res, cuidNV, stateNV, curpartsNV, 
                                 alter >>

NetworkLayerRequest == LNetReqWhile \/ LReqWaitClientReq \/ LReqIntercept

LNetResWhile == /\ pc["res"] = "LNetResWhile"
                /\ IF clientWorking
                      THEN /\ pc' = [pc EXCEPT !["res"] = "LReqWaitServerRes"]
                      ELSE /\ pc' = [pc EXCEPT !["res"] = "Done"]
                /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                cRebootAllow, cReqQueue, cResQueue, sReqQueue, 
                                sResQueue, clientWorking, uploaded, suid, req, 
                                tmppart, res, cuidNV, stateNV, curpartsNV, 
                                drop, alter >>

LReqWaitServerRes == /\ pc["res"] = "LReqWaitServerRes"
                     /\ IF sResQueue = <<>>
                           THEN /\ pc' = [pc EXCEPT !["res"] = "LNetResWhile"]
                           ELSE /\ pc' = [pc EXCEPT !["res"] = "LResIntercept"]
                     /\ UNCHANGED << dtparts, reqKillAllow, resKillAllow, 
                                     cRebootAllow, cReqQueue, cResQueue, 
                                     sReqQueue, sResQueue, clientWorking, 
                                     uploaded, suid, req, tmppart, res, cuidNV, 
                                     stateNV, curpartsNV, drop, alter >>

LResIntercept == /\ pc["res"] = "LResIntercept"
                 /\ \/ /\ alter' = (IF resKillAllow > 0 THEN CHOOSE x \in BOOLEAN: TRUE ELSE FALSE)
                       /\ resKillAllow' = resKillAllow - 1
                    \/ /\ alter' = FALSE
                       /\ UNCHANGED resKillAllow
                 /\ IF alter'
                       THEN /\ cResQueue' = Append(cResQueue, CreateResponse(RRError, 0               ))
                       ELSE /\ cResQueue' = Append(cResQueue, Head(sResQueue))
                 /\ sResQueue' = Tail(sResQueue)
                 /\ pc' = [pc EXCEPT !["res"] = "LNetResWhile"]
                 /\ UNCHANGED << dtparts, reqKillAllow, cRebootAllow, 
                                 cReqQueue, sReqQueue, clientWorking, uploaded, 
                                 suid, req, tmppart, res, cuidNV, stateNV, 
                                 curpartsNV, drop >>

NetworkLayerResponse == LNetResWhile \/ LReqWaitServerRes \/ LResIntercept

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == /\ \A self \in ProcSet: pc[self] = "Done"
               /\ UNCHANGED vars

Next == Server \/ Client \/ NetworkLayerRequest \/ NetworkLayerResponse
           \/ Terminating

Spec == /\ Init /\ [][Next]_vars
        /\ WF_vars(Server)
        /\ WF_vars(Client)
        /\ SF_vars(NetworkLayerRequest)
        /\ SF_vars(NetworkLayerResponse)

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION - the hash of the generated TLA code (remove to silence divergence warnings): TLA-b24f37ff3b3a0839f852da5f7ea4ae39

=============================================================================
\* Modification History
\* Last modified Sun Nov 29 22:15:33 ICT 2020 by so61pi
\* Created Tue Nov 24 22:11:57 ICT 2020 by so61pi
