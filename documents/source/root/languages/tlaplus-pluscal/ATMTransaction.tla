--------------------------- MODULE ATMTransaction ---------------------------
EXTENDS Integers, TLC, Sequences

CONSTANTS NULL, ATMMachines, Accounts

(* --algorithm ATMTransaction
variables
  defaultBalance \in 1..10,
  machineCompleted = [x \in ATMMachines |-> FALSE],
  balances = [x \in Accounts |-> defaultBalance],
  withdrawn = [x \in Accounts |-> 0],
  jobQueue = <<>>,
  responseQueue = [x \in ATMMachines |-> <<>>];

define
  BalanceInvariant == \A acc \in Accounts : withdrawn[acc] + balances[acc] = defaultBalance
  AllATMMachinesAreCompleted == <>[](\A x \in ATMMachines : machineCompleted[x])
end define;

process ServerProcess = "server"
variables
  job = <<>>,
  success = FALSE,
begin LWhile:
  while \E x \in ATMMachines : machineCompleted[x] = FALSE do
    LWaitForJob:
    await jobQueue /= <<>>;
    job := Head(jobQueue) || jobQueue := Tail(jobQueue);

    LExecute:
    if balances[job.account] > 0 /\ balances[job.account] >= job.amount then
      balances[job.account] := balances[job.account] - job.amount;
      success := TRUE;
    else
      success := FALSE;
    end if;

    LSendResponse:
    responseQueue[job.machine] := Append(responseQueue[job.machine], [machine |-> job.machine,
                                                                      account |-> job.account,
                                                                      amount |-> job.amount,
                                                                      success |-> success]);
    machineCompleted[job.machine] := TRUE;
  end while;
end process;

process ATMMachineProcess \in ATMMachines
variables
  response = <<>>,
begin
    \* Send request to server.
  LSendRequest:
  with account \in Accounts, amount \in 0..2*defaultBalance do
    jobQueue := Append(jobQueue, [machine |-> self, account |-> account, amount |-> amount]);
  end with;

  \* Wait for response.
  LWaitForResponse:
  await responseQueue[self] /= <<>>;
  response := Head(responseQueue[self]) || responseQueue[self] := Tail(responseQueue[self]);
  assert response.machine = self;
  if response.success then
    withdrawn[response.account] := withdrawn[response.account] + response.amount;
  end if;
end process;

end algorithm; *)
\* BEGIN TRANSLATION - the hash of the PCal code: PCal-17442a93dac078c345b8d443b6c60fd4
VARIABLES defaultBalance, machineCompleted, balances, withdrawn, jobQueue, 
          responseQueue, pc

(* define statement *)
BalanceInvariant == \A acc \in Accounts : withdrawn[acc] + balances[acc] = defaultBalance
AllATMMachinesAreCompleted == <>[](\A x \in ATMMachines : machineCompleted[x])

VARIABLES job, success, response

vars == << defaultBalance, machineCompleted, balances, withdrawn, jobQueue, 
           responseQueue, pc, job, success, response >>

ProcSet == {"server"} \cup (ATMMachines)

Init == (* Global variables *)
        /\ defaultBalance \in 1..10
        /\ machineCompleted = [x \in ATMMachines |-> FALSE]
        /\ balances = [x \in Accounts |-> defaultBalance]
        /\ withdrawn = [x \in Accounts |-> 0]
        /\ jobQueue = <<>>
        /\ responseQueue = [x \in ATMMachines |-> <<>>]
        (* Process ServerProcess *)
        /\ job = <<>>
        /\ success = FALSE
        (* Process ATMMachineProcess *)
        /\ response = [self \in ATMMachines |-> <<>>]
        /\ pc = [self \in ProcSet |-> CASE self = "server" -> "LWhile"
                                        [] self \in ATMMachines -> "LSendRequest"]

LWhile == /\ pc["server"] = "LWhile"
          /\ IF \E x \in ATMMachines : machineCompleted[x] = FALSE
                THEN /\ pc' = [pc EXCEPT !["server"] = "LWaitForJob"]
                ELSE /\ pc' = [pc EXCEPT !["server"] = "Done"]
          /\ UNCHANGED << defaultBalance, machineCompleted, balances, 
                          withdrawn, jobQueue, responseQueue, job, success, 
                          response >>

LWaitForJob == /\ pc["server"] = "LWaitForJob"
               /\ jobQueue /= <<>>
               /\ /\ job' = Head(jobQueue)
                  /\ jobQueue' = Tail(jobQueue)
               /\ pc' = [pc EXCEPT !["server"] = "LExecute"]
               /\ UNCHANGED << defaultBalance, machineCompleted, balances, 
                               withdrawn, responseQueue, success, response >>

LExecute == /\ pc["server"] = "LExecute"
            /\ IF balances[job.account] > 0 /\ balances[job.account] >= job.amount
                  THEN /\ balances' = [balances EXCEPT ![job.account] = balances[job.account] - job.amount]
                       /\ success' = TRUE
                  ELSE /\ success' = FALSE
                       /\ UNCHANGED balances
            /\ pc' = [pc EXCEPT !["server"] = "LSendResponse"]
            /\ UNCHANGED << defaultBalance, machineCompleted, withdrawn, 
                            jobQueue, responseQueue, job, response >>

LSendResponse == /\ pc["server"] = "LSendResponse"
                 /\ responseQueue' = [responseQueue EXCEPT ![job.machine] = Append(responseQueue[job.machine], [machine |-> job.machine,
                                                                                                                account |-> job.account,
                                                                                                                amount |-> job.amount,
                                                                                                                success |-> success])]
                 /\ machineCompleted' = [machineCompleted EXCEPT ![job.machine] = TRUE]
                 /\ pc' = [pc EXCEPT !["server"] = "LWhile"]
                 /\ UNCHANGED << defaultBalance, balances, withdrawn, jobQueue, 
                                 job, success, response >>

ServerProcess == LWhile \/ LWaitForJob \/ LExecute \/ LSendResponse

LSendRequest(self) == /\ pc[self] = "LSendRequest"
                      /\ \E account \in Accounts:
                           \E amount \in 0..2*defaultBalance:
                             jobQueue' = Append(jobQueue, [machine |-> self, account |-> account, amount |-> amount])
                      /\ pc' = [pc EXCEPT ![self] = "LWaitForResponse"]
                      /\ UNCHANGED << defaultBalance, machineCompleted, 
                                      balances, withdrawn, responseQueue, job, 
                                      success, response >>

LWaitForResponse(self) == /\ pc[self] = "LWaitForResponse"
                          /\ responseQueue[self] /= <<>>
                          /\ /\ response' = [response EXCEPT ![self] = Head(responseQueue[self])]
                             /\ responseQueue' = [responseQueue EXCEPT ![self] = Tail(responseQueue[self])]
                          /\ Assert(response'[self].machine = self, 
                                    "Failure of assertion at line 61, column 3.")
                          /\ IF response'[self].success
                                THEN /\ withdrawn' = [withdrawn EXCEPT ![response'[self].account] = withdrawn[response'[self].account] + response'[self].amount]
                                ELSE /\ TRUE
                                     /\ UNCHANGED withdrawn
                          /\ pc' = [pc EXCEPT ![self] = "Done"]
                          /\ UNCHANGED << defaultBalance, machineCompleted, 
                                          balances, jobQueue, job, success >>

ATMMachineProcess(self) == LSendRequest(self) \/ LWaitForResponse(self)

(* Allow infinite stuttering to prevent deadlock on termination. *)
Terminating == /\ \A self \in ProcSet: pc[self] = "Done"
               /\ UNCHANGED vars

Next == ServerProcess
           \/ (\E self \in ATMMachines: ATMMachineProcess(self))
           \/ Terminating

Spec == Init /\ [][Next]_vars

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION - the hash of the generated TLA code (remove to silence divergence warnings): TLA-34aa111e6d02b48b148ce10b9558423f
=============================================================================
\* Modification History
\* Last modified Sun Nov 29 22:28:36 ICT 2020 by so61pi
\* Created Sun Nov 08 18:41:28 ICT 2020 by so61pi
