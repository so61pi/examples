----------------------------- MODULE Elevator2 -----------------------------
EXTENDS Integers, TLC, Sequences
CONSTANTS SSTILL, SCOMEUP, SGODOWN
CONSTANTS People, NumOfStories
ASSUME People /= {}
ASSUME NumOfStories \in Nat /\ NumOfStories > 1

(* --algorithm elevator
variables
  Stories = 1..NumOfStories,
  LowerInvalidLocation = 1-1,
  UpperInvalidLocation = NumOfStories+1,
  eCurrentLocation \in Stories,
  eState = SSTILL,
  eStopAtU = [x \in Stories |-> FALSE],
  eStopAtD = [x \in Stories |-> FALSE],
  eDstU = [x \in Stories |-> {}],
  eDstD = [x \in Stories |-> {}],
  eContainedPeople = {},
  pCCur = [p \in People |-> 0],
  pASrc \in [People -> Stories],
  pBDst \in ({value \in [People -> Stories]: \A p \in People: value[p] /= pASrc[p]}),
  peopleWaitingU = {},
  peopleWaitingD = {},
  processedPeople = {};

define
  \* Utilities.
  MaxS(S) == CHOOSE x \in S: \A y \in S: x >= y
  MinS(S) == CHOOSE x \in S: \A y \in S: x <= y
  MaxN(x, y) == IF y > x THEN y ELSE x
  MinN(x, y) == IF x < y THEN x ELSE y
  MaxT(T, Pred(_), defval) == LET track == [i \in 1..Len(T) |-> <<i, T[i]>>]
                                  filtered == SelectSeq(track, LAMBDA pair: Pred(pair[2]))
                              IN  IF Len(filtered) > 0
                                  THEN filtered[Len(filtered)][1]
                                  ELSE defval
  MinT(T, Pred(_), defval) == LET track == [i \in 1..Len(T) |-> <<i, T[i]>>]
                                  filtered == SelectSeq(track, LAMBDA pair: Pred(pair[2]))
                              IN  IF Len(filtered) > 0
                                  THEN filtered[1][1]
                                  ELSE defval
  set ++ x == set \union {x}
  set -- x == set \ {x}

  \* Checks.
  TypeInvariant == /\ eState \in {SSTILL, SCOMEUP, SGODOWN}
                   /\ \A p \in People: pASrc[p] /= pBDst[p]
                   /\ eCurrentLocation \in Stories
  OnlyGoSrcToDst == \A p \in People: \/ (pCCur[p] = 0)
                                     \/ ( /\ MinN(pASrc[p], pBDst[p]) <= pCCur[p]
                                          /\ pCCur[p] <= MaxN(pASrc[p], pBDst[p]) )

  ReachSrc == \A p \in People: <>(pCCur[p] = pASrc[p])
  ReachDst == \A p \in People: <>[](pCCur[p] = pBDst[p])
  ReachSrcBeforeDst == \A p \in People: ((pCCur[p] = pASrc[p]) ~> (pCCur[p] = pBDst[p]))
  AllPeopleAreProcessed == <>[](processedPeople = People)
  NoOneIsStuck == eState = SSTILL => eContainedPeople = {}
  QueuesAreClean == processedPeople = People => ( /\ eStopAtU = [x \in Stories |-> FALSE]
                                                  /\ eStopAtD = [x \in Stories |-> FALSE]
                                                  /\ (\A x \in Stories: eDstU[x] = {})
                                                  /\ (\A x \in Stories: eDstD[x] = {}) )
end define;

fair process ControllerProcess = "c"
variables
  pt \in People, src = 0, dst = 0,
  left = People,
begin LWhile:
  while left /= {} do
    LSelect:
    pt := CHOOSE x \in left: TRUE; left := left -- pt || src := pASrc[pt] || dst := pBDst[pt];
    LSetStop:
    if src < dst then
      \* Go up.
      eStopAtU[src] := TRUE || eDstU[src] := eDstU[src] ++ dst || peopleWaitingU := peopleWaitingU ++ pt;
    elsif src > dst then
      \* Go down.
      eStopAtD[src] := TRUE || eDstD[src] := eDstD[src] ++ dst || peopleWaitingD := peopleWaitingD ++ pt; 
    else
      assert FALSE;
    end if;
  end while;
end process;

fair process ElevatorProcess = "e"
variables
  peopleAllTmp = {},
  peopleComeIn = {},
  peopleGetOut = {},
  edge = 0,
begin LWhile:
  while TRUE do
    LProcess:
    if eState = SSTILL then
      if \E x \in Stories: eDstU[x] /= {} /\ eCurrentLocation <= x then
        \* Have up and can come up.
        eState := SCOMEUP;
      elsif \E x \in Stories: eDstD[x] /= {} /\ eCurrentLocation >= x then
        \* Have down and can go down.
        eState := SGODOWN;
      elsif \E x \in Stories: eDstU[x] /= {} then
        \* Have up but cannot come up, so turn around.
        eState := SGODOWN;
      elsif \E x \in Stories: eDstD[x] /= {} then
        \* Have down but cannot go down, so turn around.
        eState := SCOMEUP;
      else
        \* No up and down, stay absolutely still.
        eState := SSTILL;
      end if;
    elsif eState = SCOMEUP then
      LGoingUp:
      edge := MaxN(MaxT(eStopAtU, LAMBDA x: x, LowerInvalidLocation), MaxT(eStopAtD, LAMBDA x: x, LowerInvalidLocation));
      if eCurrentLocation <= edge then
        LGoingUpStep:
        if eStopAtU[eCurrentLocation] then
          \* Get in - out.
          peopleComeIn := {p \in peopleWaitingU: pASrc[p] = eCurrentLocation}; peopleWaitingU := peopleWaitingU \ peopleComeIn;

          peopleGetOut := {p \in eContainedPeople: pBDst[p] = eCurrentLocation};
          peopleAllTmp := eContainedPeople \union peopleComeIn;
          pCCur := [p \in peopleAllTmp |-> eCurrentLocation] @@ pCCur;
          eContainedPeople := peopleAllTmp \ peopleGetOut;
          processedPeople := processedPeople \union peopleGetOut;

          eStopAtU := (eCurrentLocation :> FALSE) @@ [p \in eDstU[eCurrentLocation] |-> TRUE] @@ eStopAtU;
          eDstU[eCurrentLocation] := {};
        end if;
        if eCurrentLocation < edge \/ edge < MaxN(MaxT(eStopAtU, LAMBDA x: x, LowerInvalidLocation), MaxT(eStopAtD, LAMBDA x: x, LowerInvalidLocation))then
          eCurrentLocation := eCurrentLocation + 1;
          goto LGoingUp;
        else
          goto LGoingUpDone;
        end if;
      end if;
      LGoingUpDone:
      eState := SSTILL;
    elsif eState = SGODOWN then
      LGoingDown:
      edge := MinN(MinT(eStopAtD, LAMBDA x: x, UpperInvalidLocation), MinT(eStopAtU, LAMBDA x: x, UpperInvalidLocation));
      if eCurrentLocation >= edge then
        LGoingDownStep:
        if eStopAtD[eCurrentLocation] then
          \* Get in - out.
          peopleComeIn := {p \in peopleWaitingD: pASrc[p] = eCurrentLocation}; peopleWaitingD := peopleWaitingD \ peopleComeIn;

          peopleGetOut := {p \in eContainedPeople: pBDst[p] = eCurrentLocation};
          peopleAllTmp := eContainedPeople \union peopleComeIn;
          pCCur := [p \in peopleAllTmp |-> eCurrentLocation] @@ pCCur;
          eContainedPeople := peopleAllTmp \ peopleGetOut;
          processedPeople := processedPeople \union peopleGetOut;

          eStopAtD := (eCurrentLocation :> FALSE) @@ [p \in eDstD[eCurrentLocation] |-> TRUE] @@ eStopAtD;
          eDstD[eCurrentLocation] := {};
        end if;
        if eCurrentLocation > edge \/ edge > MinN(MinT(eStopAtD, LAMBDA x: x, UpperInvalidLocation), MinT(eStopAtU, LAMBDA x: x, UpperInvalidLocation)) then
          eCurrentLocation := eCurrentLocation - 1;
          goto LGoingDown;
        else
          goto LGoingDownDone;
        end if;
      end if;
      LGoingDownDone:
      eState := SSTILL;            
    else
      assert FALSE;
    end if;
  end while;
end process;

end algorithm; *)
\* BEGIN TRANSLATION - the hash of the PCal code: PCal-532b2a131ea7eb613fb96c29b16ad1c7
\* Label LWhile of process ControllerProcess at line 70 col 3 changed to LWhile_
VARIABLES Stories, LowerInvalidLocation, UpperInvalidLocation, 
          eCurrentLocation, eState, eStopAtU, eStopAtD, eDstU, eDstD, 
          eContainedPeople, pCCur, pASrc, pBDst, peopleWaitingU, 
          peopleWaitingD, processedPeople, pc

(* define statement *)
MaxS(S) == CHOOSE x \in S: \A y \in S: x >= y
MinS(S) == CHOOSE x \in S: \A y \in S: x <= y
MaxN(x, y) == IF y > x THEN y ELSE x
MinN(x, y) == IF x < y THEN x ELSE y
MaxT(T, Pred(_), defval) == LET track == [i \in 1..Len(T) |-> <<i, T[i]>>]
                                filtered == SelectSeq(track, LAMBDA pair: Pred(pair[2]))
                            IN  IF Len(filtered) > 0
                                THEN filtered[Len(filtered)][1]
                                ELSE defval
MinT(T, Pred(_), defval) == LET track == [i \in 1..Len(T) |-> <<i, T[i]>>]
                                filtered == SelectSeq(track, LAMBDA pair: Pred(pair[2]))
                            IN  IF Len(filtered) > 0
                                THEN filtered[1][1]
                                ELSE defval
set ++ x == set \union {x}
set -- x == set \ {x}


TypeInvariant == /\ eState \in {SSTILL, SCOMEUP, SGODOWN}
                 /\ \A p \in People: pASrc[p] /= pBDst[p]
                 /\ eCurrentLocation \in Stories
OnlyGoSrcToDst == \A p \in People: \/ (pCCur[p] = 0)
                                   \/ ( /\ MinN(pASrc[p], pBDst[p]) <= pCCur[p]
                                        /\ pCCur[p] <= MaxN(pASrc[p], pBDst[p]) )

ReachSrc == \A p \in People: <>(pCCur[p] = pASrc[p])
ReachDst == \A p \in People: <>[](pCCur[p] = pBDst[p])
ReachSrcBeforeDst == \A p \in People: ((pCCur[p] = pASrc[p]) ~> (pCCur[p] = pBDst[p]))
AllPeopleAreProcessed == <>[](processedPeople = People)
NoOneIsStuck == eState = SSTILL => eContainedPeople = {}
QueuesAreClean == processedPeople = People => ( /\ eStopAtU = [x \in Stories |-> FALSE]
                                                /\ eStopAtD = [x \in Stories |-> FALSE]
                                                /\ (\A x \in Stories: eDstU[x] = {})
                                                /\ (\A x \in Stories: eDstD[x] = {}) )

VARIABLES pt, src, dst, left, peopleAllTmp, peopleComeIn, peopleGetOut, edge

vars == << Stories, LowerInvalidLocation, UpperInvalidLocation, 
           eCurrentLocation, eState, eStopAtU, eStopAtD, eDstU, eDstD, 
           eContainedPeople, pCCur, pASrc, pBDst, peopleWaitingU, 
           peopleWaitingD, processedPeople, pc, pt, src, dst, left, 
           peopleAllTmp, peopleComeIn, peopleGetOut, edge >>

ProcSet == {"c"} \cup {"e"}

Init == (* Global variables *)
        /\ Stories = 1..NumOfStories
        /\ LowerInvalidLocation = 1-1
        /\ UpperInvalidLocation = NumOfStories+1
        /\ eCurrentLocation \in Stories
        /\ eState = SSTILL
        /\ eStopAtU = [x \in Stories |-> FALSE]
        /\ eStopAtD = [x \in Stories |-> FALSE]
        /\ eDstU = [x \in Stories |-> {}]
        /\ eDstD = [x \in Stories |-> {}]
        /\ eContainedPeople = {}
        /\ pCCur = [p \in People |-> 0]
        /\ pASrc \in [People -> Stories]
        /\ pBDst \in ({value \in [People -> Stories]: \A p \in People: value[p] /= pASrc[p]})
        /\ peopleWaitingU = {}
        /\ peopleWaitingD = {}
        /\ processedPeople = {}
        (* Process ControllerProcess *)
        /\ pt \in People
        /\ src = 0
        /\ dst = 0
        /\ left = People
        (* Process ElevatorProcess *)
        /\ peopleAllTmp = {}
        /\ peopleComeIn = {}
        /\ peopleGetOut = {}
        /\ edge = 0
        /\ pc = [self \in ProcSet |-> CASE self = "c" -> "LWhile_"
                                        [] self = "e" -> "LWhile"]

LWhile_ == /\ pc["c"] = "LWhile_"
           /\ IF left /= {}
                 THEN /\ pc' = [pc EXCEPT !["c"] = "LSelect"]
                 ELSE /\ pc' = [pc EXCEPT !["c"] = "Done"]
           /\ UNCHANGED << Stories, LowerInvalidLocation, UpperInvalidLocation, 
                           eCurrentLocation, eState, eStopAtU, eStopAtD, eDstU, 
                           eDstD, eContainedPeople, pCCur, pASrc, pBDst, 
                           peopleWaitingU, peopleWaitingD, processedPeople, pt, 
                           src, dst, left, peopleAllTmp, peopleComeIn, 
                           peopleGetOut, edge >>

LSelect == /\ pc["c"] = "LSelect"
           /\ pt' = (CHOOSE x \in left: TRUE)
           /\ /\ dst' = pBDst[pt']
              /\ left' = left -- pt'
              /\ src' = pASrc[pt']
           /\ pc' = [pc EXCEPT !["c"] = "LSetStop"]
           /\ UNCHANGED << Stories, LowerInvalidLocation, UpperInvalidLocation, 
                           eCurrentLocation, eState, eStopAtU, eStopAtD, eDstU, 
                           eDstD, eContainedPeople, pCCur, pASrc, pBDst, 
                           peopleWaitingU, peopleWaitingD, processedPeople, 
                           peopleAllTmp, peopleComeIn, peopleGetOut, edge >>

LSetStop == /\ pc["c"] = "LSetStop"
            /\ IF src < dst
                  THEN /\ /\ eDstU' = [eDstU EXCEPT ![src] = eDstU[src] ++ dst]
                          /\ eStopAtU' = [eStopAtU EXCEPT ![src] = TRUE]
                          /\ peopleWaitingU' = peopleWaitingU ++ pt
                       /\ UNCHANGED << eStopAtD, eDstD, peopleWaitingD >>
                  ELSE /\ IF src > dst
                             THEN /\ /\ eDstD' = [eDstD EXCEPT ![src] = eDstD[src] ++ dst]
                                     /\ eStopAtD' = [eStopAtD EXCEPT ![src] = TRUE]
                                     /\ peopleWaitingD' = peopleWaitingD ++ pt
                             ELSE /\ Assert(FALSE, 
                                            "Failure of assertion at line 81, column 7.")
                                  /\ UNCHANGED << eStopAtD, eDstD, 
                                                  peopleWaitingD >>
                       /\ UNCHANGED << eStopAtU, eDstU, peopleWaitingU >>
            /\ pc' = [pc EXCEPT !["c"] = "LWhile_"]
            /\ UNCHANGED << Stories, LowerInvalidLocation, 
                            UpperInvalidLocation, eCurrentLocation, eState, 
                            eContainedPeople, pCCur, pASrc, pBDst, 
                            processedPeople, pt, src, dst, left, peopleAllTmp, 
                            peopleComeIn, peopleGetOut, edge >>

ControllerProcess == LWhile_ \/ LSelect \/ LSetStop

LWhile == /\ pc["e"] = "LWhile"
          /\ pc' = [pc EXCEPT !["e"] = "LProcess"]
          /\ UNCHANGED << Stories, LowerInvalidLocation, UpperInvalidLocation, 
                          eCurrentLocation, eState, eStopAtU, eStopAtD, eDstU, 
                          eDstD, eContainedPeople, pCCur, pASrc, pBDst, 
                          peopleWaitingU, peopleWaitingD, processedPeople, pt, 
                          src, dst, left, peopleAllTmp, peopleComeIn, 
                          peopleGetOut, edge >>

LProcess == /\ pc["e"] = "LProcess"
            /\ IF eState = SSTILL
                  THEN /\ IF \E x \in Stories: eDstU[x] /= {} /\ eCurrentLocation <= x
                             THEN /\ eState' = SCOMEUP
                             ELSE /\ IF \E x \in Stories: eDstD[x] /= {} /\ eCurrentLocation >= x
                                        THEN /\ eState' = SGODOWN
                                        ELSE /\ IF \E x \in Stories: eDstU[x] /= {}
                                                   THEN /\ eState' = SGODOWN
                                                   ELSE /\ IF \E x \in Stories: eDstD[x] /= {}
                                                              THEN /\ eState' = SCOMEUP
                                                              ELSE /\ eState' = SSTILL
                       /\ pc' = [pc EXCEPT !["e"] = "LWhile"]
                  ELSE /\ IF eState = SCOMEUP
                             THEN /\ pc' = [pc EXCEPT !["e"] = "LGoingUp"]
                             ELSE /\ IF eState = SGODOWN
                                        THEN /\ pc' = [pc EXCEPT !["e"] = "LGoingDown"]
                                        ELSE /\ Assert(FALSE, 
                                                       "Failure of assertion at line 167, column 7.")
                                             /\ pc' = [pc EXCEPT !["e"] = "LWhile"]
                       /\ UNCHANGED eState
            /\ UNCHANGED << Stories, LowerInvalidLocation, 
                            UpperInvalidLocation, eCurrentLocation, eStopAtU, 
                            eStopAtD, eDstU, eDstD, eContainedPeople, pCCur, 
                            pASrc, pBDst, peopleWaitingU, peopleWaitingD, 
                            processedPeople, pt, src, dst, left, peopleAllTmp, 
                            peopleComeIn, peopleGetOut, edge >>

LGoingUp == /\ pc["e"] = "LGoingUp"
            /\ edge' = MaxN(MaxT(eStopAtU, LAMBDA x: x, LowerInvalidLocation), MaxT(eStopAtD, LAMBDA x: x, LowerInvalidLocation))
            /\ IF eCurrentLocation <= edge'
                  THEN /\ pc' = [pc EXCEPT !["e"] = "LGoingUpStep"]
                  ELSE /\ pc' = [pc EXCEPT !["e"] = "LGoingUpDone"]
            /\ UNCHANGED << Stories, LowerInvalidLocation, 
                            UpperInvalidLocation, eCurrentLocation, eState, 
                            eStopAtU, eStopAtD, eDstU, eDstD, eContainedPeople, 
                            pCCur, pASrc, pBDst, peopleWaitingU, 
                            peopleWaitingD, processedPeople, pt, src, dst, 
                            left, peopleAllTmp, peopleComeIn, peopleGetOut >>

LGoingUpStep == /\ pc["e"] = "LGoingUpStep"
                /\ IF eStopAtU[eCurrentLocation]
                      THEN /\ peopleComeIn' = {p \in peopleWaitingU: pASrc[p] = eCurrentLocation}
                           /\ peopleWaitingU' = peopleWaitingU \ peopleComeIn'
                           /\ peopleGetOut' = {p \in eContainedPeople: pBDst[p] = eCurrentLocation}
                           /\ peopleAllTmp' = (eContainedPeople \union peopleComeIn')
                           /\ pCCur' = [p \in peopleAllTmp' |-> eCurrentLocation] @@ pCCur
                           /\ eContainedPeople' = peopleAllTmp' \ peopleGetOut'
                           /\ processedPeople' = (processedPeople \union peopleGetOut')
                           /\ eStopAtU' = (eCurrentLocation :> FALSE) @@ [p \in eDstU[eCurrentLocation] |-> TRUE] @@ eStopAtU
                           /\ eDstU' = [eDstU EXCEPT ![eCurrentLocation] = {}]
                      ELSE /\ TRUE
                           /\ UNCHANGED << eStopAtU, eDstU, eContainedPeople, 
                                           pCCur, peopleWaitingU, 
                                           processedPeople, peopleAllTmp, 
                                           peopleComeIn, peopleGetOut >>
                /\ IF eCurrentLocation < edge \/ edge < MaxN(MaxT(eStopAtU', LAMBDA x: x, LowerInvalidLocation), MaxT(eStopAtD, LAMBDA x: x, LowerInvalidLocation))
                      THEN /\ eCurrentLocation' = eCurrentLocation + 1
                           /\ pc' = [pc EXCEPT !["e"] = "LGoingUp"]
                      ELSE /\ pc' = [pc EXCEPT !["e"] = "LGoingUpDone"]
                           /\ UNCHANGED eCurrentLocation
                /\ UNCHANGED << Stories, LowerInvalidLocation, 
                                UpperInvalidLocation, eState, eStopAtD, eDstD, 
                                pASrc, pBDst, peopleWaitingD, pt, src, dst, 
                                left, edge >>

LGoingUpDone == /\ pc["e"] = "LGoingUpDone"
                /\ eState' = SSTILL
                /\ pc' = [pc EXCEPT !["e"] = "LWhile"]
                /\ UNCHANGED << Stories, LowerInvalidLocation, 
                                UpperInvalidLocation, eCurrentLocation, 
                                eStopAtU, eStopAtD, eDstU, eDstD, 
                                eContainedPeople, pCCur, pASrc, pBDst, 
                                peopleWaitingU, peopleWaitingD, 
                                processedPeople, pt, src, dst, left, 
                                peopleAllTmp, peopleComeIn, peopleGetOut, edge >>

LGoingDown == /\ pc["e"] = "LGoingDown"
              /\ edge' = MinN(MinT(eStopAtD, LAMBDA x: x, UpperInvalidLocation), MinT(eStopAtU, LAMBDA x: x, UpperInvalidLocation))
              /\ IF eCurrentLocation >= edge'
                    THEN /\ pc' = [pc EXCEPT !["e"] = "LGoingDownStep"]
                    ELSE /\ pc' = [pc EXCEPT !["e"] = "LGoingDownDone"]
              /\ UNCHANGED << Stories, LowerInvalidLocation, 
                              UpperInvalidLocation, eCurrentLocation, eState, 
                              eStopAtU, eStopAtD, eDstU, eDstD, 
                              eContainedPeople, pCCur, pASrc, pBDst, 
                              peopleWaitingU, peopleWaitingD, processedPeople, 
                              pt, src, dst, left, peopleAllTmp, peopleComeIn, 
                              peopleGetOut >>

LGoingDownStep == /\ pc["e"] = "LGoingDownStep"
                  /\ IF eStopAtD[eCurrentLocation]
                        THEN /\ peopleComeIn' = {p \in peopleWaitingD: pASrc[p] = eCurrentLocation}
                             /\ peopleWaitingD' = peopleWaitingD \ peopleComeIn'
                             /\ peopleGetOut' = {p \in eContainedPeople: pBDst[p] = eCurrentLocation}
                             /\ peopleAllTmp' = (eContainedPeople \union peopleComeIn')
                             /\ pCCur' = [p \in peopleAllTmp' |-> eCurrentLocation] @@ pCCur
                             /\ eContainedPeople' = peopleAllTmp' \ peopleGetOut'
                             /\ processedPeople' = (processedPeople \union peopleGetOut')
                             /\ eStopAtD' = (eCurrentLocation :> FALSE) @@ [p \in eDstD[eCurrentLocation] |-> TRUE] @@ eStopAtD
                             /\ eDstD' = [eDstD EXCEPT ![eCurrentLocation] = {}]
                        ELSE /\ TRUE
                             /\ UNCHANGED << eStopAtD, eDstD, eContainedPeople, 
                                             pCCur, peopleWaitingD, 
                                             processedPeople, peopleAllTmp, 
                                             peopleComeIn, peopleGetOut >>
                  /\ IF eCurrentLocation > edge \/ edge > MinN(MinT(eStopAtD', LAMBDA x: x, UpperInvalidLocation), MinT(eStopAtU, LAMBDA x: x, UpperInvalidLocation))
                        THEN /\ eCurrentLocation' = eCurrentLocation - 1
                             /\ pc' = [pc EXCEPT !["e"] = "LGoingDown"]
                        ELSE /\ pc' = [pc EXCEPT !["e"] = "LGoingDownDone"]
                             /\ UNCHANGED eCurrentLocation
                  /\ UNCHANGED << Stories, LowerInvalidLocation, 
                                  UpperInvalidLocation, eState, eStopAtU, 
                                  eDstU, pASrc, pBDst, peopleWaitingU, pt, src, 
                                  dst, left, edge >>

LGoingDownDone == /\ pc["e"] = "LGoingDownDone"
                  /\ eState' = SSTILL
                  /\ pc' = [pc EXCEPT !["e"] = "LWhile"]
                  /\ UNCHANGED << Stories, LowerInvalidLocation, 
                                  UpperInvalidLocation, eCurrentLocation, 
                                  eStopAtU, eStopAtD, eDstU, eDstD, 
                                  eContainedPeople, pCCur, pASrc, pBDst, 
                                  peopleWaitingU, peopleWaitingD, 
                                  processedPeople, pt, src, dst, left, 
                                  peopleAllTmp, peopleComeIn, peopleGetOut, 
                                  edge >>

ElevatorProcess == LWhile \/ LProcess \/ LGoingUp \/ LGoingUpStep
                      \/ LGoingUpDone \/ LGoingDown \/ LGoingDownStep
                      \/ LGoingDownDone

Next == ControllerProcess \/ ElevatorProcess

Spec == /\ Init /\ [][Next]_vars
        /\ WF_vars(ControllerProcess)
        /\ WF_vars(ElevatorProcess)

\* END TRANSLATION - the hash of the generated TLA code (remove to silence divergence warnings): TLA-4bd63313a305e6489768f6c90941dd39

=============================================================================
\* Modification History
\* Last modified Mon Nov 30 10:53:02 ICT 2020 by so61pi
\* Created Sun Nov 08 14:26:15 ICT 2020 by so61pi
