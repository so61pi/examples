========
 Images
========

Auto deduction
==============

.. image:: images/auto-deduction.svg

Class template implicit instantiation
=====================================

.. image:: images/class-template-implicit-instantiation.svg

Constructor/Destructor exception
================================

.. image:: images/ctor-dtor-exception.svg

Constructor/Destructor inheritance
==================================

.. image:: images/ctor-dtor-inheritance.svg

Error handling
==============

.. image:: images/error-handling.svg

Function name lookup
====================

.. image:: images/function-name-lookup.svg

Implicit special member-functions
=================================

.. image:: images/implicit-special-member-functions.svg

Initialization
==============

.. image:: images/initialization.svg

Memory ordering
===============

.. image:: images/memory-ordering.svg

new-delete expression
=====================

.. image:: images/new-delete-expression.svg

Object memory layout
====================

.. image:: images/object-memory-layout.svg

shared_ptr
==========

.. image:: images/shared_ptr.svg

Standard conversion sequences
=============================

.. image:: images/standard-conversion-sequences.svg

upper_bound
===========

.. image:: images/upper_bound.svg

Value categories
================

.. image:: images/value-categories.svg
