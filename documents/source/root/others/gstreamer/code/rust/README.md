Overview
========

This directory contains several examples of using `gstreamer` framework in Rust programming language. All the sources are in `src/bin`.

To run an example, we open a terminal in this directory, and run `cargo run --bin <example>`. For example, if I want to run `02-display.rs`, I will execute `cargo run --bin 02-display`.

Example `50-webrtc`
===================

`50-webrtc` is the most complex example in this set. It demonstrates the usage of `gstreamer` element `webrtcbin` to view a remote video stream from a web browser, based on WebRTC standard. It also lets users send commands from their web browsers to begin and end recording video to file.

Here are steps to run this example, assuming we already have some terminals opened at current directory

- In 1st terminal, we will run the web server. This web server is the signaling server in WebRTC standard.

```shell
cd src/bin/50-webrtc
./startup.sh
```

- In 2nd terminal, we will run the Rust code in `50-webrtc.rs`. We can run multiple instances of this.

```shell
cargo run --bin 50-webrtc
```

- Open a web browser, and open `src/bin/50-webrtc/index.html`.

  + Since we are running everything in a same machine, WebRTC implementations of web browsers will use mDNS instead of revealing local IP address. This will make the `webrtcbin` element fail to resolve the generated hostnames sent by browsers. In **Firefox**, we can disable mDNS by setting `media.peerconnection.ice.obfuscate_host_addresses` to `false` to make our test run faster. Please see following links for more information.

    * https://gitlab.freedesktop.org/libnice/libnice/-/issues/68
    * https://stackoverflow.com/a/61270926/2064646

- After having `index.html` file opened, we need to enter our username. Then we will see a list of connected recorders, which were created 2nd terminal. Click on a recorder and we can start video streaming.

  + Type `?` in the chatbox then send to see more help...
