use anyhow::Error;
use gst::prelude::*;

#[path = "../run.rs"]
mod run;

#[path = "../utility.rs"]
mod utility;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

/*

gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
! tee name=t \
t. ! queue ! autovideosink \
t. ! queue ! videoconvert ! x264enc tune=zerolatency ! mp4mux ! filesink location=/tmp/test-video.mp4

*/
fn entry() -> Result<(), Error> {
    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source"))?;
    let sink_view = gst::ElementFactory::make("autovideosink", Some("sink_view"))?;
    let sink_file = gst::ElementFactory::make("filesink", Some("sink_file"))?;
    let tee = gst::ElementFactory::make("tee", Some("tee"))?;
    let queue_view = gst::ElementFactory::make("queue", Some("queue_view"))?;
    let queue_file = gst::ElementFactory::make("queue", Some("queue_file"))?;
    let x264enc = gst::ElementFactory::make("x264enc", Some("x264enc"))?;
    let mp4mux = gst::ElementFactory::make("mp4mux", Some("mp4mux"))?;

    /* -------------------------------------------------------------------------- */
    /*                          Create an empty pipeline                          */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    /* -------------------------------------------------------------------------- */
    /*                          Add elements to pipeline                          */
    /* -------------------------------------------------------------------------- */
    pipeline.add_many(&[
        &source,
        &sink_view,
        &sink_file,
        &tee,
        &queue_view,
        &queue_file,
        &x264enc,
        &mp4mux,
    ])?;

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    source.link(&tee)?;
    queue_view.link(&sink_view)?;
    queue_file.link(&x264enc)?;
    x264enc.link(&mp4mux)?;
    mp4mux.link(&sink_file)?;

    let tee_pad_view = tee.request_pad_simple("src_%u").unwrap();
    let queue_view_pad = queue_view.static_pad("sink").unwrap();
    tee_pad_view.link(&queue_view_pad)?;

    let tee_pad_file = tee.request_pad_simple("src_%u").unwrap();
    let queue_file_pad = queue_file.static_pad("sink").unwrap();
    tee_pad_file.link(&queue_file_pad)?;

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "ball");
    x264enc.set_property_from_str("tune", "zerolatency");
    sink_file.set_property("location", "/tmp/test-video.mp4");

    // Without this, the output video will be slower than wall-clock. See `videotestsrc` source code or this link for more info
    // https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/clock.html?gi-language=c#live-source-elements
    source.set_property("is-live", true);

    /* -------------------------------------------------------------------------- */
    /*                                Play pipeline                               */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                                Handle Ctrl+C                               */
    /* -------------------------------------------------------------------------- */
    utility::handle_ctrlc(&pipeline)?;

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    bus.add_signal_watch();
    for msg in bus.iter_timed_filtered(
        gst::ClockTime::MAX,
        &[gst::MessageType::Eos, gst::MessageType::Error],
    ) {
        match msg.view() {
            gst::MessageView::Eos(_) => break,
            gst::MessageView::Error(err) => {
                eprintln!(
                    "Error from {:?}: {} ({:?})",
                    err.src(),
                    err.error(),
                    err.debug()
                );
                break;
            }
            _ => unreachable!(),
        }
    }

    /* -------------------------------------------------------------------------- */
    /*                                Stop playing                                */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Null)?;

    Ok(())
}
