use anyhow::Error;
use gst_rtsp_server::prelude::*;

#[path = "../run.rs"]
mod run;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

fn entry() -> Result<(), Error> {
    // Initialize gstreamer.
    gst::init()?;

    // Create glib::MainLoop that will be used to run our RTSP server.
    let main_loop = glib::MainLoop::new(None, false);

    // Create an RTSP server instance.
    let server = gst_rtsp_server::RTSPServer::new();

    // Create media factory.
    let factory = gst_rtsp_server::RTSPMediaFactory::new();
    let pipeline = "videotestsrc ! x264enc ! rtph264pay name=pay0 pt=96";
    factory.set_launch(pipeline);

    // Attach media factory to an endpoint.
    let mountpoints = server.mount_points();
    mountpoints.unwrap().add_factory("/media", &factory);

    // Start server.
    server.set_service("8554"); // Set listening port.
    let id = server.attach(None).unwrap(); // Attach server into the MainContext of MainLoop.

    // Run MainLoop which in turn start listening on RTSP requests.
    main_loop.run();

    // Remove RTSP server from glib::MainLoop.
    id.remove();

    Ok(())
}
