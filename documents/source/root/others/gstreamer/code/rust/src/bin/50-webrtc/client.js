"use strict";

// Get our hostname

let myHostname = window.location.hostname ? window.location.hostname : "127.0.0.1";
log("APP", "Hostname: " + myHostname);

// WebSocket chat/signaling channel variables.

let wsConnection = null;

let myUsername = null;
let selectedRecorder = null;    // To store username of remote recorder
let myPC = null;                // RTCPeerConnection

/* -------------------------------------------------------------------------- */
/*                                  Utilities                                 */
/* -------------------------------------------------------------------------- */

// Output logging information to console.
function log(tag, text) {
  let time = new Date();
  console.log("[" + time.toLocaleTimeString() + "][" + tag + "] " + text);
}

// Output an error message to console.
function logError(tag, text) {
  let time = new Date();
  console.trace("[" + time.toLocaleTimeString() + "][" + tag + "] " + text);
}

// Handles reporting errors. Currently, we just dump stuff to console but
// in a real-world application, an appropriate (and user-friendly)
// error message should be displayed.
function reportError(errMessage) {
  logError("WEBRTC", `Error ${errMessage.name}: ${errMessage.message}`);
}

// Helper function to create websocket json object.
function makeJsonMsg(dst, type, content) {
  return {
    dst: dst,
    date: Date.now(),
    data: {
      type: type,
      content: content,
    }
  };
}

// Send a JavaScript object by converting it to JSON and sending
// it as a message on the WebSocket connection.
function sendToServer(msg) {
  let msgJSON = JSON.stringify(msg);

  log("WS", "Sending '" + msg.data.type + "' message: " + msgJSON);
  wsConnection.send(msgJSON);
}

// Send our name to server.
function setUsername() {
  myUsername = document.getElementById("name").value;
  sendToServer(makeJsonMsg("server", "username", { name: myUsername }));
}

/* -------------------------------------------------------------------------- */
/*                           Main WebSocket Function                          */
/* -------------------------------------------------------------------------- */

// Open and configure the connection to the WebSocket server.
function connect() {
  if (document.getElementById("name").value.startsWith("recorder")) {
    alert("Username cannot start with 'recorder'");
    return;
  }
  document.getElementById("connect-button").disabled = true;

  let serverUrl = "ws://" + myHostname + ":8888";

  log("APP", `Connecting to server: ${serverUrl}`);
  wsConnection = new WebSocket(serverUrl, "json");

  // Handlers...

  wsConnection.onopen = function(evt) {
    setUsername();
  };

  wsConnection.onerror = function(evt) {
    console.dir(evt);
  }

  wsConnection.onmessage = function(evt) {
    let msg = JSON.parse(evt.data);
    log("APP", "Message received: ");
    console.dir(msg);

    let time = new Date(msg.date);
    let timeStr = time.toLocaleTimeString();

    let msgContent = msg.data.content;
    let text = "";
    switch (msg.data.type) {
      case "username-ok":
        document.getElementById("text").disabled = false;
        document.getElementById("send").disabled = false;
        break;

      case "reject-username":
        myUsername = msgContent.name;
        text = "<b>Your username has been set to <em>" + myUsername +
          "</em> because the name you chose is in use.</b><br>";
        document.getElementById("text").disabled = false;
        document.getElementById("send").disabled = false;
        break;

      case "message":
        text = "(" + timeStr + ") <b>" + msg.src + "</b>: " + msgContent.text + "<br>";
        break;

      case "userlist":
        handleUserlistMsg(msgContent);
        break;

      // Signaling messages: these messages are used to trade WebRTC
      // signaling information during negotiations leading up to a video
      // call.

      case "video-offer":
        if (msg.src === selectedRecorder) {
          handleVideoOfferMsg(msgContent);
        }
        break;

      case "new-ice-candidate": // A new ICE candidate has been received
        if (msg.src === selectedRecorder) {
          handleNewICECandidateMsg(msgContent);
        }
        break;

      // Unknown message; output to console for debugging.

      default:
        logError("APP", "Unknown message received:");
        logError("APP", msg);
    }

    // If there's text to insert into the chat buffer, do so now, then
    // scroll the chat panel so that the new text is visible.

    if (text.length) {
      let chatBox = document.querySelector(".chatbox");
      chatBox.innerHTML += text;
      chatBox.scrollTop = chatBox.scrollHeight - chatBox.clientHeight;
    }
  };
}

/* -------------------------------------------------------------------------- */
/*                   User's Interactions Handling Functions                   */
/* -------------------------------------------------------------------------- */

// Handles a click on the Send button (or pressing return/enter) by
// building a "message" object and sending it to the server.
function handleSendButton() {
  let msg = makeJsonMsg("broadcast", "message", {
    text: document.getElementById("text").value,
  });
  sendToServer(msg);
  document.getElementById("text").value = "";
}

// Handler for keyboard events. This is used to intercept the return and
// enter keys so that we can call send() to transmit the entered text
// to the server.
function handleKey(evt) {
  if (evt.keyCode === 13 || evt.keyCode === 14) {
    if (!document.getElementById("send").disabled) {
      handleSendButton();
    }
  }
}

// Send a message to recorder, asking it to begin recording.
function handleBeginRecording() {
  if (selectedRecorder) {
    sendToServer(makeJsonMsg(selectedRecorder, "message", {
      text: "/begin-recording",
    }));
  }
}

// Send a message to recorder, asking it to end current recording.
function handleEndRecording() {
  if (selectedRecorder) {
    sendToServer(makeJsonMsg(selectedRecorder, "message", {
      text: "/end-recording",
    }));
  }
}

// Send a message to recorder, asking it to begin WebRTC.
function handleBeginVideoStream() {
  if (selectedRecorder) {
    log("APP", "Inviting recorder " + selectedRecorder);

    sendToServer(makeJsonMsg(selectedRecorder, "message", {
      text: "/begin-webrtc",
    }));
  }
}

// Close the RTCPeerConnection and reset variables so that the user can
// select another recorder and start new video stream. This is called both
// when the user hangs up, the recorder hangs up, or if a connection
// failure is detected.
function closeVideoStream() {
  log("APP", "Closing the call");

  // Close the RTCPeerConnection

  if (myPC) {
    log("APP", "---> Closing the peer connection");

    // Disconnect all our event listeners; we don't want stray events
    // to interfere with the hangup while it's ongoing.
    myPC.ontrack = null;
    myPC.onnicecandidate = null;
    myPC.oniceconnectionstatechange = null;
    myPC.onsignalingstatechange = null;
    myPC.onicegatheringstatechange = null;
    myPC.onnotificationneeded = null;

    // Stop all transceivers on the connection
    myPC.getTransceivers().forEach(transceiver => transceiver.stop());

    // Close the peer connection
    myPC.close();
    myPC = null;
  }

  // Disable the hangup button
  document.getElementById("hangup-button").disabled = true;
}

// Hang up the call by closing our end of the connection, then
// sending an end-webrtc message to recorder to let it clean up
// its side.
function hangUpCall() {
  closeVideoStream();

  sendToServer(makeJsonMsg(selectedRecorder, "message", {
    text: "/end-webrtc",
  }));
}

// Select a recorder for further actions.
async function recorderClicked(evt) {
  log("APP", "Select a recorder");
  let clickedRecorder = evt.target.textContent;

  if (myPC) {
    if (clickedRecorder !== selectedRecorder) {
      alert("You cannot select a new recorder because you are having a video stream!");
    }
  } else {
    // Record the recorder for future reference
    selectedRecorder = clickedRecorder;
    document.getElementById("selected-recorder").innerHTML = `Recorder <b>${selectedRecorder}</b> is selected`;
    document.getElementById("cmd-controls").style.display = "block";
  }
}

/* -------------------------------------------------------------------------- */
/*                    WebSocket Message Handling Functions                    */
/* -------------------------------------------------------------------------- */

// Given a message containing a list of users (humans & recorders).
function handleUserlistMsg(msgContent) {
  let recorderListElem = document.querySelector(".recorderlistbox");

  // Remove all current list members. We could do this smarter,
  // by adding and updating recorders instead of rebuilding from
  // scratch but this will do for this sample.
  while (recorderListElem.firstChild) {
    recorderListElem.removeChild(recorderListElem.firstChild);
  }

  // Add member names from the received list.
  msgContent.recorders.forEach(function(recorder) {
    let item = document.createElement("li");
    item.appendChild(document.createTextNode(recorder));
    item.addEventListener("click", recorderClicked, false);

    recorderListElem.appendChild(item);
  });
}


// Accept an offer to video chat. We configure our local settings,
// create our RTCPeerConnection, then create and send an answer to the caller.
async function handleVideoOfferMsg(msgContent) {
  // Create an RTCPeerConnection to be linked to the caller.
  log("WEBRTC", "Received video chat offer from " + selectedRecorder);
  createPeerConnection();

  // We need to set the remote description to the received SDP offer
  // so that our local WebRTC layer knows how to talk to the caller.
  let desc = new RTCSessionDescription(msgContent.sdp);

  // If the connection isn't stable yet, wait for it...
  if (myPC.signalingState != "stable") {
    log("WEBRTC", "  - But the signaling state isn't stable, so triggering rollback");

    // Set the local and remove descriptions for rollback; don't proceed
    // until both return.
    await Promise.all([
      myPC.setLocalDescription({type: "rollback"}),
      myPC.setRemoteDescription(desc)
    ]);
    return;
  } else {
    log("WEBRTC", "  - Setting remote description");
    await myPC.setRemoteDescription(desc);
  }

  log("WEBRTC", "---> Creating and sending answer to caller");

  await myPC.setLocalDescription(await myPC.createAnswer());

  sendToServer(makeJsonMsg(selectedRecorder, "video-answer", {
    sdp: myPC.localDescription
  }));
}

// A new ICE candidate has been received from the other peer. Call
// RTCPeerConnection.addIceCandidate() to send it along to the
// local ICE framework.
async function handleNewICECandidateMsg(msgContent) {
  let candidate = new RTCIceCandidate(msgContent.candidate);

  log("WEBRTC", "Adding received ICE candidate: " + JSON.stringify(candidate));
  try {
    await myPC.addIceCandidate(candidate)
  } catch(err) {
    reportError(err);
  }
}

/* -------------------------------------------------------------------------- */
/*                          WebRTC-Related Functions                          */
/* -------------------------------------------------------------------------- */

// Create the RTCPeerConnection which knows how to talk to our
// selected STUN/TURN server. Then we configure event handlers
// to get needed notifications on the call.
async function createPeerConnection() {
  log("APP", "Setting up a WebRTC connection...");

  // Create an RTCPeerConnection which knows to use our chosen STUN server.
  myPC = new RTCPeerConnection({
    iceServers: [
      {
        urls: "stun:stun.l.google.com:19302"
      }
    ]
  });

  // Set up event handlers for the ICE negotiation process.
  myPC.onicecandidate = handleICECandidateEvent;
  myPC.oniceconnectionstatechange = handleICEConnectionStateChangeEvent;
  myPC.onicegatheringstatechange = handleICEGatheringStateChangeEvent;
  myPC.onsignalingstatechange = handleSignalingStateChangeEvent;
  myPC.onnegotiationneeded = handleNegotiationNeededEvent;
  myPC.ontrack = handleTrackEvent;
}

// Called by the WebRTC layer to let us know when it's time to
// begin, resume, or restart ICE negotiation.
async function handleNegotiationNeededEvent() {
  log("WEBRTC", "Negotiation needed");

  try {
    log("WEBRTC", "---> Creating offer");
    const offer = await myPC.createOffer();

    // If the connection hasn't yet achieved the "stable" state,
    // return to the caller. Another negotiationneeded event will
    // be fired when the state stabilizes.
    if (myPC.signalingState != "stable") {
      log("WEBRC", "     -- The connection isn't stable yet; postponing...")
      return;
    }

    // Establish the offer as the local peer's current description.
    log("WEBRTC", "---> Setting local description to the offer");
    await myPC.setLocalDescription(offer);
  } catch(err) {
    log("WEBRTC", "The following error occurred while handling the negotiationneeded event:");
    reportError(err);
  };
}

// Called by the WebRTC layer when events occur on the media tracks
// on our WebRTC call. This includes when streams are added to and
// removed from the call.
//
// track events include the following fields:
//
// RTCRtpReceiver       receiver
// MediaStreamTrack     track
// MediaStream[]        streams
// RTCRtpTransceiver    transceiver
//
// In our case, we're just taking the first stream found and attaching
// it to the <video> element for incoming media.
function handleTrackEvent(event) {
  log("WEBRTC", "Track event");
  console.dir(event);
  document.getElementById("received-video").srcObject = event.streams[0];
  document.getElementById("hangup-button").disabled = false;
}

// Handles |icecandidate| events by forwarding the specified
// ICE candidate (created by our local ICE agent) to the other
// peer through the signaling server.
function handleICECandidateEvent(event) {
  if (event.candidate) {
    log("WEBRTC", "Outgoing ICE candidate: " + event.candidate.candidate);

    sendToServer(makeJsonMsg(selectedRecorder, "new-ice-candidate", {
      candidate: event.candidate
    }));
  }
}

// Handle |iceconnectionstatechange| events. This will detect
// when the ICE connection is closed, failed, or disconnected.
//
// This is called when the state of the ICE agent changes.
function handleICEConnectionStateChangeEvent(event) {
  log("WEBRTC", "ICE connection state changed to " + myPC.iceConnectionState);

  switch(myPC.iceConnectionState) {
    case "closed":
    case "failed":
    case "disconnected":
      closeVideoStream();
      break;
  }
}

// Set up a |signalingstatechange| event handler. This will detect when
// the signaling connection is closed.
//
// NOTE: This will actually move to the new RTCPeerConnectionState enum
// returned in the property RTCPeerConnection.connectionState when
// browsers catch up with the latest version of the specification!
function handleSignalingStateChangeEvent(event) {
  log("WEBRTC", "Signaling state changed to: " + myPC.signalingState);
  switch(myPC.signalingState) {
    case "closed":
      closeVideoStream();
      break;
  }
}

// Handle the |icegatheringstatechange| event. This lets us know what the
// ICE engine is currently working on: "new" means no networking has happened
// yet, "gathering" means the ICE engine is currently gathering candidates,
// and "complete" means gathering is complete. Note that the engine can
// alternate between "gathering" and "complete" repeatedly as needs and
// circumstances change.
//
// We don't need to do anything when this happens, but we log it to the
// console so you can see what's going on when playing with the sample.
function handleICEGatheringStateChangeEvent(event) {
  log("WEBRTC", "ICE gathering state changed to: " + myPC.iceGatheringState);
}
