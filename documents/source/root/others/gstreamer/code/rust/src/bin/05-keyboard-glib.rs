use std::thread;
use std::time;

use anyhow::Error;
use gst::prelude::*;
use termion::event::Key;
use termion::input::TermRead;

#[path = "../run.rs"]
mod run;

#[path = "../utility.rs"]
mod utility;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

/*

gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
! x264enc ! mp4mux ! filesink location=/tmp/test-video.mp4

*/
fn entry() -> Result<(), Error> {
    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                               Create pipeline                              */
    /* -------------------------------------------------------------------------- */
    let pipeline = create_pipeline()?;

    /* -------------------------------------------------------------------------- */
    /*                                Play pipeline                               */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                       Create GLib channel & main loop                      */
    /* -------------------------------------------------------------------------- */
    let ((cmd_tx, cmd_rx), main_loop) = utility::glib_create_main_with_channel();

    /* -------------------------------------------------------------------------- */
    /*                        Handle command from terminal                        */
    /* -------------------------------------------------------------------------- */
    cmd_rx.attach(Some(&main_loop.context()), {
        let pipeline_weak = pipeline.downgrade();
        move |command: Command| {
            let pipeline = match pipeline_weak.upgrade() {
                Some(pipeline) => pipeline,
                None => return glib::Continue(true),
            };
            match command {
                Command::Quit => {
                    pipeline.send_event(gst::event::Eos::new());
                }
            }
            glib::Continue(true)
        }
    });

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    bus.add_watch({
        let main_loop = main_loop.clone();
        move |_, msg| {
            match msg.view() {
                gst::MessageView::Eos(_) => main_loop.quit(),
                gst::MessageView::Error(err) => {
                    eprintln!(
                        "Error from {:?}: {} ({:?})",
                        err.src(),
                        err.error(),
                        err.debug()
                    );
                    main_loop.quit();
                }
                _ => (),
            }
            Continue(true)
        }
    })?;

    /* -------------------------------------------------------------------------- */
    /*                                Handle Ctrl+C                               */
    /* -------------------------------------------------------------------------- */
    utility::handle_ctrlc(&pipeline)?;

    /* -------------------------------------------------------------------------- */
    /*                               Handle keyboard                              */
    /* -------------------------------------------------------------------------- */
    thread::spawn(move || handle_keyboard(cmd_tx));

    /* -------------------------------------------------------------------------- */
    /*                               Start main loop                              */
    /* -------------------------------------------------------------------------- */
    main_loop.run();

    /* -------------------------------------------------------------------------- */
    /*                                Stop playing                                */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Null)?;

    Ok(())
}

fn create_pipeline() -> Result<gst::Pipeline, Error> {
    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source"))?;
    let sink = gst::ElementFactory::make("filesink", Some("sink"))?;
    let clockoverlay = gst::ElementFactory::make("clockoverlay", Some("clockoverlay"))?;
    let x264enc = gst::ElementFactory::make("x264enc", Some("x264enc"))?;
    let mp4mux = gst::ElementFactory::make("mp4mux", Some("mp4mux"))?;

    /* -------------------------------------------------------------------------- */
    /*                          Create an empty pipeline                          */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    /* -------------------------------------------------------------------------- */
    /*                          Add elements to pipeline                          */
    /* -------------------------------------------------------------------------- */
    pipeline.add_many(&[&source, &sink, &clockoverlay, &x264enc, &mp4mux])?;

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    source.link(&clockoverlay)?;
    clockoverlay.link(&x264enc)?;
    x264enc.link(&mp4mux)?;
    mp4mux.link(&sink)?;

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "ball");
    sink.set_property("location", "/tmp/test-video.mp4");
    clockoverlay.set_property("time-format", "%F %T");
    clockoverlay.set_property_from_str("valignment", "top");
    clockoverlay.set_property_from_str("halignment", "right");
    clockoverlay.set_property("font-desc", "Sans, 20");

    // Without this, the output video will be slower than wall-clock. See `videotestsrc` source code or this link for more info
    // https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/clock.html?gi-language=c#live-source-elements
    source.set_property("is-live", true);

    Ok(pipeline)
}

#[derive(Clone, Copy, PartialEq)]
enum Command {
    Quit,
}

// This is where we get the user input from the terminal.
fn handle_keyboard(cmd_tx: glib::Sender<Command>) {
    let mut stdin = termion::async_stdin().keys();

    loop {
        if let Some(Ok(input)) = stdin.next() {
            let command = match input {
                Key::Char('q') | Key::Char('Q') => Command::Quit,
                Key::Ctrl('c') | Key::Ctrl('C') => Command::Quit,
                _ => continue,
            };
            cmd_tx
                .send(command)
                .expect("failed to send data through channel");

            if command == Command::Quit {
                break;
            }
        }
        thread::sleep(time::Duration::from_millis(50));
    }
}
