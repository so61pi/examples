use anyhow::Error;
use gst::prelude::*;
use std::thread;
use std::time;
use termion::event::Key;
use termion::input::TermRead;

#[path = "../run.rs"]
mod run;

#[path = "../utility.rs"]
mod utility;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

/*

gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
! x264enc ! mp4mux ! filesink location=/tmp/test-video.mp4

*/
fn entry() -> Result<(), Error> {
    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                               Create pipeline                              */
    /* -------------------------------------------------------------------------- */
    let pipeline = create_pipeline()?;

    /* -------------------------------------------------------------------------- */
    /*                                Play pipeline                               */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                                Handle Ctrl+C                               */
    /* -------------------------------------------------------------------------- */
    utility::handle_ctrlc(&pipeline)?;

    /* -------------------------------------------------------------------------- */
    /*                               Handle keyboard                              */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    thread::spawn({
        let bus = bus.clone();
        move || handle_keyboard(&bus)
    });

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    for msg in bus.iter_timed_filtered(
        gst::ClockTime::MAX,
        &[
            gst::MessageType::Eos,
            gst::MessageType::Error,
            gst::MessageType::Application,
        ],
    ) {
        match msg.view() {
            gst::MessageView::Eos(_) => break,
            gst::MessageView::Error(err) => {
                eprintln!(
                    "Error from {:?}: {} ({:?})",
                    err.src(),
                    err.error(),
                    err.debug()
                );
                break;
            }
            gst::MessageView::Application(msg) => {
                if msg.structure().map(|s| s.name()) == Some("quit") {
                    pipeline.send_event(gst::event::Eos::new());
                }
            }
            _ => unreachable!(),
        }
    }

    /* -------------------------------------------------------------------------- */
    /*                                Stop playing                                */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Null)?;

    Ok(())
}

fn create_pipeline() -> Result<gst::Pipeline, Error> {
    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source"))?;
    let sink = gst::ElementFactory::make("filesink", Some("sink"))?;
    let clockoverlay = gst::ElementFactory::make("clockoverlay", Some("clockoverlay"))?;
    let x264enc = gst::ElementFactory::make("x264enc", Some("x264enc"))?;
    let mp4mux = gst::ElementFactory::make("mp4mux", Some("mp4mux"))?;

    /* -------------------------------------------------------------------------- */
    /*                          Create an empty pipeline                          */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    /* -------------------------------------------------------------------------- */
    /*                          Add elements to pipeline                          */
    /* -------------------------------------------------------------------------- */
    pipeline.add_many(&[&source, &sink, &clockoverlay, &x264enc, &mp4mux])?;

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    source.link(&clockoverlay)?;
    clockoverlay.link(&x264enc)?;
    x264enc.link(&mp4mux)?;
    mp4mux.link(&sink)?;

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "ball");
    sink.set_property("location", "/tmp/test-video.mp4");
    clockoverlay.set_property("time-format", "%F %T");
    clockoverlay.set_property_from_str("valignment", "top");
    clockoverlay.set_property_from_str("halignment", "right");
    clockoverlay.set_property("font-desc", "Sans, 20");

    // Without this, the output video will be slower than wall-clock. See `videotestsrc` source code or this link for more info
    // https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/clock.html?gi-language=c#live-source-elements
    source.set_property("is-live", true);

    Ok(pipeline)
}

// This is where we get the user input from the terminal.
fn handle_keyboard(bus: &gst::Bus) {
    let mut stdin = termion::async_stdin().keys();

    loop {
        if let Some(Ok(input)) = stdin.next() {
            let msg = gst::message::Application::new(gst::Structure::new_empty("quit"));
            match input {
                Key::Char('q') | Key::Char('Q') => {
                    bus.post(&msg).expect("failed to send message");
                }
                Key::Ctrl('c') | Key::Ctrl('C') => {
                    bus.post(&msg).expect("failed to send message");
                }
                _ => continue,
            }
        }
        thread::sleep(time::Duration::from_millis(50));
    }
}
