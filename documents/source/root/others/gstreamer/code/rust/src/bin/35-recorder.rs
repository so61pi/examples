use anyhow::Error;
use gst::prelude::*;
use gst::PadExtManual;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use std::thread;

#[path = "../run.rs"]
mod run;

#[path = "../utility.rs"]
mod utility;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

fn entry() -> Result<(), Error> {
    println!("--- {} ---", chrono::Local::now());
    println!("    Press 'b' to begin recording, 'e' to end recording, 'q' to quit.");
    println!("    The output file is at /tmp/test-video-*.mp4");

    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                               Create pipeline                              */
    /* -------------------------------------------------------------------------- */
    let (pipeline, textoverlay, queue, x264enc, mp4mux, sink, pad, padid) = create_pipeline()?;
    let mut padid = Some(padid);

    /* -------------------------------------------------------------------------- */
    /*                                Play pipeline                               */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                                Handle Ctrl+C                               */
    /* -------------------------------------------------------------------------- */
    // Do not handle Ctrl+C so we can quit whenever with Ctrl+C.
    // Otherwise we need to use another tool for that, and I am lazy.
    // utility::handle_ctrlc(&pipeline)?;

    /* -------------------------------------------------------------------------- */
    /*                               Handle keyboard                              */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    thread::spawn({
        let bus = bus.clone();
        move || utility::handle_keyboard(&bus)
    });

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    let mut state = State::PRERECORD;
    let mut fcount = 1u32;
    println!("--- {} ---", chrono::Local::now());
    for msg in bus.iter_timed(gst::ClockTime::MAX) {
        // eprintln!(
        //     "    {}:{:?} - PipelineState {:?}",
        //     line!(),
        //     msg,
        //     pipeline.current_state()
        // );
        match msg.view() {
            gst::MessageView::Eos(_) => {
                if state == State::QUITTING {
                    break;
                }
            }
            gst::MessageView::Error(err) => {
                eprintln!(
                    "Error from {:?}: {} ({:?})",
                    err.src(),
                    err.error(),
                    err.debug()
                );
                break;
            }
            gst::MessageView::Application(msg) => match msg.structure().map(|s| s.name()) {
                Some("quit") => {
                    println!("--- {} ---", chrono::Local::now());
                    println!("    Quitting...");

                    state = State::QUITTING;
                    if pipeline.current_state() == gst::State::Playing {
                        pipeline.send_event(gst::event::Eos::new());
                    } else {
                        eprintln!("Pipeline State: {:?}", pipeline.current_state());
                        // todo!("Handle what left in pipeline?");
                        break;
                    }
                }
                Some("begin-recording") => {
                    println!("--- {} ---", chrono::Local::now());
                    println!("    Begin Recording");

                    if state != State::PRERECORD {
                        println!(
                            "    Current state is {:?}, so we cannot start recording",
                            state
                        );
                        continue;
                    }

                    // Change text on screen.
                    textoverlay.set_property("text", "RECORDING");
                    textoverlay.set_property("color", 0xFF_FF_FF_00u32);

                    // Make sure we only create video file when begin recording,
                    // so there are no redudant files in the end.
                    let filename = format!("/tmp/test-video-{}.mp4", fcount);
                    println!("    Video File: {}", &filename);
                    sink.set_state(gst::State::Null)?;
                    sink.set_property("location", &filename);
                    sink.set_state(gst::State::Playing).unwrap();
                    fcount += 1;

                    // Only drop first buffer when accumulated data is already equal to (or greater than) 10s.
                    let time = queue.property::<u64>("current-level-time");
                    println!("    Queue's current-level-time is {}", time);
                    if time >= 10_000_000_000u64 {
                        // Install a new data probe, which is to drop the stuck buffer.
                        pad.add_probe(gst::PadProbeType::BUFFER, {
                            let counter = Arc::new(AtomicUsize::new(0));
                            move |_pad, _probe_info| {
                                // When we remove the blocking probe (below), the first buffer will go here
                                // So we need to drop it. Then we remove this pad probe on the next buffer.
                                if counter.fetch_add(1, Ordering::SeqCst) == 0 {
                                    gst::PadProbeReturn::Drop
                                } else {
                                    gst::PadProbeReturn::Remove
                                }
                            }
                        })
                        .unwrap();
                    }

                    // Remove blocking probe so buffers can flow.
                    pad.remove_probe(padid.take().unwrap());
                    state = State::RECORDING;
                }
                Some("end-recording") => {
                    println!("--- {} ---", chrono::Local::now());
                    println!("    End Recording");

                    if state != State::RECORDING {
                        println!(
                            "    Current state is {:?}, so we cannot end recording",
                            state
                        );
                        continue;
                    }

                    padid = Some(
                        pad.add_probe(
                            gst::PadProbeType::BLOCK | gst::PadProbeType::BUFFER,
                            pad_probe_block,
                        )
                        .unwrap(),
                    );

                    // Change text on screen.
                    textoverlay.set_property("text", "PRERECORD");
                    textoverlay.set_property("color", 0x77_00_FF_00u32);

                    // Reset prerecord time.
                    queue.set_property("max-size-time", 10_000_000_000u64);

                    // Send EOS to x264enc's src pad, to make sure it completes its work and pushes out all left data.
                    // The EOS will be forwarded to mp4mux and then filesink. After filesink processes it, the video
                    // file is saved safely. Filesink also sends a new EOS message to its bin, which in this case is the
                    // pipeline (and the seqnum of this EOS message is out of our control here...).
                    let peer = pad.peer().unwrap();
                    peer.send_event(gst::event::Eos::new());

                    state = State::ENDING;
                }
                _ => (),
            },
            gst::MessageView::Element(m) => {
                // Handle EOS message from filesink, make sure every bit is processed.
                if state == State::ENDING {
                    let s = m.structure().unwrap();
                    if s.name() == "GstBinForwarded" {
                        let msg: gst::Message = s.get("message").unwrap();
                        if msg.type_() == gst::MessageType::Eos {
                            x264enc.set_state(gst::State::Null).unwrap();
                            mp4mux.set_state(gst::State::Null).unwrap();

                            // The video file is closed here.
                            sink.set_state(gst::State::Null).unwrap();

                            sink.set_property("location", "/dev/null");

                            // Must set filesink back to playing, otherwise the pipeline will stuck when quit.
                            // (Maybe because when sink is at NULL, it cannot process EOS sent from pipeline,
                            // and this makes pipeline waits forever.)
                            sink.set_state(gst::State::Playing).unwrap();
                            mp4mux.set_state(gst::State::Playing).unwrap();
                            x264enc.set_state(gst::State::Playing).unwrap();

                            state = State::PRERECORD;
                        }
                    }
                }
            }
            _ => (),
        }
    }

    // - Stop playing.
    println!("--- {:?} ---", pipeline.set_state(gst::State::Null)?);

    Ok(())
}

fn create_pipeline() -> Result<
    (
        gst::Pipeline,
        gst::Element,
        gst::Element,
        gst::Element,
        gst::Element,
        gst::Element,
        gst::Pad,
        gst::PadProbeId,
    ),
    Error,
> {
    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source"))?;
    let textoverlay = gst::ElementFactory::make("textoverlay", Some("textoverlay"))?;
    let clockoverlay = gst::ElementFactory::make("clockoverlay", Some("clockoverlay"))?;
    let queue = gst::ElementFactory::make("queue", Some("queue"))?;
    let x264enc = gst::ElementFactory::make("x264enc", Some("x264enc"))?;
    let mp4mux = gst::ElementFactory::make("mp4mux", Some("mp4mux"))?;
    let sink = gst::ElementFactory::make("filesink", Some("sink"))?;

    /* -------------------------------------------------------------------------- */
    /*                          Create an empty pipeline                          */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    /* -------------------------------------------------------------------------- */
    /*                          Add elements to pipeline                          */
    /* -------------------------------------------------------------------------- */
    pipeline.add_many(&[
        &source,
        &textoverlay,
        &clockoverlay,
        &queue,
        &x264enc,
        &mp4mux,
        &sink,
    ])?;

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    gst::Element::link_many(&[
        &source,
        &textoverlay,
        &clockoverlay,
        &queue,
        &x264enc,
        &mp4mux,
        &sink,
    ])?;

    /* -------------------------------------------------------------------------- */
    /*                             Set up block probe                             */
    /* -------------------------------------------------------------------------- */
    let pad = queue.static_pad("src").unwrap();
    let padid = pad
        .add_probe(
            gst::PadProbeType::BLOCK | gst::PadProbeType::BUFFER,
            pad_probe_block,
        )
        .unwrap();

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "ball");
    sink.set_property("location", "/dev/null");
    queue.set_property("max-size-time", 10_000_000_000u64);
    queue.set_property("max-size-bytes", 0u32);
    queue.set_property("max-size-buffers", 0u32);
    queue.set_property_from_str("leaky", "downstream");
    textoverlay.set_property_from_str("valignment", "top");
    textoverlay.set_property_from_str("halignment", "left");
    textoverlay.set_property("font-desc", "Sans, 20");
    textoverlay.set_property("text", "PRERECORD");
    textoverlay.set_property("color", 0x77_00_FF_00u32);
    clockoverlay.set_property("time-format", "%F %T");
    clockoverlay.set_property_from_str("valignment", "top");
    clockoverlay.set_property_from_str("halignment", "right");
    clockoverlay.set_property("font-desc", "Sans, 20");

    // Without this, the output video will be slower than wall-clock. See `videotestsrc` source code or this link for more info
    // https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/clock.html?gi-language=c#live-source-elements
    source.set_property("is-live", true);

    // The idea is to
    // - Block queue
    //   + To make sure that there is no data flowing into x264enc anymore
    //   + Also to set up info for a new session (e.g. change text to PRERECORD...)
    // - Send EOS event to x264enc
    //   + To let it wrap its current work up.
    // - Handle EOS message from filesink
    //   + This is to make sure the file is correctly saved, and we can begin recording again.
    // So message-forward must be enabled, otherwise we won't receive EOS message from filesink.
    // https://gstreamer.freedesktop.org/documentation/gstreamer/gstbin.html?gi-language=c#GstBin:message-forward
    pipeline.set_property("message-forward", true);

    Ok((
        pipeline,
        textoverlay,
        queue,
        x264enc,
        mp4mux,
        sink,
        pad,
        padid,
    ))
}

fn pad_probe_block(_pad: &gst::Pad, _probe_info: &mut gst::PadProbeInfo) -> gst::PadProbeReturn {
    // When gst_pad_push is called to push a buffer, it'll call block probes first before other probes.
    // See https://gstreamer.freedesktop.org/documentation/gstreamer/gstpad.html?gi-language=c#gst_pad_push

    // Must return OK to trigger blocking behavior. Return DROP will let the next buffer in.
    // See https://gstreamer.freedesktop.org/documentation/gstreamer/gstpad.html?gi-language=c#GstPadProbeReturn
    //
    // Note that by returning OK, we are keeping this buffer here. So we need to remove it later, otherwise
    // it'll reach the sink.
    gst::PadProbeReturn::Ok
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum State {
    PRERECORD,
    RECORDING,
    ENDING,
    QUITTING,
}
