use anyhow::Error;
use gst::prelude::*;
use gst::PadExtManual;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use std::thread;

#[path = "../run.rs"]
mod run;

#[path = "../utility.rs"]
mod utility;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

fn entry() -> Result<(), Error> {
    println!("--- {} ---", chrono::Local::now());
    println!("    Press 'b' to begin recording, 'q' to quit.");
    println!("    The output file is at /tmp/test-video.mp4");

    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                               Create pipeline                              */
    /* -------------------------------------------------------------------------- */
    let (pipeline, textoverlay, queue, pad, padid) = create_pipeline()?;
    let mut padid = Some(padid);

    /* -------------------------------------------------------------------------- */
    /*                                Play pipeline                               */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                                Handle Ctrl+C                               */
    /* -------------------------------------------------------------------------- */
    // Do not handle Ctrl+C so we can quit whenever with Ctrl+C.
    // Otherwise we need to use another tool for that, and I am lazy.
    // utility::handle_ctrlc(&pipeline)?;

    /* -------------------------------------------------------------------------- */
    /*                               Handle keyboard                              */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    thread::spawn({
        let bus = bus.clone();
        move || utility::handle_keyboard(&bus)
    });

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    println!("--- {} ---", chrono::Local::now());
    for msg in bus.iter_timed(gst::ClockTime::MAX) {
        // eprintln!("    {}:{:?}", line!(), msg);
        match msg.view() {
            gst::MessageView::Eos(_) => break,
            gst::MessageView::Error(err) => {
                eprintln!(
                    "Error from {:?}: {} ({:?})",
                    err.src(),
                    err.error(),
                    err.debug()
                );
                break;
            }
            gst::MessageView::Application(msg) => match msg.structure().map(|s| s.name()) {
                Some("quit") => {
                    println!("--- {} ---", chrono::Local::now());
                    println!("    Quitting...");
                    if pipeline.current_state() == gst::State::Playing {
                        pipeline.send_event(gst::event::Eos::new());
                    } else {
                        eprintln!("Pipeline State: {:?}", pipeline.current_state());
                        break;
                    }
                }
                Some("begin-recording") => {
                    println!("--- {} ---", chrono::Local::now());
                    println!("    Begin Recording");

                    // Change text on screen.
                    textoverlay.set_property("text", "RECORDING");
                    textoverlay.set_property("color", 0xFF_FF_FF_00u32);

                    // Only drop first buffer when accumulated data is already equal to (or greater than) 10s.
                    let time = queue.property::<u64>("current-level-time");
                    println!("    Queue's current-level-time is {}", time);
                    if time >= 10_000_000_000u64 {
                        // Install a new data probe, which is to drop the stuck buffer.
                        pad.add_probe(gst::PadProbeType::BUFFER, {
                            let counter = Arc::new(AtomicUsize::new(0));
                            move |_pad, _probe_info| {
                                // When we remove the blocking probe (below), the first buffer will go here
                                // So we need to drop it. Then we remove this pad probe on the next buffer.
                                if counter.fetch_add(1, Ordering::SeqCst) == 0 {
                                    gst::PadProbeReturn::Drop
                                } else {
                                    gst::PadProbeReturn::Remove
                                }
                            }
                        })
                        .unwrap();
                    }

                    // Remove blocking probe so buffers can flow.
                    pad.remove_probe(padid.take().unwrap())
                }
                _ => (),
            },
            _ => (),
        }
    }

    // - Stop playing.
    pipeline.set_state(gst::State::Null)?;

    Ok(())
}

fn create_pipeline() -> Result<
    (
        gst::Pipeline,
        gst::Element,
        gst::Element,
        gst::Pad,
        gst::PadProbeId,
    ),
    Error,
> {
    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source"))?;
    let textoverlay = gst::ElementFactory::make("textoverlay", Some("textoverlay"))?;
    let clockoverlay = gst::ElementFactory::make("clockoverlay", Some("clockoverlay"))?;
    let queue = gst::ElementFactory::make("queue", Some("queue"))?;
    let x264enc = gst::ElementFactory::make("x264enc", Some("x264enc"))?;
    let mp4mux = gst::ElementFactory::make("mp4mux", Some("mp4mux"))?;
    let sink = gst::ElementFactory::make("filesink", Some("sink"))?;

    /* -------------------------------------------------------------------------- */
    /*                          Create an empty pipeline                          */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    /* -------------------------------------------------------------------------- */
    /*                          Add elements to pipeline                          */
    /* -------------------------------------------------------------------------- */
    pipeline.add_many(&[
        &source,
        &textoverlay,
        &clockoverlay,
        &queue,
        &x264enc,
        &mp4mux,
        &sink,
    ])?;

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    gst::Element::link_many(&[
        &source,
        &textoverlay,
        &clockoverlay,
        &queue,
        &x264enc,
        &mp4mux,
        &sink,
    ])?;

    /* -------------------------------------------------------------------------- */
    /*                             Set up block probe                             */
    /* -------------------------------------------------------------------------- */
    let pad = queue.static_pad("src").unwrap();
    let padid = pad
        .add_probe(
            gst::PadProbeType::BLOCK | gst::PadProbeType::BUFFER,
            move |_pad, _probe_info| {
                // When gst_pad_push is called to push a buffer, it'll call block probes first before other probes.
                // See https://gstreamer.freedesktop.org/documentation/gstreamer/gstpad.html?gi-language=c#gst_pad_push

                // Must return OK to trigger blocking behavior. Return DROP will let the next buffer in.
                // See https://gstreamer.freedesktop.org/documentation/gstreamer/gstpad.html?gi-language=c#GstPadProbeReturn
                //
                // Note that by returning OK, we are keeping this buffer here. So we need to remove it later, otherwise
                // it'll reach the sink.
                gst::PadProbeReturn::Ok
            },
        )
        .unwrap();

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "ball");
    sink.set_property("location", "/tmp/test-video.mp4");
    queue.set_property("max-size-time", 10_000_000_000u64);
    queue.set_property("max-size-bytes", 0u32);
    queue.set_property("max-size-buffers", 0u32);
    queue.set_property_from_str("leaky", "downstream");
    textoverlay.set_property_from_str("valignment", "top");
    textoverlay.set_property_from_str("halignment", "left");
    textoverlay.set_property("font-desc", "Sans, 20");
    textoverlay.set_property("text", "PRERECORD");
    textoverlay.set_property("color", 0x77_00_FF_00u32);
    clockoverlay.set_property("time-format", "%F %T");
    clockoverlay.set_property_from_str("valignment", "top");
    clockoverlay.set_property_from_str("halignment", "right");
    clockoverlay.set_property("font-desc", "Sans, 20");

    // Without this, the output video will be slower than wall-clock. See `videotestsrc` source code or this link for more info
    // https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/clock.html?gi-language=c#live-source-elements
    source.set_property("is-live", true);

    Ok((pipeline, textoverlay, queue, pad, padid))
}
