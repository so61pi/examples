use gst::prelude::*;
use std::{
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread, time,
};
use termion::event::Key;
use termion::input::TermRead;

// We want to be simple here, so this won't handle the case where users press Ctrl+C
// multiple times (e.g. due to slow handling of first Ctrl+C).
//
// How `gst-launch-1.0` handles `--eos-on-shutdown` can be found at
// https://github.com/GStreamer/gstreamer/blob/dd22ec68d203b292ca16c555f9d1db58e37cea8f/tools/gst-launch.c#L810-L827
#[allow(dead_code)]
pub(crate) fn handle_ctrlc(pipeline: &gst::Pipeline) -> Result<(), ctrlc::Error> {
    // Create a shared variable.
    let running = Arc::new(AtomicBool::new(true));

    // Setup Ctrl+C handler.
    ctrlc::set_handler({
        let running = running.clone();
        move || {
            running.store(false, Ordering::SeqCst);
        }
    })?;

    // Create monitoring thread.
    thread::spawn({
        let pipeline = pipeline.clone();
        move || {
            // Wait until Ctrl+C is pressed.
            while running.load(Ordering::SeqCst) {
                thread::sleep(time::Duration::from_millis(50));
            }

            // Send EOS so pipeline can shutdown gracefully, and file can be saved correctly.
            // However, in case pipeline is not playing, it won't react to EOS, and we need to send message directly to bus.
            if pipeline.current_state() == gst::State::Playing {
                pipeline.send_event(gst::event::Eos::new());
            } else {
                let quit = gst::message::Application::new(gst::Structure::new_empty("quit"));
                let bus = pipeline.bus().unwrap();
                bus.post(&quit).unwrap();
            }
        }
    });

    Ok(())
}

#[allow(dead_code)]
pub(crate) fn glib_create_main_with_channel<T>(
) -> ((glib::Sender<T>, glib::Receiver<T>), glib::MainLoop) {
    // Get a main context...
    let main_context = glib::MainContext::default();
    // ... and make it the main context by default so that we can then have a channel to send the
    // commands we received from the terminal.
    let _guard = main_context.acquire().expect("cannot acquire context");

    // Build the channel and main loop.
    (
        glib::MainContext::channel(glib::PRIORITY_DEFAULT),
        glib::MainLoop::new(Some(&main_context), false),
    )
}

#[allow(dead_code)]
pub(crate) fn handle_keyboard(bus: &gst::Bus) {
    let mut stdin = termion::async_stdin().keys();

    // Use closure to prevent messages from being reused, since their seqnum should be unique.
    let quit = || gst::message::Application::new(gst::Structure::new_empty("quit"));
    let brecord = || gst::message::Application::new(gst::Structure::new_empty("begin-recording"));
    let erecord = || gst::message::Application::new(gst::Structure::new_empty("end-recording"));
    loop {
        if let Some(Ok(input)) = stdin.next() {
            match input {
                Key::Char('b') | Key::Char('B') => {
                    bus.post(&brecord()).unwrap();
                }
                Key::Char('e') | Key::Char('E') => {
                    bus.post(&erecord()).unwrap();
                }
                Key::Char('q') | Key::Char('Q') | Key::Ctrl('c') | Key::Ctrl('C') => {
                    bus.post(&quit()).unwrap();
                }
                _ => continue,
            }
        }
        thread::sleep(time::Duration::from_millis(50));
    }
}
