use anyhow::Error;
use gst::prelude::*;

#[path = "../run.rs"]
mod run;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

/*

gst-launch-1.0 -v playbin uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm

*/
fn entry() -> Result<(), Error> {
    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                              Create a pipeline                             */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::parse_launch("playbin uri=https://www.freedesktop.org/software/gstreamer-sdk/data/media/sintel_trailer-480p.webm")?;

    /* -------------------------------------------------------------------------- */
    /*                                   Play it                                  */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    for msg in bus.iter_timed_filtered(
        gst::ClockTime::MAX,
        &[gst::MessageType::Eos, gst::MessageType::Error],
    ) {
        match msg.view() {
            gst::MessageView::Eos(_) => break,
            gst::MessageView::Error(err) => {
                eprintln!(
                    "Error from {:?}: {} ({:?})",
                    err.src(),
                    err.error(),
                    err.debug()
                );
                break;
            }
            _ => (),
        }
    }

    /* -------------------------------------------------------------------------- */
    /*                                Stop playing                                */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Null)?;

    Ok(())
}
