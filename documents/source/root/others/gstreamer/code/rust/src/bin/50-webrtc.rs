use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use futures::channel;
use futures::SinkExt;
use futures::StreamExt;
use gst::prelude::{Cast, *};
use gst::PadExtManual;
use gst::PadProbeId;
use serde_derive::{Deserialize, Serialize};
use tokio_tungstenite::tungstenite;
use tokio_tungstenite::tungstenite::http::Request;
use tungstenite::Message as WsMessage;

const WSADDR: &str = "ws://127.0.0.1:8888";
const STUNSERVER: &str = "stun://stun.l.google.com:19302";

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
struct Sdp {
    #[serde(rename = "type")]
    type_: String,
    sdp: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
struct IceCandidate {
    candidate: String,
    #[serde(rename = "sdpMLineIndex")]
    sdp_mline_index: u32,
}

#[allow(dead_code)]
#[derive(Deserialize)]
#[serde(rename_all = "lowercase")]
struct JsonMsgIn {
    date: u64,
    src: String,
    dst: String,
    data: JsonMsgDataIn,
}

#[allow(dead_code)]
#[derive(Deserialize)]
#[serde(tag = "type", content = "content", rename_all = "lowercase")]
enum JsonMsgDataIn {
    #[serde(rename = "username-ok")]
    UsernameOk {},

    #[serde(rename = "reject-username")]
    RejectUsername {
        name: String,
    },

    Userlist {
        recorders: Vec<String>,
        humans: Vec<String>,
    },

    Message {
        text: String,
    },

    #[serde(rename = "video-answer")]
    VideoAnswer {
        sdp: Sdp,
    },

    #[serde(rename = "new-ice-candidate")]
    NewIceCandidate {
        candidate: IceCandidate,
    },
}

#[derive(Serialize)]
#[serde(rename_all = "lowercase")]
struct JsonMsgOut {
    date: u64,
    dst: String,
    data: JsonMsgDataOut,
}

#[derive(Serialize)]
#[serde(tag = "type", content = "content", rename_all = "lowercase")]
enum JsonMsgDataOut {
    Username {
        name: String,
    },

    Message {
        text: String,
    },

    #[serde(rename = "video-offer")]
    VideoOffer {
        sdp: Sdp,
    },

    #[serde(rename = "new-ice-candidate")]
    NewIceCandidate {
        candidate: IceCandidate,
    },
}

#[path = "../run.rs"]
mod run;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("[APP] Failed: {}", err),
    }
}

#[tokio::main]
async fn entry() -> anyhow::Result<()> {
    gst::init().unwrap();

    /* -------------------------------------------------------------------------- */
    /*                               Create pipeline                              */
    /* -------------------------------------------------------------------------- */
    let (wstx, mut wsrx) = channel::mpsc::unbounded::<WsMessage>();
    let (
        mpl,
        (queue_webrtc_srcpad_blockprobe_id, queue_webrtc_sinkpad_drop_serialized_query),
        (queue_tofile_srcpad_blockprobe_id, queue_tofile_sinkpad_drop_serialized_query),
    ) = create_pipeline();
    let mut queue_webrtc_srcpad_blockprobe_id = Some(queue_webrtc_srcpad_blockprobe_id);
    mpl.pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                           Create websocket client                          */
    /* -------------------------------------------------------------------------- */
    let wsrequest = Request::builder()
        .uri(WSADDR)
        .header("Sec-WebSocket-Protocol", "json")
        .body(())
        .unwrap();
    let (ws, _) = tokio_tungstenite::connect_async(wsrequest).await.unwrap();
    let (mut wssink, wsstream) = ws.split();

    let mut myname = String::from("recorder");

    // Send our propsed name here, will handle rejection in later loop.
    wssink
        .send(WsMessage::Text(
            serde_json::to_string(&JsonMsgOut {
                date: chrono::offset::Local::now().timestamp() as u64,
                dst: "server".into(),
                data: JsonMsgDataOut::Username {
                    name: myname.clone(),
                },
            })
            .unwrap(),
        ))
        .await
        .unwrap();

    /* -------------------------------------------------------------------------- */
    /*                             Start recorder task                            */
    /* -------------------------------------------------------------------------- */
    let recordertx = {
        let (tx, mut rx) = channel::mpsc::unbounded::<RecorderTaskMessage>();
        let mpl = mpl.clone();
        tokio::spawn(async move {
            recorder(
                &mut rx,
                &mpl,
                Some(queue_tofile_srcpad_blockprobe_id),
                queue_tofile_sinkpad_drop_serialized_query,
            )
            .await
        });
        tx
    };

    /* -------------------------------------------------------------------------- */
    /*                                Handling Loop                               */
    /* -------------------------------------------------------------------------- */
    let mut webrtcdata = None;

    // `select!` requires streams to be fused
    let mut wsstream = wsstream.fuse();
    let mut gstmsgrx = mpl.pipeline.bus().unwrap().stream().fuse();
    // let mut wsrx = wsrx.fuse();
    loop {
        let txwsmsg = tokio::select! {
            /* -------------------------------------------------------------------------- */
            /*                    Handle in-coming websocket messages.                    */
            /* -------------------------------------------------------------------------- */
            wsmsg = wsstream.select_next_some() => {
                match wsmsg? {
                    WsMessage::Close(_) => {
                        println!("[WS] Peer disconnected");
                        break;
                    },
                    WsMessage::Ping(data) => Some(WsMessage::Pong(data)),
                    WsMessage::Pong(_) => None,
                    WsMessage::Binary(_) => None,
                    WsMessage::Text(jsontext) => {
                        // Handle websocket message.
                        println!("[WS] Received: {}", &jsontext);

                        match serde_json::from_str::<JsonMsgIn>(&jsontext) {
                            Err(e) => {
                                eprintln!("[WS] JSON Error: {}", e);
                                None
                            },
                            Ok(jsonmsg) => {
                                let srcdstvalid = (jsonmsg.src == "server" && jsonmsg.dst.is_empty()) || (jsonmsg.dst == "broadcast" || jsonmsg.dst == myname);
                                if srcdstvalid {
                                    match jsonmsg.data {
                                        JsonMsgDataIn::UsernameOk{} => {
                                            None
                                        },

                                        // Server gives us a new non-conflicting name, save it.
                                        JsonMsgDataIn::RejectUsername { name } => {
                                            myname = name;
                                            None
                                        },

                                        // Text message from remote peers.
                                        JsonMsgDataIn::Message { text } => {
                                            match text.as_ref() {
                                                "/help" | "?" => {
                                                    let help = "\
                                                        /status | ??: Get status of all recorders | \
                                                        /begin-recording: Begin recording all recorders | \
                                                        /end-recording: End recording all recorders | \
                                                        /end-webrtc: End WebRTC of all recorders";
                                                    let message = serde_json::to_string(&JsonMsgOut{
                                                        date: chrono::offset::Local::now().timestamp() as u64,
                                                        dst: jsonmsg.src.clone(),
                                                        data: JsonMsgDataOut::Message{
                                                            text: help.into(),
                                                        },
                                                    })
                                                    .unwrap();
                                                    Some(WsMessage::Text(message))
                                                },

                                                "/status" | "??" => {
                                                    let (tx, rx) = channel::oneshot::channel();
                                                    recordertx.unbounded_send(RecorderTaskMessage::Query { tx }).unwrap();
                                                    let recorderstate = rx.await.unwrap();

                                                    let status = format!("RecorderState: {} | WebRTCState: {}",
                                                        recorderstate.as_ref(),
                                                        if webrtcdata.is_some() { "On" } else { "Off" }
                                                    );

                                                    let message = serde_json::to_string(&JsonMsgOut{
                                                        date: chrono::offset::Local::now().timestamp() as u64,
                                                        dst: jsonmsg.src.clone(),
                                                        data: JsonMsgDataOut::Message{
                                                            text: status,
                                                        },
                                                    })
                                                    .unwrap();
                                                    Some(WsMessage::Text(message))
                                                },

                                                "/begin-webrtc" => {
                                                    // TODO: Return error to users if webrtc is already started.
                                                    if jsonmsg.dst == myname && webrtcdata.is_none() {
                                                        let (tx, mut rx) = channel::mpsc::unbounded::<WebRTCTaskMessage>();
                                                        let wstx = wstx.clone();
                                                        let mpl = mpl.clone();
                                                        let probeid = queue_webrtc_srcpad_blockprobe_id.take().unwrap();
                                                        let drop_serialized_query = queue_webrtc_sinkpad_drop_serialized_query.clone();
                                                        let peername = jsonmsg.src.clone();
                                                        let handle = tokio::spawn(async move {
                                                            webrtc(
                                                                &mut rx,
                                                                wstx,
                                                                &mpl,
                                                                probeid,
                                                                drop_serialized_query,
                                                                peername,
                                                            ).await
                                                        });
                                                        webrtcdata = Some((handle, tx));
                                                    }
                                                    None
                                                },
                                                "/end-webrtc" => {
                                                    if let Some((handle, tx)) = webrtcdata.take() {
                                                        tx.unbounded_send(WebRTCTaskMessage::EndWebRTC).unwrap();
                                                        queue_webrtc_srcpad_blockprobe_id = Some(handle.await.unwrap());
                                                    }
                                                    None
                                                },

                                                "/begin-recording" => {
                                                    // TODO: Check error?
                                                    if jsonmsg.dst == myname {
                                                        recordertx.unbounded_send(RecorderTaskMessage::BeginRecording).unwrap();
                                                    }
                                                    None
                                                },
                                                "/end-recording" => {
                                                    // TODO: Check error?
                                                    recordertx.unbounded_send(RecorderTaskMessage::EndRecording).unwrap();
                                                    None
                                                },

                                                _ => None,
                                            }
                                        },

                                        // We receive this from remote answering our offer.
                                        // -> Set remote description to webrtcbin.
                                        JsonMsgDataIn::VideoAnswer { sdp } => {
                                            if jsonmsg.dst == myname {
                                                if let Some((_, tx)) = &webrtcdata {
                                                    tx.unbounded_send(WebRTCTaskMessage::InternalMessage{
                                                        msg: WebRTCInternalMessage::VideoAnswer {
                                                            sdp
                                                        }
                                                    }).unwrap();
                                                }
                                            }
                                            None
                                        },

                                        // New ICE candidate from remote peer.
                                        // -> Add candidate to webrtcbin.
                                        JsonMsgDataIn::NewIceCandidate { candidate } => {
                                            if jsonmsg.dst == myname {
                                                if let Some((_, tx)) = &webrtcdata {
                                                    tx.unbounded_send(WebRTCTaskMessage::InternalMessage{
                                                        msg: WebRTCInternalMessage::NewIceCandidate {
                                                            candidate
                                                        }
                                                    }).unwrap();
                                                }
                                            }
                                            None
                                        },

                                        _ => {
                                            None
                                        },
                                    }
                                } else {
                                    None
                                }
                            },
                        }
                    },
                }
            },

            /* -------------------------------------------------------------------------- */
            /*                    Handle out-going websocket messages.                    */
            /* -------------------------------------------------------------------------- */
            wsmsg = wsrx.select_next_some() => Some(wsmsg),

            /* -------------------------------------------------------------------------- */
            /*                          Handle pipeline messages.                         */
            /* -------------------------------------------------------------------------- */
            gstmsg = gstmsgrx.select_next_some() => {
                use gst::message::MessageView;

                match &gstmsg.view() {
                    MessageView::Error(err) => {
                        eprintln!("[GST] Error: {:?}", err);
                        break;
                    },
                    MessageView::Warning(warning) => {
                        eprintln!("[GST] Warning: {:?}", warning);
                    }
                    MessageView::Element(m) => {
                        // Handle EOS message from `sink_tofile`, make sure every bit is processed.
                        // TODO: Make sure element is `sink_tofile`.
                        let s = m.structure().unwrap();
                        if s.name() == "GstBinForwarded" {
                            let msg: gst::Message = s.get("message").unwrap();
                            if msg.type_() == gst::MessageType::Eos {
                                recordertx.unbounded_send(RecorderTaskMessage::EndRecordingEOSReceived).unwrap();
                            }
                        }
                    }
                    _ => (),
                }

                None
            },

            else => break,
        };

        // Sending out websocket message.
        if let Some(msg) = txwsmsg {
            println!("[WS] Sending: {}", &msg);
            wssink.send(msg).await?
        }
    }

    Ok(())
}

#[derive(Debug, Clone, PartialEq, Eq, strum::AsRefStr)]
enum RecorderState {
    PreRecord,
    Recording,
    Ending,
}

#[derive(Debug)]
enum RecorderTaskMessage {
    BeginRecording,
    EndRecording,
    EndRecordingEOSReceived,

    Query {
        tx: channel::oneshot::Sender<RecorderState>,
    },
}

async fn recorder(
    msgrx: &mut channel::mpsc::UnboundedReceiver<RecorderTaskMessage>,
    mpl: &MainPipeline,
    mut queue_srcpad_blockprobe_id: Option<PadProbeId>,
    queue_sinkpad_drop_serialized_query: Arc<AtomicBool>,
) {
    let mut recorderstate = RecorderState::PreRecord;
    let mut filecount = 1u32;
    loop {
        match msgrx.select_next_some().await {
            RecorderTaskMessage::BeginRecording => {
                println!("[RECORDER-TASK] Receive BeginRecording");
                if recorderstate == RecorderState::PreRecord {
                    // Change text on screen.
                    mpl.textoverlay
                        .set_property("text", &format!("{} RECORDING", std::process::id()));
                    mpl.textoverlay.set_property("color", 0xFF_FF_FF_00u32);

                    // Make sure we only create video file when begin recording,
                    // so there are no redudant files in the end.
                    let filename =
                        format!("/tmp/test-video-{}-{}.mp4", std::process::id(), filecount);
                    println!("[RECORDER-TASK] Video File: {}", &filename);
                    mpl.sink_tofile.set_state(gst::State::Null).unwrap();
                    mpl.sink_tofile.set_property("location", &filename);
                    mpl.sink_tofile.set_state(gst::State::Playing).unwrap();
                    filecount += 1;

                    // Only drop first buffer when accumulated data is already equal to (or greater than) 10s.
                    let time = mpl.queue_tofile.property::<u64>("current-level-time");
                    println!("[RECORDER-TASK] Queue's current-level-time is {}", time);
                    if time >= 10_000_000_000u64 {
                        // Install a new data probe, which is to drop the stuck buffer.
                        mpl.queue_tofile_srcpad
                            .add_probe(gst::PadProbeType::BUFFER, {
                                let done = Arc::new(AtomicBool::new(false));
                                move |_pad, _probe_info| {
                                    // When we remove the blocking probe, the first buffer will go here, so
                                    // we need to drop it. Then we remove this pad probe on the next buffer.
                                    if done.fetch_or(true, Ordering::SeqCst) {
                                        gst::PadProbeReturn::Remove
                                    } else {
                                        gst::PadProbeReturn::Drop
                                    }
                                }
                            })
                            .unwrap();
                    }

                    // Remove blocking probe so buffers can flow.
                    mpl.queue_tofile_srcpad
                        .remove_probe(queue_srcpad_blockprobe_id.take().unwrap());

                    // The flow is back, allow serialized queries again.
                    queue_sinkpad_drop_serialized_query.store(false, Ordering::SeqCst);

                    recorderstate = RecorderState::Recording;
                }
            }

            RecorderTaskMessage::EndRecording => {
                println!("[RECORDER-TASK] Receive EndRecording");
                if recorderstate == RecorderState::Recording {
                    queue_sinkpad_drop_serialized_query.store(true, Ordering::SeqCst);
                    queue_srcpad_blockprobe_id = Some(
                        mpl.queue_tofile_srcpad
                            .add_probe(
                                gst::PadProbeType::BLOCK | gst::PadProbeType::BUFFER,
                                pad_probe_data_block,
                            )
                            .unwrap(),
                    );

                    // Change text on screen.
                    mpl.textoverlay
                        .set_property("text", &format!("{} PRERECORD", std::process::id()));
                    mpl.textoverlay.set_property("color", 0x77_00_FF_00u32);

                    // Reset prerecord time.
                    mpl.queue_tofile_srcpad
                        .set_property("max-size-time", 10_000_000_000u64);

                    // Send EOS to x264enc's src pad, to make sure it completes its work and pushes out all left data.
                    // The EOS will be forwarded to mp4mux and then filesink. After filesink processes it, the video
                    // file is saved safely. Filesink also sends a new EOS message to its bin, which in this case is the
                    // pipeline (and the seqnum of this EOS message is out of our control here...).
                    let peer = mpl.queue_tofile_srcpad.peer().unwrap();
                    peer.send_event(gst::event::Eos::new());

                    recorderstate = RecorderState::Ending;
                }
            }

            RecorderTaskMessage::EndRecordingEOSReceived => {
                println!("[RECORDER-TASK] Receive EndRecordingEOSReceived");
                if recorderstate == RecorderState::Ending {
                    mpl.x264enc.set_state(gst::State::Null).unwrap();
                    mpl.mp4mux.set_state(gst::State::Null).unwrap();

                    // The video file is closed here.
                    mpl.sink_tofile.set_state(gst::State::Null).unwrap();

                    mpl.sink_tofile.set_property("location", "/dev/null");

                    // Must set filesink back to playing, otherwise the pipeline will stuck when quit.
                    // (Maybe because when sink is at NULL, it cannot process EOS sent from pipeline,
                    // and this makes pipeline waits forever.)
                    mpl.sink_tofile.set_state(gst::State::Playing).unwrap();
                    mpl.mp4mux.set_state(gst::State::Playing).unwrap();
                    mpl.x264enc.set_state(gst::State::Playing).unwrap();

                    recorderstate = RecorderState::PreRecord;
                }
            }

            RecorderTaskMessage::Query { tx } => {
                tx.send(recorderstate.clone()).unwrap();
            }
        }
    }
}

#[derive(Debug)]
enum WebRTCTaskMessage {
    EndWebRTC,

    InternalMessage { msg: WebRTCInternalMessage },
}

#[derive(Debug)]
enum WebRTCInternalMessage {
    VideoAnswer { sdp: Sdp },
    NewIceCandidate { candidate: IceCandidate },
}

async fn webrtc(
    msgrx: &mut channel::mpsc::UnboundedReceiver<WebRTCTaskMessage>,
    wstx: channel::mpsc::UnboundedSender<WsMessage>,
    mpl: &MainPipeline,
    queue_srcpad_blockprobe_id: PadProbeId,
    queue_sinkpad_drop_serialized_query: Arc<AtomicBool>,
    peername: String,
) -> gst::PadProbeId {
    /* -------------------------------------------------------------------------- */
    /*                              Setup WebRTC bin                              */
    /* -------------------------------------------------------------------------- */
    println!("[WEBRTC-TASK] Create bin");
    let mwr = create_webrtc_bin(wstx.clone(), peername);

    mpl.pipeline.add_many(&[&mwr.bin]).unwrap();
    mpl.queue_webrtc_srcpad.link(&mwr.binsinkpad).unwrap();
    queue_sinkpad_drop_serialized_query.store(false, Ordering::SeqCst);

    // Set transceivers to 'sendonly'. The default value is 'sendrecv'.
    println!("[WEBRTC-TASK] Change transceivers to sendonly");
    // webrtcbin does have get-transceivers to get all transceivers at once,
    // but I couldn't make it to work in Rust, so here we go...
    for i in 0.. {
        let transceiver = mwr
            .webrtc
            .emit_by_name::<glib::Object>("get-transceiver", &[&i]);
        let transceiver = transceiver
            .downcast::<gst_webrtc::WebRTCRTPTransceiver>()
            .unwrap();
        transceiver.set_property(
            "direction",
            gst_webrtc::WebRTCRTPTransceiverDirection::Sendonly,
        );
    }

    /* -------------------------------------------------------------------------- */
    /*                                   Playing                                  */
    /* -------------------------------------------------------------------------- */
    // Sync webrtc's state with main pipeline, i.e. set to Playing.
    // This must be before configuring probes (below), so when we remove block probe
    // webrtcbin can handle data immediately.
    println!("[WEBRTC-TASK] Sync state with parent");
    mwr.bin.sync_state_with_parent().unwrap();

    /* -------------------------------------------------------------------------- */
    /*                          Let buffers flow through                          */
    /* -------------------------------------------------------------------------- */
    // Install a probe to remove 1st frame in block probe.
    println!("[WEBRTC-TASK] Configure probes");
    mpl.queue_webrtc_srcpad
        .add_probe(gst::PadProbeType::BUFFER, {
            let done = Arc::new(AtomicBool::new(false));
            move |_pad, _probe_info| {
                // When we remove the blocking probe, the first buffer will go here, so
                // we need to drop it. Then we remove this pad probe on the next buffer.
                if done.fetch_or(true, Ordering::SeqCst) {
                    gst::PadProbeReturn::Remove
                } else {
                    gst::PadProbeReturn::Drop
                }
            }
        })
        .unwrap();

    // Remove block probe so buffers can start flowing.
    mpl.queue_webrtc_srcpad
        .remove_probe(queue_srcpad_blockprobe_id);

    /* -------------------------------------------------------------------------- */
    /*                               Handle messages                              */
    /* -------------------------------------------------------------------------- */
    println!("[WEBRTC-TASK] Start message handling loop");
    loop {
        match msgrx.select_next_some().await {
            WebRTCTaskMessage::EndWebRTC => {
                println!("[WEBRTC-TASK] Receive EndWebRTC");

                // Drop serialized queries again, since we are going to install a ne block probe.
                queue_sinkpad_drop_serialized_query.store(true, Ordering::SeqCst);

                // Reinstall block probe.
                let queue_webrtc_srcpad_blockprobe_id = mpl
                    .queue_webrtc_srcpad
                    .add_probe(
                        gst::PadProbeType::BLOCK | gst::PadProbeType::BUFFER,
                        pad_probe_data_block,
                    )
                    .unwrap();

                // Unlink WebRTC bin by using queue_webrtc_srcpad.
                let peerpad = mpl.queue_webrtc_srcpad.peer().unwrap();
                mpl.queue_webrtc_srcpad.unlink(&peerpad).unwrap();

                // Must set to NULL to clean up leftover works.
                mwr.bin.set_state(gst::State::Null).unwrap();

                // Then remove it from the main pipeline.
                mpl.pipeline.remove(&mwr.bin).unwrap();

                // Return the newly created block probe id, so the next webrtc session can unblock.
                return queue_webrtc_srcpad_blockprobe_id;
            }

            WebRTCTaskMessage::InternalMessage { msg } => match msg {
                WebRTCInternalMessage::VideoAnswer { sdp } => {
                    println!("[WEBRTC-TASK] Receive VideoAnswer");
                    let ret = match gst_sdp::SDPMessage::parse_buffer(sdp.sdp.as_bytes()) {
                        Err(e) => {
                            eprintln!("[WEBRTC] SDP Error: {:#?}", e);
                            break;
                        }
                        Ok(ret) => ret,
                    };
                    let answer = gst_webrtc::WebRTCSessionDescription::new(
                        gst_webrtc::WebRTCSDPType::Answer,
                        ret,
                    );
                    mwr.webrtc.emit_by_name::<()>(
                        "set-remote-description",
                        &[&answer, &None::<gst::Promise>],
                    );
                }
                WebRTCInternalMessage::NewIceCandidate { candidate } => {
                    println!("[WEBRTC-TASK] Receive NewIceCandidate");
                    mwr.webrtc.emit_by_name::<()>(
                        "add-ice-candidate",
                        &[&candidate.sdp_mline_index, &candidate.candidate],
                    );
                }
            },
        }
    }

    unreachable!();
}

#[derive(Debug, Clone)]
struct MainPipeline {
    pipeline: gst::Pipeline,

    textoverlay: gst::Element,

    queue_webrtc: gst::Element,
    queue_webrtc_srcpad: gst::Pad,

    queue_tofile: gst::Element,
    queue_tofile_srcpad: gst::Pad,
    x264enc: gst::Element,
    mp4mux: gst::Element,
    sink_tofile: gst::Element,
}

fn create_pipeline() -> (
    MainPipeline,
    (gst::PadProbeId, Arc<AtomicBool>),
    (gst::PadProbeId, Arc<AtomicBool>),
) {
    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source")).unwrap();
    let textoverlay = gst::ElementFactory::make("textoverlay", Some("textoverlay")).unwrap();
    let clockoverlay = gst::ElementFactory::make("clockoverlay", Some("clockoverlay")).unwrap();

    let tee = gst::ElementFactory::make("tee", Some("tee")).unwrap();
    let queue_window = gst::ElementFactory::make("queue", Some("queue_window")).unwrap();
    let queue_tofile = gst::ElementFactory::make("queue", Some("queue_tofile")).unwrap();
    let queue_webrtc = gst::ElementFactory::make("queue", Some("queue_webrtc")).unwrap();

    let x264enc = gst::ElementFactory::make("x264enc", Some("x264enc")).unwrap();
    let mp4mux = gst::ElementFactory::make("mp4mux", Some("mp4mux")).unwrap();

    let sink_window = gst::ElementFactory::make("autovideosink", Some("sink_window")).unwrap();
    let sink_tofile = gst::ElementFactory::make("filesink", Some("sink_tofile")).unwrap();

    /* -------------------------------------------------------------------------- */
    /*                               Create pipeline                              */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    pipeline
        .add_many(&[
            &source,
            &textoverlay,
            &clockoverlay,
            &tee,
            &queue_window,
            &queue_tofile,
            &queue_webrtc,
            &x264enc,
            &mp4mux,
            &sink_window,
            &sink_tofile,
        ])
        .unwrap();

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    gst::Element::link_many(&[&source, &textoverlay, &clockoverlay, &tee]).unwrap();
    gst::Element::link_many(&[&queue_window, &sink_window]).unwrap();
    gst::Element::link_many(&[&queue_tofile, &x264enc, &mp4mux, &sink_tofile]).unwrap();

    let queue_window_pad = queue_window.static_pad("sink").unwrap();
    tee.request_pad_simple("src_%u")
        .unwrap()
        .link(&queue_window_pad)
        .unwrap();

    let queue_tofile_pad = queue_tofile.static_pad("sink").unwrap();
    tee.request_pad_simple("src_%u")
        .unwrap()
        .link(&queue_tofile_pad)
        .unwrap();

    let queue_webrtc_pad = queue_webrtc.static_pad("sink").unwrap();
    tee.request_pad_simple("src_%u")
        .unwrap()
        .link(&queue_webrtc_pad)
        .unwrap();

    /* -------------------------------------------------------------------------- */
    /*                                Set up probes                               */
    /* -------------------------------------------------------------------------- */
    // queue webrtc - block probe
    let queue_webrtc_srcpad = queue_webrtc.static_pad("src").unwrap();
    let queue_webrtc_srcpad_blockprobe_id = queue_webrtc_srcpad
        .add_probe(
            gst::PadProbeType::BLOCK | gst::PadProbeType::BUFFER,
            pad_probe_data_block,
        )
        .unwrap();
    // queue webrtc - query probe
    let queue_webrtc_sinkpad_drop_serialized_query = Arc::new(AtomicBool::new(true));
    let queue_webrtc_sinkpad_drop_serialized_query_ret =
        queue_webrtc_sinkpad_drop_serialized_query.clone();
    queue_webrtc
        .static_pad("sink")
        .unwrap()
        .add_probe(
            gst::PadProbeType::QUERY_DOWNSTREAM,
            move |pad, probe_info| {
                if queue_webrtc_sinkpad_drop_serialized_query.load(Ordering::SeqCst) {
                    pad_probe_serialized_query_drop(pad, probe_info)
                } else {
                    gst::PadProbeReturn::Ok
                }
            },
        )
        .unwrap();

    // queue tofile - block probe
    let queue_tofile_srcpad = queue_tofile.static_pad("src").unwrap();
    let queue_tofile_srcpad_blockprobe_id = queue_tofile_srcpad
        .add_probe(
            gst::PadProbeType::BLOCK | gst::PadProbeType::BUFFER,
            pad_probe_data_block,
        )
        .unwrap();
    // queue tofile - query probe
    let queue_tofile_sinkpad_drop_serialized_query = Arc::new(AtomicBool::new(true));
    let queue_tofile_sinkpad_drop_serialized_query_ret =
        queue_tofile_sinkpad_drop_serialized_query.clone();
    queue_tofile
        .static_pad("sink")
        .unwrap()
        .add_probe(
            gst::PadProbeType::QUERY_DOWNSTREAM,
            move |pad, probe_info| {
                if queue_tofile_sinkpad_drop_serialized_query.load(Ordering::SeqCst) {
                    pad_probe_serialized_query_drop(pad, probe_info)
                } else {
                    gst::PadProbeReturn::Ok
                }
            },
        )
        .unwrap();

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "ball");
    textoverlay.set_property_from_str("valignment", "top");
    textoverlay.set_property_from_str("halignment", "left");
    textoverlay.set_property("font-desc", "Sans, 20");
    textoverlay.set_property("text", &format!("{} PRERECORD", std::process::id()));
    textoverlay.set_property("color", 0x77_00_FF_00u32);
    clockoverlay.set_property("time-format", "%F %T");
    clockoverlay.set_property_from_str("valignment", "top");
    clockoverlay.set_property_from_str("halignment", "right");
    clockoverlay.set_property("font-desc", "Sans, 20");

    queue_tofile.set_property("max-size-time", 10_000_000_000u64);
    queue_tofile.set_property("max-size-bytes", 0u32);
    queue_tofile.set_property("max-size-buffers", 0u32);
    queue_tofile.set_property_from_str("leaky", "downstream");

    queue_webrtc.set_property("max-size-time", 0u32);
    queue_webrtc.set_property("max-size-bytes", 0u32);
    queue_webrtc.set_property("max-size-buffers", 1u32); // Why 1? Yeah, why...
    queue_webrtc.set_property_from_str("leaky", "downstream");

    sink_tofile.set_property("location", "/dev/null");
    sink_tofile.set_property("async", false);

    sink_window.set_property("async", false);

    // Without this, the output video will be slower than wall-clock. See `videotestsrc` source code or this link for more info
    // https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/clock.html?gi-language=c#live-source-elements
    source.set_property("is-live", true);

    // So message-forward must be enabled, otherwise we won't receive EOS message from filesink.
    // https://gstreamer.freedesktop.org/documentation/gstreamer/gstbin.html?gi-language=c#GstBin:message-forward
    pipeline.set_property("message-forward", true);

    // TODO: This is quite complicated for the readers, should refactor it somehow.
    (
        MainPipeline {
            pipeline,
            textoverlay,
            queue_webrtc,
            queue_webrtc_srcpad,
            queue_tofile,
            queue_tofile_srcpad,
            x264enc,
            mp4mux,
            sink_tofile,
        },
        (
            queue_webrtc_srcpad_blockprobe_id,
            queue_webrtc_sinkpad_drop_serialized_query_ret,
        ),
        (
            queue_tofile_srcpad_blockprobe_id,
            queue_tofile_sinkpad_drop_serialized_query_ret,
        ),
    )
}

fn pad_probe_serialized_query_drop(
    _pad: &gst::Pad,
    probe_info: &mut gst::PadProbeInfo,
) -> gst::PadProbeReturn {
    if let Some(gst::PadProbeData::Query(query)) = &probe_info.data {
        // Normally, a query has its own path so it is not blocked by buffer stream.
        // Serialized queries are different, since they follow the buffer stream.
        // This means when a buffer stream is blocked, so is the serialized queries.
        //
        // The implementation of queue element creates a new thread for pushing buffers
        // to from the source pad to its peer. When a serialized query is received from
        // the sink pad of a queue, it'll be put into the query queue, and the code will
        // wait until that serialized query is processed (by waiting for a cond variable).
        //
        // Queued serialized queries are read and send downstream in pushing thread.
        // As a result, if the pushing thread is blocked (e.g. by a block probe), the
        // waiting thread will wait forever. This, effectively, prevents all buffers from
        // being pushed by all upstream elements. And we have a stuck pipeline.
        //
        // Currently, gstreamer has 2 serialized queries, GST_QUERY_ALLOCATION and GST_QUERY_DRAIN.
        // And from my testing, dropping them while queue's source pad is blocked (e.g. with
        // block probe) works as our pipeline is not stuck waiting for these queries.
        //
        // The source code of queue elememt can be referred to enable better understanding
        // of all this comment.
        if query.is_serialized() {
            return gst::PadProbeReturn::Drop;
        }
    }
    gst::PadProbeReturn::Ok
}

fn pad_probe_data_block(
    _pad: &gst::Pad,
    _probe_info: &mut gst::PadProbeInfo,
) -> gst::PadProbeReturn {
    // When gst_pad_push is called to push a buffer, it'll call block probes first before other probes.
    // See https://gstreamer.freedesktop.org/documentation/gstreamer/gstpad.html?gi-language=c#gst_pad_push

    // Must return OK to trigger blocking behavior. Return DROP will let the next buffer in.
    // See https://gstreamer.freedesktop.org/documentation/gstreamer/gstpad.html?gi-language=c#GstPadProbeReturn
    //
    // Note that by returning OK, we are keeping this buffer here. So we need to remove it later, otherwise
    // it'll reach the sink.
    gst::PadProbeReturn::Ok
}

#[derive(Debug)]
struct AppWebRTCBin {
    peername: String,
    bin: gst::Bin,
    webrtc: gst::Element,
    binsinkpad: gst::Pad,
}

fn create_webrtc_bin(
    wstx: channel::mpsc::UnboundedSender<WsMessage>,
    peername: String,
) -> AppWebRTCBin {
    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    // `videoconvert` make sures we have compatible video stream. Otherwise we could get not-negotiated.
    // However, the use of `videoconvert` could be inefficient.
    let videoconvert = gst::ElementFactory::make("videoconvert", Some("videoconvert")).unwrap();
    let vp8enc = gst::ElementFactory::make("vp8enc", Some("vp8enc")).unwrap();
    let rtpvp8pay = gst::ElementFactory::make("rtpvp8pay", Some("rtpvp8pay")).unwrap();
    let webrtcbin = gst::ElementFactory::make("webrtcbin", Some("webrtcbin")).unwrap();

    /* -------------------------------------------------------------------------- */
    /*                     Put all created elements into a bin                    */
    /* -------------------------------------------------------------------------- */
    let bin = gst::Bin::new(Some("mywebrtcbin"));
    bin.add_many(&[&videoconvert, &vp8enc, &rtpvp8pay, &webrtcbin])
        .unwrap();
    gst::Element::link_many(&[&videoconvert, &vp8enc, &rtpvp8pay, &webrtcbin]).unwrap();

    let ghostpad = gst::GhostPad::builder(None, gst::PadDirection::Sink)
        .build_with_target(&videoconvert.static_pad("sink").unwrap())
        .unwrap();
    ghostpad.set_active(true).unwrap();
    bin.add_pad(&ghostpad).unwrap();

    /* -------------------------------------------------------------------------- */
    /*                               Set properties                               */
    /* -------------------------------------------------------------------------- */
    // We do not use TURN server since for our testing STUN is enough.
    webrtcbin.set_property("stun-server", STUNSERVER);
    webrtcbin.set_property("bundle-policy", gst_webrtc::WebRTCBundlePolicy::MaxBundle);

    /* -------------------------------------------------------------------------- */
    /*                     Setup on-negotiation-needed handler                    */
    /* -------------------------------------------------------------------------- */
    webrtcbin
        .connect(
            "on-negotiation-needed",
            false, // Call our handler before default handler
            {
                let webrtcbin = webrtcbin.clone();
                let wstx = wstx.clone();
                let peername = peername.clone();
                move |_values| {
                    /* -------------------------------------------------------------------------- */
                    /*                    Ask webrtcbin to create an SDP offer                    */
                    /* -------------------------------------------------------------------------- */
                    println!("[WS] Create offer");
                    webrtcbin
                        .emit_by_name::<()>(
                            "create-offer",
                            &[
                                &Option::<gst::Structure>::None,
                                &gst::Promise::with_change_func({
                                    let webrtcbin = webrtcbin.clone();
                                    let wstx = wstx.clone();
                                    let peername = peername.clone();
                                    move |reply: Result<
                                        Option<&gst::StructureRef>,
                                        gst::PromiseError,
                                    >| {
                                        /* -------------------------------------------------------------------------- */
                                        /*                       Send SDP offer to remote peer.                       */
                                        /* -------------------------------------------------------------------------- */
                                        let reply = match reply {
                                            Ok(Some(reply)) => reply,
                                            Ok(None) => {
                                                eprintln!("[WEBRTC] Offer creation future got no response");
                                                return;
                                            }
                                            Err(err) => {
                                                eprintln!(
                                                "[WEBRTC] Offer creation future got error response: {:?}",
                                                err
                                            );
                                                return;
                                            }
                                        };

                                        println!("[WEBRTC] Set local description");
                                        let offer = reply
                                            .value("offer")
                                            .unwrap()
                                            .get::<gst_webrtc::WebRTCSessionDescription>()
                                            .expect("Invalid argument");
                                        webrtcbin
                                            .emit_by_name::<()>(
                                                "set-local-description",
                                                &[&offer, &None::<gst::Promise>],
                                            );

                                        println!("[WEBRTC] Send SDP to peer: {}", offer.sdp().as_text().unwrap());

                                        let message = serde_json::to_string(&JsonMsgOut {
                                            date: chrono::offset::Local::now().timestamp() as u64,
                                            dst: peername.clone(),
                                            data: JsonMsgDataOut::VideoOffer {
                                                sdp: Sdp {
                                                    type_: "offer".into(),
                                                    sdp: offer.sdp().as_text().unwrap(),
                                                },
                                            },
                                        })
                                        .unwrap();

                                        wstx.unbounded_send(WsMessage::Text(message)).unwrap();
                                    }
                                }),
                            ],
                        );
                    None
                }
            },
        );

    /* -------------------------------------------------------------------------- */
    /*                       Setup on-ice-candidate handler                       */
    /* -------------------------------------------------------------------------- */
    webrtcbin.connect(
        "on-ice-candidate",
        false, // Call our handler before default handler
        {
            let peername = peername.clone();
            move |values| {
                /* -------------------------------------------------------------------------- */
                /*                     Send ICE candidate to remote peer.                     */
                /* -------------------------------------------------------------------------- */
                println!("[WEBRTC] New ICE candidate");
                let _webrtc = values[0].get::<gst::Element>().expect("Invalid argument");
                let mlineindex = values[1].get::<u32>().expect("Invalid argument");
                let candidate = values[2].get::<String>().expect("Invalid argument");

                let message = serde_json::to_string(&JsonMsgOut {
                    date: chrono::offset::Local::now().timestamp() as u64,
                    dst: peername.clone(),
                    data: JsonMsgDataOut::NewIceCandidate {
                        candidate: IceCandidate {
                            candidate,
                            sdp_mline_index: mlineindex,
                        },
                    },
                })
                .unwrap();

                wstx.unbounded_send(WsMessage::Text(message)).unwrap();
                None
            }
        },
    );

    AppWebRTCBin {
        peername,
        bin,
        webrtc: webrtcbin,
        binsinkpad: ghostpad.upcast(),
    }
}
