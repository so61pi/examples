use anyhow::Error;
use gst::prelude::ObjectExt;
use gst::prelude::*;
use std::thread;

#[path = "../run.rs"]
mod run;

#[path = "../utility.rs"]
mod utility;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

fn entry() -> Result<(), Error> {
    println!("--- {} ---", chrono::Local::now());
    println!("    Press 'b' to begin recording, 'q' to quit.");
    println!("    The output file is at /tmp/test-video.mp4");

    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                               Create pipeline                              */
    /* -------------------------------------------------------------------------- */
    let (pipeline, textoverlay, queue, oselector, pad_file) = create_pipeline()?;

    /* -------------------------------------------------------------------------- */
    /*                                Play pipeline                               */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                                Handle Ctrl+C                               */
    /* -------------------------------------------------------------------------- */
    // Do not handle Ctrl+C so we can quit whenever with Ctrl+C.
    // Otherwise we need to use another tool for that, and I am lazy.
    // utility::handle_ctrlc(&pipeline)?;

    /* -------------------------------------------------------------------------- */
    /*                               Handle keyboard                              */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    thread::spawn({
        let bus = bus.clone();
        move || utility::handle_keyboard(&bus)
    });

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    println!("--- {} ---", chrono::Local::now());
    for msg in bus.iter_timed(gst::ClockTime::MAX) {
        // eprintln!("    {}:{:?}", line!(), msg);
        match msg.view() {
            gst::MessageView::Eos(_) => break,
            gst::MessageView::Error(err) => {
                eprintln!(
                    "Error from {:?}: {} ({:?})",
                    err.src(),
                    err.error(),
                    err.debug()
                );
                break;
            }
            gst::MessageView::Application(msg) => match msg.structure().map(|s| s.name()) {
                Some("quit") => {
                    println!("--- {} ---", chrono::Local::now());
                    println!("    Quitting...");
                    if pipeline.current_state() == gst::State::Playing {
                        pipeline.send_event(gst::event::Eos::new());
                    } else {
                        eprintln!("Pipeline State: {:?}", pipeline.current_state());
                        break;
                    }
                }
                Some("begin-recording") => {
                    println!("--- {} ---", chrono::Local::now());
                    println!("    Begin Recording");
                    // Change text on screen.
                    textoverlay.set_property("text", "RECORDING");
                    textoverlay.set_property("color", 0xFF_FF_FF_00u32);

                    // Make sure we output immediately instead of waiting until the queue becomes full.
                    queue.set_property("min-threshold-time", 0u64);

                    // Switch to real pad with which data will flow to file.
                    oselector.set_property("active-pad", &pad_file);
                }
                _ => {}
            },
            _ => (),
        }
    }
    println!("--- {} ---", chrono::Local::now());

    /* -------------------------------------------------------------------------- */
    /*                                Stop playing                                */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Null)?;

    Ok(())
}

fn create_pipeline() -> Result<
    (
        gst::Pipeline,
        gst::Element,
        gst::Element,
        gst::Element,
        gst::Pad,
    ),
    Error,
> {
    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source"))?;
    let textoverlay = gst::ElementFactory::make("textoverlay", Some("textoverlay"))?;
    let clockoverlay = gst::ElementFactory::make("clockoverlay", Some("clockoverlay"))?;
    let queue = gst::ElementFactory::make("queue", Some("queue"))?;
    let oselector = gst::ElementFactory::make("output-selector", Some("oselector"))?;
    let x264enc = gst::ElementFactory::make("x264enc", Some("x264enc"))?;
    let mp4mux = gst::ElementFactory::make("mp4mux", Some("mp4mux"))?;
    let sink_file = gst::ElementFactory::make("filesink", Some("sink_file"))?;
    let sink_fake = gst::ElementFactory::make("fakesink", Some("sink_fake"))?;

    /* -------------------------------------------------------------------------- */
    /*                          Create an empty pipeline                          */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    /* -------------------------------------------------------------------------- */
    /*                          Add elements to pipeline                          */
    /* -------------------------------------------------------------------------- */
    pipeline.add_many(&[
        &source,
        &textoverlay,
        &clockoverlay,
        &queue,
        &oselector,
        &x264enc,
        &mp4mux,
        &sink_file,
        &sink_fake,
    ])?;

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    // `clockoverlay` must be put after before `queue`, otherwise buffer going to `queue` won't have correct time.
    gst::Element::link_many(&[&source, &textoverlay, &clockoverlay, &queue, &oselector])?;
    gst::Element::link_many(&[&x264enc, &mp4mux, &sink_file])?;

    // Initially, the two lines below were swapped, and that led to a nasty problem where I could not begin recording
    // before the queue became full. The log indicated that the x264 encoder was not initialized. The exact message is
    // "encoder not initialized".
    //
    // Here the sequence of actions that led to the error:
    // 1. `oselector_src_pad_file = oselector.get_request_pad("src_%u")` also sets `active_srcpad`, since it is the first `get_request_pad`.
    // 2. `oselector.set_property("active-pad", &oselector_src_pad_fake)` sets `pending_srcpad`.
    // 3. Pipeline starts playing, StartStream & Segment events flow to `oselector` in function `gst_output_selector_event`,
    //    and `gst_output_selector_get_active` returns `pending_srcpad` which is set to `oselector_src_pad_fake`.
    // 4. Buffers keep flowing into queue element, which are kept there. There are two cases
    //    + When queue is full:
    //      * The first buffer flows into `gst_output_selector_chain` will switch `active_srcpad` (is `oselector_src_pad_file`
    //        to `oselector_src_pad_fake` (in `pending_srcpad`).
    //      * Buffers then flow through `oselector_src_pad_fake` continuously.
    //      * In case we switch `active_srcpad` to `oselector_src_pad_file`, it will switch successfully.
    //        - StartStream and Segment events are resent to `oselector_src_pad_file` when the switching happens.
    //    + When queue is not full yet, and we switch `active_srcpad` to `oselector_src_pad_file`:
    //      * Since `active_srcpad` is already `oselector_src_pad_file` (set at step 1), `gst_output_selector_set_property`
    //        will not set `pending_srcpad`. Not only that, it also resets `pending_srcpad`.
    //      * This is bad because the StartStream and Segment events did not go throw `oselector_src_pad_file`, so `x264enc`
    //        did not have the chance to configure itself. That's why we see "encoder not initialized" from `x264enc`.
    //
    // TL;DR StartStream & Segment events and buffers flowing to wrong pads causes `x264enc` not being initialized.
    //
    // The solution is to assign first `oselector.get_request_pad("src_%u")` to `oselector_src_pad_fake`, like what we have now.
    // This makes sure when we switch `active_srcpad` to `oselector_src_pad_file`, the next buffer will trigger the resend of
    // StartStream and Segment events.
    //
    // To debug events flow, we can use command
    //   GST_DEBUG='4,GST_TRACER:7,GST_EVENT:7' GST_TRACERS="log" ./gst-app
    //
    // Here is the relevant source code for this analysis:
    // - https://gitlab.freedesktop.org/gstreamer/gstreamer/-/blob/1.18.4/plugins/elements/gstoutputselector.c
    //   + gst_output_selector_request_new_pad
    //   + gst_output_selector_set_property
    //   + gst_output_selector_get_active
    //   + gst_output_selector_chain
    //   + gst_output_selector_switch
    // - https://gitlab.freedesktop.org/gstreamer/gst-plugins-base/-/blob/1.18.4/gst-libs/gst/video/gstvideoencoder.c
    //   + gst_video_encoder_chain
    // - https://gitlab.freedesktop.org/gstreamer/gst-plugins-ugly/-/blob/1.18.4/ext/x264/gstx264enc.c
    let oselector_src_pad_fake = oselector.request_pad_simple("src_%u").unwrap();
    let oselector_src_pad_file = oselector.request_pad_simple("src_%u").unwrap();

    let x264enc_pad = x264enc.static_pad("sink").unwrap();
    let sink_fake_pad = sink_fake.static_pad("sink").unwrap();

    oselector_src_pad_file.link(&x264enc_pad)?;
    oselector_src_pad_fake.link(&sink_fake_pad)?;

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "ball");
    sink_file.set_property("location", "/tmp/test-video.mp4");
    textoverlay.set_property_from_str("valignment", "top");
    textoverlay.set_property_from_str("halignment", "left");
    textoverlay.set_property("font-desc", "Sans, 20");
    textoverlay.set_property("text", "PRERECORD");
    textoverlay.set_property("color", 0x77_00_FF_00u32);
    clockoverlay.set_property("time-format", "%F %T");
    clockoverlay.set_property_from_str("valignment", "top");
    clockoverlay.set_property_from_str("halignment", "right");
    clockoverlay.set_property("font-desc", "Sans, 20");

    // Set queuing time to 10 seconds. We have to set all max-size-* to 0 to disable them.
    queue.set_property("min-threshold-time", 10_000_000_000u64);
    queue.set_property("max-size-time", 0u64);
    queue.set_property("max-size-bytes", 0u32);
    queue.set_property("max-size-buffers", 0u32);
    queue.set_property_from_str("leaky", "downstream"); // Remove oldest buffers when queue is full.

    // Without this `sink_file` or `sink_fake` will block the pipeline, since one of them does not receive data
    // because of output-selector. See more at
    //
    // - https://gstreamer.freedesktop.org/documentation/additional/design/states.html?gi-language=c#state-transitions
    //   + In `READY -> PAUSED` section
    //
    // - https://gstreamer.freedesktop.org/documentation/additional/design/preroll.html?gi-language=c
    //   > A sink element can only complete the state change to PAUSED after a buffer has been queued on the input
    //   > pad or pads. This process is called prerolling and is needed to fill the pipeline with buffers so that
    //   > the transition to PLAYING goes as fast as possible, with no visual delay for the user.
    //
    // - https://gstreamer.freedesktop.org/documentation/base/gstbasesink.html?gi-language=c
    //   > GstBaseSink will handle the prerolling correctly. This means that it will return GST_STATE_CHANGE_ASYNC
    //   > from a state change to PAUSED until the first buffer arrives in this element. The base class will call
    //   > the () vmethod with this preroll buffer and will then commit the state change to the next asynchronously
    //   > pending state.
    //
    //   > The async property can be used to instruct the sink to never perform an ASYNC state change. This feature
    //   > is mostly usable when dealing with non-synchronized streams or sparse streams.
    //
    //   > When async is disabled, the sink will immediately go to PAUSED instead of waiting for a preroll buffer.
    //
    //   + https://gstreamer.freedesktop.org/documentation/base/gstbasesink.html?gi-language=c#GstBaseSink:async
    //
    // - http://gstreamer-devel.966125.n4.nabble.com/output-selector-question-td4666514.html
    //   > This is a prerolling problem, your pipeline wont be able to complete the state change to paused until all
    //   > the sinks receive a buffer, which is an unlikely situation when using an output selector.
    //
    // - https://github.com/GStreamer/gst-plugins-base/blob/master/tests/icles/output-selector-test.c
    sink_file.set_property("async", false);
    sink_fake.set_property("async", false);

    // Without this, the output video will be slower than wall-clock. See `videotestsrc` source code or this link for more info
    // https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/clock.html?gi-language=c#live-source-elements
    source.set_property("is-live", true);

    Ok((
        pipeline,
        textoverlay,
        queue,
        oselector,
        oselector_src_pad_file,
    ))
}
