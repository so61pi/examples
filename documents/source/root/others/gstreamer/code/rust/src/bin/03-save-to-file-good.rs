use anyhow::Error;
use gst::prelude::*;

#[path = "../run.rs"]
mod run;

#[path = "../utility.rs"]
mod utility;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

/*

gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
! x264enc ! mp4mux ! filesink location=/tmp/test-video.mp4

*/
fn entry() -> Result<(), Error> {
    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source"))?;
    let sink = gst::ElementFactory::make("filesink", Some("sink"))?;
    let x264enc = gst::ElementFactory::make("x264enc", Some("x264enc"))?;
    let mp4mux = gst::ElementFactory::make("mp4mux", Some("mp4mux"))?;

    /* -------------------------------------------------------------------------- */
    /*                          Create an empty pipeline                          */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    /* -------------------------------------------------------------------------- */
    /*                          Add elements to pipeline                          */
    /* -------------------------------------------------------------------------- */
    pipeline.add_many(&[&source, &sink, &x264enc, &mp4mux])?;

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    source.link(&x264enc)?;
    x264enc.link(&mp4mux)?;
    mp4mux.link(&sink)?;

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "ball");
    sink.set_property("location", "/tmp/test-video.mp4");

    // Without this, the output video will be slower than wall-clock. See `videotestsrc` source code or this link for more info
    // https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/clock.html?gi-language=c#live-source-elements
    source.set_property("is-live", true);

    /* -------------------------------------------------------------------------- */
    /*                                Play pipeline                               */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                                Handle Ctrl+C                               */
    /* -------------------------------------------------------------------------- */
    utility::handle_ctrlc(&pipeline)?;

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    for msg in bus.iter_timed_filtered(
        gst::ClockTime::MAX,
        &[gst::MessageType::Eos, gst::MessageType::Error],
    ) {
        match msg.view() {
            gst::MessageView::Eos(_) => break,
            gst::MessageView::Error(err) => {
                eprintln!(
                    "Error from {:?}: {} ({:?})",
                    err.src(),
                    err.error(),
                    err.debug()
                );
                break;
            }
            _ => unreachable!(),
        }
    }

    /* -------------------------------------------------------------------------- */
    /*                                Stop playing                                */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Null)?;

    Ok(())
}
