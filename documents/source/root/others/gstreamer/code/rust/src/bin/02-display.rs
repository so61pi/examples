use anyhow::Error;
use gst::prelude::*;

#[path = "../run.rs"]
mod run;

fn main() {
    match run::run(entry) {
        Ok(_) => {}
        Err(err) => eprintln!("Failed: {}", err),
    }
}

/*

gst-launch-1.0 -v videotestsrc ! autovideosink

*/
fn entry() -> Result<(), Error> {
    /* -------------------------------------------------------------------------- */
    /*                            Initialize gstreamer                            */
    /* -------------------------------------------------------------------------- */
    gst::init()?;

    /* -------------------------------------------------------------------------- */
    /*                               Create elements                              */
    /* -------------------------------------------------------------------------- */
    let source = gst::ElementFactory::make("videotestsrc", Some("source"))?;
    let sink = gst::ElementFactory::make("autovideosink", Some("sink"))?;

    /* -------------------------------------------------------------------------- */
    /*                          Create an empty pipeline                          */
    /* -------------------------------------------------------------------------- */
    let pipeline = gst::Pipeline::new(Some("test-pipeline"));

    /* -------------------------------------------------------------------------- */
    /*                          Add elements to pipeline                          */
    /* -------------------------------------------------------------------------- */
    pipeline.add_many(&[&source, &sink])?;

    /* -------------------------------------------------------------------------- */
    /*                                Link elements                               */
    /* -------------------------------------------------------------------------- */
    source.link(&sink)?;

    /* -------------------------------------------------------------------------- */
    /*                         Change elements' properties                        */
    /* -------------------------------------------------------------------------- */
    source.set_property_from_str("pattern", "smpte");

    /* -------------------------------------------------------------------------- */
    /*                                Play pipeline                               */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Playing)?;

    /* -------------------------------------------------------------------------- */
    /*                     Handle messages from pipeline's bus                    */
    /* -------------------------------------------------------------------------- */
    let bus = pipeline.bus().unwrap();
    for msg in bus.iter_timed_filtered(
        gst::ClockTime::MAX,
        &[gst::MessageType::Eos, gst::MessageType::Error],
    ) {
        match msg.view() {
            gst::MessageView::Eos(_) => break,
            gst::MessageView::Error(err) => {
                eprintln!(
                    "Error from {:?}: {} ({:?})",
                    err.src(),
                    err.error(),
                    err.debug()
                );
                break;
            }
            _ => unreachable!(),
        }
    }

    /* -------------------------------------------------------------------------- */
    /*                                Stop playing                                */
    /* -------------------------------------------------------------------------- */
    pipeline.set_state(gst::State::Null)?;

    Ok(())
}
