//#!/usr/bin/env node
// Requires Node.js and the websocket module (WebSocket-Node):
//
//  - http://nodejs.org/
//  - https://github.com/theturtle32/WebSocket-Node

"use strict";

let http = require('http');
let WebSocketServer = require('websocket').server;

// Used for managing the text chat user list.

let connectionArray = [];

// Output logging information to console
function log(text) {
  let time = new Date();
  console.log("[" + time.toLocaleTimeString() + "] " + text);
}

function makeJsonMsg(src, dst, type, content) {
  return {
    date: Date.now(),
    src: src,
    dst: dst,
    data: {
      type: type,
      content: content,
    }
  };
}

// Scans the list of users and see if the specified name is unique. If it is,
// return true. Otherwise, returns false. We want all users to have unique
// names.
function isUsernameUnique(name) {
  let isUnique = true;

  for (let i = 0; i < connectionArray.length; i++) {
    if (connectionArray[i].username === name) {
      isUnique = false;
      break;
    }
  }
  return isUnique;
}

// Sends a message (which is already stringified JSON) to a single
// user, given their username. We use this for the WebRTC signaling,
// and we could use it for private text messaging.
function sendToOneUser(target, msgString) {
  for (let i = 0; i < connectionArray.length; i++) {
    if (connectionArray[i].username === target) {
      connectionArray[i].sendUTF(msgString);
      break;
    }
  }
}

// Builds a message object of type "userlist" which contains the names of
// all connected users. Used to ramp up newly logged-in users and,
// inefficiently, to handle name change notifications.
function makeUserListMessage() {
  let humans = [];
  let recorders = [];
  for (let i = 0; i < connectionArray.length; i++) {
    if (connectionArray[i].username.startsWith("recorder")) {
      recorders.push(connectionArray[i].username);
    } else {
      humans.push(connectionArray[i].username);
    }
  }
  return makeJsonMsg("server", "", "userlist", { 
    humans: humans,
    recorders, recorders,
  });
}

// Sends a "userlist" message to all chat members. This is a cheesy way
// to ensure that every join/drop is reflected everywhere. It would be more
// efficient to send simple join/drop messages to each user, but this is
// good enough for this simple example.
function sendUserListToAll() {
  let userListMsg = makeUserListMessage();
  let userListMsgStr = JSON.stringify(userListMsg);

  for (let i = 0; i < connectionArray.length; i++) {
    connectionArray[i].sendUTF(userListMsgStr);
  }
}

// If we were able to get the key and certificate files, try to
// start up an HTTPS server.

let webServer = null;
try {
  webServer = http.createServer({}, handleWebRequest);
} catch(err) {
  webServer = null;
  log(`Error attempting to create HTTP(s) server: ${err.toString()}`);
}

// Our HTTPS server does nothing but service WebSocket
// connections, so every request just returns 404. Real Web
// requests are handled by the main server on the box. If you
// want to, you can return real HTML here and serve Web content.

function handleWebRequest(request, response) {
  log("Received request for " + request.url);
  response.writeHead(404);
  response.end();
}

// Spin up the HTTPS server on the port assigned to this sample.
// This will be turned into a WebSocket port very shortly.

webServer.listen(8888, function() {
  log("Server is listening on port 8888");
});

// Create the WebSocket server by converting the HTTPS server into one.

let wsServer = new WebSocketServer({
  httpServer: webServer,
  autoAcceptConnections: false
});

if (!wsServer) {
  log("ERROR: Unable to create WbeSocket server!");
}

// Set up a "connect" message handler on our WebSocket server. This is
// called whenever a user connects to the server's port using the
// WebSocket protocol.

wsServer.on('request', function(request) {
  // Accept the request and get a connection.

  let connection = request.accept("json", request.origin);

  // Add the new connection to our list of connections.

  log("Connection accepted from " + connection.remoteAddress + ".");
  connectionArray.push(connection);

  // Set up a handler for the "message" event received over WebSocket. This
  // is a message sent by a client, and may be text to share with other
  // users, a private message (text or signaling) for one user, or a command
  // to the server.

  connection.on('message', function(message) {
    if (message.type === 'utf8') {
      log("Received Message: " + message.utf8Data);

      // Process incoming data.

      let msg = JSON.parse(message.utf8Data);

      let msgContent = msg.data.content;
      switch (msg.dst) {
      case "server":
        if (msg.data.type === "username") {
          // Prevent clients from using special names.
          if (msgContent.name === "broadcast" || msgContent.name === "server") {
            msgContent.name = "noname";
          }

          let nameChanged = false;
          let origName = msgContent.name;

          // Ensure the name is unique by appending a number to it
          // if it's not; keep trying that until it works.
          let appendToMakeUnique = 1;
          while (!isUsernameUnique(msgContent.name)) {
            msgContent.name = origName + appendToMakeUnique;
            appendToMakeUnique++;
            nameChanged = true;
          }

          // If the name had to be changed, we send a "reject-username"
          // message back to the user so they know their name has been
          // altered by the server.
          if (nameChanged) {
            let changeMsg = makeJsonMsg("server", "", "reject-username", { name: msgContent.name });
            connection.sendUTF(JSON.stringify(changeMsg));
          } else {
            let okMsg = makeJsonMsg("server", "", "username-ok", {});
            connection.sendUTF(JSON.stringify(okMsg));
          }

          // Set this connection's final username and send out the
          // updated user list to all users. Yeah, we're sending a full
          // list instead of just updating. It's horribly inefficient
          // but this is a demo. Don't do this in a real app.
          connection.username = msgContent.name;
          sendUserListToAll();
        }
        break;

      case "broadcast": {
        // Broadcast messages are only from users to recorders.
        if (connection.username && !connection.username.startsWith("recorder")) {
          // msg.dst is still broadcast.
          msg.src = connection.username;
          let msgString = JSON.stringify(msg);
          for (let i = 0; i < connectionArray.length; i++) {
            connectionArray[i].sendUTF(msgString);
          }
        }
        break;
      }

      default:
        if (msg.dst && msg.dst !== connection.username) {
          msg.src = connection.username;
          let msgString = JSON.stringify(msg);
          sendToOneUser(msg.dst, msgString);
        }
      }
    }
  });

  // Handle the WebSocket "close" event; this means a user has logged off
  // or has been disconnected.
  connection.on('close', function(reason, description) {
    // First, remove the connection from the list of connections.
    connectionArray = connectionArray.filter(function(el, idx, ar) {
      return el.connected;
    });

    // Now send the updated user list. Again, please don't do this in a
    // real application. Your users won't like you very much.
    sendUserListToAll();

    // Build and output log output for close information.
    log("Connection closed: " + connection.remoteAddress + " (" + reason + ": " + description + ")");
  });
});
