===================================================
 Trusted Execution Environment (TEE) & Secure Boot
===================================================

Trusted Execution Environment
=============================

Abbreviations
-------------

.. list-table::
   :header-rows: 1

   *  - Abbreviation
      - Meaning
   *  - CA
      - Client Application
   *  - OEM
      - Original Equipment Manufacturer
   *  - OTP
      - One Time Programmable
   *  - REE
      - Rich Execution Environment
   *  - RoT
      - Root of Trust
   *  - SE
      - Secure Element
   *  - TA
      - Trusted Application
   *  - TEE
      - Trusted Execution Environment
   *  - TPM
      - Trusted Platform Module

TEE Device Architecture Overview
--------------------------------

.. image:: tee/tee-fig-2.1.png
    :scale: 70
    :align: center

- The primary purpose of a TEE is to protect its assets from the REE and other environments.
- The Trusted OS run time environment is instantiated from a RoT inside the TEE through a secure boot process using assets bound to the TEE and isolated from the REE.
- The TEE provides Trusted Storage of data and keys.
- Software outside the TEE is not able to call directly to functionality exposed by the TEE Internal APIs or the Trusted Core Framework.

.. image:: tee/tee-fig-2.2.png
    :scale: 70
    :align: center

.. list-table:: **Example Hardware Realizations of TEE**

   *  - .. image:: tee/tee-fig-2.3-1.png
            :scale: 70
            :align: center
      - .. image:: tee/tee-fig-2.3-2.png
            :scale: 70
            :align: center
      - .. image:: tee/tee-fig-2.3-3.png
            :scale: 70
            :align: center
      - .. image:: tee/tee-fig-2.3-4.png
            :scale: 70
            :align: center

TEE Software Interfaces
-----------------------

.. image:: tee/tee-fig-3.1.png
    :scale: 70
    :align: center

- **REE Interfaces to the TEE**

  * **The REE Communication Agent** provides REE support for messaging between the Client Application and the Trusted Application.
  * **The TEE Client API** is a low level communication interface designed to enable a Client Application running in the Rich OS to access and exchange data with a Trusted Application running inside a Trusted Execution Environment.
  * **The TEE Protocol Specifications** layer exposed in the REE offers Client Applications a set of higher level APIs to access some TEE services

- **Trusted OS Components**

  * The **Trusted Core Framework** which provides OS functionality to Trusted Applications.
  * **Trusted Device Drivers** which provide a communications interface to trusted peripherals that are dedicated to the TEE.

- **Trusted Applications (TAs)**

  * When a Client Application creates a session with a Trusted Application, it connects to an instance of that Trusted Application.

    + A Trusted Application instance has physical memory address space which is separated from the physical memory address space of all other Trusted Application instances.

  * A session is used to logically connect multiple commands invoked in a Trusted Application.

    + Each session has its own state, which typically contains the session context and the context(s) of the Task(s) executing the session.

  * TAs can start execution only in response to an external command.
  * CA - TA can use shared memory to pass data to the other party.

- **The TEE Internal Core API**

  * Trusted Core Framework API
  * Trusted Storage API for Data and Keys
  * Crytographic Operations API
  * Time API
  * TEE Arithmetical API
  * Peripheral API
  * Event API

Typical Boot Sequence
---------------------

.. list-table:: **Boot Sequence**

   *  - .. image:: tee/tee-fig-5.1.png
            :scale: 70
            :align: center
      - .. image:: tee/tee-fig-5.2.png
            :scale: 70
            :align: center
      - .. image:: tee/tee-fig-5.3.png
            :scale: 70
            :align: center

Hardware Support
----------------

- AMD Platform Security Processor
- Intel Trusted Execution Technology
- ARM TrustZone
- ...

References
----------

- `TEE System Architecture - Public Release v1.2 <https://globalplatform.org/wp-content/uploads/2017/01/GPD_TEE_SystemArch_v1.2_PublicRelease.pdf>`__
- https://www.trustonic.com/technical-articles/what-is-a-trusted-execution-environment-tee/
- https://www.trustonic.com/technical-articles/benefits-trusted-user-interface/
- https://www.trustonic.com/technical-articles/secure-hypervisors-trusted-execution-environments/

ARM TrustZone
=============

The general idea is applications in Rich Execution Environment will communicate with Trustlets in Trusted Execution Environment through SMC :sub:`Secure Monitor Call` instruction.

In case of QSEE :sub:`Qualcomm Secure Execution Environment`, ``/dev/qseecom``:sub:`QSEE Communicator` is a character device that user applications can use to talk with the REE. That device, in turn, will use SMC instruction to interact with Secure OS and Trustlets in the TEE.

With Trustonic Kinibi implementation, the communication point is ``/dev/mobicore`` and ``/dev/mobicore-user``.

TODO


Trustonic Kinibi
----------------

TODO

- https://blog.quarkslab.com/a-deep-dive-into-samsungs-trustzone-part-1.html
- https://blog.quarkslab.com/a-deep-dive-into-samsungs-trustzone-part-2.html
- https://blog.quarkslab.com/a-deep-dive-into-samsungs-trustzone-part-3.html
- https://azeria-labs.com/trustonics-kinibi-tee-implementation/
- https://github.com/Trustonic/trustonic-tee-driver

References
----------

- https://www.trustonic.com/technical-articles/what-is-trustzone/
- https://azeria-labs.com/trusted-execution-environments-tee-and-trustzone/
- https://blog.quarkslab.com/introduction-to-trusted-execution-environment-arms-trustzone.html
- https://developer.arm.com/documentation/ddi0333/h/programmer-s-model/secure-world-and-non-secure-world-operation-with-trustzone/how-the-secure-model-works

Secure Boot
===========

Qualcomm
--------

Secure Boot Sequence
~~~~~~~~~~~~~~~~~~~~

.. image:: tee/qualcomm-secure-boot.svg
    :scale: 100
    :align: center

Excerpted from `Secure Boot and Image Authentication`_:

- The **XBL_SEC image** acts as a root of trust for all TrustZone images and the Qualcomm TEE in particular.
  
  Since the XBL_SEC image does not have direct access to the storage device, it relies on the XBL image to copy the Qualcomm TEE images from storage into the chip's memory.
  
  The XBL_SEC image is engineered to isolate the Qualcomm TEE image so that it can only accessed by XBL_SEC, before authenticating and executing the Qualcomm TEE image.

- The **XBL image** acts as a root of trust for all non-TrustZone images that will run on the chip, including

  * the Qualcomm® Hypervisor Execution Environment (QHEE),
  * the OS boot loader (e.g., UEFI),
  * the OS kernel (e.g., the Android kernel)
  * and the peripheral images (such as the Bluetooth and WLAN images)

  The XBL image will directly load those images or will be responsible for loading intermediate software which will load those images.

- Communication between the XBL image and the XBL_SEC image will use the standard ARM SMC mechanism designed to facilitate communication between an REE and a TEE.

- The Qualcomm TEE image may be signed by both QTI and the device manufacturer in a process known as double-signing. This is designed to ensure that this security-critical image can only be executed if it has been approved by QTI **and** the device manufacturer.


Image Format
~~~~~~~~~~~~

.. note::

    All images make use of the standard ELF. An ELF image consists of some number of individual ELF segments, which might contain code, data, or metadata about the image. The data that is used to authenticate that image is contained in a special segment within the ELF file, called the *hash segment*.

    --- `Secure Boot and Image Authentication`_

.. image:: tee/qualcomm-secure-boot-image-format.svg
    :scale: 100
    :align: center

.. note::

    The QTI signature and certificate chain should only be present if the image is double-signed by QTI and the device manufacturer.

    --- `Secure Boot and Image Authentication`_

Image Loading & Validation
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: tee/qualcomm-secure-boot-image-loading.svg
    :scale: 100
    :align: center

References
----------

- `Exploring Qualcomm's Secure Execution Environment <https://bits-please.blogspot.com/2016/04/exploring-qualcomms-secure-execution.html>`__
- `Secure Boot and Image Authentication`_
- `The road to Qualcomm TrustZone apps fuzzing <https://cfp.recon.cx/media/tz_apps_fuzz.pdf>`__

.. _Secure Boot and Image Authentication: https://www.qualcomm.com/media/documents/files/secure-boot-and-image-authentication-technical-overview-v2-0.pdf
