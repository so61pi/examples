===========
 GStreamer
===========

Installation
============

OpenSUSE
--------

.. code-block:: sh

    sudo zypper install gstreamer-utils \
        gstreamer-plugins-good gstreamer-plugins-good-extra \
        gstreamer-plugins-bad gstreamer-plugins-bad-orig-addon \
        gstreamer-plugins-ugly gstreamer-plugins-ugly-orig-addon \
        gstreamer-plugins-libav \
        v4l2loopback-utils

MacOS
-----

.. code-block:: sh

    brew install gstreamer \
        gst-plugins-good \
        gst-plugins-bad \
        gst-plugins-ugly

``v4l2-ctl``
============

``v4l2-ctl`` is used to control video4linux devices.

- Show driver info.

  .. code-block:: sh

      v4l2-ctl --device /dev/video1 --info

  .. code-block::
      :linenos:

      Driver Info:
              Driver name      : uvcvideo
              Card type        : UVC Camera (046d:0825)
              Bus info         : usb-0000:00:14.0-4
              Driver version   : 5.12.0
              Capabilities     : 0x84a00001
                      Video Capture
                      Metadata Capture
                      Streaming
                      Extended Pix Format
                      Device Capabilities
              Device Caps      : 0x04200001
                      Video Capture
                      Streaming
                      Extended Pix Format

- Display all controls and their menus.

  .. code-block:: sh

      v4l2-ctl --device /dev/video1 --list-ctrls-menus

  .. code-block::
      :linenos:

                           brightness 0x00980900 (int)    : min=0 max=255 step=1 default=128 value=128
                             contrast 0x00980901 (int)    : min=0 max=255 step=1 default=32 value=32
                           saturation 0x00980902 (int)    : min=0 max=255 step=1 default=32 value=32
       white_balance_temperature_auto 0x0098090c (bool)   : default=1 value=1
                                 gain 0x00980913 (int)    : min=0 max=255 step=1 default=64 value=64
                 power_line_frequency 0x00980918 (menu)   : min=0 max=2 default=2 value=2
                                      0: Disabled
                                      1: 50 Hz
                                      2: 60 Hz
            white_balance_temperature 0x0098091a (int)    : min=0 max=10000 step=10 default=4000 value=4000 flags=inactive
                            sharpness 0x0098091b (int)    : min=0 max=255 step=1 default=24 value=24
               backlight_compensation 0x0098091c (int)    : min=0 max=1 step=1 default=0 value=0
                        exposure_auto 0x009a0901 (menu)   : min=0 max=3 default=3 value=3
                                      1: Manual Mode
                                      3: Aperture Priority Mode
                    exposure_absolute 0x009a0902 (int)    : min=1 max=10000 step=1 default=166 value=166 flags=inactive
               exposure_auto_priority 0x009a0903 (bool)   : default=0 value=1

- Display all information of a device.

  .. code-block:: sh

      v4l2-ctl --device /dev/video1 --all

  .. code-block::
      :linenos:

      Driver Info:
              Driver name      : uvcvideo
              Card type        : UVC Camera (046d:0825)
              Bus info         : usb-0000:00:14.0-4
              Driver version   : 5.12.0
              Capabilities     : 0x84a00001
                      Video Capture
                      Metadata Capture
                      Streaming
                      Extended Pix Format
                      Device Capabilities
              Device Caps      : 0x04200001
                      Video Capture
                      Streaming
                      Extended Pix Format
      Priority: 2
      Video input : 0 (Camera 1: ok)
      Format Video Capture:
              Width/Height      : 640/480
              Pixel Format      : 'YUYV' (YUYV 4:2:2)
              Field             : None
              Bytes per Line    : 1280
              Size Image        : 614400
              Colorspace        : sRGB
              Transfer Function : Rec. 709
              YCbCr/HSV Encoding: ITU-R 601
              Quantization      : Default (maps to Limited Range)
              Flags             : 
      Crop Capability Video Capture:
              Bounds      : Left 0, Top 0, Width 640, Height 480
              Default     : Left 0, Top 0, Width 640, Height 480
              Pixel Aspect: 1/1
      Selection Video Capture: crop_default, Left 0, Top 0, Width 640, Height 480, Flags: 
      Selection Video Capture: crop_bounds, Left 0, Top 0, Width 640, Height 480, Flags: 
      Streaming Parameters Video Capture:
              Capabilities     : timeperframe
              Frames per second: 30.000 (30/1)
              Read buffers     : 0
                           brightness 0x00980900 (int)    : min=0 max=255 step=1 default=128 value=128
                             contrast 0x00980901 (int)    : min=0 max=255 step=1 default=32 value=32
                           saturation 0x00980902 (int)    : min=0 max=255 step=1 default=32 value=32
       white_balance_temperature_auto 0x0098090c (bool)   : default=1 value=1
                                 gain 0x00980913 (int)    : min=0 max=255 step=1 default=64 value=64
                 power_line_frequency 0x00980918 (menu)   : min=0 max=2 default=2 value=2
                                      0: Disabled
                                      1: 50 Hz
                                      2: 60 Hz
            white_balance_temperature 0x0098091a (int)    : min=0 max=10000 step=10 default=4000 value=4000 flags=inactive
                            sharpness 0x0098091b (int)    : min=0 max=255 step=1 default=24 value=24
               backlight_compensation 0x0098091c (int)    : min=0 max=1 step=1 default=0 value=0
                        exposure_auto 0x009a0901 (menu)   : min=0 max=3 default=3 value=3
                                      1: Manual Mode
                                      3: Aperture Priority Mode
                    exposure_absolute 0x009a0902 (int)    : min=1 max=10000 step=1 default=166 value=166 flags=inactive
               exposure_auto_priority 0x009a0903 (bool)   : default=0 value=1

- ``v4l2-ctl --help`` and ``v4l2-ctl --help-all``: Show help message.

- Get supported formats (video format, resolution, framerate) of a device.

  .. code-block:: sh

      v4l2-ctl --device /dev/video1 --list-formats-ext

  .. code-block::
      :linenos:

      ioctl: VIDIOC_ENUM_FMT
              Type: Video Capture

              [0]: 'YUYV' (YUYV 4:2:2)
                      Size: Discrete 640x480
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 160x120
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 176x144
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 320x176
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 320x240
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 352x288
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 432x240
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 544x288
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 640x360
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 752x416
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 800x448
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 800x600
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 864x480
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 960x544
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 960x720
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 1024x576
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 1184x656
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 1280x720
                              Interval: Discrete 0.133s (7.500 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 1280x960
                              Interval: Discrete 0.133s (7.500 fps)
                              Interval: Discrete 0.200s (5.000 fps)
              [1]: 'MJPG' (Motion-JPEG, compressed)
                      Size: Discrete 640x480
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 160x120
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 176x144
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 320x176
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 320x240
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 352x288
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 432x240
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 544x288
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 640x360
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 752x416
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 800x448
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 800x600
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 864x480
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 960x544
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 960x720
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 1024x576
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 1184x656
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 1280x720
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)
                      Size: Discrete 1280x960
                              Interval: Discrete 0.033s (30.000 fps)
                              Interval: Discrete 0.040s (25.000 fps)
                              Interval: Discrete 0.050s (20.000 fps)
                              Interval: Discrete 0.067s (15.000 fps)
                              Interval: Discrete 0.100s (10.000 fps)
                              Interval: Discrete 0.200s (5.000 fps)

- Get current video format

  .. code-block:: sh

      v4l2-ctl --device /dev/video1 --get-fmt-video --get-parm
      #                             ^^^^^^^^^^^^^^^ ^^^^^^^^^^
      #                             Get format      Get FPS

  .. code-block::
      :linenos:

      Format Video Capture:
              Width/Height      : 640/480
              Pixel Format      : 'YUYV' (YUYV 4:2:2)
              Field             : None
              Bytes per Line    : 1280
              Size Image        : 614400
              Colorspace        : sRGB
              Transfer Function : Rec. 709
              YCbCr/HSV Encoding: ITU-R 601
              Quantization      : Default (maps to Limited Range)
              Flags             : 
      Streaming Parameters Video Capture:
              Capabilities     : timeperframe
              Frames per second: 30.000 (30/1)
              Read buffers     : 0

- Set video format

  .. code-block:: sh

      v4l2-ctl --device /dev/video1 --set-fmt-video width=1280,height=960,pixelformat=YUYV --set-parm 7.5
      #                             ^^^^^^^^^^^^^^^                                        ^^^^^^^^^^
      #                             Set format                                             Set FPS

  .. code-block::
      :linenos:

      Frame rate set to 7.500 fps

``gst-launch``
==============

``gst-launch`` is a tool to construct and run GStreamer pipelines quickly.

https://gstreamer.freedesktop.org/documentation/tools/gst-launch.html?gi-language=c#pipeline-description

Create Test Video Stream And Display It
---------------------------------------

There are multiple types of patern one can use to create test video stream at https://gstreamer.freedesktop.org/documentation/videotestsrc/index.html?gi-language=c#GstVideoTestSrcPattern.

``smpte``
~~~~~~~~~

.. code-block:: sh

    gst-launch-1.0 -v videotestsrc ! autovideosink
    # - `smpte` is the default pattern

.. image:: gstreamer/videotestsrc-smpte.png
    :align: center

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:00:01.265555432
    Setting pipeline to NULL ...
    Freeing pipeline ...

``ball``
~~~~~~~~

.. code-block:: sh

    gst-launch-1.0 -v videotestsrc pattern=ball ! autovideosink

.. image:: gstreamer/videotestsrc-ball.png
    :align: center

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:00:01.962025862
    Setting pipeline to NULL ...
    Freeing pipeline ...

Capture Video And Display
-------------------------

.. code-block:: sh

    gst-launch-1.0 -v v4l2src device=/dev/video1 ! autovideosink
    # - By default, `device` is `/dev/video0`
    # - In Linux, `autovideosink` is `xvimagesink`
    # - In MacOS, one can use `autovideosrc` or `avfvideosrc` to select its webcam

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is live and does not need PREROLL ...
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    /GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)960, framerate=(fraction)15/2, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)2:4:5:1
    /GstPipeline:pipeline0/GstXvImageSink:xvimagesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)960, framerate=(fraction)15/2, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)2:4:5:1
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:00:17.544814441
    Setting pipeline to NULL ...
    Freeing pipeline ...

Caps Negotiation
----------------

.. code-block:: sh

    gst-launch-1.0 -v v4l2src device=/dev/video1 ! 'video/x-raw,width=640,height=480,framerate=30/1' ! autovideosink

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is live and does not need PREROLL ...
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    /GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, width=(int)640, height=(int)480, framerate=(fraction)30/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)2:4:16:1
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, width=(int)640, height=(int)480, framerate=(fraction)30/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)2:4:16:1
    /GstPipeline:pipeline0/GstXvImageSink:xvimagesink0.GstPad:sink: caps = video/x-raw, width=(int)640, height=(int)480, framerate=(fraction)30/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)2:4:16:1
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, width=(int)640, height=(int)480, framerate=(fraction)30/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)2:4:16:1
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:00:04.090171385
    Setting pipeline to NULL ...
    Freeing pipeline ...

References
~~~~~~~~~~

- https://gstreamer.freedesktop.org/documentation/plugin-development/advanced/negotiation.html?gi-language=c

Manipulate Video Stream
-----------------------

Display FPS
~~~~~~~~~~~

.. code-block:: sh

    gst-launch-1.0 -v videotestsrc ! 'video/x-raw,width=640,height=480,framerate=30/1' ! fpsdisplaysink

.. image:: gstreamer/manipuate-display-fps.png
    :align: center

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstAutoVideoSink:fps-display-video_sink/GstXvImageSink:fps-display-video_sink-actual-sink-xvimage: sync = true
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstTextOverlay:fps-display-text-overlay.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstAutoVideoSink:fps-display-video_sink.GstGhostPad:sink.GstProxyPad:proxypad1: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstAutoVideoSink:fps-display-video_sink/GstXvImageSink:fps-display-video_sink-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstAutoVideoSink:fps-display-video_sink.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstTextOverlay:fps-display-text-overlay.GstPad:video_sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstAutoVideoSink:fps-display-video_sink/GstXvImageSink:fps-display-video_sink-actual-sink-xvimage: sync = true
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstTextOverlay:fps-display-text-overlay: text = rendered: 17, dropped: 0, current: 33.89, average: 33.89
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0: last-message = rendered: 17, dropped: 0, current: 33.89, average: 33.89
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstTextOverlay:fps-display-text-overlay: text = rendered: 33, dropped: 0, current: 30.02, average: 31.90
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0: last-message = rendered: 33, dropped: 0, current: 30.02, average: 31.90
    /GstPipeline:pipeline0/GstFPSDisplaySink:fpsdisplaysink0/GstTextOverlay:fps-display-text-overlay: text = rendered: 48, dropped: 0, current: 29.88, average: 31.24
    ...
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:01:40.636514797
    Setting pipeline to NULL ...
    Freeing pipeline ...

Add Text Overlay
~~~~~~~~~~~~~~~~

.. code-block:: sh

    gst-launch-1.0 -v videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! textoverlay text="Host: $(hostname)" valignment=top halignment=left font-desc="Sans, 20" \
    ! autovideosink

.. image:: gstreamer/manipuate-add-text-overlay.png
    :align: center

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:00:58.421809235
    Setting pipeline to NULL ...
    Freeing pipeline ...

Add Time Overlay
~~~~~~~~~~~~~~~~

.. code-block:: sh

    gst-launch-1.0 -v videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480,framerate=60/1' \
    ! clockoverlay time-format='%F %T' valignment=top halignment=right font-desc="Sans, 20" \
    ! autovideosink

.. image:: gstreamer/manipuate-add-time-overlay.png
    :align: center

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)60/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)60/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)60/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)60/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)60/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)60/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)60/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)60/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:01:13.941724269
    Setting pipeline to NULL ...
    Freeing pipeline ...

Add Time & Text Overlay
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sh

    # `time-format` follows time spec from `strftime` https://www.cplusplus.com/reference/ctime/strftime/
    gst-launch-1.0 -v videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! clockoverlay time-format='%F %T' valignment=top halignment=right font-desc="Sans, 20" \
    ! timeoverlay valignment=top halignment=right ypad="60" font-desc="Sans, 20" color=0xFFFFFF00 \
    ! textoverlay text="Host: $(hostname | tr '[:lower:]' '[:upper:]' || echo -n UnknownHost)" valignment=top halignment=right font-desc="Sans, 20" ypad="95" color=0x7700FF00 \
    ! autovideosink

.. image:: gstreamer/manipuate-add-time-text-overlay.png
    :align: center

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:02:36.615805412
    Setting pipeline to NULL ...
    Freeing pipeline ...

Rescale Video
~~~~~~~~~~~~~

.. code-block:: sh

    # This will show 2 windows with a same video but different resolution.
    # Visit document of `tee` and `queue` to see why we need them, and why `queue` is placed after `tee`.
    gst-launch-1.0 -v videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' ! tee name=t \
    t. ! queue ! autovideosink \
    t. ! queue ! videoscale ! 'video/x-raw,width=320,height=240' ! autovideosink

    # In case we only want to see the result video, use this command.
    gst-launch-1.0 -v videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! videoscale ! 'video/x-raw,width=320,height=240' \
    ! autovideosink

.. image:: gstreamer/manipuate-rescale-video.png
    :align: center

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTee:t.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstQueue:queue0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTee:t.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstQueue:queue1.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstVideoScale:videoscale0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink1.GstGhostPad:sink.GstProxyPad:proxypad1: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTee:t.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink1/GstXvImageSink:autovideosink1-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink1.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)320, height=(int)240, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstVideoScale:videoscale0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:01:03.847731565
    Setting pipeline to NULL ...
    Freeing pipeline ...

Save Video To File
------------------

Simple
~~~~~~

.. code-block:: sh

    gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! x264enc ! mp4mux ! filesink location=/tmp/test-video.mp4

    # Play recorded video
    vlc /tmp/test-video.mp4

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Redistribute latency...
    /GstPipeline:pipeline0/GstX264Enc:x264enc0.GstPad:sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstX264Enc:x264enc0.GstPad:src: caps = video/x-h264, codec_data=(buffer)01f4001effe1001e67f4001e90d9b281407b602d41818190000003001000000303c8f162d96001000668ebec448440, stream-format=(string)avc, alignment=(string)au, level=(string)3, profile=(string)high-4:4:4, width=(int)640, height=(int)480, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, interlace-mode=(string)progressive, colorimetry=(string)bt601, chroma-site=(string)jpeg, multiview-mode=(string)mono, multiview-flags=(GstVideoMultiviewFlagsSet)0:ffffffff:/right-view-first/left-flipped/left-flopped/right-flipped/right-flopped/half-aspect/mixed-mono
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstQTMuxPad:video_0: caps = video/x-h264, codec_data=(buffer)01f4001effe1001e67f4001e90d9b281407b602d41818190000003001000000303c8f162d96001000668ebec448440, stream-format=(string)avc, alignment=(string)au, level=(string)3, profile=(string)high-4:4:4, width=(int)640, height=(int)480, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, interlace-mode=(string)progressive, colorimetry=(string)bt601, chroma-site=(string)jpeg, multiview-mode=(string)mono, multiview-flags=(GstVideoMultiviewFlagsSet)0:ffffffff:/right-view-first/left-flipped/left-flopped/right-flipped/right-flopped/half-aspect/mixed-mono
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    EOS on shutdown enabled -- Forcing EOS on the pipeline
    Waiting for EOS...
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso, streamheader=(buffer)< 000050876d6f6f7600...
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso, streamheader=(buffer)< 000050876d6f6f7600...
    Got EOS from element "pipeline0".
    EOS received - stopping pipeline...
    Execution ended after 0:00:06.650591425
    Setting pipeline to NULL ...
    Freeing pipeline ...

With Overlay
~~~~~~~~~~~~

.. code-block:: sh

    gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! clockoverlay time-format='%F %T' valignment=top halignment=right font-desc="Sans, 20" \
    ! timeoverlay valignment=top halignment=right ypad="60" font-desc="Sans, 20" color=0xFFFFFF00 \
    ! textoverlay text="Host: $(hostname | tr '[:lower:]' '[:upper:]' || echo -n UnknownHost)" valignment=top halignment=right font-desc="Sans, 20" ypad="95" color=0x7700FF00 \
    ! x264enc ! mp4mux ! filesink location=/tmp/test-video.mp4

    # Play recorded video
    gst-launch-1.0 playbin uri=file:///tmp/test-video.mp4

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Redistribute latency...
    /GstPipeline:pipeline0/GstX264Enc:x264enc0.GstPad:sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstX264Enc:x264enc0.GstPad:src: caps = video/x-h264, codec_data=(buffer)01f4001effe1001e67f4001e90d9b281407b602d41818190000003001000000303c8f162d96001000668ebec448440, stream-format=(string)avc, alignment=(string)au, level=(string)3, profile=(string)high-4:4:4, width=(int)640, height=(int)480, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, interlace-mode=(string)progressive, colorimetry=(string)bt601, chroma-site=(string)jpeg, multiview-mode=(string)mono, multiview-flags=(GstVideoMultiviewFlagsSet)0:ffffffff:/right-view-first/left-flipped/left-flopped/right-flipped/right-flopped/half-aspect/mixed-mono
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstQTMuxPad:video_0: caps = video/x-h264, codec_data=(buffer)01f4001effe1001e67f4001e90d9b281407b602d41818190000003001000000303c8f162d96001000668ebec448440, stream-format=(string)avc, alignment=(string)au, level=(string)3, profile=(string)high-4:4:4, width=(int)640, height=(int)480, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, interlace-mode=(string)progressive, colorimetry=(string)bt601, chroma-site=(string)jpeg, multiview-mode=(string)mono, multiview-flags=(GstVideoMultiviewFlagsSet)0:ffffffff:/right-view-first/left-flipped/left-flopped/right-flipped/right-flopped/half-aspect/mixed-mono
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    EOS on shutdown enabled -- Forcing EOS on the pipeline
    Waiting for EOS...
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso, streamheader=(buffer)< 000014136d6f6f7600...
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso, streamheader=(buffer)< 000014136d6f6f7600..
    Got EOS from element "pipeline0".
    EOS received - stopping pipeline...
    Execution ended after 0:00:02.230464294
    Setting pipeline to NULL ...
    Freeing pipeline ...

Display & Save
~~~~~~~~~~~~~~

.. code-block:: sh

    # The reason for `tune=zerolatency`, and why without it the pipeline will stall.
    #
    #  https://gstreamer.freedesktop.org/documentation/coreelements/queue.html?gi-language=c
    #   > Any attempt to push more buffers into the queue will block the pushing thread until more space becomes available.
    #
    #  https://gstreamer.freedesktop.org/documentation/x264/index.html?gi-language=c
    #   > Some settings, including the default settings, may lead to quite some latency (i.e. frame buffering) in the encoder.
    #   > This may cause problems with pipeline stalling in non-trivial pipelines, because the encoder latency is often
    #   > considerably higher than the default size of a simple queue element. Such problems are caused by one of the queues
    #   > in the other non-x264enc streams/branches filling up and blocking upstream.
    #
    #  https://stackoverflow.com/questions/49398734/why-does-on-screen-video-fail-to-update-unless-i-have-two-queues
    #   > The x264enc example is especially tricky. The problem you face here is that the encoder consumes too much data
    #   > but not producing anything (yet) effectively stalling the pipeline.
    gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! clockoverlay time-format='%F %T' valignment=top halignment=right font-desc="Sans, 20" \
    ! timeoverlay valignment=top halignment=right ypad="60" font-desc="Sans, 20" color=0xFFFFFF00 \
    ! textoverlay text="Host: $(hostname | tr '[:lower:]' '[:upper:]' || echo -n UnknownHost)" valignment=top halignment=right font-desc="Sans, 20" ypad="95" color=0x7700FF00 \
    ! tee name=t \
    t. ! queue ! autovideosink \
    t. ! queue ! videoconvert ! x264enc tune=zerolatency ! mp4mux ! filesink location=/tmp/test-video.mp4

    # Play recorded video
    gst-launch-1.0 playbin uri=file:///tmp/test-video.mp4

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:src: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:src: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:src: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTee:t.GstTeePad:src_0: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTee:t.GstTeePad:src_1: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTee:t.GstTeePad:src_1: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTee:t.GstPad:sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstQueue:queue1.GstPad:src: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Redistribute latency...
    /GstPipeline:pipeline0/GstX264Enc:x264enc0.GstPad:sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)YV12, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstX264Enc:x264enc0.GstPad:src: caps = video/x-h264, codec_data=(buffer)0164001effe1001c6764001eacb201407b602d418181a940000003004000000f23c58b9201000568ebccb22c, stream-format=(string)avc, alignment=(string)au, level=(string)3, profile=(string)high, width=(int)640, height=(int)480, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, interlace-mode=(string)progressive, colorimetry=(string)bt601, chroma-site=(string)jpeg, multiview-mode=(string)mono, multiview-flags=(GstVideoMultiviewFlagsSet)0:ffffffff:/right-view-first/left-flipped/left-flopped/right-flipped/right-flopped/half-aspect/mixed-mono
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstQTMuxPad:video_0: caps = video/x-h264, codec_data=(buffer)0164001effe1001c6764001eacb201407b602d418181a940000003004000000f23c58b9201000568ebccb22c, stream-format=(string)avc, alignment=(string)au, level=(string)3, profile=(string)high, width=(int)640, height=(int)480, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, interlace-mode=(string)progressive, colorimetry=(string)bt601, chroma-site=(string)jpeg, multiview-mode=(string)mono, multiview-flags=(GstVideoMultiviewFlagsSet)0:ffffffff:/right-view-first/left-flipped/left-flopped/right-flipped/right-flopped/half-aspect/mixed-mono
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    EOS on shutdown enabled -- Forcing EOS on the pipeline
    Waiting for EOS...
    /GstPipeline:pipeline0/GstMP4Mux:mp4mux0.GstAggregatorPad:src: caps = video/quicktime, variant=(string)iso, streamheader=(buffer)< 000007886d6f6f7600...
    /GstPipeline:pipeline0/GstFileSink:filesink0.GstPad:sink: caps = video/quicktime, variant=(string)iso, streamheader=(buffer)< 000007886d6f6f7600...
    Got EOS from element "pipeline0".
    EOS received - stopping pipeline...
    Execution ended after 0:00:08.733534263
    Setting pipeline to NULL ...
    Freeing pipeline ...

Stream Video
------------

Ogg
~~~

.. code-block:: sh

    gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! clockoverlay time-format='%F %T' valignment=top halignment=right font-desc="Sans, 20" \
    ! timeoverlay valignment=top halignment=right ypad="60" font-desc="Sans, 20" color=0xFFFFFF00 \
    ! textoverlay text="Host: $(hostname | tr '[:lower:]' '[:upper:]' || echo -n UnknownHost)" valignment=top halignment=right font-desc="Sans, 20" ypad="95" color=0x7700FF00 \
    ! videoconvert \
    ! theoraenc ! oggmux \
    ! tcpserversink host=127.0.0.1 port=9876

.. literalinclude:: gstreamer/gst-stream.html
    :language: html
    :caption: gst-stream.html

.. image:: gstreamer/stream-video-ogg.png
    :align: center

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstTCPServerSink:tcpserversink0: current-port = 9876
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTheoraEnc:theoraenc0.GstPad:sink: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)Y444, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTheoraEnc:theoraenc0.GstPad:src: caps = video/x-theora, streamheader=(buffer)< 807468656f72610...
    /GstPipeline:pipeline0/GstOggMux:oggmux0.GstPad:video_1509923830: caps = video/x-theora, streamheader=(buffer)< 807468656f72610...
    /GstPipeline:pipeline0/GstOggMux:oggmux0.GstPad:src: caps = application/ogg, streamheader=(buffer)< 4f676753000200...
    /GstPipeline:pipeline0/GstTCPServerSink:tcpserversink0.GstPad:sink: caps = application/ogg, streamheader=(buffer)< 4f676753000200...
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    EOS on shutdown enabled -- Forcing EOS on the pipeline
    Waiting for EOS...
    Got EOS from element "pipeline0".
    EOS received - stopping pipeline...
    Execution ended after 0:00:29.233532044
    Setting pipeline to NULL ...
    Freeing pipeline ...

H264 RTSP
~~~~~~~~~

Sending
```````

.. code-block:: sh

    gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! clockoverlay time-format='%F %T' valignment=top halignment=right font-desc="Sans, 20" \
    ! timeoverlay valignment=top halignment=right ypad="60" font-desc="Sans, 20" color=0xFFFFFF00 \
    ! textoverlay text="Host: $(hostname | tr '[:lower:]' '[:upper:]' || echo -n UnknownHost)" valignment=top halignment=right font-desc="Sans, 20" ypad="95" color=0x7700FF00 \
    ! x264enc tune=zerolatency speed-preset=superfast \
    ! rtph264pay \
    ! udpsink host=127.0.0.1 port=5000

    # Alternative
    gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' \
    ! clockoverlay time-format='%F %T' valignment=top halignment=right font-desc="Sans, 20" \
    ! timeoverlay valignment=top halignment=right ypad="60" font-desc="Sans, 20" color=0xFFFFFF00 \
    ! textoverlay text="Host: $(hostname | tr '[:lower:]' '[:upper:]' || echo -n UnknownHost)" valignment=top halignment=right font-desc="Sans, 20" ypad="95" color=0x7700FF00 \
    ! openh264enc \
    ! rtph264pay \
    ! udpsink host=127.0.0.1 port=5000

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is PREROLLING ...
    /GstPipeline:pipeline0/GstVideoTestSrc:videotestsrc0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    Redistribute latency...
    /GstPipeline:pipeline0/GstX264Enc:x264enc0.GstPad:sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTextOverlay:textoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstTimeOverlay:timeoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstClockOverlay:clockoverlay0.GstPad:video_sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, framerate=(fraction)30/1, multiview-mode=(string)mono, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive
    /GstPipeline:pipeline0/GstX264Enc:x264enc0.GstPad:src: caps = video/x-h264, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, stream-format=(string)avc, alignment=(string)au, level=(string)3, profile=(string)high-4:4:4, width=(int)640, height=(int)480, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, interlace-mode=(string)progressive, colorimetry=(string)bt601, chroma-site=(string)jpeg, multiview-mode=(string)mono, multiview-flags=(GstVideoMultiviewFlagsSet)0:ffffffff:/right-view-first/left-flipped/left-flopped/right-flipped/right-flopped/half-aspect/mixed-mono
    /GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, profile-level-id=(string)f4001e, sprop-parameter-sets=(string)"Z/QAHpDZaAoD2wFqDAwMgAAAAwCAAAAeR4sXUA\=\=\,aO8xkhk\=", payload=(int)96, ssrc=(uint)323464798, timestamp-offset=(uint)1162624535, seqnum-offset=(uint)18195, a-framerate=(string)30
    /GstPipeline:pipeline0/GstUDPSink:udpsink0.GstPad:sink: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, profile-level-id=(string)f4001e, sprop-parameter-sets=(string)"Z/QAHpDZaAoD2wFqDAwMgAAAAwCAAAAeR4sXUA\=\=\,aO8xkhk\=", payload=(int)96, ssrc=(uint)323464798, timestamp-offset=(uint)1162624535, seqnum-offset=(uint)18195, a-framerate=(string)30
    /GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:sink: caps = video/x-h264, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, stream-format=(string)avc, alignment=(string)au, level=(string)3, profile=(string)high-4:4:4, width=(int)640, height=(int)480, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, interlace-mode=(string)progressive, colorimetry=(string)bt601, chroma-site=(string)jpeg, multiview-mode=(string)mono, multiview-flags=(GstVideoMultiviewFlagsSet)0:ffffffff:/right-view-first/left-flipped/left-flopped/right-flipped/right-flopped/half-aspect/mixed-mono
    /GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: timestamp = 1162624535
    /GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: seqnum = 18195
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    EOS on shutdown enabled -- Forcing EOS on the pipeline
    Waiting for EOS...
    Got EOS from element "pipeline0".
    EOS received - stopping pipeline...
    Execution ended after 0:00:03.103056290
    Setting pipeline to NULL ...
    Freeing pipeline ...

Receiving
`````````

.. code-block:: sh

    gst-launch-1.0 -v udpsrc port=5000 caps="application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" \
    ! rtph264depay \
    ! decodebin \
    ! videoconvert \
    ! autovideosink

    # Alternative
    #  Usage of h264parse - http://gstreamer-devel.966125.n4.nabble.com/Usage-of-h264parse-td4671122.html
    gst-launch-1.0 -v udpsrc port=5000 caps="application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" \
    ! rtph264depay \
    ! h264parse \
    ! openh264dec \
    ! videoconvert \
    ! autovideosink

.. code-block::
    :linenos:

    Setting pipeline to PAUSED ...
    Pipeline is live and does not need PREROLL ...
    Pipeline is PREROLLED ...
    Setting pipeline to PLAYING ...
    New clock: GstSystemClock
    /GstPipeline:pipeline0/GstUDPSrc:udpsrc0.GstPad:src: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96
    /GstPipeline:pipeline0/GstRtpH264Depay:rtph264depay0.GstPad:sink: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96
    /GstPipeline:pipeline0/GstRtpH264Depay:rtph264depay0.GstPad:src: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0.GstGhostPad:sink.GstProxyPad:proxypad0: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0/GstTypeFindElement:typefind.GstPad:src: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0/GstH264Parse:h264parse0.GstPad:src: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4, pixel-aspect-ratio=(fraction)1/1, width=(int)640, height=(int)480, framerate=(fraction)30/1, chroma-format=(string)4:4:4, bit-depth-luma=(uint)10, bit-depth-chroma=(uint)10, colorimetry=(string)bt601, parsed=(boolean)true
    Redistribute latency...
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0/avdec_h264:avdec_h264-0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4, pixel-aspect-ratio=(fraction)1/1, width=(int)640, height=(int)480, framerate=(fraction)30/1, chroma-format=(string)4:4:4, bit-depth-luma=(uint)10, bit-depth-chroma=(uint)10, colorimetry=(string)bt601, parsed=(boolean)true
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4, pixel-aspect-ratio=(fraction)1/1, width=(int)640, height=(int)480, framerate=(fraction)30/1, chroma-format=(string)4:4:4, bit-depth-luma=(uint)10, bit-depth-chroma=(uint)10, colorimetry=(string)bt601, parsed=(boolean)true
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4, pixel-aspect-ratio=(fraction)1/1, width=(int)640, height=(int)480, framerate=(fraction)30/1, chroma-format=(string)4:4:4, bit-depth-luma=(uint)10, bit-depth-chroma=(uint)10, colorimetry=(string)bt601, parsed=(boolean)true
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0/GstH264Parse:h264parse0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0/GstTypeFindElement:typefind.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0.GstGhostPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, codec_data=(buffer)01f4001effe1001c67f4001e90d9680a03db016a0c0c0c80000003008000001e478b175001000568ef319219, level=(string)3, profile=(string)high-4:4:4
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0/avdec_h264:avdec_h264-0.GstPad:src: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt601, framerate=(fraction)30/1
    /GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, width=(int)640, height=(int)480, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, format=(string)YUY2
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink.GstProxyPad:proxypad1: caps = video/x-raw, width=(int)640, height=(int)480, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, format=(string)YUY2
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage.GstPad:sink: caps = video/x-raw, width=(int)640, height=(int)480, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, format=(string)YUY2
    /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0.GstGhostPad:sink: caps = video/x-raw, width=(int)640, height=(int)480, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)30/1, format=(string)YUY2
    /GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt601, framerate=(fraction)30/1
    /GstPipeline:pipeline0/GstDecodeBin:decodebin0.GstDecodePad:src_0.GstProxyPad:proxypad2: caps = video/x-raw, format=(string)Y444_10LE, width=(int)640, height=(int)480, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt601, framerate=(fraction)30/1
    Redistribute latency...
    WARNING: from element /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage: Pipeline construction is invalid, please add queues.
    Additional debug info:
    ../libs/gst/base/gstbasesink.c(1249): gst_base_sink_query_latency (): /GstPipeline:pipeline0/GstAutoVideoSink:autovideosink0/GstXvImageSink:autovideosink0-actual-sink-xvimage:
    Not enough buffering available for  the processing deadline of 0:00:00.015000000, add enough queues to buffer  0:00:00.015000000 additional data. Shortening processing latency to 0:00:00.000000000.
    ^Chandling interrupt.
    Interrupt: Stopping pipeline ...
    Execution ended after 0:00:17.628258769
    Setting pipeline to NULL ...
    Freeing pipeline ...

Debugging
---------

Tracing
~~~~~~~

.. code-block:: sh

    # This traces all events and messages in our pipeline.
    GST_DEBUG="GST_TRACER:7,GST_EVENT:7,GST_MESSAGE:7" GST_TRACERS="log" GST_DEBUG_FILE=trace.log \
    gst-launch-1.0 -v --eos-on-shutdown videotestsrc pattern=ball name=video-test-src \
    ! 'video/x-raw,width=640,height=480' \
    ! x264enc name=x264-encoder \
    ! mp4mux name=mp4-muxer \
    ! filesink location=/tmp/test-video.mp4 name=file-sink

    cat trace.log

The `trace.log` can be downloaded :download:`here <gstreamer/trace.log>`.

Generate ``dot`` Files
~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sh

    export GST_DEBUG_DUMP_DOT_DIR=/tmp/

    gst-launch-1.0 -v videotestsrc pattern=ball \
    ! 'video/x-raw,width=640,height=480' ! tee name=t \
    t. ! queue ! autovideosink \
    t. ! queue ! videoscale ! 'video/x-raw,width=320,height=240' ! autovideosink

.. code-block:: sh

    ls -alh /tmp/*gst-launch*

.. code-block::
    :linenos:

    -rw-r--r-- 1 so61pi users 13K May 11 00:36 /tmp/0.00.00.019847142-gst-launch.NULL_READY.dot
    -rw-r--r-- 1 so61pi users 13K May 11 00:36 /tmp/0.00.00.031916197-gst-launch.READY_PAUSED.dot
    -rw-r--r-- 1 so61pi users 13K May 11 00:36 /tmp/0.00.00.032905813-gst-launch.PAUSED_PLAYING.dot
    -rw-r--r-- 1 so61pi users 13K May 11 00:36 /tmp/0.00.04.331805354-gst-launch.PLAYING_PAUSED.dot
    -rw-r--r-- 1 so61pi users 13K May 11 00:36 /tmp/0.00.04.333304054-gst-launch.PAUSED_READY.dot

``NULL_READY``
``````````````

  .. image:: gstreamer/gst-launch.1.NULL_READY.dot.svg
      :align: center

``READY_PAUSED``
````````````````

  .. image:: gstreamer/gst-launch.2.READY_PAUSED.dot.svg
      :align: center

``PAUSED_PLAYING``
``````````````````

  .. image:: gstreamer/gst-launch.3.PAUSED_PLAYING.dot.svg
      :align: center

``PLAYING_PAUSED``
``````````````````

  .. image:: gstreamer/gst-launch.4.PLAYING_PAUSED.dot.svg
      :align: center

``PAUSED_READY``
````````````````

  .. image:: gstreamer/gst-launch.5.PAUSED_READY.dot.svg
      :align: center

``gst-inspect``
===============

Print All Plugins And Elements
------------------------------

.. code-block:: sh

    gst-inspect-1.0

.. code-block::
    :linenos:

    libav:  avenc_comfortnoise: libav RFC 3389 comfort noise generator encoder
    libav:  avenc_s302m: libav SMPTE 302M encoder
    libav:  avenc_aac: libav AAC (Advanced Audio Coding) encoder
    libav:  avenc_ac3: libav ATSC A/52A (AC-3) encoder
    libav:  avenc_ac3_fixed: libav ATSC A/52A (AC-3) encoder
    libav:  avenc_alac: libav ALAC (Apple Lossless Audio Codec) encoder
    libav:  avenc_aptx: libav aptX (Audio Processing Technology for Bluetooth) encoder
    libav:  avenc_aptx_hd: libav aptX HD (Audio Processing Technology for Bluetooth) encoder
    libav:  avenc_dca: libav DCA (DTS Coherent Acoustics) encoder
    libav:  avenc_eac3: libav ATSC A/52 E-AC-3 encoder
    ...

Print Plugin Info
-----------------

.. code-block:: sh

    gst-inspect-1.0 libav

.. code-block::
    :linenos:

    Plugin Details:
      Name                     libav
      Description              All libav codecs and formats (system install)
      Filename                 /usr/lib64/gstreamer-1.0/libgstlibav.so
      Version                  1.18.4
      License                  LGPL
      Source module            gst-libav
      Source release date      2021-03-15
      Binary package           openSUSE GStreamer-plugins-libav package
      Origin URL               http://download.opensuse.org

      avenc_comfortnoise: libav RFC 3389 comfort noise generator encoder
      avenc_s302m: libav SMPTE 302M encoder
      avenc_aac: libav AAC (Advanced Audio Coding) encoder
      ...
      avmux_wtv: libav Windows Television (WTV) muxer
      avmux_yuv4mpegpipe: libav YUV4MPEG pipe muxer (not recommended, use y4menc instead)
      avdeinterlace: libav Deinterlace element

      653 features:
      +-- 629 elements
      +-- 24 typefinders

Print Element Info
------------------

.. code-block:: sh

    gst-inspect-1.0 avenc_comfortnoise

.. code-block::
    :linenos:

    Factory Details:
      Rank                     secondary (128)
      Long-name                libav RFC 3389 comfort noise generator encoder
      Klass                    Codec/Encoder/Audio
      Description              libav comfortnoise encoder
      Author                   Wim Taymans <wim.taymans@gmail.com>, Ronald Bultje <rbultje@ronald.bitfreak.net>

    Plugin Details:
      Name                     libav
      Description              All libav codecs and formats (system install)
      Filename                 /usr/lib64/gstreamer-1.0/libgstlibav.so
      Version                  1.18.4
      License                  LGPL
      Source module            gst-libav
      Source release date      2021-03-15
      Binary package           openSUSE GStreamer-plugins-libav package
      Origin URL               http://download.opensuse.org

    GObject
    +----GInitiallyUnowned
          +----GstObject
                +----GstElement
                      +----GstAudioEncoder
                            +----avenc_comfortnoise

    Implemented Interfaces:
      GstPreset

    Pad Templates:
      SRC template: 'src'
        Availability: Always
        Capabilities:
          unknown/unknown
      
      SINK template: 'sink'
        Availability: Always
        Capabilities:
          audio/x-raw
                  channels: [ 1, 2 ]
                      rate: [ 4000, 96000 ]
                    format: S16LE
                    layout: interleaved

    Element has no clocking capabilities.
    Element has no URI handling capabilities.

    Pads:
      SINK: 'sink'
        Pad Template: 'sink'
      SRC: 'src'
        Pad Template: 'src'

    Element Properties:
      ac                  : set number of audio channels (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: 0 - 2147483647 Default: 0 
      ar                  : set audio sampling rate (in Hz) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: 0 - 2147483647 Default: 0 
      audio-service-type  : audio service type (Generic codec option, might have no effect)
                            flags: readable, writable
                            Enum "avcodeccontext-audio-service-type" Default: 0, "ma"
                              (0): ma               - Main Audio Service
                              (1): ef               - Effects
                              (2): vi               - Visually Impaired
                              (3): hi               - Hearing Impaired
                              (4): di               - Dialogue
                              (5): co               - Commentary
                              (6): em               - Emergency
                              (7): vo               - Voice Over
                              (8): ka               - Karaoke
      bitrate             : set bitrate (in bits/s) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: 0 - 2147483647 Default: 128000 
      bufsize             : set ratecontrol buffer size (in bits) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: -2147483648 - 2147483647 Default: 0 
      channel-layout      : (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Unsigned Integer64. Range: 0 - 18446744073709550000 Default: 0 
      compression-level   : (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: -2147483648 - 2147483647 Default: -1 
      cutoff              : set cutoff bandwidth (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: -2147483648 - 2147483647 Default: 0 
      debug               : print specific debug info (Generic codec option, might have no effect)
                            flags: readable, writable
                            Flags "avcodeccontext-debug" Default: 0x00000000, "(none)"
                              (0x00000001): pict             - picture info
                              (0x00000002): rc               - rate control
                              (0x00000004): bitstream        - bitstream
                              (0x00000008): mb_type          - macroblock (MB) type
                              (0x00000010): qp               - per-block quantization parameter (QP)
                              (0x00000040): dct_coeff        - dct_coeff
                              (0x00000080): skip             - skip
                              (0x00000100): startcode        - startcode
                              (0x00000400): er               - error recognition
                              (0x00000800): mmco             - memory management control operations (H.264)
                              (0x00001000): bugs             - bugs
                              (0x00008000): buffers          - picture buffer allocations
                              (0x00010000): thread_ops       - threading operations
                              (0x00800000): green_metadata   - green_metadata
                              (0x01000000): nomc             - skip motion compensation
      dump-separator      : set information dump field separator (Generic codec option, might have no effect)
                            flags: readable, writable
                            String. Default: null
      err-detect          : set error detection flags (Generic codec option, might have no effect)
                            flags: readable, writable
                            Flags "avcodeccontext-err-detect" Default: 0x00000000, "(none)"
                              (0x00000001): crccheck         - verify embedded CRCs
                              (0x00000002): bitstream        - detect bitstream specification deviations
                              (0x00000004): buffer           - detect improper bitstream length
                              (0x00000008): explode          - abort decoding on minor error detection
                              (0x00008000): ignore_err       - ignore errors
                              (0x00010000): careful          - consider things that violate the spec, are fast to check and have not been seen in the wild as errors
                              (0x00030000): compliant        - consider all spec non compliancies as errors
                              (0x00070000): aggressive       - consider things that a sane encoder should not do as an error
      export-side-data    : Export metadata as side data (Generic codec option, might have no effect)
                            flags: readable, writable
                            Flags "avcodeccontext-export-side-data" Default: 0x00000000, "(none)"
                              (0x00000001): mvs              - export motion vectors through frame side data
                              (0x00000002): prft             - export Producer Reference Time through packet side data
                              (0x00000004): venc_params      - export video encoding parameters through frame side data
                              (0x00000008): film_grain       - export film grain parameters through frame side data
      flags               : (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Flags "avcodeccontext-flags" Default: 0x00000000, "(none)"
                              (0x00000001): unaligned        - allow decoders to produce unaligned output
                              (0x00000004): mv4              - use four motion vectors per macroblock (MPEG-4)
                              (0x00000008): output_corrupt   - Output even potentially corrupted frames
                              (0x00000010): qpel             - use 1/4-pel motion compensation
                              (0x00000020): drop_changed     - Drop frames whose parameters differ from first decoded frame
                              (0x00000800): loop             - use loop filter
                              (0x00002000): gray             - only decode/encode grayscale
                              (0x00008000): psnr             - error[?] variables will be set during encoding
                              (0x00010000): truncated        - Input bitstream might be randomly truncated
                              (0x00040000): ildct            - use interlaced DCT
                              (0x00080000): low_delay        - force low delay
                              (0x00400000): global_header    - place global headers in extradata instead of every keyframe
                              (0x00800000): bitexact         - use only bitexact functions (except (I)DCT)
                              (0x01000000): aic              - H.263 advanced intra coding / MPEG-4 AC prediction
                              (0x20000000): ilme             - interlaced motion estimation
                              (0x80000000): cgop             - closed GOP
      flags2              : (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Flags "avcodeccontext-flags2" Default: 0x00000000, "(none)"
                              (0x00000001): fast             - allow non-spec-compliant speedup tricks
                              (0x00000004): noout            - skip bitstream encoding
                              (0x00000008): local_header     - place global headers at every keyframe instead of in extradata
                              (0x00008000): chunks           - Frame data might be split into multiple chunks
                              (0x00010000): ignorecrop       - ignore cropping information from sps
                              (0x00400000): showall          - Show all frames before the first keyframe
                              (0x10000000): export_mvs       - export motion vectors through frame side data
                              (0x20000000): skip_manual      - do not skip samples and export skip information as frame side data
                              (0x40000000): ass_ro_flush_noop - do not reset ASS ReadOrder field on flush
      frame-size          : (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: 0 - 2147483647 Default: 0 
      global-quality      : (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: -2147483648 - 2147483647 Default: 0 
      hard-resync         : Perform clipping and sample flushing upon discontinuity
                            flags: readable, writable
                            Boolean. Default: false
      mark-granule        : Apply granule semantics to buffer metadata (implies perfect-timestamp)
                            flags: readable
                            Boolean. Default: false
      max-pixels          : Maximum number of pixels (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer64. Range: 0 - 2147483647 Default: 2147483647 
      max-prediction-order: (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: -2147483648 - 2147483647 Default: -1 
      max-samples         : Maximum number of samples (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer64. Range: 0 - 2147483647 Default: 2147483647 
      maxrate             : maximum bitrate (in bits/s). Used for VBV together with bufsize. (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer64. Range: 0 - 2147483647 Default: 0 
      min-prediction-order: (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: -2147483648 - 2147483647 Default: -1 
      minrate             : minimum bitrate (in bits/s). Most useful in setting up a CBR encode. It is of little use otherwise. (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer64. Range: -2147483648 - 2147483647 Default: 0 
      name                : The name of the object
                            flags: readable, writable, 0x2000
                            String. Default: "avenc_comfortnoise0"
      parent              : The parent of the object
                            flags: readable, writable, 0x2000
                            Object of type "GstObject"
      perfect-timestamp   : Favour perfect timestamps over tracking upstream timestamps
                            flags: readable, writable
                            Boolean. Default: false
      side-data-only-packets: (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Boolean. Default: true
      strict              : how strictly to follow the standards (Generic codec option, might have no effect)
                            flags: readable, writable
                            Enum "avcodeccontext-strict" Default: 0, "normal"
                              (-2): experimental     - allow non-standardized experimental things
                              (-1): unofficial       - allow unofficial extensions
                              (0): normal           - normal
                              (1): strict           - strictly conform to all the things in the spec no matter what the consequences
                              (2): very             - strictly conform to a older more strict version of the spec or reference software
      thread-type         : select multithreading type (Generic codec option, might have no effect)
                            flags: readable, writable
                            Flags "avcodeccontext-thread-type" Default: 0x00000003, "slice+frame"
                              (0x00000001): frame            - frame
                              (0x00000002): slice            - slice
      threads             : set the number of threads (Generic codec option, might have no effect)
                            flags: readable, writable
                            Enum "avcodeccontext-threads" Default: 1, "unknown"
                              (0): auto             - autodetect a suitable number of threads to use
                              (1): unknown          - Unspecified
      ticks-per-frame     : (null) (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: 1 - 2147483647 Default: 1 
      tolerance           : Consider discontinuity if timestamp jitter/imperfection exceeds tolerance (ns)
                            flags: readable, writable
                            Integer64. Range: 0 - 9223372036854775807 Default: 40000000 
      trellis             : rate-distortion optimal quantization (Generic codec option, might have no effect)
                            flags: readable, writable
                            Integer. Range: -2147483648 - 2147483647 Default: 0 

Print Everything
----------------

.. code-block:: sh

    gst-inspect-1.0 --print-all

.. code-block::
    :linenos:

    avenc_comfortnoise: Factory Details:
    avenc_comfortnoise:   Rank                     secondary (128)
    avenc_comfortnoise:   Long-name                libav RFC 3389 comfort noise generator encoder
    avenc_comfortnoise:   Klass                    Codec/Encoder/Audio
    avenc_comfortnoise:   Description              libav comfortnoise encoder
    avenc_comfortnoise:   Author                   Wim Taymans <wim.taymans@gmail.com>, Ronald Bultje <rbultje@ronald.bitfreak.net>
    avenc_comfortnoise: 
    avenc_comfortnoise: Plugin Details:
    avenc_comfortnoise:   Name                     libav
    avenc_comfortnoise:   Description              All libav codecs and formats (system install)
    avenc_comfortnoise:   Filename                 /usr/lib64/gstreamer-1.0/libgstlibav.so
    avenc_comfortnoise:   Version                  1.18.4
    avenc_comfortnoise:   License                  LGPL
    avenc_comfortnoise:   Source module            gst-libav
    avenc_comfortnoise:   Source release date      2021-03-15
    avenc_comfortnoise:   Binary package           openSUSE GStreamer-plugins-libav package
    avenc_comfortnoise:   Origin URL               http://download.opensuse.org
    avenc_comfortnoise: 
    avenc_comfortnoise: GObject
    avenc_comfortnoise:  +----GInitiallyUnowned
    avenc_comfortnoise:        +----GstObject
    avenc_comfortnoise:              +----GstElement
    avenc_comfortnoise:                    +----GstAudioEncoder
    avenc_comfortnoise:                          +----avenc_comfortnoise
    avenc_comfortnoise: 
    avenc_comfortnoise: Implemented Interfaces:
    ...

Notes
=====

- `_GMainLoop <https://github.com/GNOME/glib/blob/b1ffc838ce42c28665f45ef3854cdcdb5fdf35dc/glib/gmain.c#L329>`__ is an event loop. It waits for events from `_GMainContext <https://github.com/GNOME/glib/blob/b1ffc838ce42c28665f45ef3854cdcdb5fdf35dc/glib/gmain.c#L280>`__ then dispatches them to registered handlers.

- Every pipeline contains a `message bus <https://gstreamer.freedesktop.org/documentation/application-development/basics/bus.html?gi-language=c>`__ so streaming thread can send message to application thread. There are two ways to handle messages from a bus:

  + Run glib MainLoop. The loop will check for messages from bus and call registered callbacks.
  + Get the messages from bus directly.

References
==========

- TODO
