=====
 AWS
=====

Network Overview
================

.. raw:: html

    <iframe width="800" height="450" src="https://www.youtube.com/embed/hiKPPy584Mg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Essential VPC concepts

  * `00:44 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=44s>`__ Get started
  * `02:17 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=137s>`__ VPC Concepts & Fundamentals
  * `02:27 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=147s>`__ Choosing an IP address range
  * `04:30 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=270s>`__ Creating subnets in a VPC
  * `06:28 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=388s>`__ Routing in a VPC
  * `11:21 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=681s>`__ Network Security: Security groups
  * `14:27 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=867s>`__ Network Security: Network Access Control List (NACL)
  * `15:32 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=932s>`__ Network Security: Flow logs
  * `18:23 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=1103s>`__ DNS in a VPC

- Other things

  * `18:26 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=1106s>`__ Connectivity options in VPC (VPC peering and Transit Gateway)
  * `24:47 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=1487s>`__ Connecting to on-premises networks: VPN and Direct Connect
  * `25:18 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=1518s>`__ AWS VPN
  * `26:38 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=1598s>`__ AWS Direct Connect
  * `30:33 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=1833s>`__ What about DNS? (Amazon Route 53 Resolver)
  * `34:15 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=2055s>`__ VPC endpoints
  * `36:11 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=2171s>`__ AWS PrivateLink: VPC endpoint services
  * `37:34 <https://www.youtube.com/watch?v=hiKPPy584Mg&t=2254s>`__ Amazon Global Accelerator

.. image:: aws/network.svg

References
==========

- AWS Certified Solutions Architect Study Guide
