==================================
 MP4 - ISO Base Media File Format
==================================

Terms and Definitions
=====================

Excerpted from ISO_ document, section ``3.1 Terms and definitions``.

.. list-table::

   *  - box (called atom in some specifications)
      - object‐oriented building block defined by a unique type identifier and length
   *  - chunk
      - contiguous set of samples for one track
   *  - container box
      - box whose sole purpose is to contain and group a set of related boxes
   *  - media data box
      - box which can hold the actual media data for a presentation (``mdat``)
   *  - movie box
      - container box whose sub‐boxes define the metadata for a presentation (``moov``)
   *  - sample
      - all the data associated with a single timestamp (a sample can be thought of as an AV frame)
   *  - sample table
      - packed directory for the timing and physical layout of the samples in a track
   *  - track
      - timed sequence of related samples (q.v.) in an ISO base media file

Chunk and Sample Organization
-----------------------------

::

    +---------+--------+
    |         | Sample |
    |         +--------+
    | Chunk.0 | Sample |
    |         +--------+
    |         | Sample |
    +------------------+
    |         | Sample |
    | Chunk.1 +--------+
    |         | Sample |
    +---------+--------+
    |                  |
    |       ...        |
    |                  |
    +---------+--------+
    |         | Sample |
    |         +--------+
    | Chunk.N | Sample |
    |         +--------+
    |         | Sample |
    +---------+--------+

File Format
===========

Data in ISO file is organized in boxes. The start of each box contains type and size of it. Therefore, we can have sequential and nested boxes.

Here is the definition of a ``Box``::

    aligned(8) class Box (unsigned int(32) boxtype, optional unsigned int(8)[16] extended_type) {
        unsigned int(32) size;
        unsigned int(32) type = boxtype;
        if (size == 1) {
            unsigned int(64) largesize;
        } else if (size == 0) {
            // box extends to end of file
        }
        if (boxtype == 'uuid') {
            unsigned int(8)[16] usertype = extended_type;
        }
    }

- ``size`` indicates the size of a box, including itself.
- ``type`` shows the type of a particular box.

  * Users can define their own boxes. In this case, ``type`` must be ``uuid``.

And here is a ``FullBox``::

    aligned(8) class FullBox(unsigned int(32) boxtype, unsigned int(8) v, bit(24) f)
      extends Box(boxtype) {
        unsigned int(8) version = v;
        bit(24)         flags = f;
    }

Important Boxes
===============

More information can be found at section ``6.2.3 Box Order`` in ISO_ document.

File Type Box - ``ftyp``
------------------------

Usually at the top of a file.

::

    aligned(8) class FileTypeBox
      extends Box('ftyp') {
        unsigned int(32) major_brand;
        unsigned int(32) minor_version;
        unsigned int(32) compatible_brands[];
        // to end of the box
    }

Movie Box - ``moov``
--------------------

Contain metadata for video/audio.

::

    aligned(8) class MovieBox extends Box('moov') {
    }

Sample To Chunk Box - ``stsc``
------------------------------

    **Samples within the media data are grouped into chunks.** Chunks can be of different sizes, and the samples within a chunk can have different sizes. This table can be used to find the chunk that contains a sample, its position, and the associated sample description.

    --- ISO_

Sample Size Box - ``stsz``
--------------------------

    **This box contains the sample count and a table giving the size in bytes of each sample.** This allows the media data itself to be unframed. The total number of samples in the media is always indicated in the sample count.

    --- ISO_

Chunk Offset Box - ``stco``
---------------------------

    **The chunk offset table gives the index of each chunk into the containing file.** There are two variants, permitting the use of 32‐bit or 64‐bit offsets. The latter is useful when managing very large presentations. At most one of these variants will occur in any single instance of a sample table.

    Offsets are **file offsets**, not the offset into any box within the file (e.g. Media Data Box).

    --- ISO_

Media Data Box - ``mdat``
-------------------------

Contain media data.

::

    aligned(8) class MediaDataBox extends Box('mdat') {
        bit(8) data[];
    }


``free``/``skip``
-----------------

::

    aligned(8) class FreeSpaceBox extends Box(free_type) {
        unsigned int(8) data[];
    }

Simple Structure Of An ISO File
===============================

High level overview

.. image:: mp4-file-format/ISO-file.png

More detail structure

.. image:: mp4-file-format/ISO-structure.jpg
    :target: https://www.programmersought.com/article/92132468003/

Example
=======

Data in table below is from analyzing :download:`this MP4 file <mp4-file-format/big_buck_bunny_480p_1mb.mp4>`.

.. csv-table:: :download:`CSV <mp4-file-format/big_buck_bunny_480p_1mb.csv>`
    :file: mp4-file-format/big_buck_bunny_480p_1mb.csv
    :widths: auto
    :header-rows: 1

Using ``mp4dump`` and ``mp4info`` from bento4_, we can see the structure of the MP4 on console.

- ``mp4dump --verbosity 3 big_buck_bunny_480p_1mb.mp4``

  .. literalinclude:: mp4-file-format/big_buck_bunny_480p_1mb_mp4dump.txt

- ``mp4info --show-layout big_buck_bunny_480p_1mb.mp4``

  .. literalinclude:: mp4-file-format/big_buck_bunny_480p_1mb_mp4info.txt

References
==========

- https://thompsonng.blogspot.com/2010/11/mp4-file-format.html
- https://www.cimarronsystems.com/wp-content/uploads/2017/04/Elements-of-the-H.264-VideoAAC-Audio-MP4-Movie-v2_0.pdf
- https://bitmovin.com/fun-with-container-formats-2/
- https://www.programmersought.com/article/6659350380/
- https://developer.apple.com/library/mac/documentation/QuickTime/QTFF/QTFFPreface/qtffPreface.html
- `Information technology — Coding of audio-visual objects — Part 12: ISO base media file format <https://standards.iso.org/ittf/PubliclyAvailableStandards/c068960_ISO_IEC_14496-12_2015.zip>`__
- https://www.onlinemp4parser.com/
- bento4_

.. _ISO: https://standards.iso.org/ittf/PubliclyAvailableStandards/c068960_ISO_IEC_14496-12_2015.zip
.. _bento4: https://www.bento4.com/
