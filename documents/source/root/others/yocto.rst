=======
 Yocto
=======

Add variables from env
======================

.. code-block:: sh

    BB_ENV_EXTRAWHITE="VAR1 VAR2"
    VAR1=1
    VAR2=2

Generate tarballs
=================

.. code-block:: sh

    BB_GENERATE_MIRROR_TARBALLS = "1"

Use your own source mirror
==========================

.. code-block:: sh

    INHERIT += "own-mirrors"
    SSTATE_MIRRORS = "file://.* https://example.com/yocto/sstate-cache/PATH;downloadfilename=PATH"
    SOURCE_MIRROR_URL = "https://example.com/yocto/downloads"
