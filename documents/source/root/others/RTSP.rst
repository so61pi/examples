=====================================
 RTSP - Real Time Streaming Protocol
=====================================

References
==========

- https://www.informit.com/articles/article.aspx?p=169578&seqNum=3
- `SO: How to understand header of H264 <https://stackoverflow.com/questions/38094302/how-to-understand-header-of-h264>`__
- `SO: RTP H.264 Packet Depacketizer <https://stackoverflow.com/questions/15463692/rtp-h-264-packet-depacketizer>`__
