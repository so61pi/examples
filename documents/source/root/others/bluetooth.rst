===========
 Bluetooth
===========

Softwares / Specifications
==========================

- Bluetooth Core Specification

  +-----------------+----------------------------------------------------------------------------------------+
  | Version         | 5.2                                                                                    |
  +-----------------+----------------------------------------------------------------------------------------+
  | Hash [SHA1]     | 61798f1d281c1e1b757c7437c771e26de6899dcb                                               |
  +-----------------+----------------------------------------------------------------------------------------+
  | Source Location | https://www.bluetooth.com/specifications/bluetooth-core-specification/                 |
  |                 | https://www.bluetooth.org/docman/handlers/downloaddoc.ashx?doc_id=478726               |
  +-----------------+----------------------------------------------------------------------------------------+

Overview
========

.. image:: bluetooth/vol1-A-fig2.2.png
   :scale: 70
   :align: center

.. image:: bluetooth/vol1-A-fig3.1.png
   :scale: 50
   :align: center

.. list-table::
   :header-rows: 1

   *  -
      - Host
      - Controller
   *  - BR/EDR
      - L2CAP + SDP + GAP
      - Link Manager + Link Controller + BR/EDR Radio
   *  - LE
      - L2CAP + SMP + Attribute protocol + GAP + Generic Attribute Profile (GATT)
      - Link Manager + Link Controller + LE Radio block
   *  - AMP
      - AMP PAL + AMP MAC + AMP PHY
      -

- The primary ACL logical transport is the one that is created whenever a device joins a piconet.

  * The LMP protocol is carried on the primary ACL and active slave broadcast logical transports.
  * L2CAP has a protocol control channel that is carried over the primary ACL logical transport.
  * Additional logical transports may be created to transport synchronous data streams when required.

- L2CAP provides a multiplexing role allowing many different applications to share an ACL-U, ASB-U, LE-U, or AMP-U logical link.

Logical Links / Logical Transports
==================================

.. image:: bluetooth/vol1-A-fig3.2.png
   :scale: 70
   :align: center

.. image:: bluetooth/vol1-A-fig3.3.png
   :scale: 70
   :align: center

.. image:: bluetooth/vol1-A-tab3.4.png
   :scale: 70
   :align: center

Packet Formats
==============

.. list-table::
   :header-rows: 1

   *  - BR
      - LE
   *  - .. image:: bluetooth/packet-BR.svg
           :align: center
      - .. image:: bluetooth/packet-LE.svg
           :align: center

        The description of AD Type can be found at `Core Specification Supplement <https://www.bluetooth.com/specifications/specs/core-specification-supplement-9/>`__ and https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile/

        There are a few examples available at https://docs.silabs.com/bluetooth/latest/general/adv-and-scanning/bluetooth-adv-data-basics

SDP
===

TODO

ATT
===

.. image:: bluetooth/vol3-G-fig2.4.png
   :scale: 70
   :align: center

An attribute is a discrete value that has the following properties associated with it:

- **Type**, defined by a UUID

  * 128-bit
  * 16-bit in case it is defined/assigned by Bluetooth SIG

- **Handle**

  * 16-bit
  * Assigned by its own server to let clients reference to a specific attribute
  * Attribute value is accessed using attribute handle

- **A set of permissions** that are defined by each higher layer specification that utilizes the attribute; these permissions cannot be accessed using the Attribute protocol.

  * Access permissions

    + Readable
    + Writable
    + Readable & Writable

  * Encryption permissions

    + Encryption required
    + No encryption required

  * Authentication permissions

    + Authentication Required
    + No Authentication Required

  * Authorization permissions

    + Authorization Required
    + No Authorization Required

- **Value**

GATT
----

GATT utilizes ATT protocol, and it adds a few concepts on top like Service, Characteristic...

.. image:: bluetooth/vol3-G-fig2.7.png
   :scale: 50
   :align: center

.. list-table::
   :header-rows: 1

   *  -
      - Attribute Type (UUID)
      - Attribute Value
      - Attribute Permissions
   *  - Service
      - 0x2800 - Primary Service

        0x2801 - Secondary Service
      - 16-bit Bluetooth UUID or 128-bit UUID for Service
      - Read Only + No Authentication + No Authorization
   *  - Include
      - 0x2802
      - Included Service Attribute Handle + End Group Handle + Service UUID
      - Read Only + No Authentication + No Authorization
   *  - Characteristic Declaration
      - 0x2803
      - Characteristic Properties (1 byte) + Characteristic Value Attribute Handle (2 bytes) + Characteristic UUID (2 or 16 bytes)
      - Read Only + No Authentication + No Authorization
   *  - Characteristic Value
      - 16-bit Bluetooth UUID or 128-bit UUID for Characteristic UUID
      - Characteristic Value
      - Higher layer profile or implementation specific
   *  - Characteristic User Description
      - 0x2901
      - Characteristic User Description UTF-8 String
      - Higher layer profile or implementation specific
   *  - Characteristic Extended Properties
      - 0x2900
      -
      - Read Only + No Authentication + No Authorization
   *  - Client Characteristic Configuration
      - 0x2902
      -
      -
   *  - Server Characteristic Configuration
      - 0x2903
      -
      -
   *  - Characteristic Presentation Format
      - 0x2904
      -
      - Read Only + No Authentication + No Authorization
   *  - Characteristic Aggregate Format 
      - 0x2905
      -
      - Read Only + No Authentication + No Authorization

.. image:: bluetooth/vol3-G-tabA.1.png
   :scale: 70
   :align: center

HCI Commands
============

Bluetooth supports UART, USB, and SD as transports for HCI packets. Below information is for UART.

.. image:: bluetooth/vol4-E-fig1.1.png
   :scale: 50
   :align: center

.. list-table:: HCI Packet Types
   :header-rows: 1

   *  - Packet Type
      - Packet Indicator
      - Packet Format
   *  - [1] HCI Command packet
      - ``0x01``
      - .. image:: bluetooth/vol4-E-fig5.1.png
   *  - [2] HCI ACL Data packet
      - ``0x02``
      - .. image:: bluetooth/vol4-E-fig5.2.png
   *  - [3] HCI Synchronous Data packet (SCO)
      - ``0x03``
      - .. image:: bluetooth/vol4-E-fig5.3.png
   *  - [4] HCI Event packet
      - ``0x04``
      - .. image:: bluetooth/vol4-E-fig5.4.png
   *  - [5] HCI ISO Data packet
      - ``0x05``
      - .. image:: bluetooth/vol4-E-fig5.5.png

- Packet Indicator must be sent right before the HCI packet.
- HCI Command packets **[1]** can only be **sent to** the Bluetooth Controller.
- HCI Event packets **[4]** can only be **sent from** the Bluetooth Controller.
- HCI ACL/Synchronous/ISO Data Packets **[2][3][5]** can be **sent both to and from** the Bluetooth Controller.

Commands
--------

.. list-table::
   :header-rows: 1

   *  - Command
      - Controller
      - Description
   *  - HCI_Reset
      - ALL
      - Reset the Controller and the Link Manager on the BR/EDR Controller, the PAL on an AMP Controller, or the Link Layer on an LE Controller.
   *  - HCI_Inquiry
      - BR
      - Inquiry is used to detect and collect nearby devices.
   *  - HCI_Periodic_Inquiry
      - BR
      - Periodic inquiry is used when the inquiry procedure is to be repeated periodically.
   *  - HCI_Create_Connection
      - BR
      - Request Link Manager to create a connection to a remote device with specified address.
   *  - HCI_Disconnect
      - BR
      - Close an existing connection.
   *  - HCI_Setup_Synchronous_Connection
      - BR
      - Create a new or modify an existing connection.
   *  - HCI_LE_Set_Advertising_Parameters
      - LE
      - Set the advertising parameters.
   *  - HCI_LE_Set_Advertising_Data
      - LE
      - Set the data used in advertising packets that have a data field.
   *  - HCI_LE_Set_Advertising_Enable
      - LE
      - Request the Controller to start or stop advertising.
   *  - HCI_LE_Create_Connection
      - LE
      - Create an ACL connection to a connectable advertiser.
   
Message Sequences
=================

.. list-table::
   :header-rows: 1

   *  - BR
      - LE
   *  - .. image:: bluetooth/vol2-F-fig3.1.png
           :scale: 70
           :align: center
      -
   *  - .. image:: bluetooth/vol2-F-fig3.2.png
           :scale: 70
           :align: center
      - .. image:: bluetooth/vol6-D-fig5.1.png
           :scale: 70
           :align: center
