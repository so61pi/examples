====================
 X.509 Certificates
====================

Overview
========

    In cryptography, X.509 is a standard defining the format of public key certificates. X.509 certificates are used in many Internet protocols, including TLS/SSL, which is the basis for HTTPS, the secure protocol for browsing the web. They are also used in offline applications, like electronic signatures.

    --- https://en.wikipedia.org/wiki/X.509

Format [`ref <https://tools.ietf.org/html/rfc5280#section-4.1>`__]
==================================================================

.. image:: X509/X509.svg
   :align: center

.. note::

    Issuer's name is the subject's name of the upper certificate in the certificate chain::

            +------------------+
            | Root Certificate |
            |==================|
        +-->|   Subject Name   +<---+
        |   +------------------+    |
        +---|   Issuer  Name   |    |
            +------------------+    |
                                    |
                                    |    +--------------------------+
                                    |    | Intermediate Certificate |
                                    |    |==========================|
                                    |    |       Subject Name       +<---+
                                    |    +--------------------------+    |
                                    +----+       Issuer  Name       |    |
                                         +--------------------------+    |
                                                                         |
                                                                         |    +---------------+
                                                                         |    |  Certificate  |
                                                                         |    |===============|
                                                                         |    |  Subject Name |
                                                                         |    +---------------+
                                                                         +----+  Issuer  Name |
                                                                              +---------------+


.. note::

   	By generating this signature, a CA certifies the validity of the information in the tbsCertificate field. In particular, the CA certifies **the binding between the public key material and the subject of the certificate**.

    --- https://tools.ietf.org/html/rfc5280#section-4.1.1.3

Filename Extensions
===================

Copied from https://en.wikipedia.org/wiki/X.509#Certificate_filename_extensions

- ``.pem``: Base64 encoded DER certificate, enclosed between ``"-----BEGIN CERTIFICATE-----"`` and ``"-----END CERTIFICATE-----"``
- ``.cer``, ``.crt``, ``.der``: Usually in binary DER form, but Base64-encoded certificates are common too (see ``.pem`` above)

References
==========

- https://en.wikipedia.org/wiki/X.509
- https://tools.ietf.org/html/rfc5280
