from mitmproxy import ctx
from mitmproxy import http


class HttpsProxyServer:
    def __init__(self):
        pass


    # https://docs.mitmproxy.org/archive/v10/api/events.html#HTTPEvents
    def request(self, flow: http.HTTPFlow) -> None:
        ctx.log.info(flow.request.pretty_url)
        if flow.request.pretty_url == "https://example.com/path":
            flow.response = http.Response.make(
                200,  # (optional) status code
                b"Hello World",  # (optional) content
                {"Content-Type": "text/html"},  # (optional) headers
            )


addons = [
    HttpsProxyServer(),
]
