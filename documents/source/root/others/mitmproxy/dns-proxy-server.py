from ipaddress import IPv4Address, IPv6Address
from mitmproxy import addonmanager
from mitmproxy import ctx
from mitmproxy import dns

# https://datatracker.ietf.org/doc/html/rfc1035#section-3.3.9
def to_mx_record(pref: int, domain: str) -> bytearray:
    result = bytearray(pref.to_bytes(2, 'big'))

    # filter to remove empty part
    for part in filter(None, domain.split('.')):
        result.extend(len(part).to_bytes(1, 'big'))
        result.extend(part.encode())
    result.extend(b'\x00')

    return result


class DnsProxyServer:
    def __init__(self):
        pass


    # https://docs.mitmproxy.org/archive/v10/api/events.html#LifecycleEvents
    def load(self, loader: addonmanager.Loader):
        ctx.log.info('Addon is loaded')

    def done(self):
        ctx.log.info('Addon is done')


    # https://docs.mitmproxy.org/archive/v10/api/events.html#DNSEvents
    def dns_request(self, flow: dns.DNSFlow):
        ctx.log.info(flow)
        # Output when test with `dig example.com @127.0.0.1`
        #
        # <DNSFlow
        #   request=Message(timestamp=1718513150.992602, id=27674, query=True, op_code=0, authoritative_answer=False, truncation=False, recursion_desired=True, recursion_available=False, reserved=2, response_code=0, questions=[Question(name='example.com', type=1, class_=1)], answers=[], authorities=[], additionals=[ResourceRecord(name='', type=41, class_=4096, ttl=0, data=b'')])
        #   response=Message(timestamp=1718513150.993651, id=27674, query=False, op_code=0, authoritative_answer=False, truncation=False, recursion_desired=True, recursion_available=True, reserved=0, response_code=0, questions=[Question(name='example.com', type=1, class_=1)], answers=[ResourceRecord(name='example.com', type=1, class_=1, ttl=60, data=b']\xb8\xd7\x0e'), ResourceRecord(name='example.com', type=1, class_=1, ttl=60, data=b']\xb8\xd7\x0e')], authorities=[], additionals=[])
        # >

        answers = {
            'A': {
                'example.com': '192.168.1.20',
                'example.org': '192.168.1.21',
            },
            'AAAA': {
                'example.com': '2606:2800:21f:cb07:6820:80da:af6b:8b2c',
                'example.org': '2606:2800:21f:cb07:6820:80da:af6b:8b2c',
            },
            'MX': {
                'example.com': to_mx_record(10, "mail.example.com"),
                'example.org': to_mx_record(0, "."),
            }
        }
        if flow.request.questions is not None and len(flow.request.questions) > 0:
            question = flow.request.questions[0] # We only handle 1st question
            if question.type == dns.types.A:
                if question.name in answers['A']:
                    flow.response.answers = [
                        dns.ResourceRecord.A(question.name, IPv4Address(answers['A'][question.name])),
                    ]
            elif question.type == dns.types.AAAA:
                if question.name in answers['AAAA']:
                    flow.response.answers = [
                        dns.ResourceRecord.AAAA(question.name, IPv6Address(answers['AAAA'][question.name])),
                    ]
            elif question.type == dns.types.MX:
                if question.name in answers['MX']:
                    # mitmproxy does not handle MX, so it sets the response_code to NOTIMP (4) which makes the client fails
                    flow.response.response_code = dns.response_codes.NOERROR
                    flow.response.answers = [
                        dns.ResourceRecord(question.name, question.type, dns.classes.IN, dns.ResourceRecord.DEFAULT_TTL, answers['MX'][question.name]),
                    ]


addons = [
    DnsProxyServer(),
]
