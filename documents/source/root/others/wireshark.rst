===========
 Wireshark
===========

Filters
=======

Syntax
------

The general syntax of a filter is ``<field> <operator> <value>``. Below is the list of some examples.

.. list-table::
   :header-rows: 1

   *  - field
      - operator
      - value

   *  - eth
        
        eth.src
        
        eth.dst

        ip
        
        ip.src
        
        ip.dst

        ip.proto

        tcp.srcport

        tcp.dstport

        tcp.pdu.size
        
        http.content_type
        
        http.host

        http.user_agent

        http.request.method
        
        http.response.code
        
      - eq (==)

        ne (!=)

        gt (>)

        lt (<)

        ge (>=)

        le (<=)

        contains

        matches (~)

      - 22

        80

        443

        ff:ff:ff:ff:ff:ff

        aa-aa-aa-aa-aa-aa

        127.0.0.1

        example.com

        129.111.0.0/16

        0xc0a82c00

        "POST"

- **Functions**: There are some functions that we can use to transform ``<field>``:

  * ``upper(string-field)``
  * ``lower(string-field)``
  * ``len(field)``
  * ``count(field)``
  * ``string(field)``

- **Logical Expressions & Bitfield Operations**: We can apply logical expressions and bitfield operations on our filters, similar to C language.

- **Slice Operator**: If a field is a string or byte array (e.g. MAC address), we can take a slice of it using syntax ``field[<begin>:<end>]`` (e.g. ``eth.src[0:3]``).

Examples
--------

- ``dns``

  * All DNS packets

- ``!(icmp || dns)``

  * Ignore all ICMP and DNS packets

- ``ip.src == <peer-ip> && tcp.dstport == 22``

  * Incoming SSH packets

- ``ip.addr == 10.0.0.0/24``

  * Packets to and from any address in the ``10.0.0.0/24`` space

References
==========

- https://www.wireshark.org/docs/man-pages/wireshark-filter.html
- Available Fields: https://www.wireshark.org/docs/dfref/

  * `ip <https://www.wireshark.org/docs/dfref/i/ip.html>`__
  * `tcp <https://www.wireshark.org/docs/dfref/t/tcp.html>`__
  * `http <https://www.wireshark.org/docs/dfref/h/http.html>`__
