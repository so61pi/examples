===========
 mitmproxy
===========

Softwares
=========

- mitmproxy

  +-----------------+----------------------------------------------------------------------------------------+
  | Version         | 10.0.0                                                                                 |
  +-----------------+----------------------------------------------------------------------------------------+
  | Documentation   | https://docs.mitmproxy.org/archive/v10/                                                |
  +-----------------+----------------------------------------------------------------------------------------+

Useful Links
============

- https://docs.mitmproxy.org/archive/v10/concepts-modes/
- https://docs.mitmproxy.org/archive/v10/concepts-options/
- https://docs.mitmproxy.org/archive/v10/addons-overview/
- https://docs.mitmproxy.org/archive/v10/api/events.html
- https://docs.mitmproxy.org/archive/v10/addons-examples/
- https://docs.mitmproxy.org/archive/v10/concepts-certificates/

HTTP Proxy Server
=================

- With TLS inspection

  .. code-block:: sh

      mitmdump -p 3128

      # Testing
      curl -k -v --proxy 127.0.0.1:3128 https://example.com

- Without TLS inspection

  .. code-block:: sh

      mitmdump -p 3128 --ignore-hosts '.*'

      # Testing
      curl --proxy 127.0.0.1:3128 https://example.com

- With authentication

  .. code-block:: sh

      mitmdump -p 3128 --proxyauth "puser:ppass" --ignore-hosts '.*'

      # Testing
      curl --proxy 127.0.0.1:3128 --proxy-user "puser:ppass" https://example.com

DNS Proxy Server
================

- Without custom response

  .. code-block:: sh

        mitmdump --mode dns

        # Testing
        host example.com 127.0.0.1
        dig example.com @127.0.0.1

- With custom response

  .. code-block:: sh

        mitmdump --mode dns -s dns-proxy-server.py

        # Testing
        host example.com 127.0.0.1
        host example.org 127.0.0.1
        dig example.com @127.0.0.1
        dig example.org @127.0.0.1

  .. literalinclude:: mitmproxy/dns-proxy-server.py
      :language: python
      :caption: dns-proxy-server.py

HTTPS Inspection
================

1. Install ``mitmproxy`` CA certificate to Client
-------------------------------------------------

``mitmproxy`` generates certificates of proxied domains on the fly using ``~/.mitmproxy/mitmproxy-ca-cert.pem``, so as long as this CA certificate is trusted by the proxied system, Client in this case, ``mitmproxy`` can decrypt all requests.

.. code-block:: sh

    scp -O ~/.mitmproxy/mitmproxy-ca-cert.pem root@Client:/etc/ssl/certs/

.. note::

    Installing a new CA certificate is system dependent, please consult the document of the system you are using.

We can use ``openssl x509 -in ~/.mitmproxy/mitmproxy-ca-cert.pem -noout -text`` to view the content of ``mitmproxy`` CA certificate. Here is an example output::

  Certificate:
  Data:
      Version: 3 (0x2)
      Serial Number:
          21:07:0c:30:c4:17:35:ac:23:d8:b7:3d:c9:7a:f2:e6:88:7c:d3:59
  Signature Algorithm: sha256WithRSAEncryption
      Issuer: CN=mitmproxy, O=mitmproxy
      Validity
          Not Before: Jul 30 16:13:36 2022 GMT
          Not After : Jul 29 16:13:36 2032 GMT
      Subject: CN=mitmproxy, O=mitmproxy
      Subject Public Key Info:
          Public Key Algorithm: rsaEncryption
              Public-Key: (2048 bit)
              Modulus:
                  00:a6:0e:af:bd:25:cf:14:05:9b:d6:78:cc:a0:35:
                  ca:f6:75:ea:ec:48:4f:a2:8c:26:83:a8:d3:da:e8:
                  e9:43:3a:6a:5f:4b:c3:b7:03:9a:90:2f:92:7d:4c:
                  c2:87:99:bf:61:30:20:d2:8f:ec:a1:d5:8b:50:11:
                  99:93:8a:d6:f2:d0:58:5d:88:e6:a5:1d:cd:23:b9:
                  d9:47:3a:cc:cb:37:3d:00:4b:7e:56:e5:c0:06:2b:
                  87:10:49:26:29:0b:f8:72:93:1d:07:82:1a:f7:fb:
                  b5:90:f3:f1:a5:4a:d0:19:36:99:cb:11:7b:1d:4b:
                  7c:91:d3:ca:61:50:21:8d:c4:98:d1:55:a3:cf:7a:
                  95:a8:24:68:08:b0:1e:1c:8b:85:bc:bf:50:ec:e4:
                  46:97:8f:fc:f7:f7:08:cf:71:5e:e0:17:45:6b:66:
                  ab:8f:cf:90:80:dd:9e:8e:f9:dc:a1:12:73:68:c9:
                  f1:01:55:c0:e2:fc:9a:d7:2b:9b:03:5d:a5:3d:ff:
                  b9:56:2f:2d:97:eb:97:13:3f:9e:81:d0:1a:58:a5:
                  4d:c3:7d:0f:59:26:27:a8:8d:1e:15:32:5a:e7:59:
                  69:05:de:08:9a:ac:b4:6e:85:94:32:50:86:d5:74:
                  15:40:d6:7a:87:59:ce:c6:e1:0a:42:aa:af:ea:db:
                  e0:0b
              Exponent: 65537 (0x10001)
      X509v3 extensions:
          X509v3 Basic Constraints: critical
              CA:TRUE
          X509v3 Extended Key Usage:
              TLS Web Server Authentication
          X509v3 Key Usage: critical
              Certificate Sign, CRL Sign
          X509v3 Subject Key Identifier:
              F7:BE:F5:52:5F:D2:41:C5:50:51:69:77:3E:6F:DB:E1:51:28:7B:96
  Signature Algorithm: sha256WithRSAEncryption
       03:19:e9:51:09:c9:2b:24:da:92:0f:84:13:1e:3d:56:f1:67:
       a9:03:3c:ad:aa:1a:c0:b8:b8:1f:bd:07:f8:df:1e:81:09:ce:
       96:5c:80:f8:8b:24:77:8d:68:bc:d0:4c:d7:aa:6d:f5:99:a5:
       88:84:ec:3f:53:1b:9e:7b:5d:6b:2b:5e:cc:bc:ad:6d:55:5e:
       fe:ae:4e:5e:84:b2:06:c6:3a:7d:57:71:74:ab:bf:bd:bd:cb:
       fd:9c:4a:e5:13:22:eb:45:5f:f3:2b:b1:98:92:ba:7a:0e:90:
       68:ff:44:59:36:f2:c8:f6:5e:35:90:05:6d:20:08:53:59:6b:
       52:f2:ea:7d:5c:1a:94:49:95:f4:32:7a:db:37:5c:ab:95:e3:
       3b:e7:7e:6d:7e:18:6b:5c:c7:23:3d:fc:1f:95:66:b0:f0:62:
       d4:fa:21:7d:67:7b:47:70:83:08:35:9d:86:b7:59:44:73:10:
       9b:e5:07:58:37:dc:53:6d:8c:38:e2:ae:47:af:ac:19:90:eb:
       d7:80:6d:6b:21:2a:7a:d6:7a:13:8c:d6:19:ad:d1:67:66:15:
       0b:18:23:97:93:17:42:37:74:00:50:ef:07:7b:27:48:b7:29:
       c9:87:cb:3f:8c:9e:59:a2:42:4a:92:aa:04:a9:92:b2:22:47:
       b1:5e:5f:69

2. Run ``mitmproxy`` as a reverse proxy server to catch traffic to example.com
------------------------------------------------------------------------------

- To capture traffic for single site

  .. code-block:: sh

      # Can replace `mitmweb` with `mitmproxy` to use command line interface
      SSLKEYLOGFILE=/tmp/keys mitmweb -p 443 --mode reverse:https://example.com --set connection_strategy=lazy

- To capture traffic for multiple sites

  .. code-block:: sh

      git clone https://github.com/mitmproxy/mitmproxy.git
      cd ./mitmproxy

      # HTTPS:443 capturing
      SSLKEYLOGFILE=/tmp/keys mitmweb -p 443 -s ./examples/contrib/dns_spoofing.py --mode reverse:http://invalid.com --set keep_host_header --set  connection_strategy=lazy

      # HTTP:80 capturing
      mitmdump -p 80 --mode reverse:http://localhost:443/

3. Make requests sent by Client to example.com to go proxy server
-----------------------------------------------------------------

We have 2 options:

- Update ``/etc/hosts`` file in Client
- Make Client to use DNS server that returns IP of proxy server when example.com is queried

  a. Set up a DNS proxy server
  b. Change Client's DNS server to IP of DNS proxy server

4. Start WireShark on proxy server then change ``(Pre)-Master-Secret log filename`` to ``/tmp/keys``
----------------------------------------------------------------------------------------------------

https://wiki.wireshark.org/TLS#using-the-pre-master-secret

5. Testing
----------

In Machine A, we can use ``curl`` to send a test request.

.. code-block:: sh

    curl -v https://example.com

Here is the output of a successful setup. We have no wanring/error about invalid server certificate, and the ``Server certificate`` section indicates that the server certificate was generated by ``mitmproxy``.

.. code-block:: text
    :emphasize-lines: 29-30

    * Rebuilt URL to: https://example.com/
    *   Trying 192.168.189.1...
    * TCP_NODELAY set
    * Connected to example.com (192.168.189.1) port 443 (#0)
    * ALPN, offering h2
    * ALPN, offering http/1.1
    * successfully set certificate verify locations:
    *   CAfile: /etc/ssl/certs/ca-certificates.crt
      CApath: /etc/ssl/certs
    * TLSv1.3 (OUT), TLS handshake, Client hello (1):
    * TLSv1.3 (IN), TLS handshake, Server hello (2):
    * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
    * TLSv1.3 (IN), TLS handshake, Unknown (8):
    * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
    * TLSv1.3 (IN), TLS handshake, Certificate (11):
    * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
    * TLSv1.3 (IN), TLS handshake, CERT verify (15):
    * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
    * TLSv1.3 (IN), TLS handshake, Finished (20):
    * TLSv1.3 (OUT), TLS change cipher, Client hello (1):
    * TLSv1.3 (OUT), TLS Unknown, Certificate Status (22):
    * TLSv1.3 (OUT), TLS handshake, Finished (20):
    * SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384
    * ALPN, server did not agree to a protocol
    * Server certificate:
    *  subject: CN=www.example.org; O=InternetCorporationforAssignedNamesandNumbers
    *  start date: Oct 22 17:06:55 2022 GMT
    *  expire date: Oct 24 17:06:55 2023 GMT
    *  subjectAltName: host "example.com" matched cert's "example.com"
    *  issuer: CN=mitmproxy; O=mitmproxy
    *  SSL certificate verify ok.
    * TLSv1.3 (OUT), TLS Unknown, Unknown (23):
    > GET / HTTP/1.1
    > Host: example.com
    > User-Agent: curl/7.58.0
    > Accept: */*
    >
    * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
    * TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
    * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
    * TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
    * TLSv1.3 (IN), TLS Unknown, Unknown (23)
    < HTTP/1.1 200 OK
    < Accept-Ranges: bytes
    < Age: 116341
    < Cache-Control: max-age=604800
    < Content-Type: text/html; charset=UTF-8
    < Date: Mon, 24 Oct 2022 10:42:27 GMT
    < Etag: "3147526947"
    < Expires: Mon, 31 Oct 2022 10:42:27 GMT
    < Last-Modified: Thu, 17 Oct 2019 07:18:26 GMT
    < Server: ECS (oxr/836D)
    < Vary: Accept-Encoding
    < X-Cache: HIT
    < Content-Length: 1256
    <
    * TLSv1.3 (IN), TLS Unknown, Unknown (23):
    <!doctype html>
    <html>
    <head>
        <title>Example Domain</title>

        <meta charset="utf-8" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <style type="text/css">
        body {
            background-color: #f0f0f2;
            margin: 0;
            padding: 0;
            font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", "Open Sans", "Helvetica Neue", Helvetic

        }
        div {
            width: 600px;
            margin: 5em auto;
            padding: 2em;
            background-color: #fdfdff;
            border-radius: 0.5em;
            box-shadow: 2px 3px 7px 2px rgba(0,0,0,0.02);
        }
        a:link, a:visited {
            color: #38488f;
            text-decoration: none;
        }
        @media (max-width: 700px) {
            div {
                margin: 0 auto;
                width: auto;
            }
        }
        </style>
    </head>

    <body>
    <div>
        <h1>Example Domain</h1>
        <p>This domain is for use in illustrative examples in documents. You may use this
        domain in literature without prior coordination or asking for permission.</p>
        <p><a href="https://www.iana.org/domains/example">More information...</a></p>
    </div>
    </body>
    </html>
    * Connection #0 to host example.com left intact

6. Modify requests and response
-------------------------------

``mitmproxy`` provides us the ability to modify the content of intercepted requests and responses through addons. Below is an example of addon that returns a custom response.

  .. literalinclude:: mitmproxy/https-proxy-server.py
      :language: py
      :caption: https-proxy-server.py

To load this addon into ``mitmproxy``, we need to add option ``-s https-proxy-server.py`` when running ``mitmproxy``/``mitmweb``.

.. code-block:: sh

    SSLKEYLOGFILE=/tmp/keys mitmweb  -p 443 --mode reverse:https://example.com -s https-proxy-server.py --set connection_strategy=lazy

``curl -v https://example.com/path`` can be used to verify our addon::

  *   Trying 192.168.189.1...
  * TCP_NODELAY set
  * Connected to example.com (192.168.189.1) port 443 (#0)
  * ALPN, offering h2
  * ALPN, offering http/1.1
  * successfully set certificate verify locations:
  *   CAfile: /etc/ssl/certs/ca-certificates.crt
    CApath: /etc/ssl/certs
  * TLSv1.3 (OUT), TLS handshake, Client hello (1):
  * TLSv1.3 (IN), TLS handshake, Server hello (2):
  * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
  * TLSv1.3 (IN), TLS handshake, Unknown (8):
  * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
  * TLSv1.3 (IN), TLS handshake, Certificate (11):
  * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
  * TLSv1.3 (IN), TLS handshake, CERT verify (15):
  * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
  * TLSv1.3 (IN), TLS handshake, Finished (20):
  * TLSv1.3 (OUT), TLS change cipher, Client hello (1):
  * TLSv1.3 (OUT), TLS Unknown, Certificate Status (22):
  * TLSv1.3 (OUT), TLS handshake, Finished (20):
  * SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384
  * ALPN, server did not agree to a protocol
  * Server certificate:
  *  subject: CN=www.example.org; O=InternetCorporationforAssignedNamesandNumbers
  *  start date: Oct 22 17:06:55 2022 GMT
  *  expire date: Oct 24 17:06:55 2023 GMT
  *  subjectAltName: host "example.com" matched cert's "example.com"
  *  issuer: CN=mitmproxy; O=mitmproxy
  *  SSL certificate verify ok.
  * TLSv1.3 (OUT), TLS Unknown, Unknown (23):
  > GET /path HTTP/1.1
  > Host: example.com
  > User-Agent: curl/7.58.0
  > Accept: */*
  >
  * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
  * TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
  * TLSv1.3 (IN), TLS Unknown, Certificate Status (22):
  * TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
  * TLSv1.3 (IN), TLS Unknown, Unknown (23):
  < HTTP/1.1 200 OK
  < Content-Type: text/html
  < content-length: 11
  <
  * TLSv1.3 (IN), TLS Unknown, Unknown (23):
  * Connection #0 to host example.com left intact
  Hello World

.. note::

    We don't need to re-run ``mitmproxy`` when editing ``https-proxy-server.py``. It will be automatically reloaded when changed.

References
==========

- https://docs.mitmproxy.org/stable/concepts-certificates/
- https://docs.mitmproxy.org/stable/addons-overview/
