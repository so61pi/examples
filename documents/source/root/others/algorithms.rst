============
 Algorithms
============

.. toctree::
    :maxdepth: 2
    :glob:

    algorithms/*
