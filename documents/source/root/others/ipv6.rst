======
 IPv6
======

Notes
=====

- IPv6 Address Length: 128 bits / 16 uint8_t / **8 uint16_t** / 4 uint32_t / 2 uint64_t / 1 uint128_t
- IPv6 Address Format: ``<routing prefix><subnet ID><interface identifier>``

  * ``<interface identifier>``: Most of the time, it's 64-bit

- Min MTU: 1280
- Recommended MTU: 1500

IPv6 Address Types
==================

Unicast
-------

Global Unicast (GUA)
~~~~~~~~~~~~~~~~~~~~

- Address Block: ``2000::/3``
- Range: ``2000:0000:0000:0000:0000:0000:0000:0000 - 3fff:ffff:ffff:ffff:ffff:ffff:ffff:ffff``
- Number of addresses: 2^125

6to4
````

- Address Block: ``2002::/16``
- Range: ``2002:0000:0000:0000:0000:0000:0000:0000 - 2002:ffff:ffff:ffff:ffff:ffff:ffff:ffff``
- Number of addresses: 2^112

Documentation: https://datatracker.ietf.org/doc/html/rfc3849
````````````````````````````````````````````````````````````

- Address Block: ``2001:db8::/32``
- Range: ``2001:0db8:0000:0000:0000:0000:0000:0000 - 2001:0db8:ffff:ffff:ffff:ffff:ffff:ffff``
- Number of addresses: 2^96

Link-Local
~~~~~~~~~~

- Address Block: ``fe80::/64``
- Range: ``fe80:0000:0000:0000:0000:0000:0000:0000 - fe80:0000:0000:0000:ffff:ffff:ffff:ffff``
- Number of addresses: 2^64
- Note:

  * Multiple interfaces in a machine can share a same link-local address. To distinguish between them we need to append `%<zone-id>` to its address

Loopback
~~~~~~~~

- Address Block: ``::1/128``
- Number of addresses: 1

Unspecified
~~~~~~~~~~~~

- Address Block: ``::/128``
- Number of addresses: 1

Unique Local (ULA)
~~~~~~~~~~~~~~~~~~

- Address Block: ``fc00::/7`` | ``1111 11L0/7``

  * L = 1 means local, L = 0 is reserved

- Range: ``fc00:0000:0000:0000:0000:0000:0000:0000 - fdff:ffff:ffff:ffff:ffff:ffff:ffff:ffff``
- Number of addresses: 2^121

Site-Local (Deprecated)
```````````````````````

- Address Block: ``fec0::/10``
- Range: ``fec0:0000:0000:0000:0000:0000:0000:0000 - feff:ffff:ffff:ffff:ffff:ffff:ffff:ffff``
- Number of addresses: 2^118

Embedded IPv4
~~~~~~~~~~~~~

- Address Block: ``::/80``
- Range: ``0000:0000:0000:0000:0000:0000:0000:0000 - 0000:0000:0000:0000:0000:ffff:ffff:ffff``
- Number of addresses: 2^48

NAT64
~~~~~

- Address Block: ``0064:ff9b::/96``
- Range: ``0064:ff9b:0000:0000:0000:0000:0000:0000 - 0064:ff9b:0000:0000:0000:0000:ffff:ffff``
- Number of addresses: 2^32

Multicast
---------

Well-Known
~~~~~~~~~~

- Address Block: ``ff00::/12``
- Range: ``ff00:0000:0000:0000:0000:0000:0000:0000 - ff0f:ffff:ffff:ffff:ffff:ffff:ffff:ffff``
- Number of addresses: 2^116
- Well-known Addresses:

  * **ff02::1**: All IPv6 devices (link-local)
  * **ff02::2**: All IPv6 routers (link-local)
  * ff02::1:2: All DHCP agents (link-local)
  * ff05::1:3: All DHCP servers (site-local)
  * ff0x::101: All NTP servers

    + ff02::101: Link local
    + ff05::101: Site local

  * ff0x::fb: mDNSv6

    + ff02::fb: Link local
    + ff05::fb: Site local

Solicited-Node
~~~~~~~~~~~~~~

- Address Block: ``ff02:0:0:0:0:1:ff00::/104``
- Range: ``ff02:0000:0000:0000:0000:0001:ff00:0000 - ff02:0000:0000:0000:0000:0001:ffff:ffff``
- Number of addresses: 2^24

The OS will instruct the interface to listen to Solicited-Node address of for every IPv6 address that interface has.

Anycast
-------

Others
------

IPv4-Mapped Address
~~~~~~~~~~~~~~~~~~~

- Address Block: ``0000:0000:0000:0000:0000:ffff/96``
- Range: ``0000:0000:0000:0000:0000:ffff:0000:0000 - 0000:0000:0000:0000:0000:ffff:ffff:ffff``
- Number of addresses: 2^32

References
``````````

- https://serverfault.com/questions/1102241/purpose-of-ipv4-mapped-ipv6-address

References
----------

- http://www.iana.org/assignments/ipv6-unicast-address-assignments/ipv6-unicast-address-assignments.xhtml

ICMPv6
======

- Neighbor Discovery

  * https://datatracker.ietf.org/doc/html/rfc4861
  * https://superuser.com/questions/809679/what-is-the-mac-address-of-multicast-ipv6
  * Message Type

    + Router Solicitation

      - Sent by clients, multicast to **ff02::2**
      - Message Format: https://datatracker.ietf.org/doc/html/rfc4861#section-4.1

    + Router Advertisement

      - Sent by routers, multicast to **ff02::1**, or unicast. Sent when receive Router Solicitation, or periodically
      - Message Format: https://datatracker.ietf.org/doc/html/rfc4861#section-4.2

    + Neighbor Solicitation

      - Sent by clients, multicast to Solicited-Node address
      - Message Format: https://datatracker.ietf.org/doc/html/rfc4861#section-4.3
      - Duplicate Address Detection: Used to check if an IPv6 is already used before assigning it to an interface

        * Source IP Address: **::**
        * Dest IP Address: IPv6 address to is going to be assigned to interface

    + Neighbor Advertisement

      - Sent by clients, unicast back to requester.
      - (Unsolicited) Sent to inform MAC address changing.
      - Message Format: https://datatracker.ietf.org/doc/html/rfc4861#section-4.4

    + Redirect

      - Used by routers to inform clients there is a better route to a target machine
      - Message Format: https://datatracker.ietf.org/doc/html/rfc4861#section-4.5

- Packet Too Big

  * Path MTU Discovery: https://www.juniper.net/documentation/us/en/software/junos/flow-packet-processing/topics/concept/ipv6-flow-pmtu-messages-understanding.html

- Ping

Address Configuration
=====================

- Client sends out Router Solicitation to get Router Advertisement. How the address is configured depends on the information in received Router Advertisement message.
- Default gateway is always from Router Advertisement message.

SLAAC
-----

- Auto Other Managed = 1 0 0
- Inteface ID is 64-bit

  * Generated using EUI-64 (MAC-based) or randomly

- DNS / NTP ... extensions

SLAAC + Stateless DHCP
----------------------

- Auto Other Managed = 1 1 0
- Inteface ID is 64-bit

  * Generated using EUI-64 (MAC-based) or randomly

- Other config is retrieve from DHCP server (e.g., DNS)

  * DHCP message type
  * DHCP exchange diagram

Stateful DHCP
-------------

- Auto Other Managed = 0 0 1
- Global Unicast Address is retrieved from DHCP
- Other config is retrieve from DHCP server (e.g., DNS)

  * DHCP message type
  * DHCP exchange diagram

DHCPv6
------

Port
~~~~

- Client: UDP port 546
- Server/Relay: UDP port 547

Message
~~~~~~~

- Message Type

  * SOLICIT (1)
  * ADVERTISE (2)
  * REQUEST (3)
  * CONFIRM (4)
  * RENEW (5)
  * REBIND (6)
  * REPLY (7)
  * RELEASE (8)
  * DECLINE (9)
  * RECONFIGURE (10)
  * INFORMATION-REQUEST (11)
  * RELAY-FORW (12)
  * RELAY-REPL (13)

- Message Format: https://datatracker.ietf.org/doc/html/rfc8415#section-8

References
``````````

- https://datatracker.ietf.org/doc/html/rfc8415#section-7.3
- https://en.wikipedia.org/wiki/DHCPv6#DHCPv6_Message_types

References
~~~~~~~~~~

- https://datatracker.ietf.org/doc/html/rfc8415

References
----------

- https://datatracker.ietf.org/doc/html/rfc4862

Permanent vs Temporary Address
==============================

Default Address Selection
=========================

Source Address Selection
------------------------

- Rule 1: Prefer same address.
- Rule 2: Prefer appropriate scope.
- Rule 3: Avoid deprecated addresses.
- Rule 4: Prefer home addresses.
- Rule 5: Prefer outgoing interface.
- Rule 6: Prefer matching label.
- Rule 7: Prefer temporary addresses.
- Rule 8: Use longest matching prefix.

References
~~~~~~~~~~

- https://datatracker.ietf.org/doc/html/rfc6724#section-5

Destination Address Selection
-----------------------------

- Rule 1: Avoid unusable destinations.
- Rule 2: Prefer matching scope.
- Rule 3: Avoid deprecated addresses.
- Rule 4: Prefer home addresses.
- Rule 5: Prefer matching label.
- Rule 6: Prefer higher precedence.
- Rule 7: Prefer native transport.
- Rule 8: Prefer smaller scope.
- Rule 9: Use longest matching prefix.
- Rule 10: Otherwise, leave the order unchanged.

References
~~~~~~~~~~

- https://datatracker.ietf.org/doc/html/rfc6724#section-6

Interactions with Routing
-------------------------

    This specification of source address selection assumes that routing
    (more precisely, selecting an outgoing interface on a node with
    multiple interfaces) is done before source address selection.
    However, implementations MAY use source address considerations as a
    tiebreaker when choosing among otherwise equivalent routes.

    --- https://datatracker.ietf.org/doc/html/rfc6724#section-7

IPv6 Transition Mechanism
=========================

https://en.wikipedia.org/wiki/IPv6_transition_mechanism

Dual-Stack
==========

- https://en.wikipedia.org/wiki/Happy_Eyeballs
- https://stackoverflow.com/questions/4082081/requesting-a-and-aaaa-records-in-single-dns-query
- https://man7.org/linux/man-pages/man5/gai.conf.5.html

Linux IPv6 sysctl
=================

- https://superuser.com/questions/645205/disable-ipv6-autoconf-mac-based-ipv6-address-without-disabling-privacy-address
- https://tldp.org/HOWTO/Linux+IPv6-HOWTO/ch06s05.html
- https://www.kernel.org/doc/Documentation/networking/ip-sysctl.txt

  * disable_ipv6
  * addr_gen_mode
  * stable_secret
  * use_tempaddr

DHCPv6 Server/Client
====================

odhcpd/odhcp6c
--------------

- odhcpd gives out random addresses with Host ID/Interface ID >= 0x100

  * https://github.com/openwrt/odhcpd/blob/d8118f6e76e5519881f9a37137c3a06b3cb60fd2/src/dhcpv6-ia.c#L860-L880
  * Allowed length of Host ID/Interface ID is 12-64 bites. Default is 12 bits
  * Static IPv6 addresses must have Host ID/Interface ID smaller than 0x100. It should be greater than 0x1 to avoid conflict with default gateway

dhcpcd
------

- https://github.com/NetworkConfiguration/dhcpcd

Private IPv6 Address & NAT6 Using odhcpd On OpenWRT
---------------------------------------------------

Then run the following commands to configure the router to only assign ULA addresses to clients and use NAT6 to provide Internet access:

.. code-block:: sh

    # Announce IPv6 default route for clients using the ULA prefix
    uci set dhcp.lan.ra_default='1'

    # Disable SLAAC address generation
    # Clients will only get addresses from the DHCPv6 server
    uci set dhcp.lan.ra_slaac='0'

    uci commit dhcp

    # Assign only the ULA prefix on the LAN interface
    uci set network.lan.ip6class="local"

    #  Disable IPv6 source filter on the upstream interface
    uci set network.wan6.sourcefilter="0"

    uci commit network

    # Enable IPv6 masquerading on the upstream zone (e.g., wan6)
    uci set firewall.@zone[1].masq6="1"

    uci commit firewall

    # Restart services
    service odhcpd restart
    service firewall restart
    service network restart

References
~~~~~~~~~~

- https://openwrt.org/docs/guide-user/network/ipv6/ipv6.nat6
- https://openwrt.org/docs/guide-user/network/ipv6/ipv6_extras
- https://openwrt.org/docs/guide-user/base-system/dhcp

Private IPv6 Address & NAT6 Using Kea DHCPv6 On OpenWRT
-------------------------------------------------------

To run Kea DHCP6 server on OpenWRT, you need to install new packages with command ``opkg install kea-dhcp6 kea-lfc``.

Then you need to create a configuration file ``kea-dhcp6.conf.json`` with the following content:

.. code-block:: text
    :caption: kea-dhcp6.conf.json

    {
        "Dhcp6": {
            # First we set up global values
            "valid-lifetime": 4000,
            "renew-timer": 1000,
            "rebind-timer": 2000,
            "preferred-lifetime": 3000,

            # Next we set up the interfaces to be used by the server.
            "interfaces-config": {
                "interfaces": [ "br-lan" ]
            },

            # And we specify the type of lease database
            "lease-database": {
                "type": "memfile",
                "persist": true,
                "name": "/var/lib/kea/dhcp6.leases"
            },

            # Finally, we list the subnets from which we will be leasing addresses.
            # Note: We must ensure the subnet prefix is routable on the router
            # On OpenWRT the subnet can be retrieved by running `uci get network.globals.ula_prefix`
            "subnet6": [
                {
                    "id": 1,
                    "subnet": "2001:db8:1::/64",
                    "pools": [
                        {
                            "pool": "2001:db8:1::2-2001:db8:1::ffff"
                        }
                    ],
                    "interface": "br-lan"
                }
            ],

            # DNS servers
            "option-data": [
                {
                "name": "dns-servers",
                "data": "2606:4700:4700::1111, 2606:4700:4700::1001"
                }
            ]
        }
    }

Then run the following commands to start the server:

.. code-block:: sh

    # Disable default DHCPv6 server
    uci del dhcp.lan.dhcpv6

    # Do not include the router as a DNS server, as DNS servers are provided by DHCPv6 server
    uci set dhcp.lan.dns_service='0'

    # Announce IPv6 default route for clients using the ULA prefix
    uci set dhcp.lan.ra_default='1'

    # Disable SLAAC address generation
    # Clients will only get addresses from the DHCPv6 server
    uci set dhcp.lan.ra_slaac='0'

    uci commit dhcp

    # Assign only the ULA prefix on the LAN interface
    uci set network.lan.ip6class="local"

    #  Disable IPv6 source filter on the upstream interface
    uci set network.wan6.sourcefilter="0"

    uci commit network

    # Enable IPv6 masquerading on the upstream zone (e.g., wan6)
    uci set firewall.@zone[1].masq6="1"

    uci commit firewall

    # Restart services
    service odhcpd restart
    service firewall restart
    service network restart

    # Start KEA DHCPv6 server
    mkdir -p /var/lib/kea /var/run/kea
    kea-dhcp6 -d -c kea-dhcp6.conf.json

References
~~~~~~~~~~

- https://kea.readthedocs.io/en/latest/arm/dhcp6-srv.html
- https://openwrt.org/docs/guide-user/network/ipv6/ipv6.nat6
- https://openwrt.org/docs/guide-user/network/ipv6/ipv6_extras
- https://openwrt.org/docs/guide-user/base-system/dhcp

Prefix Delegation
=================

A prefix length can be sub delegated to a downstream router. The downstream router can then assign addresses to its downstream routers. Below is an example of how a /60 prefix can be devided.

.. code-block:: text

                                                                ┌─────┐
                                   ┌────────────────────────────┤ /60 ├────────────────────────────┐
                                   │                            └─────┘                            │
                                   │                              0000                             │
                                   │                                                               │
                                ┌──▼──┐                                                         ┌──▼──┐
                   ┌────────────┤ /61 ├────────────┐                               ┌────────────┤ /61 ├────────────┐
                   │            └─────┘            │                               │            └─────┘            │
                   │             0_000             │                               │             1_000             │
                   │                               │                               │                               │
                ┌──▼──┐                         ┌──▼──┐                         ┌──▼──┐                         ┌──▼──┐
           ┌────┤ /62 ├────┐               ┌────┤ /62 ├────┐               ┌────┤ /62 ├────┐               ┌────┤ /62 ├────┐
           │    └─────┘    │               │    └─────┘    │               │    └─────┘    │               │    └─────┘    │
           │     00_00     │               │     01_00     │               │     10_00     │               │     11_00     │
           │               │               │               │               │               │               │               │
        ┌──▼──┐         ┌──▼──┐         ┌──▼──┐         ┌──▼──┐         ┌──▼──┐         ┌──▼──┐         ┌──▼──┐         ┌──▼──┐
       ┌┤ /63 ├┐       ┌┤ /63 ├┐       ┌┤ /63 ├┐       ┌┤ /63 ├┐       ┌┤ /63 ├┐       ┌┤ /63 ├┐       ┌┤ /63 ├┐       ┌┤ /63 ├┐
       │└─────┘│       │└─────┘│       │└─────┘│       │└─────┘│       │└─────┘│       │└─────┘│       │└─────┘│       │└─────┘│
       │ 000_0 │       │ 001_0 │       │ 010_0 │       │ 011_0 │       │ 100_0 │       │ 101_0 │       │ 110_0 │       │ 111_0 │
       │       │       │       │       │       │       │       │       │       │       │       │       │       │       │       │
    ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐
    │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │
    └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘
      0000    0001    0010    0011    0100    0101    0110    0111    1000    1001    1010    1011    1100    1101    1110    1111
       x00     x01     x02     x03     x04     x05     x06     x07     x08     x09     x0A     x0B     x0C     x0D     x0E     x0F


With OpenWRT, the first ``/64`` block is used for the LAN interface and used to provide IPv6 addresses to its clients (e.g., phone, laptop, WAN port of a router). The rest of the blocks can be used for downstream routers.

    Setting the ip6assign parameter to a value < 64 will allow the DHCPv6-server to hand out all but the first /64 via DHCPv6-Prefix Delegation to downstream routers on the interface. If ip6hint is not suitable for the given ip6assign, it will be rounded down to the nearest possible value.

    --- https://openwrt.org/docs/guide-user/network/ipv6/configuration#downstream_configuration_for_lan_interfaces

.. code-block:: text

                                                                        R-X
        ┌─────────────────────────────────────────────────────────────────────────────────┐
        │                                                                                 │
        │                                                             ┌─────┐             │
        │    ┌───────┬───────┬───────┬───────┬───────┬───────┬───────┬┤ /60 ├────────┐    │
        │    │       │       │       │       │       │       │       │└─────┘        │    │
        │    │       │       │       │       │       │       │       │  0000         │    │
        │    │       │       │       │       │       │       │       │               │    │
        │    │N-X1   │N-X2   │N-X3   │N-X4   │N-X5   │N-X6   │N-X7   │N-X8           │N-X9│
        │ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐         ┌──▼──┐ │
        │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │ │ /64 │         │ /61 │ │
        │ ├─────┤ ├─────┤ ├─────┤ ├─────┤ ├─────┤ ├─────┤ ├─────┤ ├─────┤         ├─────┤ │
        │ │ 0000│ │ 0001│ │ 0010│ │ 0011│ │ 0100│ │ 0101│ │ 0110│ │ 0111│         │1_000│ │
        │ │  x00│ │  x01│ │  x02│ │  x03│ │  x04│ │  x05│ │  x06│ │  x07│         │  x08│ │
        │ ├─────┤ ├─────┤ ├─────┤ ├─────┤ ├─────┤ ├─────┤ ├─────┤ ├─────┤         ├─────┤ │
        │ │IPv6 │ │IPv6 │ │IPv6 │ │IPv6 │ │IPv6 │ │IPv6 │ │IPv6 │ │IPv6 │         │IPv6 │ │
        │ │00::1│ │01::1│ │02::1│ │03::1│ │04::1│ │05::1│ │06::1│ │07::1│     ┌───┤08::1│ │
        │ └┬───┬┘ └─────┘ └┬────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘     │   └─┬─┬─┘ │
        │  │   │           │                                                  │     │ │   │
        └──┼───┼───────────┼──────────────────────────────────────────────────┼─────┼─┼───┘
           │   │           │                                                  │     │ │
    ┌──────▼┐ ┌▼──────┐   ┌▼──────┐                                       ┌───▼───┐ │ │
    │ Phone │ │ Phone │   │  TV   │                                       │Laptop │ │ │
    ├───────┤ ├───────┤   ├───────┤                                       ├───────┤ │ │
    │ IPv6  │ │ IPv6  │   │ IPv6  │                                       │ IPv6  │ │ │
    │00::8e6│ │00::7a5│   │02::945│                                       │08::70e│ │ │
    └───────┘ └───────┘   └───────┘                                       └───────┘ │ │
                                                                                    │ │
                                           ┌────────────────────────────────────────┘ │
                                           │R-Y (08::a86)                             │
                                      ┌────▼────┐                        ┌────────────┘
                                      │         │                        │R-Z (08::db8)
                                      │ ┌─────┐ │       ┌────────────────▼────────┐
                                      │ │ /64 │ │       │                         │
                                      │ └──┬──┘ │       │             ┌─────┐     │
                                      │    │N-Y1│       │    ┌───────┬┤ /62 ├┐    │
                                      │ ┌──▼──┐ │       │    │       │└─────┘│    │
                                      │ │ /64 │ │       │    │       │ 11_00 │    │
                                      │ ├─────┤ │       │    │       │       │    │
                                      │ │ 1001│ │       │    │N-Z1   │N-Z2   │N-Z3│
                                      │ │  x09│ │       │ ┌──▼──┐ ┌──▼──┐ ┌──▼──┐ │
                                      │ ├─────┤ │       │ │ /64 │ │ /64 │ │ /63 │ │
                          ┌───────┐   │ │IPv6 │ │       │ ├─────┤ ├─────┤ ├─────┤ │
                          │ Phone ◄───┼─┤09::1│ │       │ │ 1100│ │ 1101│ │111_0│ │
                          ├───────┐   │ └─────┘ │       │ │  x0C│ │  x0D│ │  x0E│ │
                          │ IPv6  │   │         │       │ ├─────┤ ├─────┤ ├─────┤ │
                          │09::855│   └─────────┘       │ │IPv6 │ │IPv6 │ │IPv6 │ │
                          └───────┘                     │ │0C::1│ │0D::1│ │0E::1│ │
                                                        │ └─────┘ └─────┘ └┬─┬──┘ │
                                                        │                  │ │    │
                                                        └──────────────────┼─┼────┘
                                                                           │ │
                                                                ┌──────────┘ │
                                                                │            │R-T (0E::762)
                                                            ┌───▼───┐   ┌────▼────┐
                                                            │ Phone │   │         │
                                                            ├───────┤   │ ┌─────┐ │
                                                            │ IPv6  │   │ │ /64 │ │
                                                            │0E::d4a│   │ └──┬──┘ │
                                                            └───────┘   │    │N-T1│
                                                                        │ ┌──▼──┐ │
                                                                        │ │ /64 │ │
                                                                        │ ├─────┤ │
                                                                        │ │ 1111│ │
                                                                        │ │  x09│ │
                                                                        │ ├─────┤ │
                                                            ┌───────┐   │ │IPv6 │ │
                                                            │ Phone ◄───┼─┤0F::1│ │
                                                            ├───────┐   │ └─────┘ │
                                                            │ IPv6  │   │         │
                                                            │0F::263│   └─────────┘
                                                            └───────┘

Example Network Packets
=======================

Router Solicit (RS) / Router Advertisement (RA)
-----------------------------------------------

.. code-block:: text

    Ethernet II, Src: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd), Dst: IPv6mcast_02 (33:33:00:00:00:02)
        Destination: IPv6mcast_02 (33:33:00:00:00:02)
            Address: IPv6mcast_02 (33:33:00:00:00:02)
            .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
            .... ...1 .... .... .... .... = IG bit: Group address (multicast/broadcast)
        Source: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            Address: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Type: IPv6 (0x86dd)
    Internet Protocol Version 6, Src: fe80::b46a:8408:2a13:702e, Dst: ff02::2
        0110 .... = Version: 6
        .... 0000 0000 .... .... .... .... .... = Traffic Class: 0x00 (DSCP: CS0, ECN: Not-ECT)
            .... 0000 00.. .... .... .... .... .... = Differentiated Services Codepoint: Default (0)
            .... .... ..00 .... .... .... .... .... = Explicit Congestion Notification: Not ECN-Capable Transport (0)
        .... 1010 0000 0011 0100 1101 = Flow Label: 0xa034d
        Payload Length: 8
        Next Header: ICMPv6 (58)
        Hop Limit: 255
        Source Address: fe80::b46a:8408:2a13:702e
        Destination Address: ff02::2
    Internet Control Message Protocol v6
        Type: Router Solicitation (133)
        Code: 0
        Checksum: 0xaa82 [correct]
        [Checksum Status: Good]
        Reserved: 00000000

    ----------------------------------------

    Ethernet II, Src: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f), Dst: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
        Destination: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            Address: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Source: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            Address: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Type: IPv6 (0x86dd)
    Internet Protocol Version 6, Src: fe80::5e02:14ff:fed6:1e1f, Dst: fe80::b46a:8408:2a13:702e
        0110 .... = Version: 6
        .... 0000 0000 .... .... .... .... .... = Traffic Class: 0x00 (DSCP: CS0, ECN: Not-ECT)
            .... 0000 00.. .... .... .... .... .... = Differentiated Services Codepoint: Default (0)
            .... .... ..00 .... .... .... .... .... = Explicit Congestion Notification: Not ECN-Capable Transport (0)
        .... 1100 1101 0011 0110 0000 = Flow Label: 0xcd360
        Payload Length: 184
        Next Header: ICMPv6 (58)
        Hop Limit: 255
        Source Address: fe80::5e02:14ff:fed6:1e1f
        Destination Address: fe80::b46a:8408:2a13:702e
        [Source SLAAC MAC: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)]
    Internet Control Message Protocol v6
        Type: Router Advertisement (134)
        Code: 0
        Checksum: 0xe0f9 [correct]
        [Checksum Status: Good]
        Cur hop limit: 64
        Flags: 0xc0, Managed address configuration, Other configuration, Prf (Default Router Preference): Medium
            1... .... = Managed address configuration: Set
            .1.. .... = Other configuration: Set
            ..0. .... = Home Agent: Not set
            ...0 0... = Prf (Default Router Preference): Medium (0)
            .... .0.. = ND Proxy: Not set
            .... ..00 = Reserved: 0
        Router lifetime (s): 657
        Reachable time (ms): 0
        Retrans timer (ms): 0
        ICMPv6 Option (Source link-layer address : 5c:02:14:d6:1e:1f)
            Type: Source link-layer address (1)
            Length: 1 (8 bytes)
            Link-layer address: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
        ICMPv6 Option (MTU : 1492)
            Type: MTU (5)
            Length: 1 (8 bytes)
            Reserved
            MTU: 1492
        ICMPv6 Option (Prefix information : 2001:ee0:51d4:ada5::/64)
            Type: Prefix information (3)
            Length: 4 (32 bytes)
            Prefix Length: 64
            Flag: 0xc0, On-link flag(L), Autonomous address-configuration flag(A)
                1... .... = On-link flag(L): Set
                .1.. .... = Autonomous address-configuration flag(A): Set
                ..0. .... = Router address flag(R): Not set
                ...0 0000 = Reserved: 0
            Valid Lifetime: 657 (10 minutes, 57 seconds)
            Preferred Lifetime: 657 (10 minutes, 57 seconds)
            Reserved
            Prefix: 2001:ee0:51d4:ada5::
        ICMPv6 Option (Prefix information : fd3e:cfee:59f0:5::/64)
            Type: Prefix information (3)
            Length: 4 (32 bytes)
            Prefix Length: 64
            Flag: 0xc0, On-link flag(L), Autonomous address-configuration flag(A)
                1... .... = On-link flag(L): Set
                .1.. .... = Autonomous address-configuration flag(A): Set
                ..0. .... = Router address flag(R): Not set
                ...0 0000 = Reserved: 0
            Valid Lifetime: 42659 (11 hours, 50 minutes, 59 seconds)
            Preferred Lifetime: 657 (10 minutes, 57 seconds)
            Reserved
            Prefix: fd3e:cfee:59f0:5::
        ICMPv6 Option (Prefix information : fd75:730e:26fb::/64)
            Type: Prefix information (3)
            Length: 4 (32 bytes)
            Prefix Length: 64
            Flag: 0xc0, On-link flag(L), Autonomous address-configuration flag(A)
                1... .... = On-link flag(L): Set
                .1.. .... = Autonomous address-configuration flag(A): Set
                ..0. .... = Router address flag(R): Not set
                ...0 0000 = Reserved: 0
            Valid Lifetime: Infinity (4294967295) (49710 days, 6 hours, 28 minutes, 15 seconds)
            Preferred Lifetime: Infinity (4294967295) (49710 days, 6 hours, 28 minutes, 15 seconds)
            Reserved
            Prefix: fd75:730e:26fb::
        ICMPv6 Option (Route Information : Medium fd75:730e:26fb::/48)
            Type: Route Information (24)
            Length: 3 (24 bytes)
            Prefix Length: 48
            Flag: 0x00, Route Preference: Medium
                ...0 0... = Route Preference: Medium (0)
                1.   .000 = Reserved: 0
            Route Lifetime: 657 (10 minutes, 57 seconds)
            Prefix: fd75:730e:26fb::
        ICMPv6 Option (Recursive DNS Server fd75:730e:26fb::1)
            Type: Recursive DNS Server (25)
            Length: 3 (24 bytes)
            Reserved
            Lifetime: 657 (10 minutes, 57 seconds)
            Recursive DNS Servers: fd75:730e:26fb::1
        ICMPv6 Option (Advertisement Interval : 219000)
            Type: Advertisement Interval (7)
            Length: 1 (8 bytes)
            Reserved
            Advertisement Interval: 219000

Neighbour Solicit (NS) / Neighbour Advertisement (NA)
-----------------------------------------------------

.. code-block:: text

    Ethernet II, Src: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f), Dst: IPv6mcast_ff:00:09:90 (33:33:ff:00:09:90)
        Destination: IPv6mcast_ff:00:09:90 (33:33:ff:00:09:90)
            Address: IPv6mcast_ff:00:09:90 (33:33:ff:00:09:90)
            .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
            .... ...1 .... .... .... .... = IG bit: Group address (multicast/broadcast)
        Source: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            Address: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Type: IPv6 (0x86dd)
    Internet Protocol Version 6, Src: fe80::5e02:14ff:fed6:1e1f, Dst: ff02::1:ff00:990
        0110 .... = Version: 6
        .... 0000 0000 .... .... .... .... .... = Traffic Class: 0x00 (DSCP: CS0, ECN: Not-ECT)
            .... 0000 00.. .... .... .... .... .... = Differentiated Services Codepoint: Default (0)
            .... .... ..00 .... .... .... .... .... = Explicit Congestion Notification: Not ECN-Capable Transport (0)
        .... 0000 0000 0000 0000 0000 = Flow Label: 0x00000
        Payload Length: 32
        Next Header: ICMPv6 (58)
        Hop Limit: 255
        Source Address: fe80::5e02:14ff:fed6:1e1f
        Destination Address: ff02::1:ff00:990
        [Source SLAAC MAC: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)]
    Internet Control Message Protocol v6
        Type: Neighbor Solicitation (135)
        Code: 0
        Checksum: 0x1ab4 [correct]
        [Checksum Status: Good]
        Reserved: 00000000
        Target Address: 2001:ee0:51d4:ada5::990
        ICMPv6 Option (Source link-layer address : 5c:02:14:d6:1e:1f)
            Type: Source link-layer address (1)
            Length: 1 (8 bytes)
            Link-layer address: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)

    ----------------------------------------

    Ethernet II, Src: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd), Dst: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
        Destination: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            Address: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Source: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            Address: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Type: IPv6 (0x86dd)
    Internet Protocol Version 6, Src: 2001:ee0:51d4:ada5::990, Dst: fe80::5e02:14ff:fed6:1e1f
        0110 .... = Version: 6
        .... 0000 0000 .... .... .... .... .... = Traffic Class: 0x00 (DSCP: CS0, ECN: Not-ECT)
            .... 0000 00.. .... .... .... .... .... = Differentiated Services Codepoint: Default (0)
            .... .... ..00 .... .... .... .... .... = Explicit Congestion Notification: Not ECN-Capable Transport (0)
        .... 0000 0000 0000 0000 0000 = Flow Label: 0x00000
        Payload Length: 32
        Next Header: ICMPv6 (58)
        Hop Limit: 255
        Source Address: 2001:ee0:51d4:ada5::990
        Destination Address: fe80::5e02:14ff:fed6:1e1f
        [Destination SLAAC MAC: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)]
    Internet Control Message Protocol v6
        Type: Neighbor Advertisement (136)
        Code: 0
        Checksum: 0xdc90 [correct]
        [Checksum Status: Good]
        Flags: 0x60000000, Solicited, Override
            0... .... .... .... .... .... .... .... = Router: Not set
            .1.. .... .... .... .... .... .... .... = Solicited: Set
            ..1. .... .... .... .... .... .... .... = Override: Set
            ...0 0000 0000 0000 0000 0000 0000 0000 = Reserved: 0
        Target Address: 2001:ee0:51d4:ada5::990
        ICMPv6 Option (Target link-layer address : 1c:1b:0d:ac:10:fd)
            Type: Target link-layer address (2)
            Length: 1 (8 bytes)
            Link-layer address: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)

DHCPv6 Solicit / DHCPv6 Reply
-----------------------------

.. code-block:: text

    Ethernet II, Src: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd), Dst: IPv6mcast_01:00:02 (33:33:00:01:00:02)
        Destination: IPv6mcast_01:00:02 (33:33:00:01:00:02)
            Address: IPv6mcast_01:00:02 (33:33:00:01:00:02)
            .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
            .... ...1 .... .... .... .... = IG bit: Group address (multicast/broadcast)
        Source: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            Address: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Type: IPv6 (0x86dd)
    Internet Protocol Version 6, Src: fe80::b46a:8408:2a13:702e, Dst: ff02::1:2
        0110 .... = Version: 6
        .... 0000 0000 .... .... .... .... .... = Traffic Class: 0x00 (DSCP: CS0, ECN: Not-ECT)
            .... 0000 00.. .... .... .... .... .... = Differentiated Services Codepoint: Default (0)
            .... .... ..00 .... .... .... .... .... = Explicit Congestion Notification: Not ECN-Capable Transport (0)
        .... 0100 1000 1011 0101 0101 = Flow Label: 0x48b55
        Payload Length: 96
        Next Header: UDP (17)
        Hop Limit: 1
        Source Address: fe80::b46a:8408:2a13:702e
        Destination Address: ff02::1:2
    User Datagram Protocol, Src Port: 546, Dst Port: 547
        Source Port: 546
        Destination Port: 547
        Length: 96
        Checksum: 0xd0ac [unverified]
        [Checksum Status: Unverified]
        [Stream index: 38]
        [Timestamps]
            [Time since first frame: 0.000000000 seconds]
            [Time since previous frame: 0.000000000 seconds]
        UDP payload (88 bytes)
    DHCPv6
        Message type: Solicit (1)
        Transaction ID: 0x0ba536
        Rapid Commit
            Option: Rapid Commit (14)
            Length: 0
        Identity Association for Non-temporary Address
            Option: Identity Association for Non-temporary Address (3)
            Length: 12
            IAID: cb390ac7
            T1: 0
            T2: 0
        Client Fully Qualified Domain Name
            Option: Client Fully Qualified Domain Name (39)
            Length: 18
            Flags: 0x01  [CLIENT wants SERVER to update both its AAAA and PTR RRs]
                .... .0.. = N bit: Server SHOULD perform PTR RR updates
                .... ...1 = S bit: Server SHOULD perform AAAA RR updates
            Partial domain name: opensuse-desktop
        Option Request
            Option: Option Request (6)
            Length: 10
            Requested Option code: DNS recursive name server (23)
            Requested Option code: Domain Search List (24)
            Requested Option code: Simple Network Time Protocol Server (31)
            Requested Option code: NTP Server (56)
            Requested Option code: SOL_MAX_RT (82)
        Client Identifier
            Option: Client Identifier (1)
            Length: 18
            DUID: 0004af07971a081c0178124009475c1f62f8
            DUID Type: Universally Unique IDentifier (UUID) (4)
            UUID: af07971a081c0178124009475c1f62f8
        Elapsed time
            Option: Elapsed time (8)
            Length: 2
            Elapsed time: 0ms

    ----------------------------------------

    Ethernet II, Src: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f), Dst: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
        Destination: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            Address: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Source: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            Address: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Type: IPv6 (0x86dd)
    Internet Protocol Version 6, Src: fe80::5e02:14ff:fed6:1e1f, Dst: fe80::b46a:8408:2a13:702e
        0110 .... = Version: 6
        .... 0000 0000 .... .... .... .... .... = Traffic Class: 0x00 (DSCP: CS0, ECN: Not-ECT)
            .... 0000 00.. .... .... .... .... .... = Differentiated Services Codepoint: Default (0)
            .... .... ..00 .... .... .... .... .... = Explicit Congestion Notification: Not ECN-Capable Transport (0)
        .... 0101 1001 0001 1000 1000 = Flow Label: 0x59188
        Payload Length: 180
        Next Header: UDP (17)
        Hop Limit: 64
        Source Address: fe80::5e02:14ff:fed6:1e1f
        Destination Address: fe80::b46a:8408:2a13:702e
        [Source SLAAC MAC: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)]
    User Datagram Protocol, Src Port: 547, Dst Port: 546
        Source Port: 547
        Destination Port: 546
        Length: 180
        Checksum: 0x60f0 [unverified]
        [Checksum Status: Unverified]
        [Stream index: 39]
        [Timestamps]
            [Time since first frame: 0.000000000 seconds]
            [Time since previous frame: 0.000000000 seconds]
        UDP payload (172 bytes)
    DHCPv6
        Message type: Reply (7)
        Transaction ID: 0x0ba536
        Server Identifier
            Option: Server Identifier (2)
            Length: 10
            DUID: 000300015c0214d61e1f
            DUID Type: link-layer address (3)
            Hardware type: Ethernet (1)
            Link-layer address: 5c:02:14:d6:1e:1f
            Link-layer address (Ethernet): XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
        Client Identifier
            Option: Client Identifier (1)
            Length: 18
            DUID: 0004af07971a081c0178124009475c1f62f8
            DUID Type: Universally Unique IDentifier (UUID) (4)
            UUID: af07971a081c0178124009475c1f62f8
        SOL_MAX_RT
            Option: SOL_MAX_RT (82)
            Length: 4
        Rapid Commit
            Option: Rapid Commit (14)
            Length: 0
        DNS recursive name server
            Option: DNS recursive name server (23)
            Length: 16
             1 DNS server address: fd75:730e:26fb::1
        Identity Association for Non-temporary Address
            Option: Identity Association for Non-temporary Address (3)
            Length: 96
            IAID: cb390ac7
            T1: 245
            T2: 392
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: 2001:ee0:51d4:ada5::990
                Preferred lifetime: 491
                Valid lifetime: 491
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: fd3e:cfee:59f0:5::990
                Preferred lifetime: 491
                Valid lifetime: 43091
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: fd75:730e:26fb::990
                Preferred lifetime: 491
                Valid lifetime: 43200

DHCPv6 Renew / DHCPv6 Reply
---------------------------

.. code-block:: text

    Ethernet II, Src: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd), Dst: IPv6mcast_01:00:02 (33:33:00:01:00:02)
        Destination: IPv6mcast_01:00:02 (33:33:00:01:00:02)
            Address: IPv6mcast_01:00:02 (33:33:00:01:00:02)
            .... ..1. .... .... .... .... = LG bit: Locally administered address (this is NOT the factory default)
            .... ...1 .... .... .... .... = IG bit: Group address (multicast/broadcast)
        Source: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            Address: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Type: IPv6 (0x86dd)
    Internet Protocol Version 6, Src: fe80::b46a:8408:2a13:702e, Dst: ff02::1:2
        0110 .... = Version: 6
        .... 0000 0000 .... .... .... .... .... = Traffic Class: 0x00 (DSCP: CS0, ECN: Not-ECT)
            .... 0000 00.. .... .... .... .... .... = Differentiated Services Codepoint: Default (0)
            .... .... ..00 .... .... .... .... .... = Explicit Congestion Notification: Not ECN-Capable Transport (0)
        .... 0100 1000 1011 0101 0101 = Flow Label: 0x48b55
        Payload Length: 188
        Next Header: UDP (17)
        Hop Limit: 1
        Source Address: fe80::b46a:8408:2a13:702e
        Destination Address: ff02::1:2
    User Datagram Protocol, Src Port: 546, Dst Port: 547
        Source Port: 546
        Destination Port: 547
        Length: 188
        Checksum: 0xd108 [unverified]
        [Checksum Status: Unverified]
        [Stream index: 38]
        [Timestamps]
            [Time since first frame: 245.053365474 seconds]
            [Time since previous frame: 245.053365474 seconds]
        UDP payload (180 bytes)
    DHCPv6
        Message type: Renew (5)
        Transaction ID: 0x908485
        Server Identifier
            Option: Server Identifier (2)
            Length: 10
            DUID: 000300015c0214d61e1f
            DUID Type: link-layer address (3)
            Hardware type: Ethernet (1)
            Link-layer address: 5c:02:14:d6:1e:1f
            Link-layer address (Ethernet): XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
        Identity Association for Non-temporary Address
            Option: Identity Association for Non-temporary Address (3)
            Length: 96
            IAID: cb390ac7
            T1: 0
            T2: 0
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: fd75:730e:26fb::990
                Preferred lifetime: 0
                Valid lifetime: 0
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: fd3e:cfee:59f0:5::990
                Preferred lifetime: 0
                Valid lifetime: 0
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: 2001:ee0:51d4:ada5::990
                Preferred lifetime: 0
                Valid lifetime: 0
        Client Fully Qualified Domain Name
            Option: Client Fully Qualified Domain Name (39)
            Length: 18
            Flags: 0x01  [CLIENT wants SERVER to update both its AAAA and PTR RRs]
                .... .0.. = N bit: Server SHOULD perform PTR RR updates
                .... ...1 = S bit: Server SHOULD perform AAAA RR updates
            Partial domain name: opensuse-desktop
        Option Request
            Option: Option Request (6)
            Length: 8
            Requested Option code: DNS recursive name server (23)
            Requested Option code: Domain Search List (24)
            Requested Option code: Simple Network Time Protocol Server (31)
            Requested Option code: NTP Server (56)
        Client Identifier
            Option: Client Identifier (1)
            Length: 18
            DUID: 0004af07971a081c0178124009475c1f62f8
            DUID Type: Universally Unique IDentifier (UUID) (4)
            UUID: af07971a081c0178124009475c1f62f8
        Elapsed time
            Option: Elapsed time (8)
            Length: 2
            Elapsed time: 0ms

    ----------------------------------------

    Ethernet II, Src: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f), Dst: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
        Destination: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            Address: GigaByteTech_ac:10:fd (1c:1b:0d:ac:10:fd)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Source: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            Address: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
            .... ..0. .... .... .... .... = LG bit: Globally unique address (factory default)
            .... ...0 .... .... .... .... = IG bit: Individual address (unicast)
        Type: IPv6 (0x86dd)
    Internet Protocol Version 6, Src: fe80::5e02:14ff:fed6:1e1f, Dst: fe80::b46a:8408:2a13:702e
        0110 .... = Version: 6
        .... 0000 0000 .... .... .... .... .... = Traffic Class: 0x00 (DSCP: CS0, ECN: Not-ECT)
            .... 0000 00.. .... .... .... .... .... = Differentiated Services Codepoint: Default (0)
            .... .... ..00 .... .... .... .... .... = Explicit Congestion Notification: Not ECN-Capable Transport (0)
        .... 0101 1001 0001 1000 1000 = Flow Label: 0x59188
        Payload Length: 176
        Next Header: UDP (17)
        Hop Limit: 64
        Source Address: fe80::5e02:14ff:fed6:1e1f
        Destination Address: fe80::b46a:8408:2a13:702e
        [Source SLAAC MAC: XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)]
    User Datagram Protocol, Src Port: 547, Dst Port: 546
        Source Port: 547
        Destination Port: 546
        Length: 176
        Checksum: 0x798f [unverified]
        [Checksum Status: Unverified]
        [Stream index: 39]
        [Timestamps]
            [Time since first frame: 245.053601433 seconds]
            [Time since previous frame: 245.053601433 seconds]
        UDP payload (168 bytes)
    DHCPv6
        Message type: Reply (7)
        Transaction ID: 0x908485
        Server Identifier
            Option: Server Identifier (2)
            Length: 10
            DUID: 000300015c0214d61e1f
            DUID Type: link-layer address (3)
            Hardware type: Ethernet (1)
            Link-layer address: 5c:02:14:d6:1e:1f
            Link-layer address (Ethernet): XiaomiMobile_d6:1e:1f (5c:02:14:d6:1e:1f)
        Client Identifier
            Option: Client Identifier (1)
            Length: 18
            DUID: 0004af07971a081c0178124009475c1f62f8
            DUID Type: Universally Unique IDentifier (UUID) (4)
            UUID: af07971a081c0178124009475c1f62f8
        SOL_MAX_RT
            Option: SOL_MAX_RT (82)
            Length: 4
        DNS recursive name server
            Option: DNS recursive name server (23)
            Length: 16
             1 DNS server address: fd75:730e:26fb::1
        Identity Association for Non-temporary Address
            Option: Identity Association for Non-temporary Address (3)
            Length: 96
            IAID: cb390ac7
            T1: 424
            T2: 679
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: 2001:ee0:51d4:ada5::990
                Preferred lifetime: 849
                Valid lifetime: 849
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: fd3e:cfee:59f0:5::990
                Preferred lifetime: 849
                Valid lifetime: 43148
            IA Address
                Option: IA Address (5)
                Length: 24
                IPv6 address: fd75:730e:26fb::990
                Preferred lifetime: 849
                Valid lifetime: 43200
