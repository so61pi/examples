==========
 Patching
==========

Extend go-back-steps
====================

- Open ``qtcreator-4.1.0/lib/qtcreator/plugins/libCore.so`` in IDA.

- Search ``EditorManager::addCurrentPositionToNavigationHistory`` function.

  - It's at ``.text:00000000000D3540``.
  - There are 2 ``call`` instructions and 1 ``jmp`` instruction:

    - The 1st ``call`` (``.text:00000000000D3544 call sub_CDCD``) calls to ``EditorManagerPrivate::currentEditorView``.
    - The 2nd ``call`` (``.text:00000000000D354F call sub_E54C0``) calls to ``EditorView::addCurrentPositionToNavigationHistory``.
    - The ``jmp`` (``.text:00000000000D3555 jmp sub_D03C0``) is just a call to ``EditorManagerPrivate::updateActions``.

  - References

    - `EditorManager::addCurrentPositionToNavigationHistory <https://github.com/qtproject/qt-creator/blob/v4.1.0/src/plugins/coreplugin/editormanager/editormanager.cpp#L2845>`__
    - `EditorManagerPrivate::currentEditorView <https://github.com/qtproject/qt-creator/blob/v4.1.0/src/plugins/coreplugin/editormanager/editormanager.cpp#L2262>`__
    - `EditorView::addCurrentPositionToNavigationHistory <https://github.com/qtproject/qt-creator/blob/v4.1.0/src/plugins/coreplugin/editormanager/editorview.cpp#L469>`__
    - `EditorManagerPrivate::updateActions <https://github.com/qtproject/qt-creator/blob/v4.1.0/src/plugins/coreplugin/editormanager/editormanager.cpp#L1707>`__

- Patching

  - At ``.text:00000000000E57D5``

    - From ``83 FA 1D`` to ``83 FA 7D`` (from ``cmp edx, 29`` to ``cmp edx, 125``).
    - That means we change ``while (m_navigationHistory.size() >= 30)`` to ``while (m_navigationHistory.size() >= 126)``.

  - At ``.text:00000000000E57DE``

    - From ``83 BB A0 00 00 00 0F`` to ``83 BB A0 00 00 00 3F`` (from ``cmp dword ptr [rbx+0A0h], 15`` to ``cmp dword ptr [rbx+0A0h], 63``).
    - That means we change ``if (m_currentNavigationHistoryPosition > 15)`` to ``if (m_currentNavigationHistoryPosition > 63)``.

Extend locator
==============

- Search for locator widget width 730, we get here

  .. code-block:: cpp

      // https://github.com/qtproject/qt-creator/blob/v4.1.0/src/plugins/coreplugin/locator/locatorwidget.cpp#L216
      m_preferredSize = QSize(730, shint.height() * 17 + frameWidth() * 2);

- Load ``libCore.so`` into IDA

  We cannot search for containing function ``CompletionList::updatePreferredSize`` so we find nearby functions then check their callers.

  Search ``itemDelegate`` and find its callers, we get here

  .. code-block::

      .text:00000000001D8059                 call    QAbstractItemView::itemDelegate(void)
      ...
      .text:00000000001D80DA                 mov     dword ptr [rbp+30h], 2DAh

  Now we can change ``2DAh`` (730) to something bigger but that not enough because in ``CompletionList::CompletionList`` the maximum widget width is limited to 900 by calling ``setMaximumWidth(900);``

  .. code-block:: cpp

      // https://github.com/qtproject/qt-creator/blob/v4.1.0/src/plugins/coreplugin/locator/locatorwidget.cpp#L197
      setMaximumWidth(900);

  Its assembly is right above ``CompletionList::updatePreferredSize`` as in C++ code.

  .. code-block::

      .text:00000000001D7FC8                 mov     esi, 384h
      .text:00000000001D7FCD                 mov     rdi, rbx
      .text:00000000001D7FD0                 call    QWidget::setMaximumWidth(int)

  After patching this, the widget becomes bigger as we want it.

- Search pattern for patching

  ``2DAh`` (700) patch search pattern

  .. code-block::

      C7 45 30 DA 02 00 00 89 45 34 E8 47 BF EB FF 48 8B BC 24 E8 00 00 00

  ``384h`` (900) patch search pattern

  .. code-block::

      BE 84 03 00 00 48 89 DF E8 6B DD EB FF 48 89 DF E8 C3 02 EC FF

Extend search history
=====================

- We need to modify function ``SearchResultWindow::startNewSearch``, change ``MAX_SEARCH_HISTORY``.

Reference
---------

- https://github.com/qtproject/qt-creator/blob/v4.1.0/src/plugins/coreplugin/find/searchresultwindow.cpp#L51
- https://github.com/qtproject/qt-creator/blob/v4.1.0/src/plugins/coreplugin/find/searchresultwindow.cpp#L375
