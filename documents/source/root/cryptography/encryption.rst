============================
 Encrypt/Decrypt Algorithms
============================

Asymmetric
==========

RSA
---

Create Keypair
~~~~~~~~~~~~~~

- Choose two prime numbers **p = 11** and **q = 3**
- Compute **n = pq = 11 * 3 = 33**
- Compute **phi = (p - 1)(q - 1) = 10 * 2 = 20**
- Choose **e = 3** so that **gcd(e, phi) = 1**
- Choose **d = 7** such that **ed ≡ 1 (mod phi)** (https://en.wikipedia.org/wiki/Modular_arithmetic)
- Public key is **(n, e) = (33, 3)**
- Private key is **(n, d) = (33, 7)**

Encrypt
~~~~~~~

- Message **m = 7** is encrypted by computing **c = m**:sup:`e` **mod n = 7**:sup:`3` **mod 33 = 13**

Decrypt
~~~~~~~

- Cipher **c = 13** is decrypted by computing **m' = c**:sup:`d` **mod n = 13**:sup:`7` **mod 33 = 7**

References
~~~~~~~~~~

- https://www.di-mgt.com.au/rsa_alg.html

EC (Elliptic Curve)
-------------------

Overview
~~~~~~~~

An elliptic curve is the set of points that satisfy below equation.

.. math::

   y^2 = x^3 + ax + b

And here is how it looks when :math:`a = -1` and :math:`b = 1`

.. image:: encryption/elliptic-curve.png
   :scale: 30
   :align: center

On the curve, we can add points, with the following rules:

.. math::
   :nowrap:

   \begin{eqnarray}
      A + A = B & & (1) \\
      A + B = C & & (3) \\
      A + C = D & & (5) \\
   \end{eqnarray}

.. image:: encryption/elliptic-curve-point-addition.svg
   :scale: 30
   :align: center

.. note::

   :math:`A + A` creates a tangent line.

These addition operations can be collapsed into a multiplication operation.

.. math::
   :nowrap:

   \begin{eqnarray}
          & A + C         & = D \\
      <=> & A + A + B     & = D \\
      <=> & A + A + A + A & = D \\
      <=> & 4A            & = D
   \end{eqnarray}

And we can use this to encrypt our data. In the example here, :math:`4A = D`,

- :math:`4` is our **private key**.

  * In reality, this is a very big number.

- :math:`(x-of-D, y-of-D)` is our **public key**.

- :math:`A` is the **base point**.

  * Each specific curve has its own base point.

And it is proved that, with only :math:`A` and :math:`D`, we cannot infer :math:`4`, our private key (i.e. we cannot know how many times we need to jump from A in the curve to get to D). (Well, in this case we can, but not if it's a big number.)

Encrypt/Decrypt
~~~~~~~~~~~~~~~

.. image:: encryption/elliptic-curve-encryption.svg
   :align: center

References
~~~~~~~~~~

- https://arstechnica.com/information-technology/2013/10/a-relatively-easy-to-understand-primer-on-elliptic-curve-cryptography/2/
- https://en.wikipedia.org/wiki/Elliptic_curve_point_multiplication
- https://www.youtube.com/watch?v=dCvB-mhkT0w

Symmetric
=========

AES (Advanced Encryption Standard)
----------------------------------

TODO

Round Function
~~~~~~~~~~~~~~

Authenticated Encryption With Additional Data (AEAD) Modes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- **Authenticated Encryption**'s output are:

  * **Encrypted Data**
  * **Authentication Tag**: Used to make sure that **Encrypted Data** is not modified.

- **Authenticated Encryption With Additional Data**

List of some available modes

- GCM (Galois/counter)
- CCV (Counter with cipher block chaining message authentication code)
- SIV (Synthetic initialization vector)
- AES-GCM-SIV

Confidentiality Only Modes
~~~~~~~~~~~~~~~~~~~~~~~~~~

List of some available modes

- ECB (Electronic codebook)
- CBC (Cipher block chaining)
- CFB (Cipher feedback)
- OFB (Output feedback)
- CTR (Counter)

References
~~~~~~~~~~

- https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation
- https://www.highgo.ca/2019/08/08/the-difference-in-five-modes-in-the-aes-encryption-algorithm/

References
==========
