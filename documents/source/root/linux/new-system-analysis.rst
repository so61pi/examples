=====================
 New System Analysis
=====================

Commands
========

- Different system types (server, desktop, embedded...) have different essential components and their configuration files also widely different.
- Some utilities can export running configuration (e.g. ``sshd -T``).

+------------------+-------------------------------------------+
| Category         | Commands                                  |
+==================+===========================================+
| Process          | - ``ps aux``                              |
|                  | - ``ps auxf``                             |
|                  | - ``top``                                 |
|                  | - ``ls -alh /proc/1``                     |
|                  | - ``cat /proc/1/cmdline``                 |
|                  | - ``cat /proc/1/comm``                    |
+------------------+-------------------------------------------+
| Memory           | - ``free``                                |
|                  | - ``vmstat -s``                           |
|                  | - ``cat /proc/meminfo``                   |
|                  | - ``cat /proc/vmstat``                    |
|                  | - ``cat /proc/vmallocinfo``               |
+------------------+-------------------------------------------+
| Network          | - ``ip addr``                             |
|                  | - ``ip route``                            |
|                  | - ``sudo iptables -S``                    |
|                  | - # Firewall zones/rules/services/ports   |
|                  | - ``systemd-resolve --status``            |
|                  | - ``cat /etc/resolv.conf``                |
|                  | - ``dig google.com``                      |
|                  | - ``sudo lsof -i -P -n | grep LISTEN``    |
+------------------+-------------------------------------------+
| Mount/Filesystem | - ``mount``                               |
|                  | - ``mount | column -t``                   |
|                  | - ``cat /etc/fstab``                      |
|                  | - ``cat /proc/mounts``                    |
|                  | - ``cat /proc/filesystems``               |
+------------------+-------------------------------------------+
| Storage          | - ``df``                                  |
|                  | - ``lsblk``                               |
|                  | - ``sudo blkid``                          |
|                  | - ``sudo fdisk -l``                       |
|                  | - ``sudo fdisk -l /dev/sda``              |
|                  | - ``sudo fdisk -l /dev/sda1``             |
|                  | - ``cat /proc/partitions``                |
+------------------+-------------------------------------------+
| Services/Inittab | - ``systemctl status``                    |
|                  | - ``systemctl --failed``                  |
|                  | - ``systemctl list-units --type=service`` |
|                  | - ``cat /etc/inittab``                    |
+------------------+-------------------------------------------+
| Timers/Crontab   | - ``systemctl list-timers``               |
|                  | - ``cat /etc/crontab``                    |
+------------------+-------------------------------------------+
| SSH              | - ``ls -alh /etc/ssh``                    |
|                  | - ``ls -alh ~/.ssh``                      |
|                  | - ``ls -alh /etc/dropbear``               |
+------------------+-------------------------------------------+
| Time             | - ``date``                                |
|                  | - ``timedatectl``                         |
|                  | - ``cat /etc/systemd/timesyncd.conf``     |
+------------------+-------------------------------------------+
| Config           | - ``ls -alh /etc``                        |
+------------------+-------------------------------------------+
| Users/Groups     | - ``cat /etc/passwd``                     |
|                  | - ``cat /etc/group``                      |
|                  | - ``cat /etc/shadow``                     |
|                  | - ``cat /etc/sudo.conf``                  |
|                  | - ``cat /etc/sudoers``                    |
|                  | - ``ls -alh /etc/sudoers.d``              |
+------------------+-------------------------------------------+
| Log              | - ``ls -alh /var/log``                    |
|                  | - ``sudo dmesg``                          |
|                  | - ``sudo journalctl``                     |
+------------------+-------------------------------------------+
| Others           | - ``uptime``                              |
|                  | - ``uname -a``                            |
|                  | - ``sudo dmidecode``                      |
|                  | - ``zcat /proc/config.gz``                |
|                  | - ``lscpu``                               |
|                  | - ``lsusb``                               |
|                  | - ``lsusb -v``                            |
|                  | - ``cat /etc/sysctl.conf``                |
|                  | - ``ls -alh /etc/sysctl.d``               |
|                  | - ``cat /etc/nsswitch.conf``              |
|                  | - ``cat /proc/cmdline``                   |
|                  | - ``cat /proc/cpuinfo``                   |
|                  | - ``cat /proc/devices``                   |
|                  | - ``cat /proc/modules``                   |
|                  | - ``ls -alh /sys/module``                 |
|                  | - ``ls -alh /dev``                        |
|                  | - ``tree -L 2 /dev/disk``                 |
|                  | - ``ls -alh /dev/char``                   |
|                  | - ``tree -L 3 /sys``                      |
|                  | - ``ls -alh /``                           |
|                  | - ``ls -alh /boot``                       |
|                  | - ``tree /boot/efi``                      |
|                  | - ``ls -alh /bin``                        |
|                  | - ``ls -alh /sbin``                       |
|                  | - ``ls -alh /usr/bin``                    |
|                  | - ``ls -alh /usr/sbin``                   |
|                  | - ``ls -alh /var``                        |
|                  | - ``ls -alh /var/lib``                    |
+------------------+-------------------------------------------+
