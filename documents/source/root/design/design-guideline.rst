==================
 Design Guideline
==================

Cross-cutting Concerns/Global Resources
=======================================

Database Connections
--------------------

Database connections are usually placed in a pool. When our application needs one, it gets one from the pool and lock until one is avaiable for use. The way a connection is passed around during request handling depends on the execution model of our application:

- With thread-based applications, we can add db connections to thread local storage when we receive the request, use it throughout handling process, then return it back to the thread pool after done.
- With async-based applications, we can use task-local storage. This is similar to thread-based approach.
- Or, we can pass it as a parameter. This approach is a little cumbersome, but works in any case.

Logging
-------

Logging can have multiple levels of context such as commit hash, host id, app start time, user request id...

We can also place a random unique id into the data that is passed between systems to trace the flow. As an example, let's say we have a system that when a user registers, we send to them a confirmation email. If email sending is in an external service, then adding a random id to data passed by main app to email sending application can make troubleshooting faster.

Another aspect we can find helpful is the start time of an event. For instance, we may need to search log around the time our application started. We can of course search backwards in time day by day (because searching for a whole month can be very slow) until we find that point in time, but this process is time consuming. Instead, when we generate a unique id for an event, we should also include the current time.

Note that even though time used in logging should always be UTC, if the log comes from multiple places around the world, then we should also log local timezone, or other type of information that can help us to group log geographically.

  .. code-block:: python
      :caption: service1.py

      def service_1_middleware_uuid(request):
          request.global_id = request.type() + UUID() + now() # TODO: Move this somewhere else.
          request.task_id = request.type() + UUID() + now() 

      def register(request):
          log(request.global_id) # Global request id.
          log(request.task_id) # Register task id.
          send_email(request);

  .. code-block:: python
      :caption: service2.py

      def service_2_middleware_uuid(request):
          request.task_id = "send-email" + UUID() + now() 

      def send_email(request):
          log(request.global_id) # Global request id.
          log(request.task_id) # Send email task id.

The way we pass and add more context to log should be similar to database connection.

App options
-----------

It is tempting to put application options (read from config file or command line) to a global read-only variable, but this will make unit testing more complicated as we cannot run tests parallelly.

The way we pass app options should be similar to database connection.

  .. code-block:: c++

      struct AppOptions {};

      shared_ptr<AppOptions const> load_options();

      void dothing(shared_ptr<AppOptions const> opts) {}

      void dothing(AppOptions const& opts) {}

      void dothing() {
          AppOptions const& opts = tls.get_app_options();
      }

Time
----

When a function calls time-related functions, it is harder to test it. The reason is the result of given function now doesn't completely depend on its inputs alone.

There are two strategies to solve this:

- Pass required time value as inputs of function (e.g. current time).
- Create a mockable time object and use it instead of calling system provided time functions directly.

.. code-block:: python
    :caption: not-good.py

    def save_record(record):
        updated_at = now()
        # Save record & updated_at.

.. code-block:: python
    :caption: better.py

    def save_record(record, updated_at):
        # Save record & updated_at.

.. code-block:: python
    :caption: better.py

    def save_record(record, mockable_time_object):
        updated_at = mockable_time_object.now()
        # Save record & updated_at.

There are some other issues with time, please refer to :ref:`this document <link-others-time>` for more information.

Filesystem
----------

This is similar to time, but less arbitrary. Most of the time, it is fine to use system provided functions. However, in case we want to mock it for testing purpose, there can be two approaches:

- Create one big mockable object that is used across all functions.
- Create multiple small mockable objects, each is for one function.

There can be a few other issues when working with filesystem, more information can be found at the links below.

- https://danluu.com/filesystem-errors/
- https://danluu.com/file-consistency/

Database
========

- Make sure an object doesn't have more than one owner. And this works all the way down.
- Database with big volume of data can be partitioned/sharded, but that can effectively make its schema frozen.
- For removing an object, instead of actually removing, we can mark old object as deleted, and create new version of that same object.
- Data versioning + schema versioning.

Upgrading/Migrating
-------------------

Migration Types
~~~~~~~~~~~~~~~

- Have both up and down paths.

  * Down path can be helpful when we evaluate different strategies of updating schema.
  * Not all changes are reversible. If a change affects not only the schema, but also the data (e.g. ``DROP TABLE``), it is irreversible.

- Have only up path.

Versioning Tactics
~~~~~~~~~~~~~~~~~~

Migration Script Naming
```````````````````````

.. code-block:: none

    <YYYY><MM><DD><HH><MM><SS>_<COMMENT>

    db/migrations/2019-08-15-133237_create_posts/up.sql
    db/migrations/2019-08-15-133237_create_posts/down.sql

When migrations are run, all migrations that are not in database are run in alphabetical order. In case there are migration scripts with lower timestamp than in database but they are not in database yet, they will still be executed. Hence, keeping the order of the scripts at merge time is important.

For example, in our current dev database we have version `2019-08-15-050000`, then some one merges `2019-08-15-040000` in. Now if we install new instance of our application, `2019-08-15-040000` will be run before `2019-08-15-050000`. But for upgrading current dev database, `2019-08-15-040000` is after already existing versions. This may or may not lead to unexpected consequence.

Tracking
````````

.. code-block:: sql

    CREATE TABLE schema_changes (
      id SERIAL PRIMARY KEY,
      version TEXT NOT NULL, -- <YYYY><MM><DD><HH><MM><SS>_<COMMENT>
      date_applied TIMESTAMP WITH TIME ZONE
    )

Versioning
``````````

- All changes are migration scripts.

  * Releasing

    Releases are finalized on release branches. This scheme is suitable when we control all installations.

    In case we need to change the database schema on the release branch, we create a migration script for that on release branch (v2), and also pick it to the dev branch. Note that the picked version on dev must not conflict with the original one, otherwise we cannot upgrade our application from v2 (contains original version) to v3 (contains picked version). Specifically, the up path of picked version must be a no-op in case original one is already applied, and the down path of original must be a no-op if the picked version is already reverted.

    This is simple, but we cannot remove old migration scripts as all new installations invoke all of them. It can also lead to errors as the number of scripts grows.

  * Applying migrations

    .. code-block:: none

        applicable change scripts = migrations in source code - schema_changes
        apply change scripts

- One baseline script for each application version.

  * Releasing

    Releases are finalized on release branches. This scheme is suitable when users install the software on their own.

    - Imagine we have some migrations like this on master.

      .. code-block:: none

          src/
          └── db/
              ├── baseline-details/
              └── migrations/
                  ├── 0000-00-00-000000_baseline/             <-- empty at this point
                  ├── 2019-08-15-000000_v1/                   <-- only a marker, always empty
                  ├── 2019-08-16-000000_add_table/
                  ├── 2019-08-17-000000_add_column/
                  └── 2019-08-18-000000_change_column_type/

    - Then we add a new commit with empty v2 migration script to clearly separate v1 and v2. It also will help when we merge release branch back to master.

      .. code-block:: none

          src/
          └── db/
              ├── baseline-details/
              └── migrations/
                  ├── 0000-00-00-000000_baseline/
                  ├── 2019-08-15-000000_v1/
                  ├── 2019-08-16-000000_add_table/
                  ├── 2019-08-17-000000_add_column/
                  ├── 2019-08-18-000000_change_column_type/
                  └── 2019-08-19-000000_v2/                   <-- added on master, always empty

    - Create a release branch from the commit right before the above commit. Then we do all the testing. In case we need to add a new fix with a migration, we add one on both release and master branches (and they must be compatible). Finally, we create a new baseline from all current migrations.

      .. code-block:: none

          src/
          └── db/
              ├── baseline-details/                           <-- move all v1 migrations to baseline-details
              │   │                                               this means current baseline equals to baseline-details
              │   ├── 2019-08-15-000000_v1/
              │   ├── 2019-08-16-000000_add_table/
              │   ├── 2019-08-17-000000_add_column/
              │   └── 2019-08-18-000000_change_column_type/
              └── migrations/
                  ├── 0000-00-00-000000_baseline/             <-- update to new baseline = 2019-08-16-000000 + 2019-08-17-000000 + 2019-08-18-000000
                  │                                               can be generated by exporting schema from database.
                  └── 0000-00-00-000000_v1_new_install/       <-- used to mark new installs from this particular baseline

      Note that baseline is only used for new install. If upgrading is an option, we also want to create a migration package that holds all migrations in a particular version. For instance, upgrading version 1 to version 2 requires all migrations from v2 to before v3.

    - Now we need to update baseline on master by merging release branch.

    .. code-block:: none

        src/
        └── db/
            ├── baseline-details/
            │   ├── 2019-08-15-000000_v1/
            │   ├── 2019-08-16-000000_add_table/
            │   ├── 2019-08-17-000000_add_column/
            │   └── 2019-08-18-000000_change_column_type/
            └── migrations/
                ├── 0000-00-00-000000_baseline/             <-- new baseline merged back from release branch
                │                                           <-- we don't keep v1_new_install here as it is useless and will make the schema version table in database polluted
                ├── 2019-08-19-000000_v2/
                └── 2019-08-20-000000_add_table/            <-- someone already adds new migration script in v2

  * Applying migrations

    .. code-block:: none

        if schema_changes table does not exist {
          apply baseline
        }
        applicable change scripts = migrations in source code - schema_changes
        apply change scripts

When merging migration scripts from feature branch to main development branch, we must make sure the scripts on the source branch have higher versions than the ones in the dest branch.

We also have to take care of migrations in code, like migrating data with complex logic.

References
~~~~~~~~~~

- https://odetocode.com/blogs/scott/archive/2008/02/03/versioning-databases-branching-and-merging.aspx

SQL Performance
---------------

TODO

Unreliable/Slow Network
=======================

- Use binary format or compress data to save data size.
- Split data to smaller pieces to avoid retransmitting the whole thing in the presence of failure.
- Use UDP instead of TCP to avoid unnecessary packets.
- Employ peer-to-peer topology.
- TODO

Performance
===========

This session focuses on performance issues coming from inside the application. For system performance troubleshooting, please refer to :ref:`this document <link-linux-system-performance>`.

Some reasons our application is slow for a specific operation:

- CPU (on-CPU profiling)

  * Doing more work than necessary by accident?

    - Remove redudant works.
    - Caching

  * Processing big data volume?

    - Break data to smaller pieces, process them parallelly (locally or delegate to worker machines).
    - Run in a background job.
    - Preprocess data and store the result when it enters the system instead of doing it on-demand.
    - Utilize SIMD

  * Using an ineffective data structure/algorithm?

    - Switch to a more effective one (e.g. bit array, hash map, max/min heap).

- High lock contention

  * Too many threads acquiring a single lock (mutex or database row)?

    - Make data immutable to remove the need of lock.
    - Use multiple-reader single-writer lock if most of the operations are reading protected data.
    - Use a lock-free queue/channel to pass data instead of manually maintaining a lock.

- IO (off-CPU profiling)

  * Spend too much time waiting for data?

    - Do it less often with bigger data volume (e.g. aggregate logs once a week instead of once a day).
    - Caching is also an option in case the data is mostly read.

  * Large number of mostly idle connections

    - Async (e.g. coroutine/goroutine)

Data Type ``T``
===============

See :ref:`this document <link-design-type>` to understand the concepts used in this section.

#. Does ``T`` have only one single purpose?
#. What is ``T``'s invariant?

       In computer programming, specifically object-oriented programming, a class invariant (or type invariant) is an invariant used for constraining objects of a class. **Methods of the class should preserve the invariant.**

       --- https://en.wikipedia.org/wiki/Class_invariant

#. Does ``T``'s **set of valid values** equal to **the product of all sets of valid values of T's fields**?

   - How are undesirable combinations kept out?
   - Can more specialized/fundamental types be used for ``T``'s fields instead (e.g. ``NonZeroU32`` is used instead of ``u32`` for non-zero fields)?

#. Does ``T`` have any special value (e.g. when all of its fields are zero or null)? How is it handled?
#. Does ``T`` have all of the below properties?

   - Regular
   - Whole-part relationship

#. What is the precondition of each method?
#. Is there any special order for method invocation (e.g. ``method_a`` must be called before ``method_b``)?
#. Does ``T``'s computational basis have all of the below properties?

   - Efficient
   - Expressive
   - Complete (able to construct and operate on any representable value)

#. Are ``T``'s members organized in a way that has the following properties?

   - Closely related members are placed together.
   - Total size of ``T`` is smallest.
   - `False sharing <https://en.wikipedia.org/wiki/False_sharing>`__ is prevented.

#. Can ``T`` be immutable (i.e. all methods do not change any field of ``T``, they return a new copy with desired modifications instead)?

#. Are essential/inherent and accidental properties separated?

  .. code-block:: cpp

      class Document {
      private:
          std::string content; // essential property
          std::string title; // essential property
      }

      class FileDocument {
      private:
          Document document;
          std::string filepath; // accidental property
      }

Function ``F``
==============

Overview
--------

#. Does ``F`` have only one single purpose? What is it (e.g. reading configuration, parsing text, mapping errors)?

Inputs/Outputs
--------------

#. Does ``F``'s set of valid inputs equal to the product of all valid values of ``F``'s inputs?

   - How are undesirable combinations avoided?
   - Can more abstract types be used for inputs instead (e.g. iterators instead of raw pointers, base class instead of derived class)?

   .. code-block:: c++
      :caption: not-good.cpp

      UInteger mod(UInteger a, UInteger b) {
          // All inputs:       Number of valid values of UInteger ^ 2.
          // All valid inputs: Number of valid values of UInteger ^ 2 - 1.
          if (b == 0) {
              // TODO
          } else {
              return a % b;
          }
      }

   .. code-block:: c++
      :caption: better.cpp

      UInteger mod(UInteger a, NonZeroUInteger b) {
          // All inputs:       Number of valid values of UInteger * Number of valid values of NonZeroUInteger.
          // All valid inputs: Number of valid values of UInteger * Number of valid values of NonZeroUInteger.
          return a % b;
      }

#. Do ``F``'s inputs form any special combination?
#. Are ``F``'s inputs placed in **(in, in-out)** order?
#. Do ``F``'s inputs follow below convention?

   - In inputs that ``F`` consumes are passed by value.
   - In inputs that ``F`` does not consume are passed by const reference.
   - In-out inputs are passed by non-const reference.
   - Optional inputs should use ``Optional`` class (or something similar) instead of pointer.

     * String or vector are the exceptions as their empty state can be treated as optional, but it should be documented.

#. Are ``F``'s outputs placed in **(out, error)** order?

Implementation
--------------

#. Is ``F`` written in favor of readability instead of performance?
#. Does ``F`` invoke global components (e.g. time, filesystem, database...)?

   - Can those invocations be moved outside to make ``F`` **functional**?

   .. code-block:: c++
      :caption: not-good.cpp

      // Not so good, should be decomposed to smaller pieces.
      AppOptions load_options(string const& path) {
          auto file = open(path);
          auto text = read(file);

          AppOptions opts;
          XMLParserStruct* p = nullptr;
          XMLParserInit(&p);
          auto err = XMLParse(p, text);
          // Handle err.
          // Walk and parse XML to AppOptions.
          XMLParserDestruct(p);

          return opts;
      }

   .. code-block:: c++
      :caption: better.cpp

      AppOptions load_options(string const& path) {
          auto text = read_text(path);
          return parse_text_to_options(text);
      }

      string read_text(string const& path) {
          auto file = open(path);
          return read(file);
      }

      AppOptions parse_text_to_options(string const& text) {
          AppOptions opts;
          XMLParserStruct* p = nullptr;
          XMLParserInit(&p);
          auto err = XMLParse(p, text);
          // Handle err.
          // Walk and parse XML to AppOptions.
          XMLParserDestruct(p);

          return opts;
      }

#. Variables

   - Does each variable have only one single purpose?
   - Can any variable have smaller scope?

     * How many lines are there between a variable's declaration and its last usage?

   - Is there any mutable variable that can be replaced with an immutable one?

#. Loops

   - Is there any loop that can be replaced with a well-known algorithm or a combination of them?

     * folding
     * sorting
     * searching
     * selecting

       + https://en.cppreference.com/w/cpp/algorithm/nth_element

     * partitioning
     * rotating

   - Does every loop have only one single purpose? What is that purpose?
   - What is the invariant of each loop?

         In computer science, a loop invariant is a property of a program loop that is true **before (and after)** each iteration.

         --- https://en.wikipedia.org/wiki/Loop_invariant

   - Can any loop be moved to its own function?
   - Is there any nested loop? Can it be refactored into its own function?

#. Ifs

   - Can nested ``if`` be turned into fail-fast return so the level of nesting is reduced?
   - Is there any ``if`` that handles special case?

     * Can that special case be turned into normal case?

   - Can any ``if`` have smaller scope?
   - Is there any complex ``if`` statement can be refactored to improve readability?

#. Recursion

   - What is the terminating case for this recursive function?

Users Point-Of-View
-------------------

#. How easy is it for users to use ``F`` incorrectly?
#. Can users know and understand all possible errors that can be returned from ``F`` by

   - reading ``F``'s document
   - or reading ``F``'s source code (without reading any other code)

#. Does ``F`` use error messages (i.e. stating errors without providing any hint to fix) or help messages (i.e. containing some direction to correct the error)?

Error Handling
==============

- Consider errors as part of function's signature.

- Prefer structured errors (error codes, ...) to text errors.

  + Improve error handling code. Instead of searching through text which is error prone, we compare error code.
  + Easier to customize output to different receivers (humans with i18n, or machines which mostly care about error codes).
  + Simpler to aggregate and analyze later.
  + Code searching is also more convenient.

- All errors of a given function should be enclosed in an enum instead of being implicitly formed by a set of independent constants.

  .. code-block:: rust
      :caption: not-good.rs

      const ERROR_INVALID_INPUT: i32 = 1;
      const ERROR_MISSING_INPUT: i32 = 2;

      fn call() -> Result<(), i32> {}

  .. code-block:: rust
      :caption: better.rs

      enum Error {
          InvalidInput,
          MissingInput,
      }

      fn call() -> Result<(), Error> {}

- Use multiple different small error enums instead of a gigantic error enum.

  It makes error handling easier, as the default strategy is to handle all possible errors returned from a functions instead of reading the document extremely carefully. It'll also be faster to add new type of error because we can trace all users of a particular error enum quicker.

  Hence, we should have multiple error enums and do mapping/grouping between them. Mapping/grouping usually happens when an error from lower layers is passed to higher layers.

  Usually we group fatal errors that users cannot really do anything about without making changes out side of our system (e.g. db or io errors) into one enum variant. Other types of errors, each can be mapped to a seperate variant (e.g. missing input, invalid input).

  Nesting low level error enums into higher ones is also an option.

  .. code-block:: rust

      enum SystemError {
          IoError(io::Error), // external enum
          DbError(db::Error), // external enum
      }

      enum ApplicationError {
          InvalidInput,
          MissingInput,
      }

      enum HighLevelError {
          ApplicationError(ApplicationError),
          SystemError(SystemError),
      }

- Add fail points to conditionally trigger errors of a function during testing.

  + https://docs.rs/fail/0.4.0/fail/
  + http://sled.rs/errors

  This can be combined with property based testing (which generates random inputs). To make failed tests reproducible, we should avoid unnecessary randomness in our code. Saving the seed that generates random data when a test fails is also a help.

  + https://dropbox.tech/infrastructure/-testing-our-new-sync-engine

- Separate expected errors and unexpected errors.

  Let's say we write a function to read configurations from a file, it usually has three outcomes (assuming that the configuration file is in proper format). The first one is the configuration, second is file not found error, and third one is IO error. Often, we stop the program when we encounter the third error, but use the default configuration in second case.

  So if we use ``Result<Config, MyError>`` as the return type, the caller would look like this

    .. code-block:: rust
        :caption: not-good.rs

        let config = match get_config() {
            Ok(config) => config,
            Err(MyError::FileNotFound) => get_default_config(),
            Err(MyError::IoError) => return error to caller
        };

  Instead, we can separate the errors, using ``Result<Result<Config, FileNotFoundError>, IoError>``. It looks more cumbersome, but now we can pass the unexpected error to upper level easier.

    .. code-block:: rust
        :caption: better.rs

        let config = get_config()?; // pass IoError up the stack, may add some context like "reading config from file"
        let config = config.unwrap_or_else(|_| get_default_config());

- Function calls and errors flow

  .. code-block:: text

           calls      request      errors
      concrete |         1         ^ general
               |       _/ \_       |
               |     2       5     |
               |    / \     / \    |
               |   3   4   6   7   |
       general v                   | concrete

  Therefore, keeping context of errors is crucial to construct an understandable error message.

Others
======

DTO
---

If we use same object/DTO for creating and updating, they still have different validating rules (e.g. some fields required at creation aren't necessary when updating, default values only applied to creating but not updating).

When validating an object, faster and simpler validating rules should go first (although we should still keep related rules close to each other).

There can be two layers of DTO:

- The first layer is user-facing, which receive data from users without any modification/validation. This type of DTO has higher number of possible values (i.e. number of combinations of its fields), including invalid ones.
- The next layer can be constructed after validating the first one, as we don't want to pass a too-relaxed object (in terms of numbers of possible values/invariant) around.

Retrieving, creating, and updating can have different DTOs, but users should be able to paste result of retrieving to creating/updating without much editing. It fastens testing, and simplifies client code.

JSON
----

- Meta data versioning (e.g. put a field ``version`` into json beside ``type`` and ``value`` so we can handle multiple version of a same type of object during migrations or supporting old APIs, or adding versio info into ``type`` itself).

Building
--------

- Building should be customizable without making worktree dirty (i.e. ``git status`` returns clean).
- Building on build server and in local machine must follow same procedure. Preferably, there is a ``Makefile`` and all it takes to do a full build is running ``make``.

Deployment
----------

TODO

Design Procedure
================

1. Write down the functionalities from users' point-of-view.

2. Write down the simplest work flow of our system.

3. List all the requirements/problems/questions that need to be resolved, based on the workflow in step 2.

   - Functionalities

     * Use cases
     * Domain types/knowledge
     * Traps (i.e. Unexpected consequences of users' actions)
     * ...

   - Storage

     * Text file (e.g. CSV, INI, XML, JSON)
     * Binary file (e.g. SQLite, homemade format)
     * SQL database
     * No-SQL database
     * Network File System
     * ...

   - Networking

     * TCP, UDP
     * VPN, SSL
     * Message queue
     * ...

   - Security

     * Permissions
     * Authentication
     * Client/server verification
     * ...

   - Performance

     * Algorithm or IO slowness
     * Locking (e.g. mutex, database row)
     * Throughput/Latency
     * ...

   - Interactions

     * Format of data that is sent to & received from remote systems
     * Remote systems' failure handling
     * Backoff strategy
     * ...

   - Maintainability/Upgradability

     * Data structure versioning
     * ...

   - Testability
   - Deployment

     * Stateless
     * Logging
     * Alerts/Downtime
     * ...

4. With the description in 1, draw out a simplest diagram depicting all the components and their interactions that can fulfill the most important functionalities from users' point-of-view. Ignore all the points from 3 (i.e. security, performance...).

  The reasons why we do this after 3 but ignore it are:

  - Usually we forget some issues after we have first draft design, so we need to have that list of issues first.
  - We don't want to consider everything on first draft. Just have a simplest design and improve it.

5. Which items in 3 the first draft design in 4 have already fulfilled? How do we change the design to make all other issues resolved?

Example
-------

Write a web crawler.

1. Users will call our program with a root URL. The program will recursively download the web pages.

2. Workflow

  - Download HTML from root URL.
  - Save HTML.
  - Parse HTML.
  - Get new URLs from parsed HTML.
  - Put new URLs to waiting queue.
  - Repeat.

3. Issues

  - Does the program also download and save media resources? If yes, what types of media does it support?
  - Does the program have a predefined depth limit? Can users provide it instead? Is there a hard limit that users cannot go beyond?
  - How does the program store the downloaded resources?
  - How does the program handle duplicated URLs? Should we ignore a URL if we encounter it N minutes ago?
  - Does the program support loading configuration from a file? If yes, what is the file's location and its format? Do we need to version the configuration file?
  - How does the program handle HTTPS regarding to certificate verification (self-signed, wrong domain, expired cert...)?
  - Does the program support downloading CSS and Javascript? If yes, do we need to re-write the HTML to point to downloaded js and css files?

4. Components

  - Downloader: Download HTML from a given URL.
  - HTMLPaser: Parse HTML text to a tree data structure.
  - URLQueue: Waiting queue for new URLs.
  - HTMLSaver: Save HTML to file/other types of storage.

5. TODO
