==========================================
 reStructuredText Simple Template/Example
==========================================

Softwares
=========

- CloudInit

  +-----------------+----------------------------------------------------------------------------------------+
  | Version         | 19.2                                                                                   |
  +-----------------+----------------------------------------------------------------------------------------+
  | Hash            | 060b1a1ca7b2385aa7f4ed42720063fa557e0671                                               |
  +-----------------+----------------------------------------------------------------------------------------+
  | Source Location | https://github.com/cloud-init/cloud-init/tree/060b1a1ca7b2385aa7f4ed42720063fa557e0671 |
  +-----------------+----------------------------------------------------------------------------------------+
  | Documentation   | https://cloudinit.readthedocs.io/en/19.2/index.html                                    |
  +-----------------+----------------------------------------------------------------------------------------+

Chapter 1 (Title Case)
======================

Section 1 ``code``
------------------

Sub-section 1 [`ref <http://docutils.sourceforge.net/docs/user/rst/demo.html>`__]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sub-sub-section 1
`````````````````

Sub-sub-sub-section 1
'''''''''''''''''''''

Sub-sub-sub-section 2
'''''''''''''''''''''

Sub-sub-section 2
`````````````````

Sub-section 2
~~~~~~~~~~~~~

Section 2
---------

Normal text.

*Italic text.*

**Bold text.**

``inline code``.

    Quoted text starts with 4-space indentation.

    --- Anonymous.

Subscript :sub:`subscript`.

Superscript :sup:`superscript`.

Code blocks should use 4-space indentation:

.. code-block:: c
    :linenos:
    :emphasize-lines: 1,3
    :caption: main.c

    int main(int argc, char* argv[]) {
        return 0;
    }

Indented code block starts with 2-space:

  .. code-block:: c
      :emphasize-lines: 1-3

      int main(int argc, char* argv[]) {
          return 0;
      }

Literal block starts with double-colon at the end of the preceding pararaph::

    int main(int argc, char* argv[]) {
        return 0;
    }

::

    Preceding paragraph doesn't have to have any text, but must end with double-colon to start a literal block.

We can include external files too.

.. literalinclude:: example/main.c
    :language: c
    :caption: main.c
    :linenos:
    :emphasize-lines: 3-6

Separator ahead!!!

-----

Normal text.

- A bullet list, which is a child of ``Normal text``.

  Paragraph.

  * Nested bullet list must use 2-space indentation.
  * Nested item 2.

    + Third level.
    + Item 2 with same level code block.

      .. code-block:: c

          int main(int argc, char* argv[] {
              return 0;
          }

    + Item 3 with indented code block.

        .. code-block:: c

            int main(int argc, char* argv[] {
                return 0;
            }

    + Item 4.

  * Nested item 3.

#. Auto-enumerated 1.

#. Auto-enumerated 2.

5. List can start at arbitrary number.

#. Subsequent auto-enumerated items still work correctly.

=====  =====  ======
   Inputs     Output
------------  ------
  A      B    A or B
=====  =====  ======
False  False  False
True   False  True
False  True   True
True   True   True
=====  =====  ======

Table separators should be placed at TAB stops.

+-------------------------+-------------+-----------+-----------+
| Header row, column 1    | Header 2    | Header 3  | Header 4  |
| (header rows optional)  |             |           |           |
+=========================+=============+===========+===========+
| body row 1, column 1    | column 2    | column 3  | column 4  |
+-------------------------+-------------+-----------+-----------+
| body row 2              | Cells may span columns.             |
+-------------------------+-------------+-----------------------+
| body row 3              | Cells may   | - Table cells         |
+-------------------------+ span rows.  | - contain             |
| body row 4              |             | - body elements.      |
+-------------------------+-------------+-----------+-----------+
| body row 5              | Cells may also be       |           |
|                         | empty: ``-->``          |           |
+-------------------------+-------------------------+-----------+

Other Directives
================

.. attention::

    Text.

.. caution::

    Text.

.. danger::

    Text.

.. error::

    Text.

.. hint::

    Text.

.. important::

    Text.

.. note::

    Text.

.. tip::

    Text.

.. warning::

    Text.

    - 1st item.

      .. code-block:: c

          int main(int argc, char* argv[]) {
              return 0;
          }

    - 2nd item.

.. seealso::

    Text.

.. note::

    Text.

.. hlist::
   :columns: 3

   * A list of
   * short items
   * that should be
   * displayed
   * horizontally

.. math::

   (a + b)^2 = a^2 + 2ab + b^2

   (a - b)^2 = a^2 - 2ab + b^2

.. math::
   :nowrap:

   \begin{eqnarray}
      y    & = & ax^2 + bx + c \\
      f(x) & = & x^2 + 2xy + y^2
   \end{eqnarray}

Here is an inline math function :math:`y = ax^2 + bx +c`

.. topic:: Topic Title

    Topic body.

The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.

.. sidebar:: Optional Sidebar Title
   :subtitle: Optional Sidebar Subtitle

   Sidebar body.

   - 1st item.
   - 2nd item.

The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.

.. csv-table:: Table Title
   :header: "ColA", "ColB", "ColC"
   :widths: 15, 10, 30

    "A1",, "C1"
    ,22
    "A3", 23, "C3"

.. list-table:: Table Title
   :widths: 15 10 30
   :header-rows: 2

   *  - ColA
      - ColB
      - ColC
   *  - A1
      -
      - C1
   *  -
      - 22
      -
   *  - A3
      - 23
      - C3

See `References`_ for more complex structures.

References
==========

- https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html
- https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html
- https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html
- https://pygments.org/docs/lexers/
- http://docutils.sourceforge.net/docs/user/rst/demo.html and `source <http://docutils.sourceforge.net/docs/user/rst/demo.txt>`__
- `Quickstart <http://docutils.sourceforge.net/docs/user/rst/quickstart.html>`__
- Example of reusable link `Example Link`_

.. _Example Link: https://example.com/
