=======
 Proxy
=======

HTTP/HTTPS Protocol
===================

SOCKS Proxy
-----------

.. code-block:: sh

    ALL_PROXY=socks5h://127.0.0.1:3000 git pull
    git -c http.proxy=socks5h://127.0.0.1:3000 pull

Save To Config
~~~~~~~~~~~~~~

.. code-block:: sh

    git config --global http.proxy socks5h://127.0.0.1:3000
    git config --global https.proxy socks5h://127.0.0.1:3000

HTTP Proxy
----------

.. code-block:: sh

    # curl doesn't understand upper case versions of http_proxy and https_proxy
    http_proxy=http://127.0.0.1:8000 git pull
    https_proxy=http://127.0.0.1:8000 git pull

Save To Config
~~~~~~~~~~~~~~

.. code-block:: sh

    git config --global http.proxy http://127.0.0.1:8000
    git config --global https.proxy http://127.0.0.1:8000

``git`` Protocol
================

- Create port forwarding

  .. code-block:: sh

      # git protocol uses port 9418
      # All connections to 127.0.0.1:9418 are forwarded to git.denx.de.com:9418
      ssh -N -f -L 9418:git.denx.de.com:9418 admin@server.com

- Add following lines to ``/etc/hosts``

  .. code-block::

      # git://git.denx.de/u-boot.git
      127.0.0.1    git.denx.de

SSH Protocol
============

SOCKS Proxy
-----------

.. code-block:: sh

    git -c core.sshCommand='nc -X 5 -x 127.0.0.1:3000 %h %p' pull
    GIT_SSH_COMMAND='ssh -o ProxyCommand="nc -X 5 -x 127.0.0.1:3000 %h %p"' git pull

Save To Config
~~~~~~~~~~~~~~

Put below snippet to your ``~/.ssh/config`` or ``/etc/ssh/ssh_config`` (note that ``~/.ssh/config`` shouldn't be readable by other users).

.. code-block::

    Host bitbucket.org
      User git
      ProxyCommand nc -X 5 -x 127.0.0.1:3000 %h %p

Additional Info
===============

Set Up SOCKS Proxy
------------------

.. code-block:: sh

    ssh -N -f -D 127.0.0.1:3000 admin@server.com
    ssh -vvv -N -D 127.0.0.1:3000 admin@server.com

Enable Network Debug Message
----------------------------

.. code-block:: sh

    export GIT_CURL_VERBOSE=1
