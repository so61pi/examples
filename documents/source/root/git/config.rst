========
 Config
========

Freeze ``~/.gitconfig`` file to prevent accidental changes
==========================================================

.. code-block:: sh

    # never use `chmod -w ~/.gitconfig` alone, git will copy `.gitconfig`
    # to `.gitconfig.lock` and edit then copy it back to old file
    #
    # we have to change the file's attribute to immutable
    chmod -w ~/.gitconfig
    sudo chattr +i ~/.gitconfig
    
    # must have `i` in the attribute list
    lsattr ~/.gitconfig
    
    # use `sudo chattr -i ~/.gitconfig` to edit it again
