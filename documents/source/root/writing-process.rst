===========================
 Writing Process Checklist
===========================

Quoted from Handbook of Technical Writing 9th.

#. Preparation

   #. Establish your purpose
   #. Identify your audience or readers
   #. Consider the context
   #. Determine your scope of coverage

#. Research

#. Organization

   #. Choose the best methods of development

      #. Cause-and-effect method of development begins with either the cause or the effect of an event. This approach can be used to develop a report that offers a solution to a problem, beginning with the problem and moving on to the solution or vice versa.
      #. Chronological method of development emphasizes the time element of a sequence, as in a trouble report that traces events as they occurred in time.
      #. Comparison method of development is useful when writing about a new topic that is in many ways similar to another topic that is more familiar to your readers.
      #. Definition method of development extends definitions with additional details, examples, comparisons, or other explanatory devices. See also defining terms.
      #. Division-and-classification method of development either separates a whole into component parts and discusses each part separately (division) or groups parts into categories that clarify the relationship of the parts (classification).
      #. General and specific methods of development proceed either from general information to specific details or from specific information to a general conclusion.
      #. Order-of-importance method of development presents information in either decreasing order of importance, as in a proposal that begins with the most important point, or increasing order of importance, as in a presentation that ends with the most important point.
      #. Sequential method of development emphasizes the order of elements in a process and is particularly useful when writing step-by-step instructions.
      #. Spatial method of development describes the physical appearance of an object or area from top to bottom, inside to outside, front to back, and so on.

   #. Outline your notes and ideas
   #. Develop and integrate visuals
   #. Consider layout and design

#. Writing

   #. Select an appropriate point of view
   #. Adopt an appropriate style and tone
   #. Use effective sentence construction
   #. Construct effective paragraphs
   #. Use quotations and paraphrasing
   #. Write an introduction
   #. Write a conclusion
   #. Choose a title

#. Revision

   #. Check for unity and coherence, conciseness, pace, transition
   #. Check for sentence variety, emphasis, parallel structure, subordination
   #. Check for clarity, ambiguity, awkwardness, logic errors, positive writing, voice
   #. Check for ethics in writing biased language, copyright, plagiarism
   #. Check for appropriate word choice, abstract / concrete words, affectation and jargon, clichés, connotation / denotation, defining terms
   #. Eliminate problems with grammar, agreement, case, modifiers, pronoun reference, sentence faults
   #. Review mechanics and punctuation, abbreviations, capitalization, contractions, dates, italics, numbers, proofreading, spelling
