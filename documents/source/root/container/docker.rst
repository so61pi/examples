========
 Docker
========

Disable TLS
===========

- Stop docker service

  .. code-block:: sh

      sudo systemctl stop docker

- Add the following configuration to ``/etc/docker/daemon.json``

  .. code-block:: json
  
      {
        "tls": false
      }

- Start docker service

  .. code-block:: sh

      sudo systemctl start docker

Proxy
=====

For client
----------

- https://docs.docker.com/network/proxy/

For daemon
----------

- https://docs.docker.com/config/daemon/systemd/#httphttps-proxy
- To use sock proxy, we could set ``ALL_PROXY``

  .. code-block:: ini

      [Service]
      Environment="ALL_PROXY=socks5://<proxy-address>:<proxy-port>/"

Template
========

.. literalinclude:: docker/template/Dockerfile
    :language: dockerfile
    :caption: Dockerfile

.. literalinclude:: docker/template/Makefile
    :language: makefile
    :caption: Makefile
