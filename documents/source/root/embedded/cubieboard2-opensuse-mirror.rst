=======================================
 Setting up a personal openSUSE mirror
=======================================

Basically there are two essential parts of setting up a mirror, getting/syncing data and serving it. We will go through them in that order in this post to build a mirror for Tumbleweed.

Syncing From Remote Server Using ``rsync``
==========================================

In this step we setup a timer for pulling updated data from another mirror to our machine. We can find a list of openSUSE mirrors all over the world at https://mirrors.opensuse.org/list/all.html. Note that not all of them consist the same set of data at a given time as there maybe some delay between the time a new update is avaiable and the time a particular mirror server starts updating its own internal data from upstream.

The instructions at https://en.opensuse.org/openSUSE:Mirror_howto mentions rsyncing data from ``rsync://rsync.opensuse.org``, but at the time this post is written, its data is quite old and out of sync with https://download.opensuse.org/tumbleweed/repo/oss/. I personally use ``rsync://download.nus.edu.sg`` server instead, but you can choose other mirrors that are closer to your location.

Here is the output of ``rsync --list-only`` I used to check the timestamp of the files on a few sync servers.

- ``rsync --list-only rsync://rsync.opensuse.org/opensuse-full-with-factory/opensuse/tumbleweed/repo/oss/``

  .. code-block::
      :emphasize-lines: 17

      This is rsync.opensuse.org, public rsync server of openSUSE.org,
      limited to 50 connections.

      If you run a public mirror, please get in contact so we can give you
      access to the stage rsync server.
      You'll find conditions for access and further information at
      http://en.opensuse.org/Mirror_Infrastructure

      Thanks!
      admin@opensuse.org


      drwxr-xr-x          4,096 2020/01/09 10:04:28 .
      -rw-r--r--             21 2020/01/09 10:04:28 .current.txt
      -rw-r--r--            334 2019/11/19 03:55:59 .treeinfo
      -rw-r--r--    105,671,171 2020/01/08 16:33:01 ARCHIVES.gz
      -rw-r--r--         36,882 2020/01/08 17:19:40 CHECKSUMS
      -rw-r--r--            481 2020/01/08 18:16:57 CHECKSUMS.asc
      -rw-r--r--     43,646,892 2020/01/08 16:10:08 ChangeLog
      -rw-r--r--         18,092 2019/11/19 03:55:59 GPLv2.txt
      -rw-r--r--         35,147 2019/11/19 03:55:59 GPLv3.txt
      -rw-r--r--        744,914 2020/01/08 16:33:02 INDEX.gz
      -rw-r--r--          1,250 2019/11/19 03:55:59 README
      -rw-r--r--         99,678 2019/11/19 03:55:59 SUSEgo.ico
      -rw-r--r--         10,247 2019/11/19 03:55:59 SUSEgo.png
      -rw-r--r--         11,612 2019/11/19 03:55:59 SUSEgo.svg
      -rw-r--r--             91 2019/11/18 01:30:44 autorun.inf
      -rw-r--r--            615 2019/11/19 03:56:00 gpg-pubkey-307e3d54-5aaa90a5.asc
      -rw-r--r--            972 2019/11/19 03:56:00 gpg-pubkey-39db7c82-5847eb1f.asc
      -rw-r--r--          1,495 2019/11/19 03:56:00 gpg-pubkey-3dbdc284-53674dd4.asc
      -rw-r--r--        910,531 2020/01/08 16:33:02 ls-lR.gz
      -rw-r--r--        272,921 2019/11/18 01:30:44 openSUSE_installer.exe
      drwxr-xr-x             26 2020/01/09 10:04:20 EFI
      drwxr-xr-x             44 2020/01/09 10:04:21 boot
      drwxr-xr-x             74 2020/01/09 10:04:21 docu
      drwxr-xr-x      2,060,288 2020/01/09 10:04:21 i586
      drwxr-xr-x          4,096 2020/01/09 10:04:21 i686
      drwxr-xr-x             47 2020/01/09 10:04:21 media.1
      drwxr-xr-x      2,240,512 2020/01/09 10:04:23 noarch
      drwxr-xr-x          4,096 2020/01/09 10:04:23 repodata
      drwxr-xr-x      2,437,120 2020/01/09 10:04:23 x86_64

- ``rsync --list-only rsync://download.nus.edu.sg/opensuse/tumbleweed/repo/oss/``

  .. code-block::
      :emphasize-lines: 5

      drwxr-xr-x            812 2020/05/28 03:20:22 .
      -rw-r--r--             21 2020/05/28 03:20:22 .current.txt
      -rw-r--r--            334 2019/11/19 03:55:59 .treeinfo
      -rw-r--r--    110,896,839 2020/05/27 08:43:43 ARCHIVES.gz
      -rw-r--r--         37,068 2020/05/27 09:36:32 CHECKSUMS
      -rw-r--r--            481 2020/05/27 09:39:59 CHECKSUMS.asc
      -rw-r--r--     46,464,180 2020/05/27 08:19:02 ChangeLog
      -rw-r--r--         18,092 2019/11/19 03:55:59 GPLv2.txt
      -rw-r--r--         35,147 2019/11/19 03:55:59 GPLv3.txt
      -rw-r--r--        760,551 2020/05/27 08:43:44 INDEX.gz
      -rw-r--r--          1,250 2019/11/19 03:55:59 README
      -rw-r--r--         99,678 2019/11/19 03:55:59 SUSEgo.ico
      -rw-r--r--         10,247 2019/11/19 03:55:59 SUSEgo.png
      -rw-r--r--         11,612 2019/11/19 03:55:59 SUSEgo.svg
      -rw-r--r--             91 2019/11/18 01:30:44 autorun.inf
      -rw-r--r--            615 2019/11/19 03:56:00 gpg-pubkey-307e3d54-5aaa90a5.asc
      -rw-r--r--            972 2019/11/19 03:56:00 gpg-pubkey-39db7c82-5847eb1f.asc
      -rw-r--r--          1,495 2019/11/19 03:56:00 gpg-pubkey-3dbdc284-53674dd4.asc
      -rw-r--r--        922,194 2020/05/27 08:43:43 ls-lR.gz
      -rw-r--r--        272,921 2019/11/18 01:30:44 openSUSE_installer.exe
      drwxr-xr-x             22 2020/05/28 03:17:28 EFI
      drwxr-xr-x             46 2020/05/28 03:17:28 boot
      drwxr-xr-x             76 2020/05/28 03:17:28 docu
      drwxr-xr-x      1,069,244 2020/05/28 03:17:29 i586
      drwxr-xr-x            724 2020/05/28 03:17:29 i686
      drwxr-xr-x             49 2020/05/28 03:17:29 media.1
      drwxr-xr-x      1,133,869 2020/05/28 03:17:29 noarch
      drwxr-xr-x          2,133 2020/05/28 03:17:29 repodata
      drwxr-xr-x      1,268,362 2020/05/28 03:17:30 x86_64

- ``rsync --list-only rsync://mirrors.kernel.org/opensuse/tumbleweed/repo/oss/``

  .. code-block::
      :emphasize-lines: 23

      MOTD:
      MOTD:   Welcome to the Linux Kernel Archive.
      MOTD:
      MOTD:   Due to U.S. Exports Regulations, all cryptographic software on this
      MOTD:   site is subject to the following legal notice:
      MOTD:
      MOTD:   This site includes publicly available encryption source code
      MOTD:   which, together with object code resulting from the compiling of
      MOTD:   publicly available source code, may be exported from the United
      MOTD:   States under License Exception "TSU" pursuant to 15 C.F.R. Section
      MOTD:   740.13(e).
      MOTD:
      MOTD:   This legal notice applies to cryptographic software only.
      MOTD:   Please see the Bureau of Industry and Security,
      MOTD:   http://www.bis.doc.gov/ for more information about current
      MOTD:   U.S. regulations.
      MOTD:

      drwxr-xr-x          4,096 2020/05/28 03:20:22 .
      -rw-r--r--             21 2020/05/28 03:20:22 .current.txt
      -rw-r--r--            334 2019/11/19 03:55:59 .treeinfo
      -rw-r--r--    110,896,839 2020/05/27 08:43:43 ARCHIVES.gz
      -rw-r--r--         37,068 2020/05/27 09:36:32 CHECKSUMS
      -rw-r--r--            481 2020/05/27 09:39:59 CHECKSUMS.asc
      -rw-r--r--     46,464,180 2020/05/27 08:19:02 ChangeLog
      -rw-r--r--         18,092 2019/11/19 03:55:59 GPLv2.txt
      -rw-r--r--         35,147 2019/11/19 03:55:59 GPLv3.txt
      -rw-r--r--        760,551 2020/05/27 08:43:44 INDEX.gz
      -rw-r--r--          1,250 2019/11/19 03:55:59 README
      -rw-r--r--         99,678 2019/11/19 03:55:59 SUSEgo.ico
      -rw-r--r--         10,247 2019/11/19 03:55:59 SUSEgo.png
      -rw-r--r--         11,612 2019/11/19 03:55:59 SUSEgo.svg
      -rw-r--r--             91 2019/11/18 01:30:44 autorun.inf
      -rw-r--r--            615 2019/11/19 03:56:00 gpg-pubkey-307e3d54-5aaa90a5.asc
      -rw-r--r--            972 2019/11/19 03:56:00 gpg-pubkey-39db7c82-5847eb1f.asc
      -rw-r--r--          1,495 2019/11/19 03:56:00 gpg-pubkey-3dbdc284-53674dd4.asc
      -rw-r--r--        922,194 2020/05/27 08:43:43 ls-lR.gz
      -rw-r--r--        272,921 2019/11/18 01:30:44 openSUSE_installer.exe
      drwxr-xr-x             25 2020/05/28 03:17:28 EFI
      drwxr-xr-x             42 2020/05/28 03:17:28 boot
      drwxr-xr-x             72 2020/05/28 03:17:28 docu
      drwxr-xr-x      1,802,240 2020/05/28 03:17:29 i586
      drwxr-xr-x          4,096 2020/05/28 03:17:29 i686
      drwxr-xr-x             45 2020/05/28 03:17:29 media.1
      drwxr-xr-x      2,187,264 2020/05/28 03:17:29 noarch
      drwxr-xr-x          4,096 2020/05/28 03:17:29 repodata
      drwxr-xr-x      2,158,592 2020/05/28 03:17:30 x86_64

- ``rsync --list-only rsync://fr2.rpmfind.net/linux/opensuse/tumbleweed/repo/oss/``

  .. code-block::
      :emphasize-lines: 5

      drwxr-xr-x          4,096 2020/05/28 03:20:22 .
      -rw-r--r--             21 2020/05/28 03:20:22 .current.txt
      -rw-r--r--            334 2019/11/19 03:55:59 .treeinfo
      -rw-r--r--    110,896,839 2020/05/27 08:43:43 ARCHIVES.gz
      -rw-r--r--         37,068 2020/05/27 09:36:32 CHECKSUMS
      -rw-r--r--            481 2020/05/27 09:39:59 CHECKSUMS.asc
      -rw-r--r--     46,464,180 2020/05/27 08:19:02 ChangeLog
      -rw-r--r--         18,092 2019/11/19 03:55:59 GPLv2.txt
      -rw-r--r--         35,147 2019/11/19 03:55:59 GPLv3.txt
      -rw-r--r--        760,551 2020/05/27 08:43:44 INDEX.gz
      -rw-r--r--          1,250 2019/11/19 03:55:59 README
      -rw-r--r--         99,678 2019/11/19 03:55:59 SUSEgo.ico
      -rw-r--r--         10,247 2019/11/19 03:55:59 SUSEgo.png
      -rw-r--r--         11,612 2019/11/19 03:55:59 SUSEgo.svg
      -rw-r--r--             91 2019/11/18 01:30:44 autorun.inf
      -rw-r--r--            615 2019/11/19 03:56:00 gpg-pubkey-307e3d54-5aaa90a5.asc
      -rw-r--r--            972 2019/11/19 03:56:00 gpg-pubkey-39db7c82-5847eb1f.asc
      -rw-r--r--          1,495 2019/11/19 03:56:00 gpg-pubkey-3dbdc284-53674dd4.asc
      -rw-r--r--        922,194 2020/05/27 08:43:43 ls-lR.gz
      -rw-r--r--        272,921 2019/11/18 01:30:44 openSUSE_installer.exe
      drwxr-xr-x          4,096 2020/05/28 03:17:28 EFI
      drwxr-xr-x          4,096 2020/05/28 03:17:28 boot
      drwxr-xr-x          4,096 2020/05/28 03:17:28 docu
      drwxr-xr-x      2,015,232 2020/05/28 03:17:29 i586
      drwxr-xr-x          4,096 2020/05/28 03:17:29 i686
      drwxr-xr-x          4,096 2020/05/28 03:17:29 media.1
      drwxr-xr-x      2,543,616 2020/05/28 03:17:29 noarch
      drwxr-xr-x         12,288 2020/05/28 03:17:29 repodata
      drwxr-xr-x      2,392,064 2020/05/28 03:17:30 x86_64

Now that we have chosen the upstream server we will get data from. Let's write a command to pull the data we want to our machine, then set up a timer to do automatically.

Pulling files from remote rsync server manually
-----------------------------------------------

Here is the command I use to retrieve files.

.. code-block:: sh

    rsync --recursive --links --perms --times --hard-links --delete-after --delete-excluded --timeout=1800 --human-readable --itemize-changes rsync://download.nus.edu.sg/opensuse/tumbleweed/repo/ /data/opensuse/mirror/tumbleweed/repo

I get it from https://en.opensuse.org/openSUSE:Mirror_howto and expand short options to long form. Although it's already easier to read the command in this format, I think it's even better to put some explanation here so we don't need to jump to rsync's man page every time we read this command.

- ``--recursive``: get all the files, including ones in sub directories.
- ``--links``: copy symlinks as symlinks.
- ``--perms``: preserve permissions.
- ``--times``: preserve modification times.
- ``--hard-links``: preserve hard links.
- ``--delete-after``: receiver deletes after transfer, not during.
- ``--delete-excluded``: also delete excluded files from dest dirs.
- ``--timeout``: set I/O timeout in seconds.
- ``--human-readable``: output numbers in a human-readable format.
- ``--itemize-changes``: output a change-summary for all updates.
- ``rsync://download.nus.edu.sg/opensuse/tumbleweed/repo/``: path of remote directory we want to sync.

  The slash ``/`` at the end indicates we want to download the content inside that directory, not the directory itself. For example, with the slash, our result will look like this.

  .. code-block::

      non-oss
      oss
      src-non-oss -> ../../source/tumbleweed/repo/non-oss
      src-oss -> ../../source/tumbleweed/repo/oss

  Without the slash, it will download the remote ``repo`` directory, like this.

  .. code-block::

      repo
        non-oss
        oss
        src-non-oss -> ../../source/tumbleweed/repo/non-oss
        src-oss -> ../../source/tumbleweed/repo/oss

- ``/data/opensuse/mirror/tumbleweed/repo``: destination directory on our machine.

In a nutshell, we download new files then delete old files. We also preserve symlinks, permissions, times, and hard-links.

Ok, let's run this command. It will take a while as this is the first run and it needs to download all the files which will consume around **120-140GB** of your disk space. Subsequent runs will take less time, depending on how many packages are updated.

Once the download is done, the structure of our local directory looks like below (output of ``tree -L 2``).

.. code-block::

    .
    ├── non-oss
    │   ├── ARCHIVES.gz
    │   ├── boot
    │   ├── ChangeLog
    │   ├── CHECKSUMS
    │   ├── CHECKSUMS.asc
    │   ├── gpg-pubkey-307e3d54-5aaa90a5.asc
    │   ├── gpg-pubkey-39db7c82-5847eb1f.asc
    │   ├── gpg-pubkey-3dbdc284-53674dd4.asc
    │   ├── GPLv2.txt
    │   ├── GPLv3.txt
    │   ├── i586
    │   ├── INDEX.gz
    │   ├── ls-lR.gz
    │   ├── media.1
    │   ├── noarch
    │   ├── README
    │   ├── repodata
    │   ├── SUSEgo.ico
    │   ├── SUSEgo.png
    │   ├── SUSEgo.svg
    │   └── x86_64
    ├── oss
    │   ├── ARCHIVES.gz
    │   ├── autorun.inf
    │   ├── boot
    │   ├── ChangeLog
    │   ├── CHECKSUMS
    │   ├── CHECKSUMS.asc
    │   ├── docu
    │   ├── EFI
    │   ├── gpg-pubkey-307e3d54-5aaa90a5.asc
    │   ├── gpg-pubkey-39db7c82-5847eb1f.asc
    │   ├── gpg-pubkey-3dbdc284-53674dd4.asc
    │   ├── GPLv2.txt
    │   ├── GPLv3.txt
    │   ├── i586
    │   ├── i686
    │   ├── INDEX.gz
    │   ├── ls-lR.gz
    │   ├── media.1
    │   ├── noarch
    │   ├── openSUSE_installer.exe
    │   ├── README
    │   ├── repodata
    │   ├── SUSEgo.ico
    │   ├── SUSEgo.png
    │   ├── SUSEgo.svg
    │   └── x86_64
    ├── src-non-oss -> ../../source/tumbleweed/repo/non-oss
    └── src-oss -> ../../source/tumbleweed/repo/oss

What we have is 2 usable repositories, ``oss`` and ``non-oss``. The other two, ``src-non-oss`` and ``src-oss``, are symlinks to invalid locations; and we don't need to use them as they only contain the source code of installed packages.

After the initial syncing is done, we can actually serve those files to the outside world. However, they will be outdated very quickly, even in the same day (We are on Tumbleweed anyway). There are two options we can pick:

- Run rsync command every time we need to update our mirror.
- Using some kind of timer to do it every day without our intervention.

Second choice, obviously!

Setting up a timer
------------------

As I am using systemd, I use the timer functionality it provides. You can use your own way to do this as well.

.. code-block:: ini
    :caption: /etc/systemd/system/opensuse-mirror.timer

    [Unit]
    Description=Sync openSUSE mirror every 12 hours

    [Timer]
    OnCalendar=0/12:00:00
    RandomizedDelaySec=1h

    [Install]
    WantedBy=timers.target

.. code-block:: ini
    :caption: /etc/systemd/system/opensuse-mirror.service

    [Unit]
    Description=Sync openSUSE mirror

    [Service]
    Type=simple
    User=opensuse-mirror
    ExecStart=/usr/bin/rsync --recursive --links --perms --times --hard-links --delete-after --delete-excluded --timeout=1800 --human-readable --itemize-changes rsync://download.nus.edu.sg/opensuse/tumbleweed/repo/ /data/opensuse/mirror/tumbleweed/repo
    RuntimeMaxSec=10h
    Restart=on-failure

Place those files to correct location, and we enable the timer.

.. code-block:: sh

    sudo systemctl enable opensuse-mirror.timer

To check for the timer, we can use below commands:

- ``systemctl status opensuse-mirror.timer``

  .. code-block::
      :emphasize-lines: 4

      ● opensuse-mirror.timer - Sync openSUSE mirror every 12 hours
           Loaded: loaded (/etc/systemd/system/opensuse-mirror.timer; enabled; vendor preset: disabled)
           Active: active (waiting) since Wed 2020-05-27 04:13:36 UTC; 1 day 2h ago
          Trigger: Thu 2020-05-28 08:41:08 UTC; 1h 44min left
         Triggers: ● opensuse-mirror.service

      May 27 04:13:36 alarm systemd[1]: Started Sync openSUSE mirror every 12 hours.

- ``systemctl list-timers``

  .. code-block::
      :emphasize-lines: 2

      NEXT                        LEFT          LAST                        PASSED       UNIT                         ACTIVATES
      Thu 2020-05-28 08:41:08 UTC 1h 43min left Wed 2020-05-27 20:05:53 UTC 10h ago      opensuse-mirror.timer        opensuse-mirror.service
      Fri 2020-05-29 00:00:00 UTC 17h left      Thu 2020-05-28 00:00:19 UTC 6h ago       shadow.timer                 shadow.service
      Fri 2020-05-29 04:29:05 UTC 21h left      Thu 2020-05-28 04:29:05 UTC 2h 28min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

      3 timers listed.
      Pass --all to see loaded but inactive timers, too.

Certainly, we do want to know the status of the download process, not just the timer. Next two commands can assist with that.

- ``systemctl status opensuse-mirror``

  .. code-block::
      :emphasize-lines: 5

      ● opensuse-mirror.service - Sync openSUSE mirror
           Loaded: loaded (/etc/systemd/system/opensuse-mirror.service; static; vendor preset: disabled)
           Active: inactive (dead) since Wed 2020-05-27 20:09:10 UTC; 10h ago
      TriggeredBy: ● opensuse-mirror.timer
         Main PID: 31109 (code=exited, status=0/SUCCESS)
      
      May 27 20:05:54 alarm systemd[1]: Started Sync openSUSE mirror.
      May 27 20:09:10 alarm systemd[1]: opensuse-mirror.service: Succeeded.

- ``journalctl -f -u opensuse-mirror``

  .. code-block::

      -- Logs begin at Thu 2020-02-13 20:07:07 UTC. --
      May 27 12:23:15 alarm rsync[18093]: .d..t...... oss/docu/
      May 27 12:23:15 alarm rsync[18093]: .d..t...... oss/i586/
      May 27 12:23:15 alarm rsync[18093]: .d..t...... oss/i686/
      May 27 12:23:15 alarm rsync[18093]: .d..t...... oss/media.1/
      May 27 12:23:15 alarm rsync[18093]: .d..t...... oss/noarch/
      May 27 12:23:15 alarm rsync[18093]: .d..t...... oss/repodata/
      May 27 12:23:15 alarm rsync[18093]: .d..t...... oss/x86_64/
      May 27 12:23:17 alarm systemd[1]: opensuse-mirror.service: Succeeded.
      May 27 20:05:54 alarm systemd[1]: Started Sync openSUSE mirror.
      May 27 20:09:10 alarm systemd[1]: opensuse-mirror.service: Succeeded.

Everything is good! We are done at pulling the files, let's move to serving them.

Serving Files
=============

``zypper``, the package manager of openSUSE, supports a variety of different protocols for downloading packages (e.g. HTTP, FTP). In my case, because I already use nginx to manage my virtual hosts, so I will utilize it for serving static files through HTTP too.

Here is my nginx configuration.

.. code-block::
    :caption: /etc/nginx/sites-available/opensuse.mirrors.example.com

    server {
        listen 80;
        server_name opensuse.mirrors.example.com;

        access_log /var/log/nginx/access.log;

        location / {
            root /data/opensuse/mirror/;
            index index.html index.htm;
            autoindex on;
        }

        error_page 500 502 503 504 /50x.html;

        location = /50x.html {
            root /usr/share/nginx/html;
        }

        location /robots.txt {
            add_header Content-Type text/plain;
            return 200 "User-agent: *\nDisallow: /\n";
        }
    }

You should adapt this to your need and you current nginx settings. The following step is restarting nginx and head to http://opensuse.mirrors.example.com to see the result.

Our mirrored repos, ``oss`` and ``non-oss``, locate at http://opensuse.mirrors.example.com/tumbleweed/repo/oss/ and http://opensuse.mirrors.example.com/tumbleweed/repo/non-oss/, respectively.


Using New Mirror
================

Everything for setting up a mirror server is done, it's time to take advantage of it. We will add our new mirrored repos, and disabling current repos, like in the picture below.

.. image:: cubieboard2-opensuse-mirror/using-mirror.png

Enjoy fast package download time!
