=========================================
 Setting up a Cubieboard2 with ArchLinux
=========================================

Installing ArchLinux to the board
=================================

The steps laid out at https://archlinuxarm.org/platforms/armv7/allwinner/cubieboard-2 are detail enough to have a running system. Once we are able to connect to our board using a console connection (I use ``picocom -b 115200 /dev/ttyUSB0``), we should upgrade our system to the latest with ``pacman -Syu``.

Installing packages
===================

For my use, I install new packages with ``pacman -S vim sudo firewalld ssh wireguard-tools git tor``. In addition to that, ``zsh fzf aria2 wol`` can be installed too.

Configuring
===========

sudo
----

With default setup, we have 2 initial users, ``root:root`` and ``alarm:alarm``. ``root`` is, well, ``root``. It can do everything without being questioned by the system. ``alarm``, on the other hand, cannot do must. It cannot even elevate to higher privilege to run system commands.

However, ``alarm`` user is already in ``wheel`` group, so to let it use ``sudo`` we have to enable ``wheel`` group in ``/etc/sudoers`` file. Note that we have to use ``visudo`` instead of changing ``/etc/sudoers`` directly.

.. code-block:: sh

    visudo

An editor will open with the content of ``/etc/sudoers``, and all we need to do is adding ``%wheel ALL=(ALL) ALL``. This line is probably already in the file but is commented out (e.g. something like ``# %wheel ALL=(ALL) ALL``). Navigate to that line, uncomment it, then save.

Hereafter, we can login with ``alarm`` user and use ``sudo`` command.

firewalld
---------

As we use this board as an internet-facing device, having a firewall is a must. In this section, we will configure ``firewalld`` to support only neccessary services.

First, we have to enable ``firewalld`` and start it. This will also ensure firewalld service is run automatically at startup.

.. code-block:: sh

    sudo systemctl enable firewalld.service
    sudo systemctl start firewalld.service

To increase safety, we default to ``drop`` zone so any new interface added to the board will be in this zone. At the same time, we still need to add ``eth0`` interface to ``public`` zone so it can exchange data with the outside world.

.. code-block:: sh

    sudo firewall-cmd --set-default-zone=drop
    sudo firewall-cmd --permanent --zone=public --change-interface=eth0

Now we can ssh to our board, but this is not enough. Since we plan to use this board as a VPN server (and other things), we need to open required services on ``public`` zone. Otherwise, our requests will get rejected.

By default, ``public`` already allows DHCP and SSH requests (that's why we can already ssh with addition configuration). For the time being, we will add http and https to the list. (Note: You **don't** need to do this if you don't intend to use http/https services.)

.. code-block:: sh

    ~ sudo firewall-cmd --info-zone=public
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: eth0
      sources: 
      services: dhcpv6-client ssh
      ports: 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules:

    ~ sudo firewall-cmd --permanent --zone=public --add-service=http
    ~ sudo firewall-cmd --permanent --zone=public --add-service=https

    ~ sudo firewall-cmd --info-zone=public
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: eth0
      sources: 
      services: dhcpv6-client http https ssh
      ports: 
      protocols: 
      masquerade: no
      forward-ports: 
      source-ports: 
      icmp-blocks: 
      rich rules:

SSH
---

There are 2 things we need to do with SSH. First, we need to copy our public key to the board as we don't plan to use password to authenticate ssh sessions. Second thing is to update ``/etc/ssh/sshd_config``.

To help copying public keys to remote servers, ssh already provides a tool for that, as long as it can open an ssh session to the said servers. For us, we need to run ``ssh-copy-id -i ~/.ssh/id_rsa alarm@<LAN-IP-OF-THE-BOARD>``.

With ``/etc/ssh/sshd_config``, the following config will disable password login and root account.

.. code-block::
    :caption: sshd_config

    ChallengeResponseAuthentication no
    PasswordAuthentication no
    PermitRootLogin no
    UsePAM no

    AuthorizedKeysFile .ssh/authorized_keys
    Subsystem sftp internal-sftp

Don't forget to restart ssh service using ``sudo systemctl restart sshd``.

WireGuard
---------

To setup our board as an VPN server using wireguard, the most important step is configuring ``/etc/wireguard/wg0.conf``. This file contains information about the server itself and its clients. In my setup, beside the server (i.e. the board), there are 2 other machines, so I have the following config:

.. code-block:: ini
    :caption: /etc/wireguard/wg0.conf

    [Interface]
    Address = 10.200.200.1/24
    ListenPort = 51820
    PrivateKey = <SERVER-PRIVATE-KEY>

    [Peer]
    PublicKey = <CLIENT-B-PUBLIC-KEY>
    AllowedIPs = 10.200.200.100/32

    [Peer]
    PublicKey = <CLIENT-A-PUBLIC-KEY>
    AllowedIPs = 10.200.200.200/32

This config will create a new wireguard interface named ``wg0``.

Usually, we want clients be able to send data between themselves when they join the VPN. That is accomplished by enabling IP forwarding.

.. code-block::
    :caption: /etc/sysctl.d/wireguard.conf

    net.ipv4.ip_forward = 1
    net.ipv6.conf.all.forwarding = 1

Alternatively, we can run below commands to generate ``/etc/sysctl.d/wireguard.conf``.

.. code-block:: sh

    echo "net.ipv4.ip_forward = 1" | sudo tee /etc/sysctl.d/wireguard.conf
    echo "net.ipv6.conf.all.forwarding = 1" | sudo tee --append /etc/sysctl.d/wireguard.conf

We have to run ``sudo sysctl --system`` to make those values take effect immediately.

We also have to allow wireguard in firewalld. In case the installed version of firewalld doesn't already have wireguard service, we need to create one and enable it for ``public`` zone.

.. code-block:: sh

    sudo firewall-cmd --permanent --new-service=wireguard
    sudo firewall-cmd --permanent --service=wireguard --set-description="WireGuard"
    sudo firewall-cmd --permanent --service=wireguard --add-port=51820/udp
    sudo firewall-cmd --reload

    sudo firewall-cmd --permanent --zone=public --add-service=wireguard

Optionally, in case we want all clients to have access to all services on VPN server, we can add interface ``wg0`` to ``trusted`` zone.

.. code-block:: sh

    sudo firewall-cmd --permanent --zone=trusted --change-interface=wg0

At this point, all setup for wireguard on the board is in place. The last step is to enable wireguard interface (i.e. ``wg0``) when system starts. Luckily, ``wireguard-tools`` package already comes with a systemd template service. All we need to do is enabling a service with correct wireguard interface name. For our setup, below commands will get the job done.

.. code-block:: sh

    sudo systemctl enable wg-quick@wg0.service
    sudo systemctl start wg-quick@wg0.service

    systemctl status wg-quick@wg0.service

More information can be found at https://wiki.archlinux.org/index.php/WireGuard or https://www.wireguard.com

Duck DNS
--------

In my case, I don't have a static IP. Every time my modem restarts, there will be a different IP. To solve this problem, I use Duck DNS to map and update my public IP to a sub domain of ``duckdns.org``.

Basically, we need 2 things, an update script that tells Duck DNS to update the IP of my domain, and a timer to do the updating periodically (as my modem provides no indication whether a new public IP address is assigned).

.. code-block:: ini
    :caption: /etc/systemd/system/duckdns.timer

    [Unit]
    Description=DuckDNS update every 5 minutes

    [Timer]
    OnBootSec=5min
    OnCalendar=*:0/5

    [Install]
    WantedBy=basic.target

.. code-block:: ini
    :caption: /etc/systemd/system/duckdns.service

    [Unit]
    Description=Updates DuckDNS records

    [Service]
    Type=oneshot
    ExecStart=/usr/bin/sh -c 'echo url="https://www.duckdns.org/update?domains=[XXX]&token=[YYY]&ip=" | curl -k -K -'

Like for any new systemd service/timer, we want it to run at startup, we need to enable it.

.. code-block:: sh

    sudo systemctl enable duckdns.timer
    sudo systemctl start duckdns.timer

    sudo systemctl start duckdns.service
    systemctl status duckdns.service

    systemctl list-timers

tor proxy
---------

I also use this board as a tor proxy. Below is 

.. code-block:: ini
    :caption: /etc/tor/torrc

    SocksPort <LAN-IP-OF-THIS-BOARD>:9050
    SocksPolicy accept <SUBNET-THIS-BOARD>/24
    SocksPolicy reject *
    Log notice syslog
    DataDirectory /var/lib/tor

.. code-block:: sh

    sudo firewall-cmd --permanent --new-service=tor-proxy
    sudo firewall-cmd --permanent --service=tor-proxy --set-description="Tor Proxy"
    sudo firewall-cmd --permanent --service=tor-proxy --add-port=9050/tcp
    sudo firewall-cmd --reload

    sudo firewall-cmd --permanent --zone=public --add-service=tor-proxy

.. code-block:: sh

    sudo systemctl enable tor
    sudo systemctl start tor

    systemctl status tor

Others
------

git & zsh
