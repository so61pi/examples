========
 Pinmux
========

Softwares
=========

- Linux TI

  +-----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | Version         | 4.19.y                                                                                                                                                                |
  +-----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | Hash            | be5389fd85b69250aeb1ba477447879fb392152f                                                                                                                              |
  +-----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | Source Location | https://git.ti.com/cgit/processor-sdk/processor-sdk-linux/tree/arch/arm/boot/dts/am33xx.dtsi?id=be5389fd85b69250aeb1ba477447879fb392152f&h=processor-sdk-linux-4.19.y |
  +-----------------+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------+

- `AM335x TRM <https://www.ti.com/lit/ug/spruh73q/spruh73q.pdf>`__
- `AM335x Datasheet <https://www.ti.com/lit/ds/sprs717l/sprs717l.pdf>`__

Pinmux
======

DTS to configure a pin follows the format ``(register-offset-from-trm - 0x800) (OPTIONS | MUX_MODE[0-7])``.

Suppose we want to configure ``GPIO2_0`` as output pin. First, we search ``Table 4-10. General-Purpose IOs/GPIO2 Signals Description`` in the datasheet to identify the pin number, which is ``U17`` or ``T13`` for ``GPIO2_0``, depending on the package. Then, we click on the pin number to go to ``Table 4-2. Pin Attributes`` where we can have the pin name. In this case it is ``GPMC_CSn3``.

Note that pin ``GPMC_CSn3`` has several functions depending on its mode. What we want here is mode 7 which sets ``GPMC_CSn3`` up as pure GPIO.

.. image:: pinmux/datasheet.png

Next, we need to find ``register-offset-from-trm`` in ``Table 9-10. CONTROL_MODULE REGISTERS`` from... TRM. Its value is ``888h`` so the value we will put into DTS is ``0x88 = 0x888 - 0x800``.

.. image:: pinmux/trm.png

The final DTS snippet looks like below listing.

.. code-block:: text
    :linenos:

    &am33xx_pinmux {
        pinctrl-0 = <&gpio2_pins_custom>;

        gpio2_pins_custom: gpio2_pins_custom {
            pinctrl-single,pins = <
                0x88 (PIN_OUTPUT_PULLUP | MUX_MODE7) /* (U17 | T13) gpmc_csn3.gpio2_0 */
            >;
        };
    };

    #if 0
    &gpio2 {
        pinctrl-names = "default";
        pinctrl-0 = <&gpio2_pins_custom>;
        status = "okay";
    };
    #endif

Here are a few files we can use to inspect pin configuration.

- ``/sys/kernel/debug/pinctrl/44e10800.pinmux/pins``
- ``/sys/kernel/debug/pinctrl/44e10800.pinmux/pinmux-pins``
- ``/sys/kernel/debug/pinctrl/44e10800.pinmux/pingroups``
- ``/sys/kernel/debug/gpio``

See :ref:`this document <link-linux-gpio>` to export pins from userspace.

References
==========

- https://elinux.org/EBC_Device_Trees
