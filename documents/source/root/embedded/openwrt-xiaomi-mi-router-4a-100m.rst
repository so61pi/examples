===========================================
 OpenWRT - Xiaomi Mi Router 4A (MIR4A) 100M
===========================================

Softwares
=========

- OpenWRT

  +-----------------+----------------------------------------------------------------------------------------+
  | Version         | v21.02.3                                                                               |
  +-----------------+----------------------------------------------------------------------------------------+
  | Source Location | https://git.openwrt.org/?p=openwrt/openwrt.git;a=tree;hb=v21.02.3                      |
  +-----------------+----------------------------------------------------------------------------------------+

Port Map
========

Internally the used chip supports 5 ports, but only 3 of them are wired to physical RJ-45 ports.

.. list-table::
   :header-rows: 1

   *  - Port
      - Feature
   *  - Port 5???
      - Disabled
   *  - Port 4
      - LAN-1
   *  - Port 3
      - Disabled
   *  - Port 2
      - LAN-2
   *  - Port 1
      - Disabled
   *  - Port 0
      - WAN

.. code-block:: text
    :emphasize-lines: 1

    root@OpenWrt:~# swconfig dev switch0 show
    Global attributes:
            enable_vlan: 1
            alternate_vlan_disable: 0
            bc_storm_protect: 0
            led_frequency: 0
    Port 0:
            disable: 0
            doubletag: 0
            untag: 1
            led: 5
            lan: 0
            recv_bad: 0
            recv_good: 6894
            tr_bad: 0
            tr_good: 39185
            pvid: 1
            link: port:0 link:up speed:100baseT full-duplex
    Port 1:
            disable: 1
            doubletag: 0
            untag: 0
            led: 5
            lan: 1
            recv_bad: 0
            recv_good: 0
            tr_bad: 0
            tr_good: 0
            pvid: 0
            link: port:1 link:down
    Port 2:
            disable: 0
            doubletag: 0
            untag: 1
            led: 5
            lan: 1
            recv_bad: 0
            recv_good: 0
            tr_bad: 0
            tr_good: 0
            pvid: 1
            link: port:2 link:down
    Port 3:
            disable: 1
            doubletag: 0
            untag: 0
            led: 5
            lan: 1
            recv_bad: 0
            recv_good: 0
            tr_bad: 0
            tr_good: 0
            pvid: 0
            link: port:3 link:down
    Port 4:
            disable: 0
            doubletag: 0
            untag: 1
            led: 5
            lan: 1
            recv_bad: 0
            recv_good: 3401
            tr_bad: 0
            tr_good: 59577
            pvid: 1
            link: port:4 link:up speed:100baseT full-duplex
    Port 5:
            disable: 1
            doubletag: 0
            untag: 0
            led: ???
            lan: 1
            recv_bad: 0
            recv_good: 0
            tr_bad: 0
            tr_good: 0
            pvid: 0
            link: port:5 link:down
    Port 6:
            disable: 0
            doubletag: 0
            untag: 0
            led: ???
            lan: ???
            recv_bad: ???
            recv_good: ???
            tr_bad: ???
            tr_good: ???
            pvid: 0
            link: port:6 link:up speed:1000baseT full-duplex
    VLAN 1:
            ports: 0 2 4 6t

``Port 6`` is the CPU. It is used when the device needs to move packets between LAN ports and WiFi, or handle packets that target the device itself (e.g., ssh into the device).

Turn WAN Port Into LAN Port
===========================

The type of port is determined in device tree using property `mediatek,portmap <https://git.openwrt.org/?p=openwrt/openwrt.git;a=blob;f=target/linux/ramips/dts/mt7628an_xiaomi_mi-router-4a-100m.dts;hb=v21.02.3#l43>`__. The default value is ``0x3e`` ~ ``0b111110`` which means Port 0 is WAN port and other ports are LAN ports.

Since we cannot change this value on the fly, we need to change ``mediatek,portmap`` to ``0x3f`` and recompile OpenWRT firmware then upgrade our device. Below listing shows the steps to build firmware.

.. code-block:: sh

    # Clone OpenWRT source code and checkout the right branch
    git clone https://git.openwrt.org/openwrt/openwrt.git
    cd openwrt
    git checkout v21.02.3

    # Download default config for this device so we don't have to config from scratch
    wget https://downloads.openwrt.org/releases/21.02.3/targets/ramips/mt76x8/config.buildinfo -O .config

    # Change `mediatek,portmap` in `target/linux/ramips/dts/mt7628an_xiaomi_mi-router-4a-100m.dts` to `0x3f`

    # Start a docker container to isolate the build environment
    # We mount `/etc/{passwd,group,shadow}` into the container so we can use host's user and password
    docker run --env "TERM=xterm" --init -ti --name openwrt-mir4a-100m -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro -v /etc/shadow:/etc/shadow:ro -v $(pwd):/code --workdir /code opensuse/tumbleweed bash

    # Install required packages
    zypper install --no-recommends -y asciidoc bash bc binutils bzip2 \
    fastjar flex gawk gcc gcc-c++ gettext-tools git git-core intltool \
    libopenssl-devel libxslt-tools make mercurial ncurses-devel patch \
    perl-ExtUtils-MakeMaker python-devel rsync sdcc unzip util-linux \
    wget zlib-devel tar diffutils

    # Switch to a normal user to prevent build running as root
    su --shell /usr/bin/bash so61pi

    # Update the feeds
    ./scripts/feeds update -a
    ./scripts/feeds install -a

    # Configure the firmware image and the kernel
    # Since we already download the default build config for this device, we only need to select the our device in `Target Profile`. We can also include `luci` in the firmware so we don't need to install it later
    make menuconfig
    make -j $(nproc) kernel_menuconfig

    # Build the firmware image
    make -j $(nproc) defconfig download clean world

Here are the config that I selected:

.. image:: openwrt-xiaomi-mi-router-4a-100m/menuconfig-target.png
    :align: center

.. image:: openwrt-xiaomi-mi-router-4a-100m/menuconfig-luci.png
    :align: center

The firmware image is located at ``bin/targets/ramips/mt76x8/``. Here is the content of that directory::

    so61pi@3f72e09010f5:/code> ls -al bin/targets/ramips/mt76x8/
    total 243964
    drwxr-xr-x 1 so61pi users      938 Jul  6 16:37 .
    drwxr-xr-x 1 so61pi users       12 Jul  6 16:06 ..
    -rw-r--r-- 1 so61pi users     1959 Jul  6 16:06 config.buildinfo
    -rw-r--r-- 1 so61pi users      268 Jul  6 16:06 feeds.buildinfo
    -rw-r--r-- 1 so61pi users 92531655 Jul  6 16:33 kernel-debug.tar.zst
    -rw-r--r-- 1 so61pi users  5016356 Jul  6 16:34 openwrt-21.02.3-ramips-mt76x8-xiaomi_mi-router-4a-100m-initramfs-kernel.bin
    -rw-r--r-- 1 so61pi users  5243721 Jul  6 16:34 openwrt-21.02.3-ramips-mt76x8-xiaomi_mi-router-4a-100m-squashfs-sysupgrade.bin
    -rw-r--r-- 1 so61pi users     3565 Jul  6 16:34 openwrt-21.02.3-ramips-mt76x8-xiaomi_mi-router-4a-100m.manifest
    -rw-r--r-- 1 so61pi users 48386944 Jul  6 16:35 openwrt-imagebuilder-21.02.3-ramips-mt76x8.Linux-x86_64.tar.xz
    -rw-r--r-- 1 so61pi users 98500316 Jul  6 16:36 openwrt-sdk-21.02.3-ramips-mt76x8_gcc-8.4.0_musl.Linux-x86_64.tar.xz
    drwxr-xr-x 1 so61pi users    78712 Jul  6 16:37 packages
    -rw-r--r-- 1 so61pi users     1292 Jul  6 16:37 profiles.json
    -rw-r--r-- 1 so61pi users   109107 Jul  6 16:37 sha256sums
    drwxr-xr-x 1 so61pi users       20 Jul  6 16:10 u-boot-ravpower_rp-wd009
    -rw-r--r-- 1 so61pi users       18 Jul  6 16:06 version.buildinfo

After upgrading the device with **openwrt-21.02.3-ramips-mt76x8-xiaomi_mi-router-4a-100m-squashfs-sysupgrade.bin** file, we can use ``swconfig dev switch0 show`` or ``hexdump /sys/firmware/devicetree/base/esw@10110000/mediatek,portmap`` to verify the port type of Port 0.

References
==========

- https://openwrt.org/docs/guide-developer/toolchain/install-buildsystem
- https://openwrt.org/docs/guide-developer/toolchain/use-buildsystem
- https://openwrt.org/toh/xiaomi/mi_router_4a_mir4a_100m
