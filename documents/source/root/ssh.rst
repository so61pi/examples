=====
 SSH
=====

Port forwarding
===============

Local port forwarding
---------------------

.. code-block::

    # on localhost
    ssh -N -f -L 9000:example.com:80 admin@server.com

    localhost:9000 <--------------------> server <--------------------> example.com:80
                                            /\
                            ________________||_________________
                          |                                   |
                          |  All packets from localhost:9000  |
                          | are transferred to example.com:80 |
                          |___________________________________|


    ssh -N -f -L *:9000:example.com:80 admin@server.com

    *:9000 <--------------------> server <--------------------> example.com:80
                                    /\
                    ________________||_________________
                  |                                   |
                  |      All packets from *:9000      |
                  | are transferred to example.com:80 |
                  |___________________________________|

Remote port forwarding
----------------------

.. code-block::

    # on localhost
    ssh -N -f -R 9000:localhost:3000 admin@server.com

    localhost:3000 <--------------------> server:9000 <--------------------> internet
                                              /\
                          ____________________||____________________
                          |                                          |
                          | All packets from internet to server:9000 |
                          |    are transferred to localhost:3000     |
                          |------------------------------------------|
                          |      Service localhost:3000 can be       |
                          |        accessible at server:9000         |
                          |__________________________________________|


    ssh -N -f -R 9000:address:3000 admin@server.com

    address:3000 <--------------------> server:9000 <--------------------> internet
                                            /\
                        ____________________||____________________
                        |                                          |
                        | All packets from internet to server:9000 |
                        |     are transferred to address:3000      |
                        |------------------------------------------|
                        |       Service address:3000 can be        |
                        |        accessible at server:9000         |
                        |__________________________________________|

    #
    # on server
    #
    sudo vim /etc/ssh/sshd_config
    # add GatewayPorts yes

    sudo service ssh restart
    #sudo systemctl restart ssh

SOCKS proxy
-----------

.. code-block::

    ssh -N -f -D localhost:3000 admin@server.com

                   --------------------
    localhost:3000 ==================== server.com
                   --------------------
    # use localhost:3000 as a SOCKS proxy address

Enable keep-alive
-----------------

- We could use ``autossh``
- Or add below line to ``~/.ssh/config``

  .. code-block::

      ServerAliveInterval 180

Disable host key checking
=========================

- Oneshot

  .. code-block:: sh

      ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no example.com

- Permanent

  Put it to ``~/.ssh/config`` (note this file shouldn't be readable by other users).

  .. code-block::

      Host example.*
          StrictHostKeyChecking no
          UserKnownHostsFile /dev/null

Proxy
=====

- Oneshot

  .. code-block:: sh
  
      ssh -o "ProxyCommand corkscrew <proxy-address> <proxy-port> <ssh-server> 22" <ssh-server>

- Permanent

  Put it to ``~/.ssh/config`` (note this file shouldn't be readable by other users).
  
  .. code-block::
  
      Host example.*
          ProxyCommand corkscrew <proxy-address> <proxy-port> %h %p

sshfs
=====

- Mount

  .. code-block:: sh

      sshfs -o uid=0,gid=0,default_permissions,reconnect,no_readahead,cache=yes,kernel_cache,compression=no,Cipher=arcfour,ro admin@<server>:<server-dir> <local-dir>

- Unmount

  .. code-block:: sh

      fusermount -u <local-dir>
