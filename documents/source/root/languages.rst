===========
 Languages
===========

.. toctree::
    :maxdepth: 2
    :glob:

    languages/*

Examples https://gitlab.com/so61pi/examples/-/tree/master/documents/source/root/languages
