Contents
========

Personal Documentation.

.. toctree::
    :maxdepth: 2
    :glob:

    root/*

Search
======

* :ref:`search`
